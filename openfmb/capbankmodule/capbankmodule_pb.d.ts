import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class CapBankSystem extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): CapBankSystem;
  hasConductingequipment(): boolean;
  clearConductingequipment(): CapBankSystem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankSystem.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankSystem): CapBankSystem.AsObject;
  static serializeBinaryToWriter(message: CapBankSystem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankSystem;
  static deserializeBinaryFromReader(message: CapBankSystem, reader: jspb.BinaryReader): CapBankSystem;
}

export namespace CapBankSystem {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

export class CapBankControlYPSH extends jspb.Message {
  getAmplmt(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setAmplmt(value?: commonmodule_commonmodule_pb.PhaseSPC): CapBankControlYPSH;
  hasAmplmt(): boolean;
  clearAmplmt(): CapBankControlYPSH;

  getAmpthdhi(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setAmpthdhi(value?: commonmodule_commonmodule_pb.PhaseAPC): CapBankControlYPSH;
  hasAmpthdhi(): boolean;
  clearAmpthdhi(): CapBankControlYPSH;

  getAmpthdlo(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setAmpthdlo(value?: commonmodule_commonmodule_pb.PhaseAPC): CapBankControlYPSH;
  hasAmpthdlo(): boolean;
  clearAmpthdlo(): CapBankControlYPSH;

  getCtlmodeauto(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setCtlmodeauto(value?: commonmodule_commonmodule_pb.ControlSPC): CapBankControlYPSH;
  hasCtlmodeauto(): boolean;
  clearCtlmodeauto(): CapBankControlYPSH;

  getCtlmodeovrrd(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setCtlmodeovrrd(value?: commonmodule_commonmodule_pb.ControlSPC): CapBankControlYPSH;
  hasCtlmodeovrrd(): boolean;
  clearCtlmodeovrrd(): CapBankControlYPSH;

  getCtlmoderem(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setCtlmoderem(value?: commonmodule_commonmodule_pb.ControlSPC): CapBankControlYPSH;
  hasCtlmoderem(): boolean;
  clearCtlmoderem(): CapBankControlYPSH;

  getDirmode(): commonmodule_commonmodule_pb.Optional_DirectionModeKind | undefined;
  setDirmode(value?: commonmodule_commonmodule_pb.Optional_DirectionModeKind): CapBankControlYPSH;
  hasDirmode(): boolean;
  clearDirmode(): CapBankControlYPSH;

  getPos(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setPos(value?: commonmodule_commonmodule_pb.PhaseSPC): CapBankControlYPSH;
  hasPos(): boolean;
  clearPos(): CapBankControlYPSH;

  getTemplmt(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setTemplmt(value?: commonmodule_commonmodule_pb.ControlSPC): CapBankControlYPSH;
  hasTemplmt(): boolean;
  clearTemplmt(): CapBankControlYPSH;

  getTempthdhi(): commonmodule_commonmodule_pb.ControlAPC | undefined;
  setTempthdhi(value?: commonmodule_commonmodule_pb.ControlAPC): CapBankControlYPSH;
  hasTempthdhi(): boolean;
  clearTempthdhi(): CapBankControlYPSH;

  getTempthdlo(): commonmodule_commonmodule_pb.ControlAPC | undefined;
  setTempthdlo(value?: commonmodule_commonmodule_pb.ControlAPC): CapBankControlYPSH;
  hasTempthdlo(): boolean;
  clearTempthdlo(): CapBankControlYPSH;

  getVarlmt(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setVarlmt(value?: commonmodule_commonmodule_pb.PhaseSPC): CapBankControlYPSH;
  hasVarlmt(): boolean;
  clearVarlmt(): CapBankControlYPSH;

  getVarthdhi(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVarthdhi(value?: commonmodule_commonmodule_pb.PhaseAPC): CapBankControlYPSH;
  hasVarthdhi(): boolean;
  clearVarthdhi(): CapBankControlYPSH;

  getVarthdlo(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVarthdlo(value?: commonmodule_commonmodule_pb.PhaseAPC): CapBankControlYPSH;
  hasVarthdlo(): boolean;
  clearVarthdlo(): CapBankControlYPSH;

  getVollmt(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setVollmt(value?: commonmodule_commonmodule_pb.PhaseSPC): CapBankControlYPSH;
  hasVollmt(): boolean;
  clearVollmt(): CapBankControlYPSH;

  getVolthdhi(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVolthdhi(value?: commonmodule_commonmodule_pb.PhaseAPC): CapBankControlYPSH;
  hasVolthdhi(): boolean;
  clearVolthdhi(): CapBankControlYPSH;

  getVolthdlo(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVolthdlo(value?: commonmodule_commonmodule_pb.PhaseAPC): CapBankControlYPSH;
  hasVolthdlo(): boolean;
  clearVolthdlo(): CapBankControlYPSH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankControlYPSH.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankControlYPSH): CapBankControlYPSH.AsObject;
  static serializeBinaryToWriter(message: CapBankControlYPSH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankControlYPSH;
  static deserializeBinaryFromReader(message: CapBankControlYPSH, reader: jspb.BinaryReader): CapBankControlYPSH;
}

export namespace CapBankControlYPSH {
  export type AsObject = {
    amplmt?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    ampthdhi?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    ampthdlo?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    ctlmodeauto?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    ctlmodeovrrd?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    ctlmoderem?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    dirmode?: commonmodule_commonmodule_pb.Optional_DirectionModeKind.AsObject,
    pos?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    templmt?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    tempthdhi?: commonmodule_commonmodule_pb.ControlAPC.AsObject,
    tempthdlo?: commonmodule_commonmodule_pb.ControlAPC.AsObject,
    varlmt?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    varthdhi?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    varthdlo?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    vollmt?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    volthdhi?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    volthdlo?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
  }
}

export class CapBankPoint extends jspb.Message {
  getControl(): CapBankControlYPSH | undefined;
  setControl(value?: CapBankControlYPSH): CapBankPoint;
  hasControl(): boolean;
  clearControl(): CapBankPoint;

  getStarttime(): commonmodule_commonmodule_pb.Timestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.Timestamp): CapBankPoint;
  hasStarttime(): boolean;
  clearStarttime(): CapBankPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankPoint.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankPoint): CapBankPoint.AsObject;
  static serializeBinaryToWriter(message: CapBankPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankPoint;
  static deserializeBinaryFromReader(message: CapBankPoint, reader: jspb.BinaryReader): CapBankPoint;
}

export namespace CapBankPoint {
  export type AsObject = {
    control?: CapBankControlYPSH.AsObject,
    starttime?: commonmodule_commonmodule_pb.Timestamp.AsObject,
  }
}

export class CapBankCSG extends jspb.Message {
  getCrvptsList(): Array<CapBankPoint>;
  setCrvptsList(value: Array<CapBankPoint>): CapBankCSG;
  clearCrvptsList(): CapBankCSG;
  addCrvpts(value?: CapBankPoint, index?: number): CapBankPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankCSG.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankCSG): CapBankCSG.AsObject;
  static serializeBinaryToWriter(message: CapBankCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankCSG;
  static deserializeBinaryFromReader(message: CapBankCSG, reader: jspb.BinaryReader): CapBankCSG;
}

export namespace CapBankCSG {
  export type AsObject = {
    crvptsList: Array<CapBankPoint.AsObject>,
  }
}

export class CapBankControlScheduleFSCH extends jspb.Message {
  getValcsg(): CapBankCSG | undefined;
  setValcsg(value?: CapBankCSG): CapBankControlScheduleFSCH;
  hasValcsg(): boolean;
  clearValcsg(): CapBankControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankControlScheduleFSCH): CapBankControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: CapBankControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankControlScheduleFSCH;
  static deserializeBinaryFromReader(message: CapBankControlScheduleFSCH, reader: jspb.BinaryReader): CapBankControlScheduleFSCH;
}

export namespace CapBankControlScheduleFSCH {
  export type AsObject = {
    valcsg?: CapBankCSG.AsObject,
  }
}

export class CapBankControlFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): CapBankControlFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): CapBankControlFSCC;

  getCapbankcontrolschedulefsch(): CapBankControlScheduleFSCH | undefined;
  setCapbankcontrolschedulefsch(value?: CapBankControlScheduleFSCH): CapBankControlFSCC;
  hasCapbankcontrolschedulefsch(): boolean;
  clearCapbankcontrolschedulefsch(): CapBankControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankControlFSCC): CapBankControlFSCC.AsObject;
  static serializeBinaryToWriter(message: CapBankControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankControlFSCC;
  static deserializeBinaryFromReader(message: CapBankControlFSCC, reader: jspb.BinaryReader): CapBankControlFSCC;
}

export namespace CapBankControlFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    capbankcontrolschedulefsch?: CapBankControlScheduleFSCH.AsObject,
  }
}

export class CapBankControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): CapBankControl;
  hasControlvalue(): boolean;
  clearControlvalue(): CapBankControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): CapBankControl;
  hasCheck(): boolean;
  clearCheck(): CapBankControl;

  getCapbankcontrolfscc(): CapBankControlFSCC | undefined;
  setCapbankcontrolfscc(value?: CapBankControlFSCC): CapBankControl;
  hasCapbankcontrolfscc(): boolean;
  clearCapbankcontrolfscc(): CapBankControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankControl.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankControl): CapBankControl.AsObject;
  static serializeBinaryToWriter(message: CapBankControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankControl;
  static deserializeBinaryFromReader(message: CapBankControl, reader: jspb.BinaryReader): CapBankControl;
}

export namespace CapBankControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    capbankcontrolfscc?: CapBankControlFSCC.AsObject,
  }
}

export class CapBankControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): CapBankControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): CapBankControlProfile;

  getCapbankcontrol(): CapBankControl | undefined;
  setCapbankcontrol(value?: CapBankControl): CapBankControlProfile;
  hasCapbankcontrol(): boolean;
  clearCapbankcontrol(): CapBankControlProfile;

  getCapbanksystem(): CapBankSystem | undefined;
  setCapbanksystem(value?: CapBankSystem): CapBankControlProfile;
  hasCapbanksystem(): boolean;
  clearCapbanksystem(): CapBankControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankControlProfile): CapBankControlProfile.AsObject;
  static serializeBinaryToWriter(message: CapBankControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankControlProfile;
  static deserializeBinaryFromReader(message: CapBankControlProfile, reader: jspb.BinaryReader): CapBankControlProfile;
}

export namespace CapBankControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    capbankcontrol?: CapBankControl.AsObject,
    capbanksystem?: CapBankSystem.AsObject,
  }
}

export class CapBankDiscreteControlYPSH extends jspb.Message {
  getLogicalnodeforcontrol(): commonmodule_commonmodule_pb.LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: commonmodule_commonmodule_pb.LogicalNodeForControl): CapBankDiscreteControlYPSH;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): CapBankDiscreteControlYPSH;

  getControl(): CapBankControlYPSH | undefined;
  setControl(value?: CapBankControlYPSH): CapBankDiscreteControlYPSH;
  hasControl(): boolean;
  clearControl(): CapBankDiscreteControlYPSH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankDiscreteControlYPSH.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankDiscreteControlYPSH): CapBankDiscreteControlYPSH.AsObject;
  static serializeBinaryToWriter(message: CapBankDiscreteControlYPSH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankDiscreteControlYPSH;
  static deserializeBinaryFromReader(message: CapBankDiscreteControlYPSH, reader: jspb.BinaryReader): CapBankDiscreteControlYPSH;
}

export namespace CapBankDiscreteControlYPSH {
  export type AsObject = {
    logicalnodeforcontrol?: commonmodule_commonmodule_pb.LogicalNodeForControl.AsObject,
    control?: CapBankControlYPSH.AsObject,
  }
}

export class CapBankDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): CapBankDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): CapBankDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): CapBankDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): CapBankDiscreteControl;

  getCapbankdiscretecontrolypsh(): CapBankDiscreteControlYPSH | undefined;
  setCapbankdiscretecontrolypsh(value?: CapBankDiscreteControlYPSH): CapBankDiscreteControl;
  hasCapbankdiscretecontrolypsh(): boolean;
  clearCapbankdiscretecontrolypsh(): CapBankDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankDiscreteControl): CapBankDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: CapBankDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankDiscreteControl;
  static deserializeBinaryFromReader(message: CapBankDiscreteControl, reader: jspb.BinaryReader): CapBankDiscreteControl;
}

export namespace CapBankDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    capbankdiscretecontrolypsh?: CapBankDiscreteControlYPSH.AsObject,
  }
}

export class CapBankDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): CapBankDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): CapBankDiscreteControlProfile;

  getCapbankcontrol(): CapBankDiscreteControl | undefined;
  setCapbankcontrol(value?: CapBankDiscreteControl): CapBankDiscreteControlProfile;
  hasCapbankcontrol(): boolean;
  clearCapbankcontrol(): CapBankDiscreteControlProfile;

  getCapbanksystem(): CapBankSystem | undefined;
  setCapbanksystem(value?: CapBankSystem): CapBankDiscreteControlProfile;
  hasCapbanksystem(): boolean;
  clearCapbanksystem(): CapBankDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankDiscreteControlProfile): CapBankDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: CapBankDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankDiscreteControlProfile;
  static deserializeBinaryFromReader(message: CapBankDiscreteControlProfile, reader: jspb.BinaryReader): CapBankDiscreteControlProfile;
}

export namespace CapBankDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    capbankcontrol?: CapBankDiscreteControl.AsObject,
    capbanksystem?: CapBankSystem.AsObject,
  }
}

export class CapBankEventAndStatusYPSH extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): CapBankEventAndStatusYPSH;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): CapBankEventAndStatusYPSH;

  getAmplmt(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setAmplmt(value?: commonmodule_commonmodule_pb.PhaseSPS): CapBankEventAndStatusYPSH;
  hasAmplmt(): boolean;
  clearAmplmt(): CapBankEventAndStatusYPSH;

  getCtlmode(): commonmodule_commonmodule_pb.Optional_ControlModeKind | undefined;
  setCtlmode(value?: commonmodule_commonmodule_pb.Optional_ControlModeKind): CapBankEventAndStatusYPSH;
  hasCtlmode(): boolean;
  clearCtlmode(): CapBankEventAndStatusYPSH;

  getDirrev(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setDirrev(value?: commonmodule_commonmodule_pb.PhaseSPS): CapBankEventAndStatusYPSH;
  hasDirrev(): boolean;
  clearDirrev(): CapBankEventAndStatusYPSH;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): CapBankEventAndStatusYPSH;
  hasDynamictest(): boolean;
  clearDynamictest(): CapBankEventAndStatusYPSH;

  getPos(): commonmodule_commonmodule_pb.PhaseDPS | undefined;
  setPos(value?: commonmodule_commonmodule_pb.PhaseDPS): CapBankEventAndStatusYPSH;
  hasPos(): boolean;
  clearPos(): CapBankEventAndStatusYPSH;

  getTemplmt(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setTemplmt(value?: commonmodule_commonmodule_pb.PhaseSPS): CapBankEventAndStatusYPSH;
  hasTemplmt(): boolean;
  clearTemplmt(): CapBankEventAndStatusYPSH;

  getVarlmt(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setVarlmt(value?: commonmodule_commonmodule_pb.PhaseSPS): CapBankEventAndStatusYPSH;
  hasVarlmt(): boolean;
  clearVarlmt(): CapBankEventAndStatusYPSH;

  getVollmt(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setVollmt(value?: commonmodule_commonmodule_pb.PhaseSPS): CapBankEventAndStatusYPSH;
  hasVollmt(): boolean;
  clearVollmt(): CapBankEventAndStatusYPSH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankEventAndStatusYPSH.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankEventAndStatusYPSH): CapBankEventAndStatusYPSH.AsObject;
  static serializeBinaryToWriter(message: CapBankEventAndStatusYPSH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankEventAndStatusYPSH;
  static deserializeBinaryFromReader(message: CapBankEventAndStatusYPSH, reader: jspb.BinaryReader): CapBankEventAndStatusYPSH;
}

export namespace CapBankEventAndStatusYPSH {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    amplmt?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    ctlmode?: commonmodule_commonmodule_pb.Optional_ControlModeKind.AsObject,
    dirrev?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    pos?: commonmodule_commonmodule_pb.PhaseDPS.AsObject,
    templmt?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    varlmt?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    vollmt?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
  }
}

export class CapBankEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): CapBankEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): CapBankEvent;

  getCapbankeventandstatusypsh(): CapBankEventAndStatusYPSH | undefined;
  setCapbankeventandstatusypsh(value?: CapBankEventAndStatusYPSH): CapBankEvent;
  hasCapbankeventandstatusypsh(): boolean;
  clearCapbankeventandstatusypsh(): CapBankEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankEvent.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankEvent): CapBankEvent.AsObject;
  static serializeBinaryToWriter(message: CapBankEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankEvent;
  static deserializeBinaryFromReader(message: CapBankEvent, reader: jspb.BinaryReader): CapBankEvent;
}

export namespace CapBankEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    capbankeventandstatusypsh?: CapBankEventAndStatusYPSH.AsObject,
  }
}

export class CapBankEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): CapBankEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): CapBankEventProfile;

  getCapbankevent(): CapBankEvent | undefined;
  setCapbankevent(value?: CapBankEvent): CapBankEventProfile;
  hasCapbankevent(): boolean;
  clearCapbankevent(): CapBankEventProfile;

  getCapbanksystem(): CapBankSystem | undefined;
  setCapbanksystem(value?: CapBankSystem): CapBankEventProfile;
  hasCapbanksystem(): boolean;
  clearCapbanksystem(): CapBankEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankEventProfile): CapBankEventProfile.AsObject;
  static serializeBinaryToWriter(message: CapBankEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankEventProfile;
  static deserializeBinaryFromReader(message: CapBankEventProfile, reader: jspb.BinaryReader): CapBankEventProfile;
}

export namespace CapBankEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    capbankevent?: CapBankEvent.AsObject,
    capbanksystem?: CapBankSystem.AsObject,
  }
}

export class CapBankReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): CapBankReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): CapBankReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): CapBankReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): CapBankReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): CapBankReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): CapBankReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): CapBankReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): CapBankReading;

  getSecondaryreadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setSecondaryreadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): CapBankReading;
  hasSecondaryreadingmmxu(): boolean;
  clearSecondaryreadingmmxu(): CapBankReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankReading.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankReading): CapBankReading.AsObject;
  static serializeBinaryToWriter(message: CapBankReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankReading;
  static deserializeBinaryFromReader(message: CapBankReading, reader: jspb.BinaryReader): CapBankReading;
}

export namespace CapBankReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
    secondaryreadingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class CapBankReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): CapBankReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): CapBankReadingProfile;

  getCapbankreading(): CapBankReading | undefined;
  setCapbankreading(value?: CapBankReading): CapBankReadingProfile;
  hasCapbankreading(): boolean;
  clearCapbankreading(): CapBankReadingProfile;

  getCapbanksystem(): CapBankSystem | undefined;
  setCapbanksystem(value?: CapBankSystem): CapBankReadingProfile;
  hasCapbanksystem(): boolean;
  clearCapbanksystem(): CapBankReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankReadingProfile): CapBankReadingProfile.AsObject;
  static serializeBinaryToWriter(message: CapBankReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankReadingProfile;
  static deserializeBinaryFromReader(message: CapBankReadingProfile, reader: jspb.BinaryReader): CapBankReadingProfile;
}

export namespace CapBankReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    capbankreading?: CapBankReading.AsObject,
    capbanksystem?: CapBankSystem.AsObject,
  }
}

export class CapBankStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): CapBankStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): CapBankStatus;

  getCapbankeventandstatusypsh(): CapBankEventAndStatusYPSH | undefined;
  setCapbankeventandstatusypsh(value?: CapBankEventAndStatusYPSH): CapBankStatus;
  hasCapbankeventandstatusypsh(): boolean;
  clearCapbankeventandstatusypsh(): CapBankStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankStatus.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankStatus): CapBankStatus.AsObject;
  static serializeBinaryToWriter(message: CapBankStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankStatus;
  static deserializeBinaryFromReader(message: CapBankStatus, reader: jspb.BinaryReader): CapBankStatus;
}

export namespace CapBankStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    capbankeventandstatusypsh?: CapBankEventAndStatusYPSH.AsObject,
  }
}

export class CapBankStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): CapBankStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): CapBankStatusProfile;

  getCapbankstatus(): CapBankStatus | undefined;
  setCapbankstatus(value?: CapBankStatus): CapBankStatusProfile;
  hasCapbankstatus(): boolean;
  clearCapbankstatus(): CapBankStatusProfile;

  getCapbanksystem(): CapBankSystem | undefined;
  setCapbanksystem(value?: CapBankSystem): CapBankStatusProfile;
  hasCapbanksystem(): boolean;
  clearCapbanksystem(): CapBankStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapBankStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CapBankStatusProfile): CapBankStatusProfile.AsObject;
  static serializeBinaryToWriter(message: CapBankStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapBankStatusProfile;
  static deserializeBinaryFromReader(message: CapBankStatusProfile, reader: jspb.BinaryReader): CapBankStatusProfile;
}

export namespace CapBankStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    capbankstatus?: CapBankStatus.AsObject,
    capbanksystem?: CapBankSystem.AsObject,
  }
}

