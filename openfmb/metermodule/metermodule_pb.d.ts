import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class MeterReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): MeterReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): MeterReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): MeterReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): MeterReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): MeterReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): MeterReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): MeterReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): MeterReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MeterReading.AsObject;
  static toObject(includeInstance: boolean, msg: MeterReading): MeterReading.AsObject;
  static serializeBinaryToWriter(message: MeterReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MeterReading;
  static deserializeBinaryFromReader(message: MeterReading, reader: jspb.BinaryReader): MeterReading;
}

export namespace MeterReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class MeterReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): MeterReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): MeterReadingProfile;

  getMeter(): commonmodule_commonmodule_pb.Meter | undefined;
  setMeter(value?: commonmodule_commonmodule_pb.Meter): MeterReadingProfile;
  hasMeter(): boolean;
  clearMeter(): MeterReadingProfile;

  getMeterreading(): MeterReading | undefined;
  setMeterreading(value?: MeterReading): MeterReadingProfile;
  hasMeterreading(): boolean;
  clearMeterreading(): MeterReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MeterReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: MeterReadingProfile): MeterReadingProfile.AsObject;
  static serializeBinaryToWriter(message: MeterReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MeterReadingProfile;
  static deserializeBinaryFromReader(message: MeterReadingProfile, reader: jspb.BinaryReader): MeterReadingProfile;
}

export namespace MeterReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    meter?: commonmodule_commonmodule_pb.Meter.AsObject,
    meterreading?: MeterReading.AsObject,
  }
}

