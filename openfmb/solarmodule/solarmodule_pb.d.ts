import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class SolarInverter extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): SolarInverter;
  hasConductingequipment(): boolean;
  clearConductingequipment(): SolarInverter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarInverter.AsObject;
  static toObject(includeInstance: boolean, msg: SolarInverter): SolarInverter.AsObject;
  static serializeBinaryToWriter(message: SolarInverter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarInverter;
  static deserializeBinaryFromReader(message: SolarInverter, reader: jspb.BinaryReader): SolarInverter;
}

export namespace SolarInverter {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

export class SolarCapabilityConfiguration extends jspb.Message {
  getSourcecapabilityconfiguration(): commonmodule_commonmodule_pb.SourceCapabilityConfiguration | undefined;
  setSourcecapabilityconfiguration(value?: commonmodule_commonmodule_pb.SourceCapabilityConfiguration): SolarCapabilityConfiguration;
  hasSourcecapabilityconfiguration(): boolean;
  clearSourcecapabilityconfiguration(): SolarCapabilityConfiguration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCapabilityConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCapabilityConfiguration): SolarCapabilityConfiguration.AsObject;
  static serializeBinaryToWriter(message: SolarCapabilityConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCapabilityConfiguration;
  static deserializeBinaryFromReader(message: SolarCapabilityConfiguration, reader: jspb.BinaryReader): SolarCapabilityConfiguration;
}

export namespace SolarCapabilityConfiguration {
  export type AsObject = {
    sourcecapabilityconfiguration?: commonmodule_commonmodule_pb.SourceCapabilityConfiguration.AsObject,
  }
}

export class SolarCapabilityOverride extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): SolarCapabilityOverride;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): SolarCapabilityOverride;

  getSolarcapabilityconfiguration(): SolarCapabilityConfiguration | undefined;
  setSolarcapabilityconfiguration(value?: SolarCapabilityConfiguration): SolarCapabilityOverride;
  hasSolarcapabilityconfiguration(): boolean;
  clearSolarcapabilityconfiguration(): SolarCapabilityOverride;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCapabilityOverride.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCapabilityOverride): SolarCapabilityOverride.AsObject;
  static serializeBinaryToWriter(message: SolarCapabilityOverride, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCapabilityOverride;
  static deserializeBinaryFromReader(message: SolarCapabilityOverride, reader: jspb.BinaryReader): SolarCapabilityOverride;
}

export namespace SolarCapabilityOverride {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    solarcapabilityconfiguration?: SolarCapabilityConfiguration.AsObject,
  }
}

export class SolarCapabilityOverrideProfile extends jspb.Message {
  getCapabilitymessageinfo(): commonmodule_commonmodule_pb.CapabilityMessageInfo | undefined;
  setCapabilitymessageinfo(value?: commonmodule_commonmodule_pb.CapabilityMessageInfo): SolarCapabilityOverrideProfile;
  hasCapabilitymessageinfo(): boolean;
  clearCapabilitymessageinfo(): SolarCapabilityOverrideProfile;

  getSolarcapabilityoverride(): SolarCapabilityOverride | undefined;
  setSolarcapabilityoverride(value?: SolarCapabilityOverride): SolarCapabilityOverrideProfile;
  hasSolarcapabilityoverride(): boolean;
  clearSolarcapabilityoverride(): SolarCapabilityOverrideProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarCapabilityOverrideProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarCapabilityOverrideProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCapabilityOverrideProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCapabilityOverrideProfile): SolarCapabilityOverrideProfile.AsObject;
  static serializeBinaryToWriter(message: SolarCapabilityOverrideProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCapabilityOverrideProfile;
  static deserializeBinaryFromReader(message: SolarCapabilityOverrideProfile, reader: jspb.BinaryReader): SolarCapabilityOverrideProfile;
}

export namespace SolarCapabilityOverrideProfile {
  export type AsObject = {
    capabilitymessageinfo?: commonmodule_commonmodule_pb.CapabilityMessageInfo.AsObject,
    solarcapabilityoverride?: SolarCapabilityOverride.AsObject,
    solarinverter?: SolarInverter.AsObject,
  }
}

export class SolarCapabilityRatings extends jspb.Message {
  getSourcecapabilityratings(): commonmodule_commonmodule_pb.SourceCapabilityRatings | undefined;
  setSourcecapabilityratings(value?: commonmodule_commonmodule_pb.SourceCapabilityRatings): SolarCapabilityRatings;
  hasSourcecapabilityratings(): boolean;
  clearSourcecapabilityratings(): SolarCapabilityRatings;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCapabilityRatings.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCapabilityRatings): SolarCapabilityRatings.AsObject;
  static serializeBinaryToWriter(message: SolarCapabilityRatings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCapabilityRatings;
  static deserializeBinaryFromReader(message: SolarCapabilityRatings, reader: jspb.BinaryReader): SolarCapabilityRatings;
}

export namespace SolarCapabilityRatings {
  export type AsObject = {
    sourcecapabilityratings?: commonmodule_commonmodule_pb.SourceCapabilityRatings.AsObject,
  }
}

export class SolarCapability extends jspb.Message {
  getNameplatevalue(): commonmodule_commonmodule_pb.NameplateValue | undefined;
  setNameplatevalue(value?: commonmodule_commonmodule_pb.NameplateValue): SolarCapability;
  hasNameplatevalue(): boolean;
  clearNameplatevalue(): SolarCapability;

  getSolarcapabilityconfiguration(): SolarCapabilityConfiguration | undefined;
  setSolarcapabilityconfiguration(value?: SolarCapabilityConfiguration): SolarCapability;
  hasSolarcapabilityconfiguration(): boolean;
  clearSolarcapabilityconfiguration(): SolarCapability;

  getSolarcapabilityratings(): SolarCapabilityRatings | undefined;
  setSolarcapabilityratings(value?: SolarCapabilityRatings): SolarCapability;
  hasSolarcapabilityratings(): boolean;
  clearSolarcapabilityratings(): SolarCapability;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCapability.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCapability): SolarCapability.AsObject;
  static serializeBinaryToWriter(message: SolarCapability, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCapability;
  static deserializeBinaryFromReader(message: SolarCapability, reader: jspb.BinaryReader): SolarCapability;
}

export namespace SolarCapability {
  export type AsObject = {
    nameplatevalue?: commonmodule_commonmodule_pb.NameplateValue.AsObject,
    solarcapabilityconfiguration?: SolarCapabilityConfiguration.AsObject,
    solarcapabilityratings?: SolarCapabilityRatings.AsObject,
  }
}

export class SolarCapabilityProfile extends jspb.Message {
  getCapabilitymessageinfo(): commonmodule_commonmodule_pb.CapabilityMessageInfo | undefined;
  setCapabilitymessageinfo(value?: commonmodule_commonmodule_pb.CapabilityMessageInfo): SolarCapabilityProfile;
  hasCapabilitymessageinfo(): boolean;
  clearCapabilitymessageinfo(): SolarCapabilityProfile;

  getSolarcapability(): SolarCapability | undefined;
  setSolarcapability(value?: SolarCapability): SolarCapabilityProfile;
  hasSolarcapability(): boolean;
  clearSolarcapability(): SolarCapabilityProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarCapabilityProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarCapabilityProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCapabilityProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCapabilityProfile): SolarCapabilityProfile.AsObject;
  static serializeBinaryToWriter(message: SolarCapabilityProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCapabilityProfile;
  static deserializeBinaryFromReader(message: SolarCapabilityProfile, reader: jspb.BinaryReader): SolarCapabilityProfile;
}

export namespace SolarCapabilityProfile {
  export type AsObject = {
    capabilitymessageinfo?: commonmodule_commonmodule_pb.CapabilityMessageInfo.AsObject,
    solarcapability?: SolarCapability.AsObject,
    solarinverter?: SolarInverter.AsObject,
  }
}

export class SolarPoint extends jspb.Message {
  getMode(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setMode(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): SolarPoint;
  hasMode(): boolean;
  clearMode(): SolarPoint;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): SolarPoint;
  hasRamprates(): boolean;
  clearRamprates(): SolarPoint;

  getReset(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReset(value?: commonmodule_commonmodule_pb.ControlSPC): SolarPoint;
  hasReset(): boolean;
  clearReset(): SolarPoint;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): SolarPoint;
  hasState(): boolean;
  clearState(): SolarPoint;

  getEnterserviceoperation(): commonmodule_commonmodule_pb.EnterServiceAPC | undefined;
  setEnterserviceoperation(value?: commonmodule_commonmodule_pb.EnterServiceAPC): SolarPoint;
  hasEnterserviceoperation(): boolean;
  clearEnterserviceoperation(): SolarPoint;

  getHzwoperation(): commonmodule_commonmodule_pb.HzWAPC | undefined;
  setHzwoperation(value?: commonmodule_commonmodule_pb.HzWAPC): SolarPoint;
  hasHzwoperation(): boolean;
  clearHzwoperation(): SolarPoint;

  getLimitwoperation(): commonmodule_commonmodule_pb.LimitWAPC | undefined;
  setLimitwoperation(value?: commonmodule_commonmodule_pb.LimitWAPC): SolarPoint;
  hasLimitwoperation(): boolean;
  clearLimitwoperation(): SolarPoint;

  getPfoperation(): commonmodule_commonmodule_pb.PFSPC | undefined;
  setPfoperation(value?: commonmodule_commonmodule_pb.PFSPC): SolarPoint;
  hasPfoperation(): boolean;
  clearPfoperation(): SolarPoint;

  getTmhztripoperation(): commonmodule_commonmodule_pb.TmHzCSG | undefined;
  setTmhztripoperation(value?: commonmodule_commonmodule_pb.TmHzCSG): SolarPoint;
  hasTmhztripoperation(): boolean;
  clearTmhztripoperation(): SolarPoint;

  getTmvolttripoperation(): commonmodule_commonmodule_pb.TmVoltCSG | undefined;
  setTmvolttripoperation(value?: commonmodule_commonmodule_pb.TmVoltCSG): SolarPoint;
  hasTmvolttripoperation(): boolean;
  clearTmvolttripoperation(): SolarPoint;

  getVaroperation(): commonmodule_commonmodule_pb.VarSPC | undefined;
  setVaroperation(value?: commonmodule_commonmodule_pb.VarSPC): SolarPoint;
  hasVaroperation(): boolean;
  clearVaroperation(): SolarPoint;

  getVoltvaroperation(): commonmodule_commonmodule_pb.VoltVarCSG | undefined;
  setVoltvaroperation(value?: commonmodule_commonmodule_pb.VoltVarCSG): SolarPoint;
  hasVoltvaroperation(): boolean;
  clearVoltvaroperation(): SolarPoint;

  getVoltwoperation(): commonmodule_commonmodule_pb.VoltWCSG | undefined;
  setVoltwoperation(value?: commonmodule_commonmodule_pb.VoltWCSG): SolarPoint;
  hasVoltwoperation(): boolean;
  clearVoltwoperation(): SolarPoint;

  getWvaroperation(): commonmodule_commonmodule_pb.WVarCSG | undefined;
  setWvaroperation(value?: commonmodule_commonmodule_pb.WVarCSG): SolarPoint;
  hasWvaroperation(): boolean;
  clearWvaroperation(): SolarPoint;

  getBlackstartenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.ControlSPC): SolarPoint;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): SolarPoint;

  getWoperation(): commonmodule_commonmodule_pb.WSPC | undefined;
  setWoperation(value?: commonmodule_commonmodule_pb.WSPC): SolarPoint;
  hasWoperation(): boolean;
  clearWoperation(): SolarPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarPoint.AsObject;
  static toObject(includeInstance: boolean, msg: SolarPoint): SolarPoint.AsObject;
  static serializeBinaryToWriter(message: SolarPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarPoint;
  static deserializeBinaryFromReader(message: SolarPoint, reader: jspb.BinaryReader): SolarPoint;
}

export namespace SolarPoint {
  export type AsObject = {
    mode?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reset?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    enterserviceoperation?: commonmodule_commonmodule_pb.EnterServiceAPC.AsObject,
    hzwoperation?: commonmodule_commonmodule_pb.HzWAPC.AsObject,
    limitwoperation?: commonmodule_commonmodule_pb.LimitWAPC.AsObject,
    pfoperation?: commonmodule_commonmodule_pb.PFSPC.AsObject,
    tmhztripoperation?: commonmodule_commonmodule_pb.TmHzCSG.AsObject,
    tmvolttripoperation?: commonmodule_commonmodule_pb.TmVoltCSG.AsObject,
    varoperation?: commonmodule_commonmodule_pb.VarSPC.AsObject,
    voltvaroperation?: commonmodule_commonmodule_pb.VoltVarCSG.AsObject,
    voltwoperation?: commonmodule_commonmodule_pb.VoltWCSG.AsObject,
    wvaroperation?: commonmodule_commonmodule_pb.WVarCSG.AsObject,
    blackstartenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    woperation?: commonmodule_commonmodule_pb.WSPC.AsObject,
  }
}

export class SolarCurvePoint extends jspb.Message {
  getControl(): SolarPoint | undefined;
  setControl(value?: SolarPoint): SolarCurvePoint;
  hasControl(): boolean;
  clearControl(): SolarCurvePoint;

  getStarttime(): commonmodule_commonmodule_pb.ControlTimestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.ControlTimestamp): SolarCurvePoint;
  hasStarttime(): boolean;
  clearStarttime(): SolarCurvePoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCurvePoint.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCurvePoint): SolarCurvePoint.AsObject;
  static serializeBinaryToWriter(message: SolarCurvePoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCurvePoint;
  static deserializeBinaryFromReader(message: SolarCurvePoint, reader: jspb.BinaryReader): SolarCurvePoint;
}

export namespace SolarCurvePoint {
  export type AsObject = {
    control?: SolarPoint.AsObject,
    starttime?: commonmodule_commonmodule_pb.ControlTimestamp.AsObject,
  }
}

export class SolarCSG extends jspb.Message {
  getCrvptsList(): Array<SolarCurvePoint>;
  setCrvptsList(value: Array<SolarCurvePoint>): SolarCSG;
  clearCrvptsList(): SolarCSG;
  addCrvpts(value?: SolarCurvePoint, index?: number): SolarCurvePoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarCSG.AsObject;
  static toObject(includeInstance: boolean, msg: SolarCSG): SolarCSG.AsObject;
  static serializeBinaryToWriter(message: SolarCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarCSG;
  static deserializeBinaryFromReader(message: SolarCSG, reader: jspb.BinaryReader): SolarCSG;
}

export namespace SolarCSG {
  export type AsObject = {
    crvptsList: Array<SolarCurvePoint.AsObject>,
  }
}

export class SolarControlScheduleFSCH extends jspb.Message {
  getValdcsg(): SolarCSG | undefined;
  setValdcsg(value?: SolarCSG): SolarControlScheduleFSCH;
  hasValdcsg(): boolean;
  clearValdcsg(): SolarControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: SolarControlScheduleFSCH): SolarControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: SolarControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarControlScheduleFSCH;
  static deserializeBinaryFromReader(message: SolarControlScheduleFSCH, reader: jspb.BinaryReader): SolarControlScheduleFSCH;
}

export namespace SolarControlScheduleFSCH {
  export type AsObject = {
    valdcsg?: SolarCSG.AsObject,
  }
}

export class SolarControlFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): SolarControlFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): SolarControlFSCC;

  getSolarcontrolschedulefsch(): SolarControlScheduleFSCH | undefined;
  setSolarcontrolschedulefsch(value?: SolarControlScheduleFSCH): SolarControlFSCC;
  hasSolarcontrolschedulefsch(): boolean;
  clearSolarcontrolschedulefsch(): SolarControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: SolarControlFSCC): SolarControlFSCC.AsObject;
  static serializeBinaryToWriter(message: SolarControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarControlFSCC;
  static deserializeBinaryFromReader(message: SolarControlFSCC, reader: jspb.BinaryReader): SolarControlFSCC;
}

export namespace SolarControlFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    solarcontrolschedulefsch?: SolarControlScheduleFSCH.AsObject,
  }
}

export class SolarControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): SolarControl;
  hasControlvalue(): boolean;
  clearControlvalue(): SolarControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): SolarControl;
  hasCheck(): boolean;
  clearCheck(): SolarControl;

  getSolarcontrolfscc(): SolarControlFSCC | undefined;
  setSolarcontrolfscc(value?: SolarControlFSCC): SolarControl;
  hasSolarcontrolfscc(): boolean;
  clearSolarcontrolfscc(): SolarControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarControl.AsObject;
  static toObject(includeInstance: boolean, msg: SolarControl): SolarControl.AsObject;
  static serializeBinaryToWriter(message: SolarControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarControl;
  static deserializeBinaryFromReader(message: SolarControl, reader: jspb.BinaryReader): SolarControl;
}

export namespace SolarControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    solarcontrolfscc?: SolarControlFSCC.AsObject,
  }
}

export class SolarControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): SolarControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): SolarControlProfile;

  getSolarcontrol(): SolarControl | undefined;
  setSolarcontrol(value?: SolarControl): SolarControlProfile;
  hasSolarcontrol(): boolean;
  clearSolarcontrol(): SolarControlProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarControlProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarControlProfile): SolarControlProfile.AsObject;
  static serializeBinaryToWriter(message: SolarControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarControlProfile;
  static deserializeBinaryFromReader(message: SolarControlProfile, reader: jspb.BinaryReader): SolarControlProfile;
}

export namespace SolarControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    solarcontrol?: SolarControl.AsObject,
    solarinverter?: SolarInverter.AsObject,
  }
}

export class SolarDiscreteControlPV extends jspb.Message {
  getLogicalnodeforcontrol(): commonmodule_commonmodule_pb.LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: commonmodule_commonmodule_pb.LogicalNodeForControl): SolarDiscreteControlPV;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): SolarDiscreteControlPV;

  getControl(): SolarPoint | undefined;
  setControl(value?: SolarPoint): SolarDiscreteControlPV;
  hasControl(): boolean;
  clearControl(): SolarDiscreteControlPV;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarDiscreteControlPV.AsObject;
  static toObject(includeInstance: boolean, msg: SolarDiscreteControlPV): SolarDiscreteControlPV.AsObject;
  static serializeBinaryToWriter(message: SolarDiscreteControlPV, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarDiscreteControlPV;
  static deserializeBinaryFromReader(message: SolarDiscreteControlPV, reader: jspb.BinaryReader): SolarDiscreteControlPV;
}

export namespace SolarDiscreteControlPV {
  export type AsObject = {
    logicalnodeforcontrol?: commonmodule_commonmodule_pb.LogicalNodeForControl.AsObject,
    control?: SolarPoint.AsObject,
  }
}

export class SolarDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): SolarDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): SolarDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): SolarDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): SolarDiscreteControl;

  getSolardiscretecontrolpv(): SolarDiscreteControlPV | undefined;
  setSolardiscretecontrolpv(value?: SolarDiscreteControlPV): SolarDiscreteControl;
  hasSolardiscretecontrolpv(): boolean;
  clearSolardiscretecontrolpv(): SolarDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: SolarDiscreteControl): SolarDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: SolarDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarDiscreteControl;
  static deserializeBinaryFromReader(message: SolarDiscreteControl, reader: jspb.BinaryReader): SolarDiscreteControl;
}

export namespace SolarDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    solardiscretecontrolpv?: SolarDiscreteControlPV.AsObject,
  }
}

export class SolarDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): SolarDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): SolarDiscreteControlProfile;

  getSolardiscretecontrol(): SolarDiscreteControl | undefined;
  setSolardiscretecontrol(value?: SolarDiscreteControl): SolarDiscreteControlProfile;
  hasSolardiscretecontrol(): boolean;
  clearSolardiscretecontrol(): SolarDiscreteControlProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarDiscreteControlProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarDiscreteControlProfile): SolarDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: SolarDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarDiscreteControlProfile;
  static deserializeBinaryFromReader(message: SolarDiscreteControlProfile, reader: jspb.BinaryReader): SolarDiscreteControlProfile;
}

export namespace SolarDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    solardiscretecontrol?: SolarDiscreteControl.AsObject,
    solarinverter?: SolarInverter.AsObject,
  }
}

export class SolarPointStatus extends jspb.Message {
  getFrequencysetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setFrequencysetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): SolarPointStatus;
  hasFrequencysetpointenabled(): boolean;
  clearFrequencysetpointenabled(): SolarPointStatus;

  getMode(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setMode(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): SolarPointStatus;
  hasMode(): boolean;
  clearMode(): SolarPointStatus;

  getPcthzdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPcthzdroop(value?: google_protobuf_wrappers_pb.FloatValue): SolarPointStatus;
  hasPcthzdroop(): boolean;
  clearPcthzdroop(): SolarPointStatus;

  getPctvdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPctvdroop(value?: google_protobuf_wrappers_pb.FloatValue): SolarPointStatus;
  hasPctvdroop(): boolean;
  clearPctvdroop(): SolarPointStatus;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): SolarPointStatus;
  hasRamprates(): boolean;
  clearRamprates(): SolarPointStatus;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): SolarPointStatus;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): SolarPointStatus;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): SolarPointStatus;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): SolarPointStatus;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): SolarPointStatus;
  hasState(): boolean;
  clearState(): SolarPointStatus;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): SolarPointStatus;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): SolarPointStatus;

  getBlackstartenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.ControlSPC): SolarPointStatus;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): SolarPointStatus;

  getEnterserviceoperation(): commonmodule_commonmodule_pb.EnterServiceAPC | undefined;
  setEnterserviceoperation(value?: commonmodule_commonmodule_pb.EnterServiceAPC): SolarPointStatus;
  hasEnterserviceoperation(): boolean;
  clearEnterserviceoperation(): SolarPointStatus;

  getHzwoperation(): commonmodule_commonmodule_pb.HzWPoint | undefined;
  setHzwoperation(value?: commonmodule_commonmodule_pb.HzWPoint): SolarPointStatus;
  hasHzwoperation(): boolean;
  clearHzwoperation(): SolarPointStatus;

  getLimitwoperation(): commonmodule_commonmodule_pb.LimitWAPC | undefined;
  setLimitwoperation(value?: commonmodule_commonmodule_pb.LimitWAPC): SolarPointStatus;
  hasLimitwoperation(): boolean;
  clearLimitwoperation(): SolarPointStatus;

  getPfoperation(): commonmodule_commonmodule_pb.PFSPC | undefined;
  setPfoperation(value?: commonmodule_commonmodule_pb.PFSPC): SolarPointStatus;
  hasPfoperation(): boolean;
  clearPfoperation(): SolarPointStatus;

  getSyncbacktogrid(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setSyncbacktogrid(value?: commonmodule_commonmodule_pb.ControlSPC): SolarPointStatus;
  hasSyncbacktogrid(): boolean;
  clearSyncbacktogrid(): SolarPointStatus;

  getTmhztripoperation(): commonmodule_commonmodule_pb.TmHzCSG | undefined;
  setTmhztripoperation(value?: commonmodule_commonmodule_pb.TmHzCSG): SolarPointStatus;
  hasTmhztripoperation(): boolean;
  clearTmhztripoperation(): SolarPointStatus;

  getTmvolttripoperation(): commonmodule_commonmodule_pb.TmVoltCSG | undefined;
  setTmvolttripoperation(value?: commonmodule_commonmodule_pb.TmVoltCSG): SolarPointStatus;
  hasTmvolttripoperation(): boolean;
  clearTmvolttripoperation(): SolarPointStatus;

  getVaroperation(): commonmodule_commonmodule_pb.VarSPC | undefined;
  setVaroperation(value?: commonmodule_commonmodule_pb.VarSPC): SolarPointStatus;
  hasVaroperation(): boolean;
  clearVaroperation(): SolarPointStatus;

  getVoltvaroperation(): commonmodule_commonmodule_pb.VoltVarCSG | undefined;
  setVoltvaroperation(value?: commonmodule_commonmodule_pb.VoltVarCSG): SolarPointStatus;
  hasVoltvaroperation(): boolean;
  clearVoltvaroperation(): SolarPointStatus;

  getVoltwoperation(): commonmodule_commonmodule_pb.VoltWCSG | undefined;
  setVoltwoperation(value?: commonmodule_commonmodule_pb.VoltWCSG): SolarPointStatus;
  hasVoltwoperation(): boolean;
  clearVoltwoperation(): SolarPointStatus;

  getWvaroperation(): commonmodule_commonmodule_pb.WVarCSG | undefined;
  setWvaroperation(value?: commonmodule_commonmodule_pb.WVarCSG): SolarPointStatus;
  hasWvaroperation(): boolean;
  clearWvaroperation(): SolarPointStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarPointStatus.AsObject;
  static toObject(includeInstance: boolean, msg: SolarPointStatus): SolarPointStatus.AsObject;
  static serializeBinaryToWriter(message: SolarPointStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarPointStatus;
  static deserializeBinaryFromReader(message: SolarPointStatus, reader: jspb.BinaryReader): SolarPointStatus;
}

export namespace SolarPointStatus {
  export type AsObject = {
    frequencysetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    mode?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
    pcthzdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    pctvdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    blackstartenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    enterserviceoperation?: commonmodule_commonmodule_pb.EnterServiceAPC.AsObject,
    hzwoperation?: commonmodule_commonmodule_pb.HzWPoint.AsObject,
    limitwoperation?: commonmodule_commonmodule_pb.LimitWAPC.AsObject,
    pfoperation?: commonmodule_commonmodule_pb.PFSPC.AsObject,
    syncbacktogrid?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    tmhztripoperation?: commonmodule_commonmodule_pb.TmHzCSG.AsObject,
    tmvolttripoperation?: commonmodule_commonmodule_pb.TmVoltCSG.AsObject,
    varoperation?: commonmodule_commonmodule_pb.VarSPC.AsObject,
    voltvaroperation?: commonmodule_commonmodule_pb.VoltVarCSG.AsObject,
    voltwoperation?: commonmodule_commonmodule_pb.VoltWCSG.AsObject,
    wvaroperation?: commonmodule_commonmodule_pb.WVarCSG.AsObject,
  }
}

export class SolarEventAndStatusZGEN extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): SolarEventAndStatusZGEN;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): SolarEventAndStatusZGEN;

  getAuxpwrst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setAuxpwrst(value?: commonmodule_commonmodule_pb.StatusSPS): SolarEventAndStatusZGEN;
  hasAuxpwrst(): boolean;
  clearAuxpwrst(): SolarEventAndStatusZGEN;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): SolarEventAndStatusZGEN;
  hasDynamictest(): boolean;
  clearDynamictest(): SolarEventAndStatusZGEN;

  getEmgstop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setEmgstop(value?: commonmodule_commonmodule_pb.StatusSPS): SolarEventAndStatusZGEN;
  hasEmgstop(): boolean;
  clearEmgstop(): SolarEventAndStatusZGEN;

  getPointstatus(): SolarPointStatus | undefined;
  setPointstatus(value?: SolarPointStatus): SolarEventAndStatusZGEN;
  hasPointstatus(): boolean;
  clearPointstatus(): SolarEventAndStatusZGEN;

  getAlrm(): commonmodule_commonmodule_pb.Optional_AlrmKind | undefined;
  setAlrm(value?: commonmodule_commonmodule_pb.Optional_AlrmKind): SolarEventAndStatusZGEN;
  hasAlrm(): boolean;
  clearAlrm(): SolarEventAndStatusZGEN;

  getGnsynst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setGnsynst(value?: commonmodule_commonmodule_pb.StatusSPS): SolarEventAndStatusZGEN;
  hasGnsynst(): boolean;
  clearGnsynst(): SolarEventAndStatusZGEN;

  getGridconnectionstate(): commonmodule_commonmodule_pb.Optional_GridConnectionStateKind | undefined;
  setGridconnectionstate(value?: commonmodule_commonmodule_pb.Optional_GridConnectionStateKind): SolarEventAndStatusZGEN;
  hasGridconnectionstate(): boolean;
  clearGridconnectionstate(): SolarEventAndStatusZGEN;

  getManalrminfo(): google_protobuf_wrappers_pb.StringValue | undefined;
  setManalrminfo(value?: google_protobuf_wrappers_pb.StringValue): SolarEventAndStatusZGEN;
  hasManalrminfo(): boolean;
  clearManalrminfo(): SolarEventAndStatusZGEN;

  getOperatingstate(): commonmodule_commonmodule_pb.Optional_OperatingStateKind | undefined;
  setOperatingstate(value?: commonmodule_commonmodule_pb.Optional_OperatingStateKind): SolarEventAndStatusZGEN;
  hasOperatingstate(): boolean;
  clearOperatingstate(): SolarEventAndStatusZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarEventAndStatusZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: SolarEventAndStatusZGEN): SolarEventAndStatusZGEN.AsObject;
  static serializeBinaryToWriter(message: SolarEventAndStatusZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarEventAndStatusZGEN;
  static deserializeBinaryFromReader(message: SolarEventAndStatusZGEN, reader: jspb.BinaryReader): SolarEventAndStatusZGEN;
}

export namespace SolarEventAndStatusZGEN {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    auxpwrst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    emgstop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    pointstatus?: SolarPointStatus.AsObject,
    alrm?: commonmodule_commonmodule_pb.Optional_AlrmKind.AsObject,
    gnsynst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    gridconnectionstate?: commonmodule_commonmodule_pb.Optional_GridConnectionStateKind.AsObject,
    manalrminfo?: google_protobuf_wrappers_pb.StringValue.AsObject,
    operatingstate?: commonmodule_commonmodule_pb.Optional_OperatingStateKind.AsObject,
  }
}

export class SolarEventZGEN extends jspb.Message {
  getSolareventandstatuszgen(): SolarEventAndStatusZGEN | undefined;
  setSolareventandstatuszgen(value?: SolarEventAndStatusZGEN): SolarEventZGEN;
  hasSolareventandstatuszgen(): boolean;
  clearSolareventandstatuszgen(): SolarEventZGEN;

  getGrimod(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setGrimod(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): SolarEventZGEN;
  hasGrimod(): boolean;
  clearGrimod(): SolarEventZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarEventZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: SolarEventZGEN): SolarEventZGEN.AsObject;
  static serializeBinaryToWriter(message: SolarEventZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarEventZGEN;
  static deserializeBinaryFromReader(message: SolarEventZGEN, reader: jspb.BinaryReader): SolarEventZGEN;
}

export namespace SolarEventZGEN {
  export type AsObject = {
    solareventandstatuszgen?: SolarEventAndStatusZGEN.AsObject,
    grimod?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
  }
}

export class SolarEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): SolarEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): SolarEvent;

  getSolareventzgen(): SolarEventZGEN | undefined;
  setSolareventzgen(value?: SolarEventZGEN): SolarEvent;
  hasSolareventzgen(): boolean;
  clearSolareventzgen(): SolarEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarEvent.AsObject;
  static toObject(includeInstance: boolean, msg: SolarEvent): SolarEvent.AsObject;
  static serializeBinaryToWriter(message: SolarEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarEvent;
  static deserializeBinaryFromReader(message: SolarEvent, reader: jspb.BinaryReader): SolarEvent;
}

export namespace SolarEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    solareventzgen?: SolarEventZGEN.AsObject,
  }
}

export class SolarEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): SolarEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): SolarEventProfile;

  getSolarevent(): SolarEvent | undefined;
  setSolarevent(value?: SolarEvent): SolarEventProfile;
  hasSolarevent(): boolean;
  clearSolarevent(): SolarEventProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarEventProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarEventProfile): SolarEventProfile.AsObject;
  static serializeBinaryToWriter(message: SolarEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarEventProfile;
  static deserializeBinaryFromReader(message: SolarEventProfile, reader: jspb.BinaryReader): SolarEventProfile;
}

export namespace SolarEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    solarevent?: SolarEvent.AsObject,
    solarinverter?: SolarInverter.AsObject,
  }
}

export class SolarReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): SolarReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): SolarReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): SolarReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): SolarReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): SolarReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): SolarReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): SolarReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): SolarReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarReading.AsObject;
  static toObject(includeInstance: boolean, msg: SolarReading): SolarReading.AsObject;
  static serializeBinaryToWriter(message: SolarReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarReading;
  static deserializeBinaryFromReader(message: SolarReading, reader: jspb.BinaryReader): SolarReading;
}

export namespace SolarReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class SolarReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): SolarReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): SolarReadingProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarReadingProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarReadingProfile;

  getSolarreading(): SolarReading | undefined;
  setSolarreading(value?: SolarReading): SolarReadingProfile;
  hasSolarreading(): boolean;
  clearSolarreading(): SolarReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarReadingProfile): SolarReadingProfile.AsObject;
  static serializeBinaryToWriter(message: SolarReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarReadingProfile;
  static deserializeBinaryFromReader(message: SolarReadingProfile, reader: jspb.BinaryReader): SolarReadingProfile;
}

export namespace SolarReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    solarinverter?: SolarInverter.AsObject,
    solarreading?: SolarReading.AsObject,
  }
}

export class SolarStatusZGEN extends jspb.Message {
  getSolareventandstatuszgen(): SolarEventAndStatusZGEN | undefined;
  setSolareventandstatuszgen(value?: SolarEventAndStatusZGEN): SolarStatusZGEN;
  hasSolareventandstatuszgen(): boolean;
  clearSolareventandstatuszgen(): SolarStatusZGEN;

  getGrimod(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setGrimod(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): SolarStatusZGEN;
  hasGrimod(): boolean;
  clearGrimod(): SolarStatusZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarStatusZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: SolarStatusZGEN): SolarStatusZGEN.AsObject;
  static serializeBinaryToWriter(message: SolarStatusZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarStatusZGEN;
  static deserializeBinaryFromReader(message: SolarStatusZGEN, reader: jspb.BinaryReader): SolarStatusZGEN;
}

export namespace SolarStatusZGEN {
  export type AsObject = {
    solareventandstatuszgen?: SolarEventAndStatusZGEN.AsObject,
    grimod?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
  }
}

export class SolarStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): SolarStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): SolarStatus;

  getSolarstatuszgen(): SolarStatusZGEN | undefined;
  setSolarstatuszgen(value?: SolarStatusZGEN): SolarStatus;
  hasSolarstatuszgen(): boolean;
  clearSolarstatuszgen(): SolarStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarStatus.AsObject;
  static toObject(includeInstance: boolean, msg: SolarStatus): SolarStatus.AsObject;
  static serializeBinaryToWriter(message: SolarStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarStatus;
  static deserializeBinaryFromReader(message: SolarStatus, reader: jspb.BinaryReader): SolarStatus;
}

export namespace SolarStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    solarstatuszgen?: SolarStatusZGEN.AsObject,
  }
}

export class SolarStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): SolarStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): SolarStatusProfile;

  getSolarinverter(): SolarInverter | undefined;
  setSolarinverter(value?: SolarInverter): SolarStatusProfile;
  hasSolarinverter(): boolean;
  clearSolarinverter(): SolarStatusProfile;

  getSolarstatus(): SolarStatus | undefined;
  setSolarstatus(value?: SolarStatus): SolarStatusProfile;
  hasSolarstatus(): boolean;
  clearSolarstatus(): SolarStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SolarStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SolarStatusProfile): SolarStatusProfile.AsObject;
  static serializeBinaryToWriter(message: SolarStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SolarStatusProfile;
  static deserializeBinaryFromReader(message: SolarStatusProfile, reader: jspb.BinaryReader): SolarStatusProfile;
}

export namespace SolarStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    solarinverter?: SolarInverter.AsObject,
    solarstatus?: SolarStatus.AsObject,
  }
}

