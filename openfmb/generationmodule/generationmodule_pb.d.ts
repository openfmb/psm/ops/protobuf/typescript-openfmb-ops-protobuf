import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class GeneratingUnit extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): GeneratingUnit;
  hasConductingequipment(): boolean;
  clearConductingequipment(): GeneratingUnit;

  getMaxoperatingp(): commonmodule_commonmodule_pb.ActivePower | undefined;
  setMaxoperatingp(value?: commonmodule_commonmodule_pb.ActivePower): GeneratingUnit;
  hasMaxoperatingp(): boolean;
  clearMaxoperatingp(): GeneratingUnit;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GeneratingUnit.AsObject;
  static toObject(includeInstance: boolean, msg: GeneratingUnit): GeneratingUnit.AsObject;
  static serializeBinaryToWriter(message: GeneratingUnit, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GeneratingUnit;
  static deserializeBinaryFromReader(message: GeneratingUnit, reader: jspb.BinaryReader): GeneratingUnit;
}

export namespace GeneratingUnit {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    maxoperatingp?: commonmodule_commonmodule_pb.ActivePower.AsObject,
  }
}

export class GenerationCapabilityConfiguration extends jspb.Message {
  getSourcecapabilityconfiguration(): commonmodule_commonmodule_pb.SourceCapabilityConfiguration | undefined;
  setSourcecapabilityconfiguration(value?: commonmodule_commonmodule_pb.SourceCapabilityConfiguration): GenerationCapabilityConfiguration;
  hasSourcecapabilityconfiguration(): boolean;
  clearSourcecapabilityconfiguration(): GenerationCapabilityConfiguration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCapabilityConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCapabilityConfiguration): GenerationCapabilityConfiguration.AsObject;
  static serializeBinaryToWriter(message: GenerationCapabilityConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCapabilityConfiguration;
  static deserializeBinaryFromReader(message: GenerationCapabilityConfiguration, reader: jspb.BinaryReader): GenerationCapabilityConfiguration;
}

export namespace GenerationCapabilityConfiguration {
  export type AsObject = {
    sourcecapabilityconfiguration?: commonmodule_commonmodule_pb.SourceCapabilityConfiguration.AsObject,
  }
}

export class GenerationCapabilityOverride extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): GenerationCapabilityOverride;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): GenerationCapabilityOverride;

  getGenerationcapabilityconfiguration(): GenerationCapabilityConfiguration | undefined;
  setGenerationcapabilityconfiguration(value?: GenerationCapabilityConfiguration): GenerationCapabilityOverride;
  hasGenerationcapabilityconfiguration(): boolean;
  clearGenerationcapabilityconfiguration(): GenerationCapabilityOverride;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCapabilityOverride.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCapabilityOverride): GenerationCapabilityOverride.AsObject;
  static serializeBinaryToWriter(message: GenerationCapabilityOverride, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCapabilityOverride;
  static deserializeBinaryFromReader(message: GenerationCapabilityOverride, reader: jspb.BinaryReader): GenerationCapabilityOverride;
}

export namespace GenerationCapabilityOverride {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    generationcapabilityconfiguration?: GenerationCapabilityConfiguration.AsObject,
  }
}

export class GenerationCapabilityOverrideProfile extends jspb.Message {
  getCapabilitymessageinfo(): commonmodule_commonmodule_pb.CapabilityMessageInfo | undefined;
  setCapabilitymessageinfo(value?: commonmodule_commonmodule_pb.CapabilityMessageInfo): GenerationCapabilityOverrideProfile;
  hasCapabilitymessageinfo(): boolean;
  clearCapabilitymessageinfo(): GenerationCapabilityOverrideProfile;

  getGenerationcapabilityoverride(): GenerationCapabilityOverride | undefined;
  setGenerationcapabilityoverride(value?: GenerationCapabilityOverride): GenerationCapabilityOverrideProfile;
  hasGenerationcapabilityoverride(): boolean;
  clearGenerationcapabilityoverride(): GenerationCapabilityOverrideProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationCapabilityOverrideProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationCapabilityOverrideProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCapabilityOverrideProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCapabilityOverrideProfile): GenerationCapabilityOverrideProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationCapabilityOverrideProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCapabilityOverrideProfile;
  static deserializeBinaryFromReader(message: GenerationCapabilityOverrideProfile, reader: jspb.BinaryReader): GenerationCapabilityOverrideProfile;
}

export namespace GenerationCapabilityOverrideProfile {
  export type AsObject = {
    capabilitymessageinfo?: commonmodule_commonmodule_pb.CapabilityMessageInfo.AsObject,
    generationcapabilityoverride?: GenerationCapabilityOverride.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
  }
}

export class GenerationCapabilityRatings extends jspb.Message {
  getSourcecapabilityratings(): commonmodule_commonmodule_pb.SourceCapabilityRatings | undefined;
  setSourcecapabilityratings(value?: commonmodule_commonmodule_pb.SourceCapabilityRatings): GenerationCapabilityRatings;
  hasSourcecapabilityratings(): boolean;
  clearSourcecapabilityratings(): GenerationCapabilityRatings;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCapabilityRatings.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCapabilityRatings): GenerationCapabilityRatings.AsObject;
  static serializeBinaryToWriter(message: GenerationCapabilityRatings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCapabilityRatings;
  static deserializeBinaryFromReader(message: GenerationCapabilityRatings, reader: jspb.BinaryReader): GenerationCapabilityRatings;
}

export namespace GenerationCapabilityRatings {
  export type AsObject = {
    sourcecapabilityratings?: commonmodule_commonmodule_pb.SourceCapabilityRatings.AsObject,
  }
}

export class GenerationCapability extends jspb.Message {
  getNameplatevalue(): commonmodule_commonmodule_pb.NameplateValue | undefined;
  setNameplatevalue(value?: commonmodule_commonmodule_pb.NameplateValue): GenerationCapability;
  hasNameplatevalue(): boolean;
  clearNameplatevalue(): GenerationCapability;

  getGenerationcapabilityratings(): GenerationCapabilityRatings | undefined;
  setGenerationcapabilityratings(value?: GenerationCapabilityRatings): GenerationCapability;
  hasGenerationcapabilityratings(): boolean;
  clearGenerationcapabilityratings(): GenerationCapability;

  getGenerationcapabilityconfiguration(): GenerationCapabilityConfiguration | undefined;
  setGenerationcapabilityconfiguration(value?: GenerationCapabilityConfiguration): GenerationCapability;
  hasGenerationcapabilityconfiguration(): boolean;
  clearGenerationcapabilityconfiguration(): GenerationCapability;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCapability.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCapability): GenerationCapability.AsObject;
  static serializeBinaryToWriter(message: GenerationCapability, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCapability;
  static deserializeBinaryFromReader(message: GenerationCapability, reader: jspb.BinaryReader): GenerationCapability;
}

export namespace GenerationCapability {
  export type AsObject = {
    nameplatevalue?: commonmodule_commonmodule_pb.NameplateValue.AsObject,
    generationcapabilityratings?: GenerationCapabilityRatings.AsObject,
    generationcapabilityconfiguration?: GenerationCapabilityConfiguration.AsObject,
  }
}

export class GenerationCapabilityProfile extends jspb.Message {
  getCapabilitymessageinfo(): commonmodule_commonmodule_pb.CapabilityMessageInfo | undefined;
  setCapabilitymessageinfo(value?: commonmodule_commonmodule_pb.CapabilityMessageInfo): GenerationCapabilityProfile;
  hasCapabilitymessageinfo(): boolean;
  clearCapabilitymessageinfo(): GenerationCapabilityProfile;

  getGenerationcapability(): GenerationCapability | undefined;
  setGenerationcapability(value?: GenerationCapability): GenerationCapabilityProfile;
  hasGenerationcapability(): boolean;
  clearGenerationcapability(): GenerationCapabilityProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationCapabilityProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationCapabilityProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCapabilityProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCapabilityProfile): GenerationCapabilityProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationCapabilityProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCapabilityProfile;
  static deserializeBinaryFromReader(message: GenerationCapabilityProfile, reader: jspb.BinaryReader): GenerationCapabilityProfile;
}

export namespace GenerationCapabilityProfile {
  export type AsObject = {
    capabilitymessageinfo?: commonmodule_commonmodule_pb.CapabilityMessageInfo.AsObject,
    generationcapability?: GenerationCapability.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
  }
}

export class GenerationPoint extends jspb.Message {
  getBlackstartenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): GenerationPoint;

  getFrequencysetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setFrequencysetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasFrequencysetpointenabled(): boolean;
  clearFrequencysetpointenabled(): GenerationPoint;

  getPcthzdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPcthzdroop(value?: google_protobuf_wrappers_pb.FloatValue): GenerationPoint;
  hasPcthzdroop(): boolean;
  clearPcthzdroop(): GenerationPoint;

  getPctvdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPctvdroop(value?: google_protobuf_wrappers_pb.FloatValue): GenerationPoint;
  hasPctvdroop(): boolean;
  clearPctvdroop(): GenerationPoint;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): GenerationPoint;
  hasRamprates(): boolean;
  clearRamprates(): GenerationPoint;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): GenerationPoint;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): GenerationPoint;

  getReset(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReset(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasReset(): boolean;
  clearReset(): GenerationPoint;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): GenerationPoint;
  hasState(): boolean;
  clearState(): GenerationPoint;

  getSyncbacktogrid(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setSyncbacktogrid(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasSyncbacktogrid(): boolean;
  clearSyncbacktogrid(): GenerationPoint;

  getTranstoislndongridlossenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setTranstoislndongridlossenabled(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasTranstoislndongridlossenabled(): boolean;
  clearTranstoislndongridlossenabled(): GenerationPoint;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): GenerationPoint;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): GenerationPoint;

  getStarttime(): commonmodule_commonmodule_pb.ControlTimestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.ControlTimestamp): GenerationPoint;
  hasStarttime(): boolean;
  clearStarttime(): GenerationPoint;

  getEnterserviceoperation(): commonmodule_commonmodule_pb.EnterServiceAPC | undefined;
  setEnterserviceoperation(value?: commonmodule_commonmodule_pb.EnterServiceAPC): GenerationPoint;
  hasEnterserviceoperation(): boolean;
  clearEnterserviceoperation(): GenerationPoint;

  getHzwoperation(): commonmodule_commonmodule_pb.HzWAPC | undefined;
  setHzwoperation(value?: commonmodule_commonmodule_pb.HzWAPC): GenerationPoint;
  hasHzwoperation(): boolean;
  clearHzwoperation(): GenerationPoint;

  getLimitwoperation(): commonmodule_commonmodule_pb.LimitWAPC | undefined;
  setLimitwoperation(value?: commonmodule_commonmodule_pb.LimitWAPC): GenerationPoint;
  hasLimitwoperation(): boolean;
  clearLimitwoperation(): GenerationPoint;

  getPfoperation(): commonmodule_commonmodule_pb.PFSPC | undefined;
  setPfoperation(value?: commonmodule_commonmodule_pb.PFSPC): GenerationPoint;
  hasPfoperation(): boolean;
  clearPfoperation(): GenerationPoint;

  getTmhztripoperation(): commonmodule_commonmodule_pb.TmHzCSG | undefined;
  setTmhztripoperation(value?: commonmodule_commonmodule_pb.TmHzCSG): GenerationPoint;
  hasTmhztripoperation(): boolean;
  clearTmhztripoperation(): GenerationPoint;

  getTmvolttripoperation(): commonmodule_commonmodule_pb.TmVoltCSG | undefined;
  setTmvolttripoperation(value?: commonmodule_commonmodule_pb.TmVoltCSG): GenerationPoint;
  hasTmvolttripoperation(): boolean;
  clearTmvolttripoperation(): GenerationPoint;

  getVaroperation(): commonmodule_commonmodule_pb.VarSPC | undefined;
  setVaroperation(value?: commonmodule_commonmodule_pb.VarSPC): GenerationPoint;
  hasVaroperation(): boolean;
  clearVaroperation(): GenerationPoint;

  getVoltvaroperation(): commonmodule_commonmodule_pb.VoltVarCSG | undefined;
  setVoltvaroperation(value?: commonmodule_commonmodule_pb.VoltVarCSG): GenerationPoint;
  hasVoltvaroperation(): boolean;
  clearVoltvaroperation(): GenerationPoint;

  getVoltwoperation(): commonmodule_commonmodule_pb.VoltWCSG | undefined;
  setVoltwoperation(value?: commonmodule_commonmodule_pb.VoltWCSG): GenerationPoint;
  hasVoltwoperation(): boolean;
  clearVoltwoperation(): GenerationPoint;

  getWvaroperation(): commonmodule_commonmodule_pb.WVarCSG | undefined;
  setWvaroperation(value?: commonmodule_commonmodule_pb.WVarCSG): GenerationPoint;
  hasWvaroperation(): boolean;
  clearWvaroperation(): GenerationPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationPoint.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationPoint): GenerationPoint.AsObject;
  static serializeBinaryToWriter(message: GenerationPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationPoint;
  static deserializeBinaryFromReader(message: GenerationPoint, reader: jspb.BinaryReader): GenerationPoint;
}

export namespace GenerationPoint {
  export type AsObject = {
    blackstartenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    frequencysetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    pcthzdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    pctvdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    reset?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    syncbacktogrid?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    transtoislndongridlossenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    starttime?: commonmodule_commonmodule_pb.ControlTimestamp.AsObject,
    enterserviceoperation?: commonmodule_commonmodule_pb.EnterServiceAPC.AsObject,
    hzwoperation?: commonmodule_commonmodule_pb.HzWAPC.AsObject,
    limitwoperation?: commonmodule_commonmodule_pb.LimitWAPC.AsObject,
    pfoperation?: commonmodule_commonmodule_pb.PFSPC.AsObject,
    tmhztripoperation?: commonmodule_commonmodule_pb.TmHzCSG.AsObject,
    tmvolttripoperation?: commonmodule_commonmodule_pb.TmVoltCSG.AsObject,
    varoperation?: commonmodule_commonmodule_pb.VarSPC.AsObject,
    voltvaroperation?: commonmodule_commonmodule_pb.VoltVarCSG.AsObject,
    voltwoperation?: commonmodule_commonmodule_pb.VoltWCSG.AsObject,
    wvaroperation?: commonmodule_commonmodule_pb.WVarCSG.AsObject,
  }
}

export class GenerationCSG extends jspb.Message {
  getCrvptsList(): Array<GenerationPoint>;
  setCrvptsList(value: Array<GenerationPoint>): GenerationCSG;
  clearCrvptsList(): GenerationCSG;
  addCrvpts(value?: GenerationPoint, index?: number): GenerationPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationCSG.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationCSG): GenerationCSG.AsObject;
  static serializeBinaryToWriter(message: GenerationCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationCSG;
  static deserializeBinaryFromReader(message: GenerationCSG, reader: jspb.BinaryReader): GenerationCSG;
}

export namespace GenerationCSG {
  export type AsObject = {
    crvptsList: Array<GenerationPoint.AsObject>,
  }
}

export class GenerationControlScheduleFSCH extends jspb.Message {
  getValdcsg(): GenerationCSG | undefined;
  setValdcsg(value?: GenerationCSG): GenerationControlScheduleFSCH;
  hasValdcsg(): boolean;
  clearValdcsg(): GenerationControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationControlScheduleFSCH): GenerationControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: GenerationControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationControlScheduleFSCH;
  static deserializeBinaryFromReader(message: GenerationControlScheduleFSCH, reader: jspb.BinaryReader): GenerationControlScheduleFSCH;
}

export namespace GenerationControlScheduleFSCH {
  export type AsObject = {
    valdcsg?: GenerationCSG.AsObject,
  }
}

export class GenerationControlFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): GenerationControlFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): GenerationControlFSCC;

  getGenerationcontrolschedulefsch(): GenerationControlScheduleFSCH | undefined;
  setGenerationcontrolschedulefsch(value?: GenerationControlScheduleFSCH): GenerationControlFSCC;
  hasGenerationcontrolschedulefsch(): boolean;
  clearGenerationcontrolschedulefsch(): GenerationControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationControlFSCC): GenerationControlFSCC.AsObject;
  static serializeBinaryToWriter(message: GenerationControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationControlFSCC;
  static deserializeBinaryFromReader(message: GenerationControlFSCC, reader: jspb.BinaryReader): GenerationControlFSCC;
}

export namespace GenerationControlFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    generationcontrolschedulefsch?: GenerationControlScheduleFSCH.AsObject,
  }
}

export class GenerationControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): GenerationControl;
  hasControlvalue(): boolean;
  clearControlvalue(): GenerationControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): GenerationControl;
  hasCheck(): boolean;
  clearCheck(): GenerationControl;

  getGenerationcontrolfscc(): GenerationControlFSCC | undefined;
  setGenerationcontrolfscc(value?: GenerationControlFSCC): GenerationControl;
  hasGenerationcontrolfscc(): boolean;
  clearGenerationcontrolfscc(): GenerationControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationControl.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationControl): GenerationControl.AsObject;
  static serializeBinaryToWriter(message: GenerationControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationControl;
  static deserializeBinaryFromReader(message: GenerationControl, reader: jspb.BinaryReader): GenerationControl;
}

export namespace GenerationControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    generationcontrolfscc?: GenerationControlFSCC.AsObject,
  }
}

export class GenerationControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): GenerationControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): GenerationControlProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationControlProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationControlProfile;

  getGenerationcontrol(): GenerationControl | undefined;
  setGenerationcontrol(value?: GenerationControl): GenerationControlProfile;
  hasGenerationcontrol(): boolean;
  clearGenerationcontrol(): GenerationControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationControlProfile): GenerationControlProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationControlProfile;
  static deserializeBinaryFromReader(message: GenerationControlProfile, reader: jspb.BinaryReader): GenerationControlProfile;
}

export namespace GenerationControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
    generationcontrol?: GenerationControl.AsObject,
  }
}

export class DroopParameter extends jspb.Message {
  getSlope(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSlope(value?: google_protobuf_wrappers_pb.FloatValue): DroopParameter;
  hasSlope(): boolean;
  clearSlope(): DroopParameter;

  getUnloadedoffset(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setUnloadedoffset(value?: google_protobuf_wrappers_pb.FloatValue): DroopParameter;
  hasUnloadedoffset(): boolean;
  clearUnloadedoffset(): DroopParameter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DroopParameter.AsObject;
  static toObject(includeInstance: boolean, msg: DroopParameter): DroopParameter.AsObject;
  static serializeBinaryToWriter(message: DroopParameter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DroopParameter;
  static deserializeBinaryFromReader(message: DroopParameter, reader: jspb.BinaryReader): DroopParameter;
}

export namespace DroopParameter {
  export type AsObject = {
    slope?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    unloadedoffset?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class RealPowerControl extends jspb.Message {
  getDroopsetpoint(): DroopParameter | undefined;
  setDroopsetpoint(value?: DroopParameter): RealPowerControl;
  hasDroopsetpoint(): boolean;
  clearDroopsetpoint(): RealPowerControl;

  getIsochronoussetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setIsochronoussetpoint(value?: google_protobuf_wrappers_pb.FloatValue): RealPowerControl;
  hasIsochronoussetpoint(): boolean;
  clearIsochronoussetpoint(): RealPowerControl;

  getRealpowercontrolmode(): commonmodule_commonmodule_pb.Optional_RealPowerControlKind | undefined;
  setRealpowercontrolmode(value?: commonmodule_commonmodule_pb.Optional_RealPowerControlKind): RealPowerControl;
  hasRealpowercontrolmode(): boolean;
  clearRealpowercontrolmode(): RealPowerControl;

  getRealpowersetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setRealpowersetpoint(value?: google_protobuf_wrappers_pb.FloatValue): RealPowerControl;
  hasRealpowersetpoint(): boolean;
  clearRealpowersetpoint(): RealPowerControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RealPowerControl.AsObject;
  static toObject(includeInstance: boolean, msg: RealPowerControl): RealPowerControl.AsObject;
  static serializeBinaryToWriter(message: RealPowerControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RealPowerControl;
  static deserializeBinaryFromReader(message: RealPowerControl, reader: jspb.BinaryReader): RealPowerControl;
}

export namespace RealPowerControl {
  export type AsObject = {
    droopsetpoint?: DroopParameter.AsObject,
    isochronoussetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    realpowercontrolmode?: commonmodule_commonmodule_pb.Optional_RealPowerControlKind.AsObject,
    realpowersetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class ReactivePowerControl extends jspb.Message {
  getDroopsetpoint(): DroopParameter | undefined;
  setDroopsetpoint(value?: DroopParameter): ReactivePowerControl;
  hasDroopsetpoint(): boolean;
  clearDroopsetpoint(): ReactivePowerControl;

  getPowerfactorsetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPowerfactorsetpoint(value?: google_protobuf_wrappers_pb.FloatValue): ReactivePowerControl;
  hasPowerfactorsetpoint(): boolean;
  clearPowerfactorsetpoint(): ReactivePowerControl;

  getReactivepowercontrolmode(): commonmodule_commonmodule_pb.Optional_ReactivePowerControlKind | undefined;
  setReactivepowercontrolmode(value?: commonmodule_commonmodule_pb.Optional_ReactivePowerControlKind): ReactivePowerControl;
  hasReactivepowercontrolmode(): boolean;
  clearReactivepowercontrolmode(): ReactivePowerControl;

  getReactivepowersetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setReactivepowersetpoint(value?: google_protobuf_wrappers_pb.FloatValue): ReactivePowerControl;
  hasReactivepowersetpoint(): boolean;
  clearReactivepowersetpoint(): ReactivePowerControl;

  getVoltagesetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setVoltagesetpoint(value?: google_protobuf_wrappers_pb.FloatValue): ReactivePowerControl;
  hasVoltagesetpoint(): boolean;
  clearVoltagesetpoint(): ReactivePowerControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReactivePowerControl.AsObject;
  static toObject(includeInstance: boolean, msg: ReactivePowerControl): ReactivePowerControl.AsObject;
  static serializeBinaryToWriter(message: ReactivePowerControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReactivePowerControl;
  static deserializeBinaryFromReader(message: ReactivePowerControl, reader: jspb.BinaryReader): ReactivePowerControl;
}

export namespace ReactivePowerControl {
  export type AsObject = {
    droopsetpoint?: DroopParameter.AsObject,
    powerfactorsetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    reactivepowercontrolmode?: commonmodule_commonmodule_pb.Optional_ReactivePowerControlKind.AsObject,
    reactivepowersetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    voltagesetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class GenerationDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): GenerationDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): GenerationDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): GenerationDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): GenerationDiscreteControl;

  getReactivepowercontrol(): ReactivePowerControl | undefined;
  setReactivepowercontrol(value?: ReactivePowerControl): GenerationDiscreteControl;
  hasReactivepowercontrol(): boolean;
  clearReactivepowercontrol(): GenerationDiscreteControl;

  getRealpowercontrol(): RealPowerControl | undefined;
  setRealpowercontrol(value?: RealPowerControl): GenerationDiscreteControl;
  hasRealpowercontrol(): boolean;
  clearRealpowercontrol(): GenerationDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationDiscreteControl): GenerationDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: GenerationDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationDiscreteControl;
  static deserializeBinaryFromReader(message: GenerationDiscreteControl, reader: jspb.BinaryReader): GenerationDiscreteControl;
}

export namespace GenerationDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    reactivepowercontrol?: ReactivePowerControl.AsObject,
    realpowercontrol?: RealPowerControl.AsObject,
  }
}

export class GenerationDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): GenerationDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): GenerationDiscreteControlProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationDiscreteControlProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationDiscreteControlProfile;

  getGenerationdiscretecontrol(): GenerationDiscreteControl | undefined;
  setGenerationdiscretecontrol(value?: GenerationDiscreteControl): GenerationDiscreteControlProfile;
  hasGenerationdiscretecontrol(): boolean;
  clearGenerationdiscretecontrol(): GenerationDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationDiscreteControlProfile): GenerationDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationDiscreteControlProfile;
  static deserializeBinaryFromReader(message: GenerationDiscreteControlProfile, reader: jspb.BinaryReader): GenerationDiscreteControlProfile;
}

export namespace GenerationDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
    generationdiscretecontrol?: GenerationDiscreteControl.AsObject,
  }
}

export class GenerationReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): GenerationReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): GenerationReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): GenerationReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): GenerationReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): GenerationReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): GenerationReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): GenerationReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): GenerationReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationReading.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationReading): GenerationReading.AsObject;
  static serializeBinaryToWriter(message: GenerationReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationReading;
  static deserializeBinaryFromReader(message: GenerationReading, reader: jspb.BinaryReader): GenerationReading;
}

export namespace GenerationReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class GenerationReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): GenerationReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): GenerationReadingProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationReadingProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationReadingProfile;

  getGenerationreading(): GenerationReading | undefined;
  setGenerationreading(value?: GenerationReading): GenerationReadingProfile;
  hasGenerationreading(): boolean;
  clearGenerationreading(): GenerationReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationReadingProfile): GenerationReadingProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationReadingProfile;
  static deserializeBinaryFromReader(message: GenerationReadingProfile, reader: jspb.BinaryReader): GenerationReadingProfile;
}

export namespace GenerationReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
    generationreading?: GenerationReading.AsObject,
  }
}

export class GenerationPointStatus extends jspb.Message {
  getBlackstartenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): GenerationPointStatus;

  getFrequencysetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setFrequencysetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasFrequencysetpointenabled(): boolean;
  clearFrequencysetpointenabled(): GenerationPointStatus;

  getPcthzdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPcthzdroop(value?: google_protobuf_wrappers_pb.FloatValue): GenerationPointStatus;
  hasPcthzdroop(): boolean;
  clearPcthzdroop(): GenerationPointStatus;

  getPctvdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPctvdroop(value?: google_protobuf_wrappers_pb.FloatValue): GenerationPointStatus;
  hasPctvdroop(): boolean;
  clearPctvdroop(): GenerationPointStatus;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): GenerationPointStatus;
  hasRamprates(): boolean;
  clearRamprates(): GenerationPointStatus;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): GenerationPointStatus;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): GenerationPointStatus;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): GenerationPointStatus;
  hasState(): boolean;
  clearState(): GenerationPointStatus;

  getSyncbacktogrid(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setSyncbacktogrid(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasSyncbacktogrid(): boolean;
  clearSyncbacktogrid(): GenerationPointStatus;

  getTranstoislndongridlossenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setTranstoislndongridlossenabled(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasTranstoislndongridlossenabled(): boolean;
  clearTranstoislndongridlossenabled(): GenerationPointStatus;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationPointStatus;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): GenerationPointStatus;

  getEnterserviceoperation(): commonmodule_commonmodule_pb.EnterServiceAPC | undefined;
  setEnterserviceoperation(value?: commonmodule_commonmodule_pb.EnterServiceAPC): GenerationPointStatus;
  hasEnterserviceoperation(): boolean;
  clearEnterserviceoperation(): GenerationPointStatus;

  getHzwoperation(): commonmodule_commonmodule_pb.HzWAPC | undefined;
  setHzwoperation(value?: commonmodule_commonmodule_pb.HzWAPC): GenerationPointStatus;
  hasHzwoperation(): boolean;
  clearHzwoperation(): GenerationPointStatus;

  getLimitwoperation(): commonmodule_commonmodule_pb.LimitWAPC | undefined;
  setLimitwoperation(value?: commonmodule_commonmodule_pb.LimitWAPC): GenerationPointStatus;
  hasLimitwoperation(): boolean;
  clearLimitwoperation(): GenerationPointStatus;

  getPfoperation(): commonmodule_commonmodule_pb.PFSPC | undefined;
  setPfoperation(value?: commonmodule_commonmodule_pb.PFSPC): GenerationPointStatus;
  hasPfoperation(): boolean;
  clearPfoperation(): GenerationPointStatus;

  getTmhztripoperation(): commonmodule_commonmodule_pb.TmHzCSG | undefined;
  setTmhztripoperation(value?: commonmodule_commonmodule_pb.TmHzCSG): GenerationPointStatus;
  hasTmhztripoperation(): boolean;
  clearTmhztripoperation(): GenerationPointStatus;

  getTmvolttripoperation(): commonmodule_commonmodule_pb.TmVoltCSG | undefined;
  setTmvolttripoperation(value?: commonmodule_commonmodule_pb.TmVoltCSG): GenerationPointStatus;
  hasTmvolttripoperation(): boolean;
  clearTmvolttripoperation(): GenerationPointStatus;

  getVaroperation(): commonmodule_commonmodule_pb.VarSPC | undefined;
  setVaroperation(value?: commonmodule_commonmodule_pb.VarSPC): GenerationPointStatus;
  hasVaroperation(): boolean;
  clearVaroperation(): GenerationPointStatus;

  getVoltvaroperation(): commonmodule_commonmodule_pb.VoltVarCSG | undefined;
  setVoltvaroperation(value?: commonmodule_commonmodule_pb.VoltVarCSG): GenerationPointStatus;
  hasVoltvaroperation(): boolean;
  clearVoltvaroperation(): GenerationPointStatus;

  getVoltwoperation(): commonmodule_commonmodule_pb.VoltWCSG | undefined;
  setVoltwoperation(value?: commonmodule_commonmodule_pb.VoltWCSG): GenerationPointStatus;
  hasVoltwoperation(): boolean;
  clearVoltwoperation(): GenerationPointStatus;

  getWvaroperation(): commonmodule_commonmodule_pb.WVarCSG | undefined;
  setWvaroperation(value?: commonmodule_commonmodule_pb.WVarCSG): GenerationPointStatus;
  hasWvaroperation(): boolean;
  clearWvaroperation(): GenerationPointStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationPointStatus.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationPointStatus): GenerationPointStatus.AsObject;
  static serializeBinaryToWriter(message: GenerationPointStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationPointStatus;
  static deserializeBinaryFromReader(message: GenerationPointStatus, reader: jspb.BinaryReader): GenerationPointStatus;
}

export namespace GenerationPointStatus {
  export type AsObject = {
    blackstartenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    frequencysetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    pcthzdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    pctvdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    syncbacktogrid?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    transtoislndongridlossenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    enterserviceoperation?: commonmodule_commonmodule_pb.EnterServiceAPC.AsObject,
    hzwoperation?: commonmodule_commonmodule_pb.HzWAPC.AsObject,
    limitwoperation?: commonmodule_commonmodule_pb.LimitWAPC.AsObject,
    pfoperation?: commonmodule_commonmodule_pb.PFSPC.AsObject,
    tmhztripoperation?: commonmodule_commonmodule_pb.TmHzCSG.AsObject,
    tmvolttripoperation?: commonmodule_commonmodule_pb.TmVoltCSG.AsObject,
    varoperation?: commonmodule_commonmodule_pb.VarSPC.AsObject,
    voltvaroperation?: commonmodule_commonmodule_pb.VoltVarCSG.AsObject,
    voltwoperation?: commonmodule_commonmodule_pb.VoltWCSG.AsObject,
    wvaroperation?: commonmodule_commonmodule_pb.WVarCSG.AsObject,
  }
}

export class GenerationEventAndStatusZGEN extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): GenerationEventAndStatusZGEN;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): GenerationEventAndStatusZGEN;

  getAuxpwrst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setAuxpwrst(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationEventAndStatusZGEN;
  hasAuxpwrst(): boolean;
  clearAuxpwrst(): GenerationEventAndStatusZGEN;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): GenerationEventAndStatusZGEN;
  hasDynamictest(): boolean;
  clearDynamictest(): GenerationEventAndStatusZGEN;

  getEmgstop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setEmgstop(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationEventAndStatusZGEN;
  hasEmgstop(): boolean;
  clearEmgstop(): GenerationEventAndStatusZGEN;

  getGnsynst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setGnsynst(value?: commonmodule_commonmodule_pb.StatusSPS): GenerationEventAndStatusZGEN;
  hasGnsynst(): boolean;
  clearGnsynst(): GenerationEventAndStatusZGEN;

  getPointstatus(): GenerationPointStatus | undefined;
  setPointstatus(value?: GenerationPointStatus): GenerationEventAndStatusZGEN;
  hasPointstatus(): boolean;
  clearPointstatus(): GenerationEventAndStatusZGEN;

  getAlrm(): commonmodule_commonmodule_pb.Optional_AlrmKind | undefined;
  setAlrm(value?: commonmodule_commonmodule_pb.Optional_AlrmKind): GenerationEventAndStatusZGEN;
  hasAlrm(): boolean;
  clearAlrm(): GenerationEventAndStatusZGEN;

  getGridconnectionstate(): commonmodule_commonmodule_pb.Optional_GridConnectionStateKind | undefined;
  setGridconnectionstate(value?: commonmodule_commonmodule_pb.Optional_GridConnectionStateKind): GenerationEventAndStatusZGEN;
  hasGridconnectionstate(): boolean;
  clearGridconnectionstate(): GenerationEventAndStatusZGEN;

  getManalrminfo(): google_protobuf_wrappers_pb.StringValue | undefined;
  setManalrminfo(value?: google_protobuf_wrappers_pb.StringValue): GenerationEventAndStatusZGEN;
  hasManalrminfo(): boolean;
  clearManalrminfo(): GenerationEventAndStatusZGEN;

  getOperatingstate(): commonmodule_commonmodule_pb.Optional_OperatingStateKind | undefined;
  setOperatingstate(value?: commonmodule_commonmodule_pb.Optional_OperatingStateKind): GenerationEventAndStatusZGEN;
  hasOperatingstate(): boolean;
  clearOperatingstate(): GenerationEventAndStatusZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationEventAndStatusZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationEventAndStatusZGEN): GenerationEventAndStatusZGEN.AsObject;
  static serializeBinaryToWriter(message: GenerationEventAndStatusZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationEventAndStatusZGEN;
  static deserializeBinaryFromReader(message: GenerationEventAndStatusZGEN, reader: jspb.BinaryReader): GenerationEventAndStatusZGEN;
}

export namespace GenerationEventAndStatusZGEN {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    auxpwrst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    emgstop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    gnsynst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    pointstatus?: GenerationPointStatus.AsObject,
    alrm?: commonmodule_commonmodule_pb.Optional_AlrmKind.AsObject,
    gridconnectionstate?: commonmodule_commonmodule_pb.Optional_GridConnectionStateKind.AsObject,
    manalrminfo?: google_protobuf_wrappers_pb.StringValue.AsObject,
    operatingstate?: commonmodule_commonmodule_pb.Optional_OperatingStateKind.AsObject,
  }
}

export class GenerationEventZGEN extends jspb.Message {
  getGenerationeventandstatuszgen(): GenerationEventAndStatusZGEN | undefined;
  setGenerationeventandstatuszgen(value?: GenerationEventAndStatusZGEN): GenerationEventZGEN;
  hasGenerationeventandstatuszgen(): boolean;
  clearGenerationeventandstatuszgen(): GenerationEventZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationEventZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationEventZGEN): GenerationEventZGEN.AsObject;
  static serializeBinaryToWriter(message: GenerationEventZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationEventZGEN;
  static deserializeBinaryFromReader(message: GenerationEventZGEN, reader: jspb.BinaryReader): GenerationEventZGEN;
}

export namespace GenerationEventZGEN {
  export type AsObject = {
    generationeventandstatuszgen?: GenerationEventAndStatusZGEN.AsObject,
  }
}

export class GenerationEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): GenerationEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): GenerationEvent;

  getGenerationeventzgen(): GenerationEventZGEN | undefined;
  setGenerationeventzgen(value?: GenerationEventZGEN): GenerationEvent;
  hasGenerationeventzgen(): boolean;
  clearGenerationeventzgen(): GenerationEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationEvent.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationEvent): GenerationEvent.AsObject;
  static serializeBinaryToWriter(message: GenerationEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationEvent;
  static deserializeBinaryFromReader(message: GenerationEvent, reader: jspb.BinaryReader): GenerationEvent;
}

export namespace GenerationEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    generationeventzgen?: GenerationEventZGEN.AsObject,
  }
}

export class GenerationEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): GenerationEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): GenerationEventProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationEventProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationEventProfile;

  getGenerationevent(): GenerationEvent | undefined;
  setGenerationevent(value?: GenerationEvent): GenerationEventProfile;
  hasGenerationevent(): boolean;
  clearGenerationevent(): GenerationEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationEventProfile): GenerationEventProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationEventProfile;
  static deserializeBinaryFromReader(message: GenerationEventProfile, reader: jspb.BinaryReader): GenerationEventProfile;
}

export namespace GenerationEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
    generationevent?: GenerationEvent.AsObject,
  }
}

export class GenerationStatusZGEN extends jspb.Message {
  getGenerationeventandstatuszgen(): GenerationEventAndStatusZGEN | undefined;
  setGenerationeventandstatuszgen(value?: GenerationEventAndStatusZGEN): GenerationStatusZGEN;
  hasGenerationeventandstatuszgen(): boolean;
  clearGenerationeventandstatuszgen(): GenerationStatusZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationStatusZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationStatusZGEN): GenerationStatusZGEN.AsObject;
  static serializeBinaryToWriter(message: GenerationStatusZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationStatusZGEN;
  static deserializeBinaryFromReader(message: GenerationStatusZGEN, reader: jspb.BinaryReader): GenerationStatusZGEN;
}

export namespace GenerationStatusZGEN {
  export type AsObject = {
    generationeventandstatuszgen?: GenerationEventAndStatusZGEN.AsObject,
  }
}

export class GenerationStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): GenerationStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): GenerationStatus;

  getGenerationstatuszgen(): GenerationStatusZGEN | undefined;
  setGenerationstatuszgen(value?: GenerationStatusZGEN): GenerationStatus;
  hasGenerationstatuszgen(): boolean;
  clearGenerationstatuszgen(): GenerationStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationStatus.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationStatus): GenerationStatus.AsObject;
  static serializeBinaryToWriter(message: GenerationStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationStatus;
  static deserializeBinaryFromReader(message: GenerationStatus, reader: jspb.BinaryReader): GenerationStatus;
}

export namespace GenerationStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    generationstatuszgen?: GenerationStatusZGEN.AsObject,
  }
}

export class GenerationStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): GenerationStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): GenerationStatusProfile;

  getGeneratingunit(): GeneratingUnit | undefined;
  setGeneratingunit(value?: GeneratingUnit): GenerationStatusProfile;
  hasGeneratingunit(): boolean;
  clearGeneratingunit(): GenerationStatusProfile;

  getGenerationstatus(): GenerationStatus | undefined;
  setGenerationstatus(value?: GenerationStatus): GenerationStatusProfile;
  hasGenerationstatus(): boolean;
  clearGenerationstatus(): GenerationStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GenerationStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: GenerationStatusProfile): GenerationStatusProfile.AsObject;
  static serializeBinaryToWriter(message: GenerationStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GenerationStatusProfile;
  static deserializeBinaryFromReader(message: GenerationStatusProfile, reader: jspb.BinaryReader): GenerationStatusProfile;
}

export namespace GenerationStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    generatingunit?: GeneratingUnit.AsObject,
    generationstatus?: GenerationStatus.AsObject,
  }
}

