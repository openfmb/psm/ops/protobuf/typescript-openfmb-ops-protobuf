import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class InterconnectionPoint extends jspb.Message {
  getBlackstartenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.ControlSPC): InterconnectionPoint;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): InterconnectionPoint;

  getFrequencysetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setFrequencysetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): InterconnectionPoint;
  hasFrequencysetpointenabled(): boolean;
  clearFrequencysetpointenabled(): InterconnectionPoint;

  getIsland(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setIsland(value?: commonmodule_commonmodule_pb.ControlSPC): InterconnectionPoint;
  hasIsland(): boolean;
  clearIsland(): InterconnectionPoint;

  getPcthzdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPcthzdroop(value?: google_protobuf_wrappers_pb.FloatValue): InterconnectionPoint;
  hasPcthzdroop(): boolean;
  clearPcthzdroop(): InterconnectionPoint;

  getPctvdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPctvdroop(value?: google_protobuf_wrappers_pb.FloatValue): InterconnectionPoint;
  hasPctvdroop(): boolean;
  clearPctvdroop(): InterconnectionPoint;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): InterconnectionPoint;
  hasRamprates(): boolean;
  clearRamprates(): InterconnectionPoint;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): InterconnectionPoint;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): InterconnectionPoint;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): InterconnectionPoint;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): InterconnectionPoint;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): InterconnectionPoint;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): InterconnectionPoint;

  getStarttime(): commonmodule_commonmodule_pb.Timestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.Timestamp): InterconnectionPoint;
  hasStarttime(): boolean;
  clearStarttime(): InterconnectionPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionPoint.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionPoint): InterconnectionPoint.AsObject;
  static serializeBinaryToWriter(message: InterconnectionPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionPoint;
  static deserializeBinaryFromReader(message: InterconnectionPoint, reader: jspb.BinaryReader): InterconnectionPoint;
}

export namespace InterconnectionPoint {
  export type AsObject = {
    blackstartenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    frequencysetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    island?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    pcthzdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    pctvdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    starttime?: commonmodule_commonmodule_pb.Timestamp.AsObject,
  }
}

export class InterconnectionCSG extends jspb.Message {
  getCrvptsList(): Array<InterconnectionPoint>;
  setCrvptsList(value: Array<InterconnectionPoint>): InterconnectionCSG;
  clearCrvptsList(): InterconnectionCSG;
  addCrvpts(value?: InterconnectionPoint, index?: number): InterconnectionPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionCSG.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionCSG): InterconnectionCSG.AsObject;
  static serializeBinaryToWriter(message: InterconnectionCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionCSG;
  static deserializeBinaryFromReader(message: InterconnectionCSG, reader: jspb.BinaryReader): InterconnectionCSG;
}

export namespace InterconnectionCSG {
  export type AsObject = {
    crvptsList: Array<InterconnectionPoint.AsObject>,
  }
}

export class InterconnectionControlScheduleFSCH extends jspb.Message {
  getValdcsg(): InterconnectionCSG | undefined;
  setValdcsg(value?: InterconnectionCSG): InterconnectionControlScheduleFSCH;
  hasValdcsg(): boolean;
  clearValdcsg(): InterconnectionControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionControlScheduleFSCH): InterconnectionControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: InterconnectionControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionControlScheduleFSCH;
  static deserializeBinaryFromReader(message: InterconnectionControlScheduleFSCH, reader: jspb.BinaryReader): InterconnectionControlScheduleFSCH;
}

export namespace InterconnectionControlScheduleFSCH {
  export type AsObject = {
    valdcsg?: InterconnectionCSG.AsObject,
  }
}

export class InterconnectionScheduleFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): InterconnectionScheduleFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): InterconnectionScheduleFSCC;

  getInterconnectioncontrolschedulefschList(): Array<InterconnectionControlScheduleFSCH>;
  setInterconnectioncontrolschedulefschList(value: Array<InterconnectionControlScheduleFSCH>): InterconnectionScheduleFSCC;
  clearInterconnectioncontrolschedulefschList(): InterconnectionScheduleFSCC;
  addInterconnectioncontrolschedulefsch(value?: InterconnectionControlScheduleFSCH, index?: number): InterconnectionControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionScheduleFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionScheduleFSCC): InterconnectionScheduleFSCC.AsObject;
  static serializeBinaryToWriter(message: InterconnectionScheduleFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionScheduleFSCC;
  static deserializeBinaryFromReader(message: InterconnectionScheduleFSCC, reader: jspb.BinaryReader): InterconnectionScheduleFSCC;
}

export namespace InterconnectionScheduleFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    interconnectioncontrolschedulefschList: Array<InterconnectionControlScheduleFSCH.AsObject>,
  }
}

export class InterconnectionSchedule extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): InterconnectionSchedule;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): InterconnectionSchedule;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): InterconnectionSchedule;
  hasCheck(): boolean;
  clearCheck(): InterconnectionSchedule;

  getInterconnectionschedulefscc(): InterconnectionScheduleFSCC | undefined;
  setInterconnectionschedulefscc(value?: InterconnectionScheduleFSCC): InterconnectionSchedule;
  hasInterconnectionschedulefscc(): boolean;
  clearInterconnectionschedulefscc(): InterconnectionSchedule;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionSchedule.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionSchedule): InterconnectionSchedule.AsObject;
  static serializeBinaryToWriter(message: InterconnectionSchedule, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionSchedule;
  static deserializeBinaryFromReader(message: InterconnectionSchedule, reader: jspb.BinaryReader): InterconnectionSchedule;
}

export namespace InterconnectionSchedule {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    interconnectionschedulefscc?: InterconnectionScheduleFSCC.AsObject,
  }
}

export class InterconnectionPlannedScheduleProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): InterconnectionPlannedScheduleProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): InterconnectionPlannedScheduleProfile;

  getRequestercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRequestercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): InterconnectionPlannedScheduleProfile;
  hasRequestercircuitsegmentservice(): boolean;
  clearRequestercircuitsegmentservice(): InterconnectionPlannedScheduleProfile;

  getInterconnectionschedule(): InterconnectionSchedule | undefined;
  setInterconnectionschedule(value?: InterconnectionSchedule): InterconnectionPlannedScheduleProfile;
  hasInterconnectionschedule(): boolean;
  clearInterconnectionschedule(): InterconnectionPlannedScheduleProfile;

  getTiepoint(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setTiepoint(value?: commonmodule_commonmodule_pb.ConductingEquipment): InterconnectionPlannedScheduleProfile;
  hasTiepoint(): boolean;
  clearTiepoint(): InterconnectionPlannedScheduleProfile;

  getRespondercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRespondercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): InterconnectionPlannedScheduleProfile;
  hasRespondercircuitsegmentservice(): boolean;
  clearRespondercircuitsegmentservice(): InterconnectionPlannedScheduleProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionPlannedScheduleProfile.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionPlannedScheduleProfile): InterconnectionPlannedScheduleProfile.AsObject;
  static serializeBinaryToWriter(message: InterconnectionPlannedScheduleProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionPlannedScheduleProfile;
  static deserializeBinaryFromReader(message: InterconnectionPlannedScheduleProfile, reader: jspb.BinaryReader): InterconnectionPlannedScheduleProfile;
}

export namespace InterconnectionPlannedScheduleProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    requestercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    interconnectionschedule?: InterconnectionSchedule.AsObject,
    tiepoint?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    respondercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
  }
}

export class InterconnectionRequestedScheduleProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): InterconnectionRequestedScheduleProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): InterconnectionRequestedScheduleProfile;

  getRequestercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRequestercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): InterconnectionRequestedScheduleProfile;
  hasRequestercircuitsegmentservice(): boolean;
  clearRequestercircuitsegmentservice(): InterconnectionRequestedScheduleProfile;

  getInterconnectionschedule(): InterconnectionSchedule | undefined;
  setInterconnectionschedule(value?: InterconnectionSchedule): InterconnectionRequestedScheduleProfile;
  hasInterconnectionschedule(): boolean;
  clearInterconnectionschedule(): InterconnectionRequestedScheduleProfile;

  getTiepoint(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setTiepoint(value?: commonmodule_commonmodule_pb.ConductingEquipment): InterconnectionRequestedScheduleProfile;
  hasTiepoint(): boolean;
  clearTiepoint(): InterconnectionRequestedScheduleProfile;

  getRespondercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRespondercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): InterconnectionRequestedScheduleProfile;
  hasRespondercircuitsegmentservice(): boolean;
  clearRespondercircuitsegmentservice(): InterconnectionRequestedScheduleProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InterconnectionRequestedScheduleProfile.AsObject;
  static toObject(includeInstance: boolean, msg: InterconnectionRequestedScheduleProfile): InterconnectionRequestedScheduleProfile.AsObject;
  static serializeBinaryToWriter(message: InterconnectionRequestedScheduleProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InterconnectionRequestedScheduleProfile;
  static deserializeBinaryFromReader(message: InterconnectionRequestedScheduleProfile, reader: jspb.BinaryReader): InterconnectionRequestedScheduleProfile;
}

export namespace InterconnectionRequestedScheduleProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    requestercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    interconnectionschedule?: InterconnectionSchedule.AsObject,
    tiepoint?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    respondercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
  }
}

