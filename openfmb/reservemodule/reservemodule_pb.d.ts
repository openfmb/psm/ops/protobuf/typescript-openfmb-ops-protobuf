import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class ReserveMargin extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): ReserveMargin;
  hasLogicalnode(): boolean;
  clearLogicalnode(): ReserveMargin;

  getA(): commonmodule_commonmodule_pb.PMG | undefined;
  setA(value?: commonmodule_commonmodule_pb.PMG): ReserveMargin;
  hasA(): boolean;
  clearA(): ReserveMargin;

  getVa(): commonmodule_commonmodule_pb.PMG | undefined;
  setVa(value?: commonmodule_commonmodule_pb.PMG): ReserveMargin;
  hasVa(): boolean;
  clearVa(): ReserveMargin;

  getVar(): commonmodule_commonmodule_pb.PMG | undefined;
  setVar(value?: commonmodule_commonmodule_pb.PMG): ReserveMargin;
  hasVar(): boolean;
  clearVar(): ReserveMargin;

  getW(): commonmodule_commonmodule_pb.PMG | undefined;
  setW(value?: commonmodule_commonmodule_pb.PMG): ReserveMargin;
  hasW(): boolean;
  clearW(): ReserveMargin;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReserveMargin.AsObject;
  static toObject(includeInstance: boolean, msg: ReserveMargin): ReserveMargin.AsObject;
  static serializeBinaryToWriter(message: ReserveMargin, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReserveMargin;
  static deserializeBinaryFromReader(message: ReserveMargin, reader: jspb.BinaryReader): ReserveMargin;
}

export namespace ReserveMargin {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    a?: commonmodule_commonmodule_pb.PMG.AsObject,
    va?: commonmodule_commonmodule_pb.PMG.AsObject,
    pb_var?: commonmodule_commonmodule_pb.PMG.AsObject,
    w?: commonmodule_commonmodule_pb.PMG.AsObject,
  }
}

export class ReserveAvailability extends jspb.Message {
  getIncrementalmargin(): ReserveMargin | undefined;
  setIncrementalmargin(value?: ReserveMargin): ReserveAvailability;
  hasIncrementalmargin(): boolean;
  clearIncrementalmargin(): ReserveAvailability;

  getMargin(): ReserveMargin | undefined;
  setMargin(value?: ReserveMargin): ReserveAvailability;
  hasMargin(): boolean;
  clearMargin(): ReserveAvailability;

  getStandbymargin(): ReserveMargin | undefined;
  setStandbymargin(value?: ReserveMargin): ReserveAvailability;
  hasStandbymargin(): boolean;
  clearStandbymargin(): ReserveAvailability;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReserveAvailability.AsObject;
  static toObject(includeInstance: boolean, msg: ReserveAvailability): ReserveAvailability.AsObject;
  static serializeBinaryToWriter(message: ReserveAvailability, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReserveAvailability;
  static deserializeBinaryFromReader(message: ReserveAvailability, reader: jspb.BinaryReader): ReserveAvailability;
}

export namespace ReserveAvailability {
  export type AsObject = {
    incrementalmargin?: ReserveMargin.AsObject,
    margin?: ReserveMargin.AsObject,
    standbymargin?: ReserveMargin.AsObject,
  }
}

export class AllocatedMargin extends jspb.Message {
  getRequestid(): string;
  setRequestid(value: string): AllocatedMargin;

  getAllocatedmargin(): ReserveMargin | undefined;
  setAllocatedmargin(value?: ReserveMargin): AllocatedMargin;
  hasAllocatedmargin(): boolean;
  clearAllocatedmargin(): AllocatedMargin;

  getAllocatedstandbymargin(): ReserveMargin | undefined;
  setAllocatedstandbymargin(value?: ReserveMargin): AllocatedMargin;
  hasAllocatedstandbymargin(): boolean;
  clearAllocatedstandbymargin(): AllocatedMargin;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AllocatedMargin.AsObject;
  static toObject(includeInstance: boolean, msg: AllocatedMargin): AllocatedMargin.AsObject;
  static serializeBinaryToWriter(message: AllocatedMargin, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AllocatedMargin;
  static deserializeBinaryFromReader(message: AllocatedMargin, reader: jspb.BinaryReader): AllocatedMargin;
}

export namespace AllocatedMargin {
  export type AsObject = {
    requestid: string,
    allocatedmargin?: ReserveMargin.AsObject,
    allocatedstandbymargin?: ReserveMargin.AsObject,
  }
}

export class ReserveAvailabilityProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): ReserveAvailabilityProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): ReserveAvailabilityProfile;

  getAllocatedmargin(): AllocatedMargin | undefined;
  setAllocatedmargin(value?: AllocatedMargin): ReserveAvailabilityProfile;
  hasAllocatedmargin(): boolean;
  clearAllocatedmargin(): ReserveAvailabilityProfile;

  getRequestercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRequestercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): ReserveAvailabilityProfile;
  hasRequestercircuitsegmentservice(): boolean;
  clearRequestercircuitsegmentservice(): ReserveAvailabilityProfile;

  getReserveavailability(): ReserveAvailability | undefined;
  setReserveavailability(value?: ReserveAvailability): ReserveAvailabilityProfile;
  hasReserveavailability(): boolean;
  clearReserveavailability(): ReserveAvailabilityProfile;

  getRespondercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRespondercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): ReserveAvailabilityProfile;
  hasRespondercircuitsegmentservice(): boolean;
  clearRespondercircuitsegmentservice(): ReserveAvailabilityProfile;

  getTiepoint(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setTiepoint(value?: commonmodule_commonmodule_pb.ConductingEquipment): ReserveAvailabilityProfile;
  hasTiepoint(): boolean;
  clearTiepoint(): ReserveAvailabilityProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReserveAvailabilityProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ReserveAvailabilityProfile): ReserveAvailabilityProfile.AsObject;
  static serializeBinaryToWriter(message: ReserveAvailabilityProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReserveAvailabilityProfile;
  static deserializeBinaryFromReader(message: ReserveAvailabilityProfile, reader: jspb.BinaryReader): ReserveAvailabilityProfile;
}

export namespace ReserveAvailabilityProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    allocatedmargin?: AllocatedMargin.AsObject,
    requestercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    reserveavailability?: ReserveAvailability.AsObject,
    respondercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    tiepoint?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

export class ReserveRequest extends jspb.Message {
  getRequestid(): string;
  setRequestid(value: string): ReserveRequest;

  getMargin(): ReserveMargin | undefined;
  setMargin(value?: ReserveMargin): ReserveRequest;
  hasMargin(): boolean;
  clearMargin(): ReserveRequest;

  getStandbymargin(): ReserveMargin | undefined;
  setStandbymargin(value?: ReserveMargin): ReserveRequest;
  hasStandbymargin(): boolean;
  clearStandbymargin(): ReserveRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReserveRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ReserveRequest): ReserveRequest.AsObject;
  static serializeBinaryToWriter(message: ReserveRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReserveRequest;
  static deserializeBinaryFromReader(message: ReserveRequest, reader: jspb.BinaryReader): ReserveRequest;
}

export namespace ReserveRequest {
  export type AsObject = {
    requestid: string,
    margin?: ReserveMargin.AsObject,
    standbymargin?: ReserveMargin.AsObject,
  }
}

export class ReserveRequestProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): ReserveRequestProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): ReserveRequestProfile;

  getRequestercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRequestercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): ReserveRequestProfile;
  hasRequestercircuitsegmentservice(): boolean;
  clearRequestercircuitsegmentservice(): ReserveRequestProfile;

  getReserverequest(): ReserveRequest | undefined;
  setReserverequest(value?: ReserveRequest): ReserveRequestProfile;
  hasReserverequest(): boolean;
  clearReserverequest(): ReserveRequestProfile;

  getRespondercircuitsegmentservice(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setRespondercircuitsegmentservice(value?: commonmodule_commonmodule_pb.ApplicationSystem): ReserveRequestProfile;
  hasRespondercircuitsegmentservice(): boolean;
  clearRespondercircuitsegmentservice(): ReserveRequestProfile;

  getTiepoint(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setTiepoint(value?: commonmodule_commonmodule_pb.ConductingEquipment): ReserveRequestProfile;
  hasTiepoint(): boolean;
  clearTiepoint(): ReserveRequestProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReserveRequestProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ReserveRequestProfile): ReserveRequestProfile.AsObject;
  static serializeBinaryToWriter(message: ReserveRequestProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReserveRequestProfile;
  static deserializeBinaryFromReader(message: ReserveRequestProfile, reader: jspb.BinaryReader): ReserveRequestProfile;
}

export namespace ReserveRequestProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    requestercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    reserverequest?: ReserveRequest.AsObject,
    respondercircuitsegmentservice?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    tiepoint?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

