import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class LoadPoint extends jspb.Message {
  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): LoadPoint;
  hasRamprates(): boolean;
  clearRamprates(): LoadPoint;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): LoadPoint;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): LoadPoint;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.ControlSPC): LoadPoint;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): LoadPoint;

  getReset(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReset(value?: commonmodule_commonmodule_pb.ControlSPC): LoadPoint;
  hasReset(): boolean;
  clearReset(): LoadPoint;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): LoadPoint;
  hasState(): boolean;
  clearState(): LoadPoint;

  getStarttime(): commonmodule_commonmodule_pb.ControlTimestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.ControlTimestamp): LoadPoint;
  hasStarttime(): boolean;
  clearStarttime(): LoadPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadPoint.AsObject;
  static toObject(includeInstance: boolean, msg: LoadPoint): LoadPoint.AsObject;
  static serializeBinaryToWriter(message: LoadPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadPoint;
  static deserializeBinaryFromReader(message: LoadPoint, reader: jspb.BinaryReader): LoadPoint;
}

export namespace LoadPoint {
  export type AsObject = {
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    reset?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    starttime?: commonmodule_commonmodule_pb.ControlTimestamp.AsObject,
  }
}

export class LoadCSG extends jspb.Message {
  getCrvptsList(): Array<LoadPoint>;
  setCrvptsList(value: Array<LoadPoint>): LoadCSG;
  clearCrvptsList(): LoadCSG;
  addCrvpts(value?: LoadPoint, index?: number): LoadPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadCSG.AsObject;
  static toObject(includeInstance: boolean, msg: LoadCSG): LoadCSG.AsObject;
  static serializeBinaryToWriter(message: LoadCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadCSG;
  static deserializeBinaryFromReader(message: LoadCSG, reader: jspb.BinaryReader): LoadCSG;
}

export namespace LoadCSG {
  export type AsObject = {
    crvptsList: Array<LoadPoint.AsObject>,
  }
}

export class LoadControlScheduleFSCH extends jspb.Message {
  getValdcsg(): LoadCSG | undefined;
  setValdcsg(value?: LoadCSG): LoadControlScheduleFSCH;
  hasValdcsg(): boolean;
  clearValdcsg(): LoadControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: LoadControlScheduleFSCH): LoadControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: LoadControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadControlScheduleFSCH;
  static deserializeBinaryFromReader(message: LoadControlScheduleFSCH, reader: jspb.BinaryReader): LoadControlScheduleFSCH;
}

export namespace LoadControlScheduleFSCH {
  export type AsObject = {
    valdcsg?: LoadCSG.AsObject,
  }
}

export class LoadControlFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): LoadControlFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): LoadControlFSCC;

  getLoadcontrolschedulefsch(): LoadControlScheduleFSCH | undefined;
  setLoadcontrolschedulefsch(value?: LoadControlScheduleFSCH): LoadControlFSCC;
  hasLoadcontrolschedulefsch(): boolean;
  clearLoadcontrolschedulefsch(): LoadControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: LoadControlFSCC): LoadControlFSCC.AsObject;
  static serializeBinaryToWriter(message: LoadControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadControlFSCC;
  static deserializeBinaryFromReader(message: LoadControlFSCC, reader: jspb.BinaryReader): LoadControlFSCC;
}

export namespace LoadControlFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    loadcontrolschedulefsch?: LoadControlScheduleFSCH.AsObject,
  }
}

export class LoadControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): LoadControl;
  hasControlvalue(): boolean;
  clearControlvalue(): LoadControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): LoadControl;
  hasCheck(): boolean;
  clearCheck(): LoadControl;

  getLoadcontrolfscc(): LoadControlFSCC | undefined;
  setLoadcontrolfscc(value?: LoadControlFSCC): LoadControl;
  hasLoadcontrolfscc(): boolean;
  clearLoadcontrolfscc(): LoadControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadControl.AsObject;
  static toObject(includeInstance: boolean, msg: LoadControl): LoadControl.AsObject;
  static serializeBinaryToWriter(message: LoadControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadControl;
  static deserializeBinaryFromReader(message: LoadControl, reader: jspb.BinaryReader): LoadControl;
}

export namespace LoadControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    loadcontrolfscc?: LoadControlFSCC.AsObject,
  }
}

export class LoadControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): LoadControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): LoadControlProfile;

  getEnergyconsumer(): commonmodule_commonmodule_pb.EnergyConsumer | undefined;
  setEnergyconsumer(value?: commonmodule_commonmodule_pb.EnergyConsumer): LoadControlProfile;
  hasEnergyconsumer(): boolean;
  clearEnergyconsumer(): LoadControlProfile;

  getLoadcontrol(): LoadControl | undefined;
  setLoadcontrol(value?: LoadControl): LoadControlProfile;
  hasLoadcontrol(): boolean;
  clearLoadcontrol(): LoadControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: LoadControlProfile): LoadControlProfile.AsObject;
  static serializeBinaryToWriter(message: LoadControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadControlProfile;
  static deserializeBinaryFromReader(message: LoadControlProfile, reader: jspb.BinaryReader): LoadControlProfile;
}

export namespace LoadControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    energyconsumer?: commonmodule_commonmodule_pb.EnergyConsumer.AsObject,
    loadcontrol?: LoadControl.AsObject,
  }
}

export class LoadPointStatus extends jspb.Message {
  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): LoadPointStatus;
  hasRamprates(): boolean;
  clearRamprates(): LoadPointStatus;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): LoadPointStatus;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): LoadPointStatus;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): LoadPointStatus;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): LoadPointStatus;

  getReset(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setReset(value?: commonmodule_commonmodule_pb.StatusSPS): LoadPointStatus;
  hasReset(): boolean;
  clearReset(): LoadPointStatus;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): LoadPointStatus;
  hasState(): boolean;
  clearState(): LoadPointStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadPointStatus.AsObject;
  static toObject(includeInstance: boolean, msg: LoadPointStatus): LoadPointStatus.AsObject;
  static serializeBinaryToWriter(message: LoadPointStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadPointStatus;
  static deserializeBinaryFromReader(message: LoadPointStatus, reader: jspb.BinaryReader): LoadPointStatus;
}

export namespace LoadPointStatus {
  export type AsObject = {
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    reset?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
  }
}

export class LoadEventAndStatusZGLD extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): LoadEventAndStatusZGLD;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): LoadEventAndStatusZGLD;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): LoadEventAndStatusZGLD;
  hasDynamictest(): boolean;
  clearDynamictest(): LoadEventAndStatusZGLD;

  getEmgstop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setEmgstop(value?: commonmodule_commonmodule_pb.StatusSPS): LoadEventAndStatusZGLD;
  hasEmgstop(): boolean;
  clearEmgstop(): LoadEventAndStatusZGLD;

  getPointstatus(): LoadPointStatus | undefined;
  setPointstatus(value?: LoadPointStatus): LoadEventAndStatusZGLD;
  hasPointstatus(): boolean;
  clearPointstatus(): LoadEventAndStatusZGLD;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadEventAndStatusZGLD.AsObject;
  static toObject(includeInstance: boolean, msg: LoadEventAndStatusZGLD): LoadEventAndStatusZGLD.AsObject;
  static serializeBinaryToWriter(message: LoadEventAndStatusZGLD, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadEventAndStatusZGLD;
  static deserializeBinaryFromReader(message: LoadEventAndStatusZGLD, reader: jspb.BinaryReader): LoadEventAndStatusZGLD;
}

export namespace LoadEventAndStatusZGLD {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    emgstop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    pointstatus?: LoadPointStatus.AsObject,
  }
}

export class LoadEventZGLD extends jspb.Message {
  getLoadeventandstatuszgld(): LoadEventAndStatusZGLD | undefined;
  setLoadeventandstatuszgld(value?: LoadEventAndStatusZGLD): LoadEventZGLD;
  hasLoadeventandstatuszgld(): boolean;
  clearLoadeventandstatuszgld(): LoadEventZGLD;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadEventZGLD.AsObject;
  static toObject(includeInstance: boolean, msg: LoadEventZGLD): LoadEventZGLD.AsObject;
  static serializeBinaryToWriter(message: LoadEventZGLD, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadEventZGLD;
  static deserializeBinaryFromReader(message: LoadEventZGLD, reader: jspb.BinaryReader): LoadEventZGLD;
}

export namespace LoadEventZGLD {
  export type AsObject = {
    loadeventandstatuszgld?: LoadEventAndStatusZGLD.AsObject,
  }
}

export class LoadEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): LoadEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): LoadEvent;

  getLoadeventzgld(): LoadEventZGLD | undefined;
  setLoadeventzgld(value?: LoadEventZGLD): LoadEvent;
  hasLoadeventzgld(): boolean;
  clearLoadeventzgld(): LoadEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadEvent.AsObject;
  static toObject(includeInstance: boolean, msg: LoadEvent): LoadEvent.AsObject;
  static serializeBinaryToWriter(message: LoadEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadEvent;
  static deserializeBinaryFromReader(message: LoadEvent, reader: jspb.BinaryReader): LoadEvent;
}

export namespace LoadEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    loadeventzgld?: LoadEventZGLD.AsObject,
  }
}

export class LoadEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): LoadEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): LoadEventProfile;

  getEnergyconsumer(): commonmodule_commonmodule_pb.EnergyConsumer | undefined;
  setEnergyconsumer(value?: commonmodule_commonmodule_pb.EnergyConsumer): LoadEventProfile;
  hasEnergyconsumer(): boolean;
  clearEnergyconsumer(): LoadEventProfile;

  getLoadevent(): LoadEvent | undefined;
  setLoadevent(value?: LoadEvent): LoadEventProfile;
  hasLoadevent(): boolean;
  clearLoadevent(): LoadEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: LoadEventProfile): LoadEventProfile.AsObject;
  static serializeBinaryToWriter(message: LoadEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadEventProfile;
  static deserializeBinaryFromReader(message: LoadEventProfile, reader: jspb.BinaryReader): LoadEventProfile;
}

export namespace LoadEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    energyconsumer?: commonmodule_commonmodule_pb.EnergyConsumer.AsObject,
    loadevent?: LoadEvent.AsObject,
  }
}

export class LoadReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): LoadReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): LoadReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): LoadReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): LoadReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): LoadReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): LoadReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): LoadReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): LoadReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadReading.AsObject;
  static toObject(includeInstance: boolean, msg: LoadReading): LoadReading.AsObject;
  static serializeBinaryToWriter(message: LoadReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadReading;
  static deserializeBinaryFromReader(message: LoadReading, reader: jspb.BinaryReader): LoadReading;
}

export namespace LoadReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class LoadReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): LoadReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): LoadReadingProfile;

  getEnergyconsumer(): commonmodule_commonmodule_pb.EnergyConsumer | undefined;
  setEnergyconsumer(value?: commonmodule_commonmodule_pb.EnergyConsumer): LoadReadingProfile;
  hasEnergyconsumer(): boolean;
  clearEnergyconsumer(): LoadReadingProfile;

  getLoadreading(): LoadReading | undefined;
  setLoadreading(value?: LoadReading): LoadReadingProfile;
  hasLoadreading(): boolean;
  clearLoadreading(): LoadReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: LoadReadingProfile): LoadReadingProfile.AsObject;
  static serializeBinaryToWriter(message: LoadReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadReadingProfile;
  static deserializeBinaryFromReader(message: LoadReadingProfile, reader: jspb.BinaryReader): LoadReadingProfile;
}

export namespace LoadReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    energyconsumer?: commonmodule_commonmodule_pb.EnergyConsumer.AsObject,
    loadreading?: LoadReading.AsObject,
  }
}

export class LoadStatusZGLD extends jspb.Message {
  getLoadeventandstatuszgld(): LoadEventAndStatusZGLD | undefined;
  setLoadeventandstatuszgld(value?: LoadEventAndStatusZGLD): LoadStatusZGLD;
  hasLoadeventandstatuszgld(): boolean;
  clearLoadeventandstatuszgld(): LoadStatusZGLD;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadStatusZGLD.AsObject;
  static toObject(includeInstance: boolean, msg: LoadStatusZGLD): LoadStatusZGLD.AsObject;
  static serializeBinaryToWriter(message: LoadStatusZGLD, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadStatusZGLD;
  static deserializeBinaryFromReader(message: LoadStatusZGLD, reader: jspb.BinaryReader): LoadStatusZGLD;
}

export namespace LoadStatusZGLD {
  export type AsObject = {
    loadeventandstatuszgld?: LoadEventAndStatusZGLD.AsObject,
  }
}

export class LoadStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): LoadStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): LoadStatus;

  getIsuncontrollable(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsuncontrollable(value?: google_protobuf_wrappers_pb.BoolValue): LoadStatus;
  hasIsuncontrollable(): boolean;
  clearIsuncontrollable(): LoadStatus;

  getLoadstatuszgld(): LoadStatusZGLD | undefined;
  setLoadstatuszgld(value?: LoadStatusZGLD): LoadStatus;
  hasLoadstatuszgld(): boolean;
  clearLoadstatuszgld(): LoadStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadStatus.AsObject;
  static toObject(includeInstance: boolean, msg: LoadStatus): LoadStatus.AsObject;
  static serializeBinaryToWriter(message: LoadStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadStatus;
  static deserializeBinaryFromReader(message: LoadStatus, reader: jspb.BinaryReader): LoadStatus;
}

export namespace LoadStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    isuncontrollable?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    loadstatuszgld?: LoadStatusZGLD.AsObject,
  }
}

export class LoadStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): LoadStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): LoadStatusProfile;

  getEnergyconsumer(): commonmodule_commonmodule_pb.EnergyConsumer | undefined;
  setEnergyconsumer(value?: commonmodule_commonmodule_pb.EnergyConsumer): LoadStatusProfile;
  hasEnergyconsumer(): boolean;
  clearEnergyconsumer(): LoadStatusProfile;

  getLoadstatus(): LoadStatus | undefined;
  setLoadstatus(value?: LoadStatus): LoadStatusProfile;
  hasLoadstatus(): boolean;
  clearLoadstatus(): LoadStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoadStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: LoadStatusProfile): LoadStatusProfile.AsObject;
  static serializeBinaryToWriter(message: LoadStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoadStatusProfile;
  static deserializeBinaryFromReader(message: LoadStatusProfile, reader: jspb.BinaryReader): LoadStatusProfile;
}

export namespace LoadStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    energyconsumer?: commonmodule_commonmodule_pb.EnergyConsumer.AsObject,
    loadstatus?: LoadStatus.AsObject,
  }
}

