import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class BreakerDiscreteControlXCBR extends jspb.Message {
  getDiscretecontrolxcbr(): commonmodule_commonmodule_pb.DiscreteControlXCBR | undefined;
  setDiscretecontrolxcbr(value?: commonmodule_commonmodule_pb.DiscreteControlXCBR): BreakerDiscreteControlXCBR;
  hasDiscretecontrolxcbr(): boolean;
  clearDiscretecontrolxcbr(): BreakerDiscreteControlXCBR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerDiscreteControlXCBR.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerDiscreteControlXCBR): BreakerDiscreteControlXCBR.AsObject;
  static serializeBinaryToWriter(message: BreakerDiscreteControlXCBR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerDiscreteControlXCBR;
  static deserializeBinaryFromReader(message: BreakerDiscreteControlXCBR, reader: jspb.BinaryReader): BreakerDiscreteControlXCBR;
}

export namespace BreakerDiscreteControlXCBR {
  export type AsObject = {
    discretecontrolxcbr?: commonmodule_commonmodule_pb.DiscreteControlXCBR.AsObject,
  }
}

export class BreakerDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): BreakerDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): BreakerDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): BreakerDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): BreakerDiscreteControl;

  getBreakerdiscretecontrolxcbr(): BreakerDiscreteControlXCBR | undefined;
  setBreakerdiscretecontrolxcbr(value?: BreakerDiscreteControlXCBR): BreakerDiscreteControl;
  hasBreakerdiscretecontrolxcbr(): boolean;
  clearBreakerdiscretecontrolxcbr(): BreakerDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerDiscreteControl): BreakerDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: BreakerDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerDiscreteControl;
  static deserializeBinaryFromReader(message: BreakerDiscreteControl, reader: jspb.BinaryReader): BreakerDiscreteControl;
}

export namespace BreakerDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    breakerdiscretecontrolxcbr?: BreakerDiscreteControlXCBR.AsObject,
  }
}

export class Breaker extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): Breaker;
  hasConductingequipment(): boolean;
  clearConductingequipment(): Breaker;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Breaker.AsObject;
  static toObject(includeInstance: boolean, msg: Breaker): Breaker.AsObject;
  static serializeBinaryToWriter(message: Breaker, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Breaker;
  static deserializeBinaryFromReader(message: Breaker, reader: jspb.BinaryReader): Breaker;
}

export namespace Breaker {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

export class BreakerDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): BreakerDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): BreakerDiscreteControlProfile;

  getBreaker(): Breaker | undefined;
  setBreaker(value?: Breaker): BreakerDiscreteControlProfile;
  hasBreaker(): boolean;
  clearBreaker(): BreakerDiscreteControlProfile;

  getBreakerdiscretecontrol(): BreakerDiscreteControl | undefined;
  setBreakerdiscretecontrol(value?: BreakerDiscreteControl): BreakerDiscreteControlProfile;
  hasBreakerdiscretecontrol(): boolean;
  clearBreakerdiscretecontrol(): BreakerDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerDiscreteControlProfile): BreakerDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: BreakerDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerDiscreteControlProfile;
  static deserializeBinaryFromReader(message: BreakerDiscreteControlProfile, reader: jspb.BinaryReader): BreakerDiscreteControlProfile;
}

export namespace BreakerDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    breaker?: Breaker.AsObject,
    breakerdiscretecontrol?: BreakerDiscreteControl.AsObject,
  }
}

export class BreakerEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): BreakerEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): BreakerEvent;

  getStatusandeventxcbr(): commonmodule_commonmodule_pb.StatusAndEventXCBR | undefined;
  setStatusandeventxcbr(value?: commonmodule_commonmodule_pb.StatusAndEventXCBR): BreakerEvent;
  hasStatusandeventxcbr(): boolean;
  clearStatusandeventxcbr(): BreakerEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerEvent.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerEvent): BreakerEvent.AsObject;
  static serializeBinaryToWriter(message: BreakerEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerEvent;
  static deserializeBinaryFromReader(message: BreakerEvent, reader: jspb.BinaryReader): BreakerEvent;
}

export namespace BreakerEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    statusandeventxcbr?: commonmodule_commonmodule_pb.StatusAndEventXCBR.AsObject,
  }
}

export class BreakerEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): BreakerEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): BreakerEventProfile;

  getBreaker(): Breaker | undefined;
  setBreaker(value?: Breaker): BreakerEventProfile;
  hasBreaker(): boolean;
  clearBreaker(): BreakerEventProfile;

  getBreakerevent(): BreakerEvent | undefined;
  setBreakerevent(value?: BreakerEvent): BreakerEventProfile;
  hasBreakerevent(): boolean;
  clearBreakerevent(): BreakerEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerEventProfile): BreakerEventProfile.AsObject;
  static serializeBinaryToWriter(message: BreakerEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerEventProfile;
  static deserializeBinaryFromReader(message: BreakerEventProfile, reader: jspb.BinaryReader): BreakerEventProfile;
}

export namespace BreakerEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    breaker?: Breaker.AsObject,
    breakerevent?: BreakerEvent.AsObject,
  }
}

export class BreakerReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): BreakerReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): BreakerReading;

  getDiffreadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setDiffreadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): BreakerReading;
  hasDiffreadingmmxu(): boolean;
  clearDiffreadingmmxu(): BreakerReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): BreakerReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): BreakerReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): BreakerReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): BreakerReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): BreakerReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): BreakerReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerReading.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerReading): BreakerReading.AsObject;
  static serializeBinaryToWriter(message: BreakerReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerReading;
  static deserializeBinaryFromReader(message: BreakerReading, reader: jspb.BinaryReader): BreakerReading;
}

export namespace BreakerReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    diffreadingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class BreakerReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): BreakerReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): BreakerReadingProfile;

  getBreaker(): Breaker | undefined;
  setBreaker(value?: Breaker): BreakerReadingProfile;
  hasBreaker(): boolean;
  clearBreaker(): BreakerReadingProfile;

  getBreakerreadingList(): Array<BreakerReading>;
  setBreakerreadingList(value: Array<BreakerReading>): BreakerReadingProfile;
  clearBreakerreadingList(): BreakerReadingProfile;
  addBreakerreading(value?: BreakerReading, index?: number): BreakerReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerReadingProfile): BreakerReadingProfile.AsObject;
  static serializeBinaryToWriter(message: BreakerReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerReadingProfile;
  static deserializeBinaryFromReader(message: BreakerReadingProfile, reader: jspb.BinaryReader): BreakerReadingProfile;
}

export namespace BreakerReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    breaker?: Breaker.AsObject,
    breakerreadingList: Array<BreakerReading.AsObject>,
  }
}

export class BreakerStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): BreakerStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): BreakerStatus;

  getStatusandeventxcbr(): commonmodule_commonmodule_pb.StatusAndEventXCBR | undefined;
  setStatusandeventxcbr(value?: commonmodule_commonmodule_pb.StatusAndEventXCBR): BreakerStatus;
  hasStatusandeventxcbr(): boolean;
  clearStatusandeventxcbr(): BreakerStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerStatus.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerStatus): BreakerStatus.AsObject;
  static serializeBinaryToWriter(message: BreakerStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerStatus;
  static deserializeBinaryFromReader(message: BreakerStatus, reader: jspb.BinaryReader): BreakerStatus;
}

export namespace BreakerStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    statusandeventxcbr?: commonmodule_commonmodule_pb.StatusAndEventXCBR.AsObject,
  }
}

export class BreakerStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): BreakerStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): BreakerStatusProfile;

  getBreaker(): Breaker | undefined;
  setBreaker(value?: Breaker): BreakerStatusProfile;
  hasBreaker(): boolean;
  clearBreaker(): BreakerStatusProfile;

  getBreakerstatus(): BreakerStatus | undefined;
  setBreakerstatus(value?: BreakerStatus): BreakerStatusProfile;
  hasBreakerstatus(): boolean;
  clearBreakerstatus(): BreakerStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BreakerStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: BreakerStatusProfile): BreakerStatusProfile.AsObject;
  static serializeBinaryToWriter(message: BreakerStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BreakerStatusProfile;
  static deserializeBinaryFromReader(message: BreakerStatusProfile, reader: jspb.BinaryReader): BreakerStatusProfile;
}

export namespace BreakerStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    breaker?: Breaker.AsObject,
    breakerstatus?: BreakerStatus.AsObject,
  }
}

