import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class DirectionalATCC extends jspb.Message {
  getBndwid(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setBndwid(value?: commonmodule_commonmodule_pb.PhaseAPC): DirectionalATCC;
  hasBndwid(): boolean;
  clearBndwid(): DirectionalATCC;

  getCtldltmms(): commonmodule_commonmodule_pb.PhaseISC | undefined;
  setCtldltmms(value?: commonmodule_commonmodule_pb.PhaseISC): DirectionalATCC;
  hasCtldltmms(): boolean;
  clearCtldltmms(): DirectionalATCC;

  getLdcr(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setLdcr(value?: commonmodule_commonmodule_pb.PhaseAPC): DirectionalATCC;
  hasLdcr(): boolean;
  clearLdcr(): DirectionalATCC;

  getLdcx(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setLdcx(value?: commonmodule_commonmodule_pb.PhaseAPC): DirectionalATCC;
  hasLdcx(): boolean;
  clearLdcx(): DirectionalATCC;

  getVolspt(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVolspt(value?: commonmodule_commonmodule_pb.PhaseAPC): DirectionalATCC;
  hasVolspt(): boolean;
  clearVolspt(): DirectionalATCC;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.PhaseDPC | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.PhaseDPC): DirectionalATCC;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): DirectionalATCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DirectionalATCC.AsObject;
  static toObject(includeInstance: boolean, msg: DirectionalATCC): DirectionalATCC.AsObject;
  static serializeBinaryToWriter(message: DirectionalATCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DirectionalATCC;
  static deserializeBinaryFromReader(message: DirectionalATCC, reader: jspb.BinaryReader): DirectionalATCC;
}

export namespace DirectionalATCC {
  export type AsObject = {
    bndwid?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    ctldltmms?: commonmodule_commonmodule_pb.PhaseISC.AsObject,
    ldcr?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    ldcx?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    volspt?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.PhaseDPC.AsObject,
  }
}

export class RegulatorControlATCC extends jspb.Message {
  getLogicalnodeforcontrol(): commonmodule_commonmodule_pb.LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: commonmodule_commonmodule_pb.LogicalNodeForControl): RegulatorControlATCC;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): RegulatorControlATCC;

  getDirfwd(): DirectionalATCC | undefined;
  setDirfwd(value?: DirectionalATCC): RegulatorControlATCC;
  hasDirfwd(): boolean;
  clearDirfwd(): RegulatorControlATCC;

  getDirmode(): commonmodule_commonmodule_pb.Optional_DirectionModeKind | undefined;
  setDirmode(value?: commonmodule_commonmodule_pb.Optional_DirectionModeKind): RegulatorControlATCC;
  hasDirmode(): boolean;
  clearDirmode(): RegulatorControlATCC;

  getDirrev(): DirectionalATCC | undefined;
  setDirrev(value?: DirectionalATCC): RegulatorControlATCC;
  hasDirrev(): boolean;
  clearDirrev(): RegulatorControlATCC;

  getDirthd(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setDirthd(value?: commonmodule_commonmodule_pb.PhaseAPC): RegulatorControlATCC;
  hasDirthd(): boolean;
  clearDirthd(): RegulatorControlATCC;

  getParop(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setParop(value?: commonmodule_commonmodule_pb.PhaseSPC): RegulatorControlATCC;
  hasParop(): boolean;
  clearParop(): RegulatorControlATCC;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): RegulatorControlATCC;
  hasRamprates(): boolean;
  clearRamprates(): RegulatorControlATCC;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): RegulatorControlATCC;
  hasState(): boolean;
  clearState(): RegulatorControlATCC;

  getTapopl(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setTapopl(value?: commonmodule_commonmodule_pb.PhaseSPC): RegulatorControlATCC;
  hasTapopl(): boolean;
  clearTapopl(): RegulatorControlATCC;

  getTapopr(): commonmodule_commonmodule_pb.PhaseSPC | undefined;
  setTapopr(value?: commonmodule_commonmodule_pb.PhaseSPC): RegulatorControlATCC;
  hasTapopr(): boolean;
  clearTapopr(): RegulatorControlATCC;

  getVollmthi(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVollmthi(value?: commonmodule_commonmodule_pb.PhaseAPC): RegulatorControlATCC;
  hasVollmthi(): boolean;
  clearVollmthi(): RegulatorControlATCC;

  getVollmtlo(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVollmtlo(value?: commonmodule_commonmodule_pb.PhaseAPC): RegulatorControlATCC;
  hasVollmtlo(): boolean;
  clearVollmtlo(): RegulatorControlATCC;

  getVollmtmode(): commonmodule_commonmodule_pb.Optional_VoltLimitModeKind | undefined;
  setVollmtmode(value?: commonmodule_commonmodule_pb.Optional_VoltLimitModeKind): RegulatorControlATCC;
  hasVollmtmode(): boolean;
  clearVollmtmode(): RegulatorControlATCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorControlATCC.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorControlATCC): RegulatorControlATCC.AsObject;
  static serializeBinaryToWriter(message: RegulatorControlATCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorControlATCC;
  static deserializeBinaryFromReader(message: RegulatorControlATCC, reader: jspb.BinaryReader): RegulatorControlATCC;
}

export namespace RegulatorControlATCC {
  export type AsObject = {
    logicalnodeforcontrol?: commonmodule_commonmodule_pb.LogicalNodeForControl.AsObject,
    dirfwd?: DirectionalATCC.AsObject,
    dirmode?: commonmodule_commonmodule_pb.Optional_DirectionModeKind.AsObject,
    dirrev?: DirectionalATCC.AsObject,
    dirthd?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    parop?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    tapopl?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    tapopr?: commonmodule_commonmodule_pb.PhaseSPC.AsObject,
    vollmthi?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    vollmtlo?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    vollmtmode?: commonmodule_commonmodule_pb.Optional_VoltLimitModeKind.AsObject,
  }
}

export class RegulatorPoint extends jspb.Message {
  getControl(): RegulatorControlATCC | undefined;
  setControl(value?: RegulatorControlATCC): RegulatorPoint;
  hasControl(): boolean;
  clearControl(): RegulatorPoint;

  getStarttime(): commonmodule_commonmodule_pb.Timestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.Timestamp): RegulatorPoint;
  hasStarttime(): boolean;
  clearStarttime(): RegulatorPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorPoint.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorPoint): RegulatorPoint.AsObject;
  static serializeBinaryToWriter(message: RegulatorPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorPoint;
  static deserializeBinaryFromReader(message: RegulatorPoint, reader: jspb.BinaryReader): RegulatorPoint;
}

export namespace RegulatorPoint {
  export type AsObject = {
    control?: RegulatorControlATCC.AsObject,
    starttime?: commonmodule_commonmodule_pb.Timestamp.AsObject,
  }
}

export class RegulatorCSG extends jspb.Message {
  getCrvptsList(): Array<RegulatorPoint>;
  setCrvptsList(value: Array<RegulatorPoint>): RegulatorCSG;
  clearCrvptsList(): RegulatorCSG;
  addCrvpts(value?: RegulatorPoint, index?: number): RegulatorPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorCSG.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorCSG): RegulatorCSG.AsObject;
  static serializeBinaryToWriter(message: RegulatorCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorCSG;
  static deserializeBinaryFromReader(message: RegulatorCSG, reader: jspb.BinaryReader): RegulatorCSG;
}

export namespace RegulatorCSG {
  export type AsObject = {
    crvptsList: Array<RegulatorPoint.AsObject>,
  }
}

export class RegulatorControlScheduleFSCH extends jspb.Message {
  getValdcsg(): RegulatorCSG | undefined;
  setValdcsg(value?: RegulatorCSG): RegulatorControlScheduleFSCH;
  hasValdcsg(): boolean;
  clearValdcsg(): RegulatorControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorControlScheduleFSCH): RegulatorControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: RegulatorControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorControlScheduleFSCH;
  static deserializeBinaryFromReader(message: RegulatorControlScheduleFSCH, reader: jspb.BinaryReader): RegulatorControlScheduleFSCH;
}

export namespace RegulatorControlScheduleFSCH {
  export type AsObject = {
    valdcsg?: RegulatorCSG.AsObject,
  }
}

export class RegulatorControlFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): RegulatorControlFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): RegulatorControlFSCC;

  getRegulatorcontrolschedulefsch(): RegulatorControlScheduleFSCH | undefined;
  setRegulatorcontrolschedulefsch(value?: RegulatorControlScheduleFSCH): RegulatorControlFSCC;
  hasRegulatorcontrolschedulefsch(): boolean;
  clearRegulatorcontrolschedulefsch(): RegulatorControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorControlFSCC): RegulatorControlFSCC.AsObject;
  static serializeBinaryToWriter(message: RegulatorControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorControlFSCC;
  static deserializeBinaryFromReader(message: RegulatorControlFSCC, reader: jspb.BinaryReader): RegulatorControlFSCC;
}

export namespace RegulatorControlFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    regulatorcontrolschedulefsch?: RegulatorControlScheduleFSCH.AsObject,
  }
}

export class RegulatorControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): RegulatorControl;
  hasControlvalue(): boolean;
  clearControlvalue(): RegulatorControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): RegulatorControl;
  hasCheck(): boolean;
  clearCheck(): RegulatorControl;

  getRegulatorcontrolfscc(): RegulatorControlFSCC | undefined;
  setRegulatorcontrolfscc(value?: RegulatorControlFSCC): RegulatorControl;
  hasRegulatorcontrolfscc(): boolean;
  clearRegulatorcontrolfscc(): RegulatorControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorControl.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorControl): RegulatorControl.AsObject;
  static serializeBinaryToWriter(message: RegulatorControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorControl;
  static deserializeBinaryFromReader(message: RegulatorControl, reader: jspb.BinaryReader): RegulatorControl;
}

export namespace RegulatorControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    regulatorcontrolfscc?: RegulatorControlFSCC.AsObject,
  }
}

export class RegulatorSystem extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): RegulatorSystem;
  hasConductingequipment(): boolean;
  clearConductingequipment(): RegulatorSystem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorSystem.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorSystem): RegulatorSystem.AsObject;
  static serializeBinaryToWriter(message: RegulatorSystem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorSystem;
  static deserializeBinaryFromReader(message: RegulatorSystem, reader: jspb.BinaryReader): RegulatorSystem;
}

export namespace RegulatorSystem {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

export class RegulatorControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): RegulatorControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): RegulatorControlProfile;

  getRegulatorcontrol(): RegulatorControl | undefined;
  setRegulatorcontrol(value?: RegulatorControl): RegulatorControlProfile;
  hasRegulatorcontrol(): boolean;
  clearRegulatorcontrol(): RegulatorControlProfile;

  getRegulatorsystem(): RegulatorSystem | undefined;
  setRegulatorsystem(value?: RegulatorSystem): RegulatorControlProfile;
  hasRegulatorsystem(): boolean;
  clearRegulatorsystem(): RegulatorControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorControlProfile): RegulatorControlProfile.AsObject;
  static serializeBinaryToWriter(message: RegulatorControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorControlProfile;
  static deserializeBinaryFromReader(message: RegulatorControlProfile, reader: jspb.BinaryReader): RegulatorControlProfile;
}

export namespace RegulatorControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    regulatorcontrol?: RegulatorControl.AsObject,
    regulatorsystem?: RegulatorSystem.AsObject,
  }
}

export class RegulatorDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): RegulatorDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): RegulatorDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): RegulatorDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): RegulatorDiscreteControl;

  getRegulatorcontrolatcc(): RegulatorControlATCC | undefined;
  setRegulatorcontrolatcc(value?: RegulatorControlATCC): RegulatorDiscreteControl;
  hasRegulatorcontrolatcc(): boolean;
  clearRegulatorcontrolatcc(): RegulatorDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorDiscreteControl): RegulatorDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: RegulatorDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorDiscreteControl;
  static deserializeBinaryFromReader(message: RegulatorDiscreteControl, reader: jspb.BinaryReader): RegulatorDiscreteControl;
}

export namespace RegulatorDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    regulatorcontrolatcc?: RegulatorControlATCC.AsObject,
  }
}

export class RegulatorDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): RegulatorDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): RegulatorDiscreteControlProfile;

  getRegulatordiscretecontrol(): RegulatorDiscreteControl | undefined;
  setRegulatordiscretecontrol(value?: RegulatorDiscreteControl): RegulatorDiscreteControlProfile;
  hasRegulatordiscretecontrol(): boolean;
  clearRegulatordiscretecontrol(): RegulatorDiscreteControlProfile;

  getRegulatorsystem(): RegulatorSystem | undefined;
  setRegulatorsystem(value?: RegulatorSystem): RegulatorDiscreteControlProfile;
  hasRegulatorsystem(): boolean;
  clearRegulatorsystem(): RegulatorDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorDiscreteControlProfile): RegulatorDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: RegulatorDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorDiscreteControlProfile;
  static deserializeBinaryFromReader(message: RegulatorDiscreteControlProfile, reader: jspb.BinaryReader): RegulatorDiscreteControlProfile;
}

export namespace RegulatorDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    regulatordiscretecontrol?: RegulatorDiscreteControl.AsObject,
    regulatorsystem?: RegulatorSystem.AsObject,
  }
}

export class RegulatorEventAndStatusATCC extends jspb.Message {
  getBndctr(): commonmodule_commonmodule_pb.ASG | undefined;
  setBndctr(value?: commonmodule_commonmodule_pb.ASG): RegulatorEventAndStatusATCC;
  hasBndctr(): boolean;
  clearBndctr(): RegulatorEventAndStatusATCC;

  getBndwid(): commonmodule_commonmodule_pb.ASG | undefined;
  setBndwid(value?: commonmodule_commonmodule_pb.ASG): RegulatorEventAndStatusATCC;
  hasBndwid(): boolean;
  clearBndwid(): RegulatorEventAndStatusATCC;

  getBndwidhi(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setBndwidhi(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasBndwidhi(): boolean;
  clearBndwidhi(): RegulatorEventAndStatusATCC;

  getBndwidlo(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setBndwidlo(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasBndwidlo(): boolean;
  clearBndwidlo(): RegulatorEventAndStatusATCC;

  getDirctlrev(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setDirctlrev(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasDirctlrev(): boolean;
  clearDirctlrev(): RegulatorEventAndStatusATCC;

  getDirindt(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setDirindt(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasDirindt(): boolean;
  clearDirindt(): RegulatorEventAndStatusATCC;

  getDirrev(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setDirrev(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasDirrev(): boolean;
  clearDirrev(): RegulatorEventAndStatusATCC;

  getLdcr(): commonmodule_commonmodule_pb.ASG | undefined;
  setLdcr(value?: commonmodule_commonmodule_pb.ASG): RegulatorEventAndStatusATCC;
  hasLdcr(): boolean;
  clearLdcr(): RegulatorEventAndStatusATCC;

  getLdcx(): commonmodule_commonmodule_pb.ASG | undefined;
  setLdcx(value?: commonmodule_commonmodule_pb.ASG): RegulatorEventAndStatusATCC;
  hasLdcx(): boolean;
  clearLdcx(): RegulatorEventAndStatusATCC;

  getParop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setParop(value?: commonmodule_commonmodule_pb.StatusSPS): RegulatorEventAndStatusATCC;
  hasParop(): boolean;
  clearParop(): RegulatorEventAndStatusATCC;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): RegulatorEventAndStatusATCC;
  hasRamprates(): boolean;
  clearRamprates(): RegulatorEventAndStatusATCC;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): RegulatorEventAndStatusATCC;
  hasState(): boolean;
  clearState(): RegulatorEventAndStatusATCC;

  getStdltmms(): commonmodule_commonmodule_pb.StatusINC | undefined;
  setStdltmms(value?: commonmodule_commonmodule_pb.StatusINC): RegulatorEventAndStatusATCC;
  hasStdltmms(): boolean;
  clearStdltmms(): RegulatorEventAndStatusATCC;

  getTapoperr(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setTapoperr(value?: commonmodule_commonmodule_pb.StatusSPS): RegulatorEventAndStatusATCC;
  hasTapoperr(): boolean;
  clearTapoperr(): RegulatorEventAndStatusATCC;

  getTappos(): commonmodule_commonmodule_pb.PhaseINS | undefined;
  setTappos(value?: commonmodule_commonmodule_pb.PhaseINS): RegulatorEventAndStatusATCC;
  hasTappos(): boolean;
  clearTappos(): RegulatorEventAndStatusATCC;

  getVollmthi(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setVollmthi(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasVollmthi(): boolean;
  clearVollmthi(): RegulatorEventAndStatusATCC;

  getVollmtlo(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setVollmtlo(value?: commonmodule_commonmodule_pb.PhaseSPS): RegulatorEventAndStatusATCC;
  hasVollmtlo(): boolean;
  clearVollmtlo(): RegulatorEventAndStatusATCC;

  getVolspt(): commonmodule_commonmodule_pb.PhaseAPC | undefined;
  setVolspt(value?: commonmodule_commonmodule_pb.PhaseAPC): RegulatorEventAndStatusATCC;
  hasVolspt(): boolean;
  clearVolspt(): RegulatorEventAndStatusATCC;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): RegulatorEventAndStatusATCC;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): RegulatorEventAndStatusATCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorEventAndStatusATCC.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorEventAndStatusATCC): RegulatorEventAndStatusATCC.AsObject;
  static serializeBinaryToWriter(message: RegulatorEventAndStatusATCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorEventAndStatusATCC;
  static deserializeBinaryFromReader(message: RegulatorEventAndStatusATCC, reader: jspb.BinaryReader): RegulatorEventAndStatusATCC;
}

export namespace RegulatorEventAndStatusATCC {
  export type AsObject = {
    bndctr?: commonmodule_commonmodule_pb.ASG.AsObject,
    bndwid?: commonmodule_commonmodule_pb.ASG.AsObject,
    bndwidhi?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    bndwidlo?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    dirctlrev?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    dirindt?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    dirrev?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    ldcr?: commonmodule_commonmodule_pb.ASG.AsObject,
    ldcx?: commonmodule_commonmodule_pb.ASG.AsObject,
    parop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    stdltmms?: commonmodule_commonmodule_pb.StatusINC.AsObject,
    tapoperr?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    tappos?: commonmodule_commonmodule_pb.PhaseINS.AsObject,
    vollmthi?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    vollmtlo?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
    volspt?: commonmodule_commonmodule_pb.PhaseAPC.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
  }
}

export class RegulatorEventAndStatusANCR extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): RegulatorEventAndStatusANCR;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): RegulatorEventAndStatusANCR;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): RegulatorEventAndStatusANCR;
  hasDynamictest(): boolean;
  clearDynamictest(): RegulatorEventAndStatusANCR;

  getPointstatus(): RegulatorEventAndStatusATCC | undefined;
  setPointstatus(value?: RegulatorEventAndStatusATCC): RegulatorEventAndStatusANCR;
  hasPointstatus(): boolean;
  clearPointstatus(): RegulatorEventAndStatusANCR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorEventAndStatusANCR.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorEventAndStatusANCR): RegulatorEventAndStatusANCR.AsObject;
  static serializeBinaryToWriter(message: RegulatorEventAndStatusANCR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorEventAndStatusANCR;
  static deserializeBinaryFromReader(message: RegulatorEventAndStatusANCR, reader: jspb.BinaryReader): RegulatorEventAndStatusANCR;
}

export namespace RegulatorEventAndStatusANCR {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    pointstatus?: RegulatorEventAndStatusATCC.AsObject,
  }
}

export class RegulatorEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): RegulatorEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): RegulatorEvent;

  getRegulatoreventandstatusancr(): RegulatorEventAndStatusANCR | undefined;
  setRegulatoreventandstatusancr(value?: RegulatorEventAndStatusANCR): RegulatorEvent;
  hasRegulatoreventandstatusancr(): boolean;
  clearRegulatoreventandstatusancr(): RegulatorEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorEvent.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorEvent): RegulatorEvent.AsObject;
  static serializeBinaryToWriter(message: RegulatorEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorEvent;
  static deserializeBinaryFromReader(message: RegulatorEvent, reader: jspb.BinaryReader): RegulatorEvent;
}

export namespace RegulatorEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    regulatoreventandstatusancr?: RegulatorEventAndStatusANCR.AsObject,
  }
}

export class RegulatorEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): RegulatorEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): RegulatorEventProfile;

  getRegulatorevent(): RegulatorEvent | undefined;
  setRegulatorevent(value?: RegulatorEvent): RegulatorEventProfile;
  hasRegulatorevent(): boolean;
  clearRegulatorevent(): RegulatorEventProfile;

  getRegulatorsystem(): RegulatorSystem | undefined;
  setRegulatorsystem(value?: RegulatorSystem): RegulatorEventProfile;
  hasRegulatorsystem(): boolean;
  clearRegulatorsystem(): RegulatorEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorEventProfile): RegulatorEventProfile.AsObject;
  static serializeBinaryToWriter(message: RegulatorEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorEventProfile;
  static deserializeBinaryFromReader(message: RegulatorEventProfile, reader: jspb.BinaryReader): RegulatorEventProfile;
}

export namespace RegulatorEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    regulatorevent?: RegulatorEvent.AsObject,
    regulatorsystem?: RegulatorSystem.AsObject,
  }
}

export class RegulatorReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): RegulatorReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): RegulatorReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): RegulatorReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): RegulatorReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): RegulatorReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): RegulatorReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): RegulatorReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): RegulatorReading;

  getSecondaryreadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setSecondaryreadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): RegulatorReading;
  hasSecondaryreadingmmxu(): boolean;
  clearSecondaryreadingmmxu(): RegulatorReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorReading.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorReading): RegulatorReading.AsObject;
  static serializeBinaryToWriter(message: RegulatorReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorReading;
  static deserializeBinaryFromReader(message: RegulatorReading, reader: jspb.BinaryReader): RegulatorReading;
}

export namespace RegulatorReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
    secondaryreadingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class RegulatorReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): RegulatorReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): RegulatorReadingProfile;

  getRegulatorreadingList(): Array<RegulatorReading>;
  setRegulatorreadingList(value: Array<RegulatorReading>): RegulatorReadingProfile;
  clearRegulatorreadingList(): RegulatorReadingProfile;
  addRegulatorreading(value?: RegulatorReading, index?: number): RegulatorReading;

  getRegulatorsystem(): RegulatorSystem | undefined;
  setRegulatorsystem(value?: RegulatorSystem): RegulatorReadingProfile;
  hasRegulatorsystem(): boolean;
  clearRegulatorsystem(): RegulatorReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorReadingProfile): RegulatorReadingProfile.AsObject;
  static serializeBinaryToWriter(message: RegulatorReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorReadingProfile;
  static deserializeBinaryFromReader(message: RegulatorReadingProfile, reader: jspb.BinaryReader): RegulatorReadingProfile;
}

export namespace RegulatorReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    regulatorreadingList: Array<RegulatorReading.AsObject>,
    regulatorsystem?: RegulatorSystem.AsObject,
  }
}

export class RegulatorStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): RegulatorStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): RegulatorStatus;

  getRegulatoreventandstatusancr(): RegulatorEventAndStatusANCR | undefined;
  setRegulatoreventandstatusancr(value?: RegulatorEventAndStatusANCR): RegulatorStatus;
  hasRegulatoreventandstatusancr(): boolean;
  clearRegulatoreventandstatusancr(): RegulatorStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorStatus.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorStatus): RegulatorStatus.AsObject;
  static serializeBinaryToWriter(message: RegulatorStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorStatus;
  static deserializeBinaryFromReader(message: RegulatorStatus, reader: jspb.BinaryReader): RegulatorStatus;
}

export namespace RegulatorStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    regulatoreventandstatusancr?: RegulatorEventAndStatusANCR.AsObject,
  }
}

export class RegulatorStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): RegulatorStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): RegulatorStatusProfile;

  getRegulatorstatus(): RegulatorStatus | undefined;
  setRegulatorstatus(value?: RegulatorStatus): RegulatorStatusProfile;
  hasRegulatorstatus(): boolean;
  clearRegulatorstatus(): RegulatorStatusProfile;

  getRegulatorsystem(): RegulatorSystem | undefined;
  setRegulatorsystem(value?: RegulatorSystem): RegulatorStatusProfile;
  hasRegulatorsystem(): boolean;
  clearRegulatorsystem(): RegulatorStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegulatorStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RegulatorStatusProfile): RegulatorStatusProfile.AsObject;
  static serializeBinaryToWriter(message: RegulatorStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegulatorStatusProfile;
  static deserializeBinaryFromReader(message: RegulatorStatusProfile, reader: jspb.BinaryReader): RegulatorStatusProfile;
}

export namespace RegulatorStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    regulatorstatus?: RegulatorStatus.AsObject,
    regulatorsystem?: RegulatorSystem.AsObject,
  }
}

