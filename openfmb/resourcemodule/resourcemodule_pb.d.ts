import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class BooleanControlGGIO extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): BooleanControlGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): BooleanControlGGIO;

  getPhase(): commonmodule_commonmodule_pb.Optional_PhaseCodeKind | undefined;
  setPhase(value?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind): BooleanControlGGIO;
  hasPhase(): boolean;
  clearPhase(): BooleanControlGGIO;

  getSpcso(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setSpcso(value?: commonmodule_commonmodule_pb.ControlSPC): BooleanControlGGIO;
  hasSpcso(): boolean;
  clearSpcso(): BooleanControlGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BooleanControlGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: BooleanControlGGIO): BooleanControlGGIO.AsObject;
  static serializeBinaryToWriter(message: BooleanControlGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BooleanControlGGIO;
  static deserializeBinaryFromReader(message: BooleanControlGGIO, reader: jspb.BinaryReader): BooleanControlGGIO;
}

export namespace BooleanControlGGIO {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    phase?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind.AsObject,
    spcso?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
  }
}

export class IntegerControlGGIO extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): IntegerControlGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): IntegerControlGGIO;

  getIscso(): commonmodule_commonmodule_pb.ControlINC | undefined;
  setIscso(value?: commonmodule_commonmodule_pb.ControlINC): IntegerControlGGIO;
  hasIscso(): boolean;
  clearIscso(): IntegerControlGGIO;

  getPhase(): commonmodule_commonmodule_pb.Optional_PhaseCodeKind | undefined;
  setPhase(value?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind): IntegerControlGGIO;
  hasPhase(): boolean;
  clearPhase(): IntegerControlGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): IntegerControlGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: IntegerControlGGIO): IntegerControlGGIO.AsObject;
  static serializeBinaryToWriter(message: IntegerControlGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): IntegerControlGGIO;
  static deserializeBinaryFromReader(message: IntegerControlGGIO, reader: jspb.BinaryReader): IntegerControlGGIO;
}

export namespace IntegerControlGGIO {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    iscso?: commonmodule_commonmodule_pb.ControlINC.AsObject,
    phase?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind.AsObject,
  }
}

export class StringControlGGIO extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): StringControlGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): StringControlGGIO;

  getPhase(): commonmodule_commonmodule_pb.Optional_PhaseCodeKind | undefined;
  setPhase(value?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind): StringControlGGIO;
  hasPhase(): boolean;
  clearPhase(): StringControlGGIO;

  getStrout(): commonmodule_commonmodule_pb.VSC | undefined;
  setStrout(value?: commonmodule_commonmodule_pb.VSC): StringControlGGIO;
  hasStrout(): boolean;
  clearStrout(): StringControlGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StringControlGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: StringControlGGIO): StringControlGGIO.AsObject;
  static serializeBinaryToWriter(message: StringControlGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StringControlGGIO;
  static deserializeBinaryFromReader(message: StringControlGGIO, reader: jspb.BinaryReader): StringControlGGIO;
}

export namespace StringControlGGIO {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    phase?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind.AsObject,
    strout?: commonmodule_commonmodule_pb.VSC.AsObject,
  }
}

export class AnalogControlGGIO extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): AnalogControlGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): AnalogControlGGIO;

  getAnout(): commonmodule_commonmodule_pb.ControlAPC | undefined;
  setAnout(value?: commonmodule_commonmodule_pb.ControlAPC): AnalogControlGGIO;
  hasAnout(): boolean;
  clearAnout(): AnalogControlGGIO;

  getPhase(): commonmodule_commonmodule_pb.Optional_PhaseCodeKind | undefined;
  setPhase(value?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind): AnalogControlGGIO;
  hasPhase(): boolean;
  clearPhase(): AnalogControlGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AnalogControlGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: AnalogControlGGIO): AnalogControlGGIO.AsObject;
  static serializeBinaryToWriter(message: AnalogControlGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AnalogControlGGIO;
  static deserializeBinaryFromReader(message: AnalogControlGGIO, reader: jspb.BinaryReader): AnalogControlGGIO;
}

export namespace AnalogControlGGIO {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    anout?: commonmodule_commonmodule_pb.ControlAPC.AsObject,
    phase?: commonmodule_commonmodule_pb.Optional_PhaseCodeKind.AsObject,
  }
}

export class ResourceDiscreteControl extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): ResourceDiscreteControl;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ResourceDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): ResourceDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): ResourceDiscreteControl;

  getAnalogcontrolggioList(): Array<AnalogControlGGIO>;
  setAnalogcontrolggioList(value: Array<AnalogControlGGIO>): ResourceDiscreteControl;
  clearAnalogcontrolggioList(): ResourceDiscreteControl;
  addAnalogcontrolggio(value?: AnalogControlGGIO, index?: number): AnalogControlGGIO;

  getBooleancontrolggioList(): Array<BooleanControlGGIO>;
  setBooleancontrolggioList(value: Array<BooleanControlGGIO>): ResourceDiscreteControl;
  clearBooleancontrolggioList(): ResourceDiscreteControl;
  addBooleancontrolggio(value?: BooleanControlGGIO, index?: number): BooleanControlGGIO;

  getIntegercontrolggioList(): Array<IntegerControlGGIO>;
  setIntegercontrolggioList(value: Array<IntegerControlGGIO>): ResourceDiscreteControl;
  clearIntegercontrolggioList(): ResourceDiscreteControl;
  addIntegercontrolggio(value?: IntegerControlGGIO, index?: number): IntegerControlGGIO;

  getStringcontrolggioList(): Array<StringControlGGIO>;
  setStringcontrolggioList(value: Array<StringControlGGIO>): ResourceDiscreteControl;
  clearStringcontrolggioList(): ResourceDiscreteControl;
  addStringcontrolggio(value?: StringControlGGIO, index?: number): StringControlGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceDiscreteControl): ResourceDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: ResourceDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceDiscreteControl;
  static deserializeBinaryFromReader(message: ResourceDiscreteControl, reader: jspb.BinaryReader): ResourceDiscreteControl;
}

export namespace ResourceDiscreteControl {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    analogcontrolggioList: Array<AnalogControlGGIO.AsObject>,
    booleancontrolggioList: Array<BooleanControlGGIO.AsObject>,
    integercontrolggioList: Array<IntegerControlGGIO.AsObject>,
    stringcontrolggioList: Array<StringControlGGIO.AsObject>,
  }
}

export class ResourceDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): ResourceDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): ResourceDiscreteControlProfile;

  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): ResourceDiscreteControlProfile;
  hasConductingequipment(): boolean;
  clearConductingequipment(): ResourceDiscreteControlProfile;

  getResourcediscretecontrol(): ResourceDiscreteControl | undefined;
  setResourcediscretecontrol(value?: ResourceDiscreteControl): ResourceDiscreteControlProfile;
  hasResourcediscretecontrol(): boolean;
  clearResourcediscretecontrol(): ResourceDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceDiscreteControlProfile): ResourceDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: ResourceDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceDiscreteControlProfile;
  static deserializeBinaryFromReader(message: ResourceDiscreteControlProfile, reader: jspb.BinaryReader): ResourceDiscreteControlProfile;
}

export namespace ResourceDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    resourcediscretecontrol?: ResourceDiscreteControl.AsObject,
  }
}

export class ResourceReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): ResourceReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): ResourceReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): ResourceReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): ResourceReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): ResourceReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): ResourceReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): ResourceReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): ResourceReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceReading.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceReading): ResourceReading.AsObject;
  static serializeBinaryToWriter(message: ResourceReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceReading;
  static deserializeBinaryFromReader(message: ResourceReading, reader: jspb.BinaryReader): ResourceReading;
}

export namespace ResourceReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class ResourceReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): ResourceReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): ResourceReadingProfile;

  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): ResourceReadingProfile;
  hasConductingequipment(): boolean;
  clearConductingequipment(): ResourceReadingProfile;

  getResourcereading(): ResourceReading | undefined;
  setResourcereading(value?: ResourceReading): ResourceReadingProfile;
  hasResourcereading(): boolean;
  clearResourcereading(): ResourceReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceReadingProfile): ResourceReadingProfile.AsObject;
  static serializeBinaryToWriter(message: ResourceReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceReadingProfile;
  static deserializeBinaryFromReader(message: ResourceReadingProfile, reader: jspb.BinaryReader): ResourceReadingProfile;
}

export namespace ResourceReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    resourcereading?: ResourceReading.AsObject,
  }
}

export class ResourceEvent extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): ResourceEvent;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ResourceEvent;

  getAnalogeventandstatusggioList(): Array<commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO>;
  setAnalogeventandstatusggioList(value: Array<commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO>): ResourceEvent;
  clearAnalogeventandstatusggioList(): ResourceEvent;
  addAnalogeventandstatusggio(value?: commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO;

  getBooleaneventandstatusggioList(): Array<commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO>;
  setBooleaneventandstatusggioList(value: Array<commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO>): ResourceEvent;
  clearBooleaneventandstatusggioList(): ResourceEvent;
  addBooleaneventandstatusggio(value?: commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO;

  getIntegereventandstatusggioList(): Array<commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO>;
  setIntegereventandstatusggioList(value: Array<commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO>): ResourceEvent;
  clearIntegereventandstatusggioList(): ResourceEvent;
  addIntegereventandstatusggio(value?: commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO;

  getStringeventandstatusggioList(): Array<commonmodule_commonmodule_pb.StringEventAndStatusGGIO>;
  setStringeventandstatusggioList(value: Array<commonmodule_commonmodule_pb.StringEventAndStatusGGIO>): ResourceEvent;
  clearStringeventandstatusggioList(): ResourceEvent;
  addStringeventandstatusggio(value?: commonmodule_commonmodule_pb.StringEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.StringEventAndStatusGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceEvent.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceEvent): ResourceEvent.AsObject;
  static serializeBinaryToWriter(message: ResourceEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceEvent;
  static deserializeBinaryFromReader(message: ResourceEvent, reader: jspb.BinaryReader): ResourceEvent;
}

export namespace ResourceEvent {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    analogeventandstatusggioList: Array<commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO.AsObject>,
    booleaneventandstatusggioList: Array<commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO.AsObject>,
    integereventandstatusggioList: Array<commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO.AsObject>,
    stringeventandstatusggioList: Array<commonmodule_commonmodule_pb.StringEventAndStatusGGIO.AsObject>,
  }
}

export class ResourceEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): ResourceEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): ResourceEventProfile;

  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): ResourceEventProfile;
  hasConductingequipment(): boolean;
  clearConductingequipment(): ResourceEventProfile;

  getResourceevent(): ResourceEvent | undefined;
  setResourceevent(value?: ResourceEvent): ResourceEventProfile;
  hasResourceevent(): boolean;
  clearResourceevent(): ResourceEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceEventProfile): ResourceEventProfile.AsObject;
  static serializeBinaryToWriter(message: ResourceEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceEventProfile;
  static deserializeBinaryFromReader(message: ResourceEventProfile, reader: jspb.BinaryReader): ResourceEventProfile;
}

export namespace ResourceEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    resourceevent?: ResourceEvent.AsObject,
  }
}

export class ResourceStatus extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): ResourceStatus;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ResourceStatus;

  getAnalogeventandstatusggioList(): Array<commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO>;
  setAnalogeventandstatusggioList(value: Array<commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO>): ResourceStatus;
  clearAnalogeventandstatusggioList(): ResourceStatus;
  addAnalogeventandstatusggio(value?: commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO;

  getBooleaneventandstatusggioList(): Array<commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO>;
  setBooleaneventandstatusggioList(value: Array<commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO>): ResourceStatus;
  clearBooleaneventandstatusggioList(): ResourceStatus;
  addBooleaneventandstatusggio(value?: commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO;

  getIntegereventandstatusggioList(): Array<commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO>;
  setIntegereventandstatusggioList(value: Array<commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO>): ResourceStatus;
  clearIntegereventandstatusggioList(): ResourceStatus;
  addIntegereventandstatusggio(value?: commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO;

  getStringeventandstatusggioList(): Array<commonmodule_commonmodule_pb.StringEventAndStatusGGIO>;
  setStringeventandstatusggioList(value: Array<commonmodule_commonmodule_pb.StringEventAndStatusGGIO>): ResourceStatus;
  clearStringeventandstatusggioList(): ResourceStatus;
  addStringeventandstatusggio(value?: commonmodule_commonmodule_pb.StringEventAndStatusGGIO, index?: number): commonmodule_commonmodule_pb.StringEventAndStatusGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceStatus.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceStatus): ResourceStatus.AsObject;
  static serializeBinaryToWriter(message: ResourceStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceStatus;
  static deserializeBinaryFromReader(message: ResourceStatus, reader: jspb.BinaryReader): ResourceStatus;
}

export namespace ResourceStatus {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    analogeventandstatusggioList: Array<commonmodule_commonmodule_pb.AnalogEventAndStatusGGIO.AsObject>,
    booleaneventandstatusggioList: Array<commonmodule_commonmodule_pb.BooleanEventAndStatusGGIO.AsObject>,
    integereventandstatusggioList: Array<commonmodule_commonmodule_pb.IntegerEventAndStatusGGIO.AsObject>,
    stringeventandstatusggioList: Array<commonmodule_commonmodule_pb.StringEventAndStatusGGIO.AsObject>,
  }
}

export class ResourceStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): ResourceStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): ResourceStatusProfile;

  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): ResourceStatusProfile;
  hasConductingequipment(): boolean;
  clearConductingequipment(): ResourceStatusProfile;

  getResourcestatus(): ResourceStatus | undefined;
  setResourcestatus(value?: ResourceStatus): ResourceStatusProfile;
  hasResourcestatus(): boolean;
  clearResourcestatus(): ResourceStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceStatusProfile): ResourceStatusProfile.AsObject;
  static serializeBinaryToWriter(message: ResourceStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceStatusProfile;
  static deserializeBinaryFromReader(message: ResourceStatusProfile, reader: jspb.BinaryReader): ResourceStatusProfile;
}

export namespace ResourceStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    resourcestatus?: ResourceStatus.AsObject,
  }
}

