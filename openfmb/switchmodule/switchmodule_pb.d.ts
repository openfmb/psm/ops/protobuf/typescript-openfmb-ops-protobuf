import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class SwitchDiscreteControlXSWI extends jspb.Message {
  getLogicalnodeforcontrol(): commonmodule_commonmodule_pb.LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: commonmodule_commonmodule_pb.LogicalNodeForControl): SwitchDiscreteControlXSWI;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): SwitchDiscreteControlXSWI;

  getPos(): commonmodule_commonmodule_pb.PhaseDPC | undefined;
  setPos(value?: commonmodule_commonmodule_pb.PhaseDPC): SwitchDiscreteControlXSWI;
  hasPos(): boolean;
  clearPos(): SwitchDiscreteControlXSWI;

  getResetprotectionpickup(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setResetprotectionpickup(value?: commonmodule_commonmodule_pb.ControlSPC): SwitchDiscreteControlXSWI;
  hasResetprotectionpickup(): boolean;
  clearResetprotectionpickup(): SwitchDiscreteControlXSWI;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchDiscreteControlXSWI.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchDiscreteControlXSWI): SwitchDiscreteControlXSWI.AsObject;
  static serializeBinaryToWriter(message: SwitchDiscreteControlXSWI, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchDiscreteControlXSWI;
  static deserializeBinaryFromReader(message: SwitchDiscreteControlXSWI, reader: jspb.BinaryReader): SwitchDiscreteControlXSWI;
}

export namespace SwitchDiscreteControlXSWI {
  export type AsObject = {
    logicalnodeforcontrol?: commonmodule_commonmodule_pb.LogicalNodeForControl.AsObject,
    pos?: commonmodule_commonmodule_pb.PhaseDPC.AsObject,
    resetprotectionpickup?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
  }
}

export class SwitchDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): SwitchDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): SwitchDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): SwitchDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): SwitchDiscreteControl;

  getSwitchdiscretecontrolxswi(): SwitchDiscreteControlXSWI | undefined;
  setSwitchdiscretecontrolxswi(value?: SwitchDiscreteControlXSWI): SwitchDiscreteControl;
  hasSwitchdiscretecontrolxswi(): boolean;
  clearSwitchdiscretecontrolxswi(): SwitchDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchDiscreteControl): SwitchDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: SwitchDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchDiscreteControl;
  static deserializeBinaryFromReader(message: SwitchDiscreteControl, reader: jspb.BinaryReader): SwitchDiscreteControl;
}

export namespace SwitchDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    switchdiscretecontrolxswi?: SwitchDiscreteControlXSWI.AsObject,
  }
}

export class ProtectedSwitch extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): ProtectedSwitch;
  hasConductingequipment(): boolean;
  clearConductingequipment(): ProtectedSwitch;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProtectedSwitch.AsObject;
  static toObject(includeInstance: boolean, msg: ProtectedSwitch): ProtectedSwitch.AsObject;
  static serializeBinaryToWriter(message: ProtectedSwitch, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProtectedSwitch;
  static deserializeBinaryFromReader(message: ProtectedSwitch, reader: jspb.BinaryReader): ProtectedSwitch;
}

export namespace ProtectedSwitch {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
  }
}

export class SwitchDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): SwitchDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): SwitchDiscreteControlProfile;

  getProtectedswitch(): ProtectedSwitch | undefined;
  setProtectedswitch(value?: ProtectedSwitch): SwitchDiscreteControlProfile;
  hasProtectedswitch(): boolean;
  clearProtectedswitch(): SwitchDiscreteControlProfile;

  getSwitchdiscretecontrol(): SwitchDiscreteControl | undefined;
  setSwitchdiscretecontrol(value?: SwitchDiscreteControl): SwitchDiscreteControlProfile;
  hasSwitchdiscretecontrol(): boolean;
  clearSwitchdiscretecontrol(): SwitchDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchDiscreteControlProfile): SwitchDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: SwitchDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchDiscreteControlProfile;
  static deserializeBinaryFromReader(message: SwitchDiscreteControlProfile, reader: jspb.BinaryReader): SwitchDiscreteControlProfile;
}

export namespace SwitchDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    protectedswitch?: ProtectedSwitch.AsObject,
    switchdiscretecontrol?: SwitchDiscreteControl.AsObject,
  }
}

export class SwitchEventXSWI extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): SwitchEventXSWI;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): SwitchEventXSWI;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): SwitchEventXSWI;
  hasDynamictest(): boolean;
  clearDynamictest(): SwitchEventXSWI;

  getPos(): commonmodule_commonmodule_pb.PhaseDPS | undefined;
  setPos(value?: commonmodule_commonmodule_pb.PhaseDPS): SwitchEventXSWI;
  hasPos(): boolean;
  clearPos(): SwitchEventXSWI;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchEventXSWI.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchEventXSWI): SwitchEventXSWI.AsObject;
  static serializeBinaryToWriter(message: SwitchEventXSWI, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchEventXSWI;
  static deserializeBinaryFromReader(message: SwitchEventXSWI, reader: jspb.BinaryReader): SwitchEventXSWI;
}

export namespace SwitchEventXSWI {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    pos?: commonmodule_commonmodule_pb.PhaseDPS.AsObject,
  }
}

export class SwitchEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): SwitchEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): SwitchEvent;

  getSwitcheventxswi(): SwitchEventXSWI | undefined;
  setSwitcheventxswi(value?: SwitchEventXSWI): SwitchEvent;
  hasSwitcheventxswi(): boolean;
  clearSwitcheventxswi(): SwitchEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchEvent.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchEvent): SwitchEvent.AsObject;
  static serializeBinaryToWriter(message: SwitchEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchEvent;
  static deserializeBinaryFromReader(message: SwitchEvent, reader: jspb.BinaryReader): SwitchEvent;
}

export namespace SwitchEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    switcheventxswi?: SwitchEventXSWI.AsObject,
  }
}

export class SwitchEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): SwitchEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): SwitchEventProfile;

  getProtectedswitch(): ProtectedSwitch | undefined;
  setProtectedswitch(value?: ProtectedSwitch): SwitchEventProfile;
  hasProtectedswitch(): boolean;
  clearProtectedswitch(): SwitchEventProfile;

  getSwitchevent(): SwitchEvent | undefined;
  setSwitchevent(value?: SwitchEvent): SwitchEventProfile;
  hasSwitchevent(): boolean;
  clearSwitchevent(): SwitchEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchEventProfile): SwitchEventProfile.AsObject;
  static serializeBinaryToWriter(message: SwitchEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchEventProfile;
  static deserializeBinaryFromReader(message: SwitchEventProfile, reader: jspb.BinaryReader): SwitchEventProfile;
}

export namespace SwitchEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    protectedswitch?: ProtectedSwitch.AsObject,
    switchevent?: SwitchEvent.AsObject,
  }
}

export class SwitchReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): SwitchReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): SwitchReading;

  getDiffreadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setDiffreadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): SwitchReading;
  hasDiffreadingmmxu(): boolean;
  clearDiffreadingmmxu(): SwitchReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): SwitchReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): SwitchReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): SwitchReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): SwitchReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): SwitchReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): SwitchReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchReading.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchReading): SwitchReading.AsObject;
  static serializeBinaryToWriter(message: SwitchReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchReading;
  static deserializeBinaryFromReader(message: SwitchReading, reader: jspb.BinaryReader): SwitchReading;
}

export namespace SwitchReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    diffreadingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class SwitchReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): SwitchReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): SwitchReadingProfile;

  getProtectedswitch(): ProtectedSwitch | undefined;
  setProtectedswitch(value?: ProtectedSwitch): SwitchReadingProfile;
  hasProtectedswitch(): boolean;
  clearProtectedswitch(): SwitchReadingProfile;

  getSwitchreadingList(): Array<SwitchReading>;
  setSwitchreadingList(value: Array<SwitchReading>): SwitchReadingProfile;
  clearSwitchreadingList(): SwitchReadingProfile;
  addSwitchreading(value?: SwitchReading, index?: number): SwitchReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchReadingProfile): SwitchReadingProfile.AsObject;
  static serializeBinaryToWriter(message: SwitchReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchReadingProfile;
  static deserializeBinaryFromReader(message: SwitchReadingProfile, reader: jspb.BinaryReader): SwitchReadingProfile;
}

export namespace SwitchReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    protectedswitch?: ProtectedSwitch.AsObject,
    switchreadingList: Array<SwitchReading.AsObject>,
  }
}

export class SwitchStatusXSWI extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): SwitchStatusXSWI;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): SwitchStatusXSWI;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): SwitchStatusXSWI;
  hasDynamictest(): boolean;
  clearDynamictest(): SwitchStatusXSWI;

  getPos(): commonmodule_commonmodule_pb.PhaseDPS | undefined;
  setPos(value?: commonmodule_commonmodule_pb.PhaseDPS): SwitchStatusXSWI;
  hasPos(): boolean;
  clearPos(): SwitchStatusXSWI;

  getProtectionpickup(): commonmodule_commonmodule_pb.PhaseSPS | undefined;
  setProtectionpickup(value?: commonmodule_commonmodule_pb.PhaseSPS): SwitchStatusXSWI;
  hasProtectionpickup(): boolean;
  clearProtectionpickup(): SwitchStatusXSWI;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchStatusXSWI.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchStatusXSWI): SwitchStatusXSWI.AsObject;
  static serializeBinaryToWriter(message: SwitchStatusXSWI, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchStatusXSWI;
  static deserializeBinaryFromReader(message: SwitchStatusXSWI, reader: jspb.BinaryReader): SwitchStatusXSWI;
}

export namespace SwitchStatusXSWI {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    pos?: commonmodule_commonmodule_pb.PhaseDPS.AsObject,
    protectionpickup?: commonmodule_commonmodule_pb.PhaseSPS.AsObject,
  }
}

export class SwitchStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): SwitchStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): SwitchStatus;

  getSwitchstatusxswi(): SwitchStatusXSWI | undefined;
  setSwitchstatusxswi(value?: SwitchStatusXSWI): SwitchStatus;
  hasSwitchstatusxswi(): boolean;
  clearSwitchstatusxswi(): SwitchStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchStatus.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchStatus): SwitchStatus.AsObject;
  static serializeBinaryToWriter(message: SwitchStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchStatus;
  static deserializeBinaryFromReader(message: SwitchStatus, reader: jspb.BinaryReader): SwitchStatus;
}

export namespace SwitchStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    switchstatusxswi?: SwitchStatusXSWI.AsObject,
  }
}

export class SwitchStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): SwitchStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): SwitchStatusProfile;

  getProtectedswitch(): ProtectedSwitch | undefined;
  setProtectedswitch(value?: ProtectedSwitch): SwitchStatusProfile;
  hasProtectedswitch(): boolean;
  clearProtectedswitch(): SwitchStatusProfile;

  getSwitchstatus(): SwitchStatus | undefined;
  setSwitchstatus(value?: SwitchStatus): SwitchStatusProfile;
  hasSwitchstatus(): boolean;
  clearSwitchstatus(): SwitchStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchStatusProfile): SwitchStatusProfile.AsObject;
  static serializeBinaryToWriter(message: SwitchStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchStatusProfile;
  static deserializeBinaryFromReader(message: SwitchStatusProfile, reader: jspb.BinaryReader): SwitchStatusProfile;
}

export namespace SwitchStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    protectedswitch?: ProtectedSwitch.AsObject,
    switchstatus?: SwitchStatus.AsObject,
  }
}

