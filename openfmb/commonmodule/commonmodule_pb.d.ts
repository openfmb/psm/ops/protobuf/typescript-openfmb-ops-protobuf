import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';


export class Optional_FaultDirectionKind extends jspb.Message {
  getValue(): FaultDirectionKind;
  setValue(value: FaultDirectionKind): Optional_FaultDirectionKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_FaultDirectionKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_FaultDirectionKind): Optional_FaultDirectionKind.AsObject;
  static serializeBinaryToWriter(message: Optional_FaultDirectionKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_FaultDirectionKind;
  static deserializeBinaryFromReader(message: Optional_FaultDirectionKind, reader: jspb.BinaryReader): Optional_FaultDirectionKind;
}

export namespace Optional_FaultDirectionKind {
  export type AsObject = {
    value: FaultDirectionKind,
  }
}

export class Optional_PhaseFaultDirectionKind extends jspb.Message {
  getValue(): PhaseFaultDirectionKind;
  setValue(value: PhaseFaultDirectionKind): Optional_PhaseFaultDirectionKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_PhaseFaultDirectionKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_PhaseFaultDirectionKind): Optional_PhaseFaultDirectionKind.AsObject;
  static serializeBinaryToWriter(message: Optional_PhaseFaultDirectionKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_PhaseFaultDirectionKind;
  static deserializeBinaryFromReader(message: Optional_PhaseFaultDirectionKind, reader: jspb.BinaryReader): Optional_PhaseFaultDirectionKind;
}

export namespace Optional_PhaseFaultDirectionKind {
  export type AsObject = {
    value: PhaseFaultDirectionKind,
  }
}

export class ACD extends jspb.Message {
  getDirgeneral(): FaultDirectionKind;
  setDirgeneral(value: FaultDirectionKind): ACD;

  getDirneut(): Optional_PhaseFaultDirectionKind | undefined;
  setDirneut(value?: Optional_PhaseFaultDirectionKind): ACD;
  hasDirneut(): boolean;
  clearDirneut(): ACD;

  getDirphsa(): Optional_PhaseFaultDirectionKind | undefined;
  setDirphsa(value?: Optional_PhaseFaultDirectionKind): ACD;
  hasDirphsa(): boolean;
  clearDirphsa(): ACD;

  getDirphsb(): Optional_PhaseFaultDirectionKind | undefined;
  setDirphsb(value?: Optional_PhaseFaultDirectionKind): ACD;
  hasDirphsb(): boolean;
  clearDirphsb(): ACD;

  getDirphsc(): Optional_PhaseFaultDirectionKind | undefined;
  setDirphsc(value?: Optional_PhaseFaultDirectionKind): ACD;
  hasDirphsc(): boolean;
  clearDirphsc(): ACD;

  getGeneral(): boolean;
  setGeneral(value: boolean): ACD;

  getNeut(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setNeut(value?: google_protobuf_wrappers_pb.BoolValue): ACD;
  hasNeut(): boolean;
  clearNeut(): ACD;

  getPhsa(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setPhsa(value?: google_protobuf_wrappers_pb.BoolValue): ACD;
  hasPhsa(): boolean;
  clearPhsa(): ACD;

  getPhsb(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setPhsb(value?: google_protobuf_wrappers_pb.BoolValue): ACD;
  hasPhsb(): boolean;
  clearPhsb(): ACD;

  getPhsc(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setPhsc(value?: google_protobuf_wrappers_pb.BoolValue): ACD;
  hasPhsc(): boolean;
  clearPhsc(): ACD;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ACD.AsObject;
  static toObject(includeInstance: boolean, msg: ACD): ACD.AsObject;
  static serializeBinaryToWriter(message: ACD, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ACD;
  static deserializeBinaryFromReader(message: ACD, reader: jspb.BinaryReader): ACD;
}

export namespace ACD {
  export type AsObject = {
    dirgeneral: FaultDirectionKind,
    dirneut?: Optional_PhaseFaultDirectionKind.AsObject,
    dirphsa?: Optional_PhaseFaultDirectionKind.AsObject,
    dirphsb?: Optional_PhaseFaultDirectionKind.AsObject,
    dirphsc?: Optional_PhaseFaultDirectionKind.AsObject,
    general: boolean,
    neut?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    phsa?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    phsb?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    phsc?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class IdentifiedObject extends jspb.Message {
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): IdentifiedObject;
  hasDescription(): boolean;
  clearDescription(): IdentifiedObject;

  getMrid(): google_protobuf_wrappers_pb.StringValue | undefined;
  setMrid(value?: google_protobuf_wrappers_pb.StringValue): IdentifiedObject;
  hasMrid(): boolean;
  clearMrid(): IdentifiedObject;

  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): IdentifiedObject;
  hasName(): boolean;
  clearName(): IdentifiedObject;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): IdentifiedObject.AsObject;
  static toObject(includeInstance: boolean, msg: IdentifiedObject): IdentifiedObject.AsObject;
  static serializeBinaryToWriter(message: IdentifiedObject, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): IdentifiedObject;
  static deserializeBinaryFromReader(message: IdentifiedObject, reader: jspb.BinaryReader): IdentifiedObject;
}

export namespace IdentifiedObject {
  export type AsObject = {
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    mrid?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ACDCTerminal extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): ACDCTerminal;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ACDCTerminal;

  getConnected(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setConnected(value?: google_protobuf_wrappers_pb.BoolValue): ACDCTerminal;
  hasConnected(): boolean;
  clearConnected(): ACDCTerminal;

  getSequencenumber(): google_protobuf_wrappers_pb.Int32Value | undefined;
  setSequencenumber(value?: google_protobuf_wrappers_pb.Int32Value): ACDCTerminal;
  hasSequencenumber(): boolean;
  clearSequencenumber(): ACDCTerminal;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ACDCTerminal.AsObject;
  static toObject(includeInstance: boolean, msg: ACDCTerminal): ACDCTerminal.AsObject;
  static serializeBinaryToWriter(message: ACDCTerminal, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ACDCTerminal;
  static deserializeBinaryFromReader(message: ACDCTerminal, reader: jspb.BinaryReader): ACDCTerminal;
}

export namespace ACDCTerminal {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
    connected?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    sequencenumber?: google_protobuf_wrappers_pb.Int32Value.AsObject,
  }
}

export class Optional_UnitSymbolKind extends jspb.Message {
  getValue(): UnitSymbolKind;
  setValue(value: UnitSymbolKind): Optional_UnitSymbolKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_UnitSymbolKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_UnitSymbolKind): Optional_UnitSymbolKind.AsObject;
  static serializeBinaryToWriter(message: Optional_UnitSymbolKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_UnitSymbolKind;
  static deserializeBinaryFromReader(message: Optional_UnitSymbolKind, reader: jspb.BinaryReader): Optional_UnitSymbolKind;
}

export namespace Optional_UnitSymbolKind {
  export type AsObject = {
    value: UnitSymbolKind,
  }
}

export class Optional_UnitMultiplierKind extends jspb.Message {
  getValue(): UnitMultiplierKind;
  setValue(value: UnitMultiplierKind): Optional_UnitMultiplierKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_UnitMultiplierKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_UnitMultiplierKind): Optional_UnitMultiplierKind.AsObject;
  static serializeBinaryToWriter(message: Optional_UnitMultiplierKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_UnitMultiplierKind;
  static deserializeBinaryFromReader(message: Optional_UnitMultiplierKind, reader: jspb.BinaryReader): Optional_UnitMultiplierKind;
}

export namespace Optional_UnitMultiplierKind {
  export type AsObject = {
    value: UnitMultiplierKind,
  }
}

export class ActivePower extends jspb.Message {
  getMultiplier(): Optional_UnitMultiplierKind | undefined;
  setMultiplier(value?: Optional_UnitMultiplierKind): ActivePower;
  hasMultiplier(): boolean;
  clearMultiplier(): ActivePower;

  getUnit(): Optional_UnitSymbolKind | undefined;
  setUnit(value?: Optional_UnitSymbolKind): ActivePower;
  hasUnit(): boolean;
  clearUnit(): ActivePower;

  getValue(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setValue(value?: google_protobuf_wrappers_pb.FloatValue): ActivePower;
  hasValue(): boolean;
  clearValue(): ActivePower;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivePower.AsObject;
  static toObject(includeInstance: boolean, msg: ActivePower): ActivePower.AsObject;
  static serializeBinaryToWriter(message: ActivePower, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivePower;
  static deserializeBinaryFromReader(message: ActivePower, reader: jspb.BinaryReader): ActivePower;
}

export namespace ActivePower {
  export type AsObject = {
    multiplier?: Optional_UnitMultiplierKind.AsObject,
    unit?: Optional_UnitSymbolKind.AsObject,
    value?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class Optional_PhaseCodeKind extends jspb.Message {
  getValue(): PhaseCodeKind;
  setValue(value: PhaseCodeKind): Optional_PhaseCodeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_PhaseCodeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_PhaseCodeKind): Optional_PhaseCodeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_PhaseCodeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_PhaseCodeKind;
  static deserializeBinaryFromReader(message: Optional_PhaseCodeKind, reader: jspb.BinaryReader): Optional_PhaseCodeKind;
}

export namespace Optional_PhaseCodeKind {
  export type AsObject = {
    value: PhaseCodeKind,
  }
}

export class Unit extends jspb.Message {
  getMultiplier(): Optional_UnitMultiplierKind | undefined;
  setMultiplier(value?: Optional_UnitMultiplierKind): Unit;
  hasMultiplier(): boolean;
  clearMultiplier(): Unit;

  getSiunit(): UnitSymbolKind;
  setSiunit(value: UnitSymbolKind): Unit;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Unit.AsObject;
  static toObject(includeInstance: boolean, msg: Unit): Unit.AsObject;
  static serializeBinaryToWriter(message: Unit, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Unit;
  static deserializeBinaryFromReader(message: Unit, reader: jspb.BinaryReader): Unit;
}

export namespace Unit {
  export type AsObject = {
    multiplier?: Optional_UnitMultiplierKind.AsObject,
    siunit: UnitSymbolKind,
  }
}

export class Optional_ValidityKind extends jspb.Message {
  getValue(): ValidityKind;
  setValue(value: ValidityKind): Optional_ValidityKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_ValidityKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_ValidityKind): Optional_ValidityKind.AsObject;
  static serializeBinaryToWriter(message: Optional_ValidityKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_ValidityKind;
  static deserializeBinaryFromReader(message: Optional_ValidityKind, reader: jspb.BinaryReader): Optional_ValidityKind;
}

export namespace Optional_ValidityKind {
  export type AsObject = {
    value: ValidityKind,
  }
}

export class DetailQual extends jspb.Message {
  getBadreference(): boolean;
  setBadreference(value: boolean): DetailQual;

  getFailure(): boolean;
  setFailure(value: boolean): DetailQual;

  getInaccurate(): boolean;
  setInaccurate(value: boolean): DetailQual;

  getInconsistent(): boolean;
  setInconsistent(value: boolean): DetailQual;

  getOlddata(): boolean;
  setOlddata(value: boolean): DetailQual;

  getOscillatory(): boolean;
  setOscillatory(value: boolean): DetailQual;

  getOutofrange(): boolean;
  setOutofrange(value: boolean): DetailQual;

  getOverflow(): boolean;
  setOverflow(value: boolean): DetailQual;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DetailQual.AsObject;
  static toObject(includeInstance: boolean, msg: DetailQual): DetailQual.AsObject;
  static serializeBinaryToWriter(message: DetailQual, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DetailQual;
  static deserializeBinaryFromReader(message: DetailQual, reader: jspb.BinaryReader): DetailQual;
}

export namespace DetailQual {
  export type AsObject = {
    badreference: boolean,
    failure: boolean,
    inaccurate: boolean,
    inconsistent: boolean,
    olddata: boolean,
    oscillatory: boolean,
    outofrange: boolean,
    overflow: boolean,
  }
}

export class Optional_SourceKind extends jspb.Message {
  getValue(): SourceKind;
  setValue(value: SourceKind): Optional_SourceKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_SourceKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_SourceKind): Optional_SourceKind.AsObject;
  static serializeBinaryToWriter(message: Optional_SourceKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_SourceKind;
  static deserializeBinaryFromReader(message: Optional_SourceKind, reader: jspb.BinaryReader): Optional_SourceKind;
}

export namespace Optional_SourceKind {
  export type AsObject = {
    value: SourceKind,
  }
}

export class Quality extends jspb.Message {
  getDetailqual(): DetailQual | undefined;
  setDetailqual(value?: DetailQual): Quality;
  hasDetailqual(): boolean;
  clearDetailqual(): Quality;

  getOperatorblocked(): boolean;
  setOperatorblocked(value: boolean): Quality;

  getSource(): SourceKind;
  setSource(value: SourceKind): Quality;

  getTest(): boolean;
  setTest(value: boolean): Quality;

  getValidity(): ValidityKind;
  setValidity(value: ValidityKind): Quality;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Quality.AsObject;
  static toObject(includeInstance: boolean, msg: Quality): Quality.AsObject;
  static serializeBinaryToWriter(message: Quality, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Quality;
  static deserializeBinaryFromReader(message: Quality, reader: jspb.BinaryReader): Quality;
}

export namespace Quality {
  export type AsObject = {
    detailqual?: DetailQual.AsObject,
    operatorblocked: boolean,
    source: SourceKind,
    test: boolean,
    validity: ValidityKind,
  }
}

export class Optional_TimeAccuracyKind extends jspb.Message {
  getValue(): TimeAccuracyKind;
  setValue(value: TimeAccuracyKind): Optional_TimeAccuracyKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_TimeAccuracyKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_TimeAccuracyKind): Optional_TimeAccuracyKind.AsObject;
  static serializeBinaryToWriter(message: Optional_TimeAccuracyKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_TimeAccuracyKind;
  static deserializeBinaryFromReader(message: Optional_TimeAccuracyKind, reader: jspb.BinaryReader): Optional_TimeAccuracyKind;
}

export namespace Optional_TimeAccuracyKind {
  export type AsObject = {
    value: TimeAccuracyKind,
  }
}

export class TimeQuality extends jspb.Message {
  getClockfailure(): boolean;
  setClockfailure(value: boolean): TimeQuality;

  getClocknotsynchronized(): boolean;
  setClocknotsynchronized(value: boolean): TimeQuality;

  getLeapsecondsknown(): boolean;
  setLeapsecondsknown(value: boolean): TimeQuality;

  getTimeaccuracy(): TimeAccuracyKind;
  setTimeaccuracy(value: TimeAccuracyKind): TimeQuality;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TimeQuality.AsObject;
  static toObject(includeInstance: boolean, msg: TimeQuality): TimeQuality.AsObject;
  static serializeBinaryToWriter(message: TimeQuality, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TimeQuality;
  static deserializeBinaryFromReader(message: TimeQuality, reader: jspb.BinaryReader): TimeQuality;
}

export namespace TimeQuality {
  export type AsObject = {
    clockfailure: boolean,
    clocknotsynchronized: boolean,
    leapsecondsknown: boolean,
    timeaccuracy: TimeAccuracyKind,
  }
}

export class Timestamp extends jspb.Message {
  getSeconds(): number;
  setSeconds(value: number): Timestamp;

  getTq(): TimeQuality | undefined;
  setTq(value?: TimeQuality): Timestamp;
  hasTq(): boolean;
  clearTq(): Timestamp;

  getNanoseconds(): number;
  setNanoseconds(value: number): Timestamp;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Timestamp.AsObject;
  static toObject(includeInstance: boolean, msg: Timestamp): Timestamp.AsObject;
  static serializeBinaryToWriter(message: Timestamp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Timestamp;
  static deserializeBinaryFromReader(message: Timestamp, reader: jspb.BinaryReader): Timestamp;
}

export namespace Timestamp {
  export type AsObject = {
    seconds: number,
    tq?: TimeQuality.AsObject,
    nanoseconds: number,
  }
}

export class MV extends jspb.Message {
  getMag(): number;
  setMag(value: number): MV;

  getQ(): Quality | undefined;
  setQ(value?: Quality): MV;
  hasQ(): boolean;
  clearQ(): MV;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): MV;
  hasT(): boolean;
  clearT(): MV;

  getUnits(): Unit | undefined;
  setUnits(value?: Unit): MV;
  hasUnits(): boolean;
  clearUnits(): MV;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MV.AsObject;
  static toObject(includeInstance: boolean, msg: MV): MV.AsObject;
  static serializeBinaryToWriter(message: MV, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MV;
  static deserializeBinaryFromReader(message: MV, reader: jspb.BinaryReader): MV;
}

export namespace MV {
  export type AsObject = {
    mag: number,
    q?: Quality.AsObject,
    t?: Timestamp.AsObject,
    units?: Unit.AsObject,
  }
}

export class LogicalNode extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): LogicalNode;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): LogicalNode;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LogicalNode.AsObject;
  static toObject(includeInstance: boolean, msg: LogicalNode): LogicalNode.AsObject;
  static serializeBinaryToWriter(message: LogicalNode, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LogicalNode;
  static deserializeBinaryFromReader(message: LogicalNode, reader: jspb.BinaryReader): LogicalNode;
}

export namespace LogicalNode {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
  }
}

export class AnalogEventAndStatusGGIO extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): AnalogEventAndStatusGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): AnalogEventAndStatusGGIO;

  getAnin(): MV | undefined;
  setAnin(value?: MV): AnalogEventAndStatusGGIO;
  hasAnin(): boolean;
  clearAnin(): AnalogEventAndStatusGGIO;

  getPhase(): Optional_PhaseCodeKind | undefined;
  setPhase(value?: Optional_PhaseCodeKind): AnalogEventAndStatusGGIO;
  hasPhase(): boolean;
  clearPhase(): AnalogEventAndStatusGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AnalogEventAndStatusGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: AnalogEventAndStatusGGIO): AnalogEventAndStatusGGIO.AsObject;
  static serializeBinaryToWriter(message: AnalogEventAndStatusGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AnalogEventAndStatusGGIO;
  static deserializeBinaryFromReader(message: AnalogEventAndStatusGGIO, reader: jspb.BinaryReader): AnalogEventAndStatusGGIO;
}

export namespace AnalogEventAndStatusGGIO {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    anin?: MV.AsObject,
    phase?: Optional_PhaseCodeKind.AsObject,
  }
}

export class NamedObject extends jspb.Message {
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): NamedObject;
  hasDescription(): boolean;
  clearDescription(): NamedObject;

  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): NamedObject;
  hasName(): boolean;
  clearName(): NamedObject;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NamedObject.AsObject;
  static toObject(includeInstance: boolean, msg: NamedObject): NamedObject.AsObject;
  static serializeBinaryToWriter(message: NamedObject, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NamedObject;
  static deserializeBinaryFromReader(message: NamedObject, reader: jspb.BinaryReader): NamedObject;
}

export namespace NamedObject {
  export type AsObject = {
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ApplicationSystem extends jspb.Message {
  getNamedobject(): NamedObject | undefined;
  setNamedobject(value?: NamedObject): ApplicationSystem;
  hasNamedobject(): boolean;
  clearNamedobject(): ApplicationSystem;

  getMrid(): string;
  setMrid(value: string): ApplicationSystem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplicationSystem.AsObject;
  static toObject(includeInstance: boolean, msg: ApplicationSystem): ApplicationSystem.AsObject;
  static serializeBinaryToWriter(message: ApplicationSystem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplicationSystem;
  static deserializeBinaryFromReader(message: ApplicationSystem, reader: jspb.BinaryReader): ApplicationSystem;
}

export namespace ApplicationSystem {
  export type AsObject = {
    namedobject?: NamedObject.AsObject,
    mrid: string,
  }
}

export class ASG extends jspb.Message {
  getSetmag(): number;
  setSetmag(value: number): ASG;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ASG.AsObject;
  static toObject(includeInstance: boolean, msg: ASG): ASG.AsObject;
  static serializeBinaryToWriter(message: ASG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ASG;
  static deserializeBinaryFromReader(message: ASG, reader: jspb.BinaryReader): ASG;
}

export namespace ASG {
  export type AsObject = {
    setmag: number,
  }
}

export class BCR extends jspb.Message {
  getActval(): number;
  setActval(value: number): BCR;

  getQ(): Quality | undefined;
  setQ(value?: Quality): BCR;
  hasQ(): boolean;
  clearQ(): BCR;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): BCR;
  hasT(): boolean;
  clearT(): BCR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BCR.AsObject;
  static toObject(includeInstance: boolean, msg: BCR): BCR.AsObject;
  static serializeBinaryToWriter(message: BCR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BCR;
  static deserializeBinaryFromReader(message: BCR, reader: jspb.BinaryReader): BCR;
}

export namespace BCR {
  export type AsObject = {
    actval: number,
    q?: Quality.AsObject,
    t?: Timestamp.AsObject,
  }
}

export class StatusSPS extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): StatusSPS;
  hasQ(): boolean;
  clearQ(): StatusSPS;

  getStval(): boolean;
  setStval(value: boolean): StatusSPS;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): StatusSPS;
  hasT(): boolean;
  clearT(): StatusSPS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusSPS.AsObject;
  static toObject(includeInstance: boolean, msg: StatusSPS): StatusSPS.AsObject;
  static serializeBinaryToWriter(message: StatusSPS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusSPS;
  static deserializeBinaryFromReader(message: StatusSPS, reader: jspb.BinaryReader): StatusSPS;
}

export namespace StatusSPS {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: boolean,
    t?: Timestamp.AsObject,
  }
}

export class BooleanEventAndStatusGGIO extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): BooleanEventAndStatusGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): BooleanEventAndStatusGGIO;

  getInd(): StatusSPS | undefined;
  setInd(value?: StatusSPS): BooleanEventAndStatusGGIO;
  hasInd(): boolean;
  clearInd(): BooleanEventAndStatusGGIO;

  getPhase(): Optional_PhaseCodeKind | undefined;
  setPhase(value?: Optional_PhaseCodeKind): BooleanEventAndStatusGGIO;
  hasPhase(): boolean;
  clearPhase(): BooleanEventAndStatusGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BooleanEventAndStatusGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: BooleanEventAndStatusGGIO): BooleanEventAndStatusGGIO.AsObject;
  static serializeBinaryToWriter(message: BooleanEventAndStatusGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BooleanEventAndStatusGGIO;
  static deserializeBinaryFromReader(message: BooleanEventAndStatusGGIO, reader: jspb.BinaryReader): BooleanEventAndStatusGGIO;
}

export namespace BooleanEventAndStatusGGIO {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    ind?: StatusSPS.AsObject,
    phase?: Optional_PhaseCodeKind.AsObject,
  }
}

export class MessageInfo extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): MessageInfo;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): MessageInfo;

  getMessagetimestamp(): Timestamp | undefined;
  setMessagetimestamp(value?: Timestamp): MessageInfo;
  hasMessagetimestamp(): boolean;
  clearMessagetimestamp(): MessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: MessageInfo): MessageInfo.AsObject;
  static serializeBinaryToWriter(message: MessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageInfo;
  static deserializeBinaryFromReader(message: MessageInfo, reader: jspb.BinaryReader): MessageInfo;
}

export namespace MessageInfo {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
    messagetimestamp?: Timestamp.AsObject,
  }
}

export class CapabilityMessageInfo extends jspb.Message {
  getMessageinfo(): MessageInfo | undefined;
  setMessageinfo(value?: MessageInfo): CapabilityMessageInfo;
  hasMessageinfo(): boolean;
  clearMessageinfo(): CapabilityMessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapabilityMessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: CapabilityMessageInfo): CapabilityMessageInfo.AsObject;
  static serializeBinaryToWriter(message: CapabilityMessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapabilityMessageInfo;
  static deserializeBinaryFromReader(message: CapabilityMessageInfo, reader: jspb.BinaryReader): CapabilityMessageInfo;
}

export namespace CapabilityMessageInfo {
  export type AsObject = {
    messageinfo?: MessageInfo.AsObject,
  }
}

export class CheckConditions extends jspb.Message {
  getInterlockcheck(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setInterlockcheck(value?: google_protobuf_wrappers_pb.BoolValue): CheckConditions;
  hasInterlockcheck(): boolean;
  clearInterlockcheck(): CheckConditions;

  getSynchrocheck(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setSynchrocheck(value?: google_protobuf_wrappers_pb.BoolValue): CheckConditions;
  hasSynchrocheck(): boolean;
  clearSynchrocheck(): CheckConditions;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckConditions.AsObject;
  static toObject(includeInstance: boolean, msg: CheckConditions): CheckConditions.AsObject;
  static serializeBinaryToWriter(message: CheckConditions, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckConditions;
  static deserializeBinaryFromReader(message: CheckConditions, reader: jspb.BinaryReader): CheckConditions;
}

export namespace CheckConditions {
  export type AsObject = {
    interlockcheck?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    synchrocheck?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ClearingTime extends jspb.Message {
  getSeconds(): number;
  setSeconds(value: number): ClearingTime;

  getNanoseconds(): number;
  setNanoseconds(value: number): ClearingTime;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClearingTime.AsObject;
  static toObject(includeInstance: boolean, msg: ClearingTime): ClearingTime.AsObject;
  static serializeBinaryToWriter(message: ClearingTime, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClearingTime;
  static deserializeBinaryFromReader(message: ClearingTime, reader: jspb.BinaryReader): ClearingTime;
}

export namespace ClearingTime {
  export type AsObject = {
    seconds: number,
    nanoseconds: number,
  }
}

export class Vector extends jspb.Message {
  getAng(): google_protobuf_wrappers_pb.DoubleValue | undefined;
  setAng(value?: google_protobuf_wrappers_pb.DoubleValue): Vector;
  hasAng(): boolean;
  clearAng(): Vector;

  getMag(): number;
  setMag(value: number): Vector;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Vector.AsObject;
  static toObject(includeInstance: boolean, msg: Vector): Vector.AsObject;
  static serializeBinaryToWriter(message: Vector, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Vector;
  static deserializeBinaryFromReader(message: Vector, reader: jspb.BinaryReader): Vector;
}

export namespace Vector {
  export type AsObject = {
    ang?: google_protobuf_wrappers_pb.DoubleValue.AsObject,
    mag: number,
  }
}

export class CMV extends jspb.Message {
  getCval(): Vector | undefined;
  setCval(value?: Vector): CMV;
  hasCval(): boolean;
  clearCval(): CMV;

  getQ(): Quality | undefined;
  setQ(value?: Quality): CMV;
  hasQ(): boolean;
  clearQ(): CMV;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): CMV;
  hasT(): boolean;
  clearT(): CMV;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CMV.AsObject;
  static toObject(includeInstance: boolean, msg: CMV): CMV.AsObject;
  static serializeBinaryToWriter(message: CMV, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CMV;
  static deserializeBinaryFromReader(message: CMV, reader: jspb.BinaryReader): CMV;
}

export namespace CMV {
  export type AsObject = {
    cval?: Vector.AsObject,
    q?: Quality.AsObject,
    t?: Timestamp.AsObject,
  }
}

export class ConductingEquipment extends jspb.Message {
  getNamedobject(): NamedObject | undefined;
  setNamedobject(value?: NamedObject): ConductingEquipment;
  hasNamedobject(): boolean;
  clearNamedobject(): ConductingEquipment;

  getMrid(): string;
  setMrid(value: string): ConductingEquipment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConductingEquipment.AsObject;
  static toObject(includeInstance: boolean, msg: ConductingEquipment): ConductingEquipment.AsObject;
  static serializeBinaryToWriter(message: ConductingEquipment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConductingEquipment;
  static deserializeBinaryFromReader(message: ConductingEquipment, reader: jspb.BinaryReader): ConductingEquipment;
}

export namespace ConductingEquipment {
  export type AsObject = {
    namedobject?: NamedObject.AsObject,
    mrid: string,
  }
}

export class Terminal extends jspb.Message {
  getAcdcterminal(): ACDCTerminal | undefined;
  setAcdcterminal(value?: ACDCTerminal): Terminal;
  hasAcdcterminal(): boolean;
  clearAcdcterminal(): Terminal;

  getPhases(): Optional_PhaseCodeKind | undefined;
  setPhases(value?: Optional_PhaseCodeKind): Terminal;
  hasPhases(): boolean;
  clearPhases(): Terminal;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Terminal.AsObject;
  static toObject(includeInstance: boolean, msg: Terminal): Terminal.AsObject;
  static serializeBinaryToWriter(message: Terminal, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Terminal;
  static deserializeBinaryFromReader(message: Terminal, reader: jspb.BinaryReader): Terminal;
}

export namespace Terminal {
  export type AsObject = {
    acdcterminal?: ACDCTerminal.AsObject,
    phases?: Optional_PhaseCodeKind.AsObject,
  }
}

export class ConductingEquipmentTerminalReading extends jspb.Message {
  getTerminal(): Terminal | undefined;
  setTerminal(value?: Terminal): ConductingEquipmentTerminalReading;
  hasTerminal(): boolean;
  clearTerminal(): ConductingEquipmentTerminalReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConductingEquipmentTerminalReading.AsObject;
  static toObject(includeInstance: boolean, msg: ConductingEquipmentTerminalReading): ConductingEquipmentTerminalReading.AsObject;
  static serializeBinaryToWriter(message: ConductingEquipmentTerminalReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConductingEquipmentTerminalReading;
  static deserializeBinaryFromReader(message: ConductingEquipmentTerminalReading, reader: jspb.BinaryReader): ConductingEquipmentTerminalReading;
}

export namespace ConductingEquipmentTerminalReading {
  export type AsObject = {
    terminal?: Terminal.AsObject,
  }
}

export class ControlAPC extends jspb.Message {
  getCtlval(): number;
  setCtlval(value: number): ControlAPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlAPC.AsObject;
  static toObject(includeInstance: boolean, msg: ControlAPC): ControlAPC.AsObject;
  static serializeBinaryToWriter(message: ControlAPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlAPC;
  static deserializeBinaryFromReader(message: ControlAPC, reader: jspb.BinaryReader): ControlAPC;
}

export namespace ControlAPC {
  export type AsObject = {
    ctlval: number,
  }
}

export class ControlDPC extends jspb.Message {
  getCtlval(): boolean;
  setCtlval(value: boolean): ControlDPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlDPC.AsObject;
  static toObject(includeInstance: boolean, msg: ControlDPC): ControlDPC.AsObject;
  static serializeBinaryToWriter(message: ControlDPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlDPC;
  static deserializeBinaryFromReader(message: ControlDPC, reader: jspb.BinaryReader): ControlDPC;
}

export namespace ControlDPC {
  export type AsObject = {
    ctlval: boolean,
  }
}

export class ControlTimestamp extends jspb.Message {
  getSeconds(): number;
  setSeconds(value: number): ControlTimestamp;

  getNanoseconds(): number;
  setNanoseconds(value: number): ControlTimestamp;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlTimestamp.AsObject;
  static toObject(includeInstance: boolean, msg: ControlTimestamp): ControlTimestamp.AsObject;
  static serializeBinaryToWriter(message: ControlTimestamp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlTimestamp;
  static deserializeBinaryFromReader(message: ControlTimestamp, reader: jspb.BinaryReader): ControlTimestamp;
}

export namespace ControlTimestamp {
  export type AsObject = {
    seconds: number,
    nanoseconds: number,
  }
}

export class Optional_ScheduleParameterKind extends jspb.Message {
  getValue(): ScheduleParameterKind;
  setValue(value: ScheduleParameterKind): Optional_ScheduleParameterKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_ScheduleParameterKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_ScheduleParameterKind): Optional_ScheduleParameterKind.AsObject;
  static serializeBinaryToWriter(message: Optional_ScheduleParameterKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_ScheduleParameterKind;
  static deserializeBinaryFromReader(message: Optional_ScheduleParameterKind, reader: jspb.BinaryReader): Optional_ScheduleParameterKind;
}

export namespace Optional_ScheduleParameterKind {
  export type AsObject = {
    value: ScheduleParameterKind,
  }
}

export class ENG_ScheduleParameter extends jspb.Message {
  getScheduleparametertype(): ScheduleParameterKind;
  setScheduleparametertype(value: ScheduleParameterKind): ENG_ScheduleParameter;

  getValue(): number;
  setValue(value: number): ENG_ScheduleParameter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENG_ScheduleParameter.AsObject;
  static toObject(includeInstance: boolean, msg: ENG_ScheduleParameter): ENG_ScheduleParameter.AsObject;
  static serializeBinaryToWriter(message: ENG_ScheduleParameter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENG_ScheduleParameter;
  static deserializeBinaryFromReader(message: ENG_ScheduleParameter, reader: jspb.BinaryReader): ENG_ScheduleParameter;
}

export namespace ENG_ScheduleParameter {
  export type AsObject = {
    scheduleparametertype: ScheduleParameterKind,
    value: number,
  }
}

export class SchedulePoint extends jspb.Message {
  getScheduleparameterList(): Array<ENG_ScheduleParameter>;
  setScheduleparameterList(value: Array<ENG_ScheduleParameter>): SchedulePoint;
  clearScheduleparameterList(): SchedulePoint;
  addScheduleparameter(value?: ENG_ScheduleParameter, index?: number): ENG_ScheduleParameter;

  getStarttime(): ControlTimestamp | undefined;
  setStarttime(value?: ControlTimestamp): SchedulePoint;
  hasStarttime(): boolean;
  clearStarttime(): SchedulePoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SchedulePoint.AsObject;
  static toObject(includeInstance: boolean, msg: SchedulePoint): SchedulePoint.AsObject;
  static serializeBinaryToWriter(message: SchedulePoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SchedulePoint;
  static deserializeBinaryFromReader(message: SchedulePoint, reader: jspb.BinaryReader): SchedulePoint;
}

export namespace SchedulePoint {
  export type AsObject = {
    scheduleparameterList: Array<ENG_ScheduleParameter.AsObject>,
    starttime?: ControlTimestamp.AsObject,
  }
}

export class ScheduleCSG extends jspb.Message {
  getSchptsList(): Array<SchedulePoint>;
  setSchptsList(value: Array<SchedulePoint>): ScheduleCSG;
  clearSchptsList(): ScheduleCSG;
  addSchpts(value?: SchedulePoint, index?: number): SchedulePoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScheduleCSG.AsObject;
  static toObject(includeInstance: boolean, msg: ScheduleCSG): ScheduleCSG.AsObject;
  static serializeBinaryToWriter(message: ScheduleCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScheduleCSG;
  static deserializeBinaryFromReader(message: ScheduleCSG, reader: jspb.BinaryReader): ScheduleCSG;
}

export namespace ScheduleCSG {
  export type AsObject = {
    schptsList: Array<SchedulePoint.AsObject>,
  }
}

export class ControlScheduleFSCH extends jspb.Message {
  getValacsg(): ScheduleCSG | undefined;
  setValacsg(value?: ScheduleCSG): ControlScheduleFSCH;
  hasValacsg(): boolean;
  clearValacsg(): ControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: ControlScheduleFSCH): ControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: ControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlScheduleFSCH;
  static deserializeBinaryFromReader(message: ControlScheduleFSCH, reader: jspb.BinaryReader): ControlScheduleFSCH;
}

export namespace ControlScheduleFSCH {
  export type AsObject = {
    valacsg?: ScheduleCSG.AsObject,
  }
}

export class LogicalNodeForControl extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): LogicalNodeForControl;
  hasLogicalnode(): boolean;
  clearLogicalnode(): LogicalNodeForControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LogicalNodeForControl.AsObject;
  static toObject(includeInstance: boolean, msg: LogicalNodeForControl): LogicalNodeForControl.AsObject;
  static serializeBinaryToWriter(message: LogicalNodeForControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LogicalNodeForControl;
  static deserializeBinaryFromReader(message: LogicalNodeForControl, reader: jspb.BinaryReader): LogicalNodeForControl;
}

export namespace LogicalNodeForControl {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
  }
}

export class ControlFSCC extends jspb.Message {
  getLogicalnodeforcontrol(): LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: LogicalNodeForControl): ControlFSCC;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): ControlFSCC;

  getControlschedulefsch(): ControlScheduleFSCH | undefined;
  setControlschedulefsch(value?: ControlScheduleFSCH): ControlFSCC;
  hasControlschedulefsch(): boolean;
  clearControlschedulefsch(): ControlFSCC;

  getIslandcontrolschedulefsch(): ControlScheduleFSCH | undefined;
  setIslandcontrolschedulefsch(value?: ControlScheduleFSCH): ControlFSCC;
  hasIslandcontrolschedulefsch(): boolean;
  clearIslandcontrolschedulefsch(): ControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: ControlFSCC): ControlFSCC.AsObject;
  static serializeBinaryToWriter(message: ControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlFSCC;
  static deserializeBinaryFromReader(message: ControlFSCC, reader: jspb.BinaryReader): ControlFSCC;
}

export namespace ControlFSCC {
  export type AsObject = {
    logicalnodeforcontrol?: LogicalNodeForControl.AsObject,
    controlschedulefsch?: ControlScheduleFSCH.AsObject,
    islandcontrolschedulefsch?: ControlScheduleFSCH.AsObject,
  }
}

export class ControlINC extends jspb.Message {
  getCtlval(): number;
  setCtlval(value: number): ControlINC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlINC.AsObject;
  static toObject(includeInstance: boolean, msg: ControlINC): ControlINC.AsObject;
  static serializeBinaryToWriter(message: ControlINC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlINC;
  static deserializeBinaryFromReader(message: ControlINC, reader: jspb.BinaryReader): ControlINC;
}

export namespace ControlINC {
  export type AsObject = {
    ctlval: number,
  }
}

export class ControlING extends jspb.Message {
  getSetval(): number;
  setSetval(value: number): ControlING;

  getUnits(): Unit | undefined;
  setUnits(value?: Unit): ControlING;
  hasUnits(): boolean;
  clearUnits(): ControlING;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlING.AsObject;
  static toObject(includeInstance: boolean, msg: ControlING): ControlING.AsObject;
  static serializeBinaryToWriter(message: ControlING, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlING;
  static deserializeBinaryFromReader(message: ControlING, reader: jspb.BinaryReader): ControlING;
}

export namespace ControlING {
  export type AsObject = {
    setval: number,
    units?: Unit.AsObject,
  }
}

export class ControlISC extends jspb.Message {
  getCtlval(): number;
  setCtlval(value: number): ControlISC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlISC.AsObject;
  static toObject(includeInstance: boolean, msg: ControlISC): ControlISC.AsObject;
  static serializeBinaryToWriter(message: ControlISC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlISC;
  static deserializeBinaryFromReader(message: ControlISC, reader: jspb.BinaryReader): ControlISC;
}

export namespace ControlISC {
  export type AsObject = {
    ctlval: number,
  }
}

export class ControlMessageInfo extends jspb.Message {
  getMessageinfo(): MessageInfo | undefined;
  setMessageinfo(value?: MessageInfo): ControlMessageInfo;
  hasMessageinfo(): boolean;
  clearMessageinfo(): ControlMessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlMessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: ControlMessageInfo): ControlMessageInfo.AsObject;
  static serializeBinaryToWriter(message: ControlMessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlMessageInfo;
  static deserializeBinaryFromReader(message: ControlMessageInfo, reader: jspb.BinaryReader): ControlMessageInfo;
}

export namespace ControlMessageInfo {
  export type AsObject = {
    messageinfo?: MessageInfo.AsObject,
  }
}

export class ControlSPC extends jspb.Message {
  getCtlval(): boolean;
  setCtlval(value: boolean): ControlSPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlSPC.AsObject;
  static toObject(includeInstance: boolean, msg: ControlSPC): ControlSPC.AsObject;
  static serializeBinaryToWriter(message: ControlSPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlSPC;
  static deserializeBinaryFromReader(message: ControlSPC, reader: jspb.BinaryReader): ControlSPC;
}

export namespace ControlSPC {
  export type AsObject = {
    ctlval: boolean,
  }
}

export class ControlValue extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): ControlValue;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ControlValue;

  getModblk(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setModblk(value?: google_protobuf_wrappers_pb.BoolValue): ControlValue;
  hasModblk(): boolean;
  clearModblk(): ControlValue;

  getReset(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setReset(value?: google_protobuf_wrappers_pb.BoolValue): ControlValue;
  hasReset(): boolean;
  clearReset(): ControlValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ControlValue.AsObject;
  static toObject(includeInstance: boolean, msg: ControlValue): ControlValue.AsObject;
  static serializeBinaryToWriter(message: ControlValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ControlValue;
  static deserializeBinaryFromReader(message: ControlValue, reader: jspb.BinaryReader): ControlValue;
}

export namespace ControlValue {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
    modblk?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    reset?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class CumulativeTime extends jspb.Message {
  getSeconds(): number;
  setSeconds(value: number): CumulativeTime;

  getNanoseconds(): number;
  setNanoseconds(value: number): CumulativeTime;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CumulativeTime.AsObject;
  static toObject(includeInstance: boolean, msg: CumulativeTime): CumulativeTime.AsObject;
  static serializeBinaryToWriter(message: CumulativeTime, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CumulativeTime;
  static deserializeBinaryFromReader(message: CumulativeTime, reader: jspb.BinaryReader): CumulativeTime;
}

export namespace CumulativeTime {
  export type AsObject = {
    seconds: number,
    nanoseconds: number,
  }
}

export class DateTimeInterval extends jspb.Message {
  getEnd(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setEnd(value?: google_protobuf_wrappers_pb.Int64Value): DateTimeInterval;
  hasEnd(): boolean;
  clearEnd(): DateTimeInterval;

  getStart(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setStart(value?: google_protobuf_wrappers_pb.Int64Value): DateTimeInterval;
  hasStart(): boolean;
  clearStart(): DateTimeInterval;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DateTimeInterval.AsObject;
  static toObject(includeInstance: boolean, msg: DateTimeInterval): DateTimeInterval.AsObject;
  static serializeBinaryToWriter(message: DateTimeInterval, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DateTimeInterval;
  static deserializeBinaryFromReader(message: DateTimeInterval, reader: jspb.BinaryReader): DateTimeInterval;
}

export namespace DateTimeInterval {
  export type AsObject = {
    end?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    start?: google_protobuf_wrappers_pb.Int64Value.AsObject,
  }
}

export class DEL extends jspb.Message {
  getPhsab(): CMV | undefined;
  setPhsab(value?: CMV): DEL;
  hasPhsab(): boolean;
  clearPhsab(): DEL;

  getPhsbc(): CMV | undefined;
  setPhsbc(value?: CMV): DEL;
  hasPhsbc(): boolean;
  clearPhsbc(): DEL;

  getPhsca(): CMV | undefined;
  setPhsca(value?: CMV): DEL;
  hasPhsca(): boolean;
  clearPhsca(): DEL;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DEL.AsObject;
  static toObject(includeInstance: boolean, msg: DEL): DEL.AsObject;
  static serializeBinaryToWriter(message: DEL, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DEL;
  static deserializeBinaryFromReader(message: DEL, reader: jspb.BinaryReader): DEL;
}

export namespace DEL {
  export type AsObject = {
    phsab?: CMV.AsObject,
    phsbc?: CMV.AsObject,
    phsca?: CMV.AsObject,
  }
}

export class PhaseDPC extends jspb.Message {
  getPhs3(): ControlDPC | undefined;
  setPhs3(value?: ControlDPC): PhaseDPC;
  hasPhs3(): boolean;
  clearPhs3(): PhaseDPC;

  getPhsa(): ControlDPC | undefined;
  setPhsa(value?: ControlDPC): PhaseDPC;
  hasPhsa(): boolean;
  clearPhsa(): PhaseDPC;

  getPhsb(): ControlDPC | undefined;
  setPhsb(value?: ControlDPC): PhaseDPC;
  hasPhsb(): boolean;
  clearPhsb(): PhaseDPC;

  getPhsc(): ControlDPC | undefined;
  setPhsc(value?: ControlDPC): PhaseDPC;
  hasPhsc(): boolean;
  clearPhsc(): PhaseDPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseDPC.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseDPC): PhaseDPC.AsObject;
  static serializeBinaryToWriter(message: PhaseDPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseDPC;
  static deserializeBinaryFromReader(message: PhaseDPC, reader: jspb.BinaryReader): PhaseDPC;
}

export namespace PhaseDPC {
  export type AsObject = {
    phs3?: ControlDPC.AsObject,
    phsa?: ControlDPC.AsObject,
    phsb?: ControlDPC.AsObject,
    phsc?: ControlDPC.AsObject,
  }
}

export class DiscreteControlXCBR extends jspb.Message {
  getLogicalnodeforcontrol(): LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: LogicalNodeForControl): DiscreteControlXCBR;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): DiscreteControlXCBR;

  getPos(): PhaseDPC | undefined;
  setPos(value?: PhaseDPC): DiscreteControlXCBR;
  hasPos(): boolean;
  clearPos(): DiscreteControlXCBR;

  getProtectionmode(): ControlINC | undefined;
  setProtectionmode(value?: ControlINC): DiscreteControlXCBR;
  hasProtectionmode(): boolean;
  clearProtectionmode(): DiscreteControlXCBR;

  getRecloseenabled(): ControlSPC | undefined;
  setRecloseenabled(value?: ControlSPC): DiscreteControlXCBR;
  hasRecloseenabled(): boolean;
  clearRecloseenabled(): DiscreteControlXCBR;

  getResetprotectionpickup(): ControlSPC | undefined;
  setResetprotectionpickup(value?: ControlSPC): DiscreteControlXCBR;
  hasResetprotectionpickup(): boolean;
  clearResetprotectionpickup(): DiscreteControlXCBR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscreteControlXCBR.AsObject;
  static toObject(includeInstance: boolean, msg: DiscreteControlXCBR): DiscreteControlXCBR.AsObject;
  static serializeBinaryToWriter(message: DiscreteControlXCBR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscreteControlXCBR;
  static deserializeBinaryFromReader(message: DiscreteControlXCBR, reader: jspb.BinaryReader): DiscreteControlXCBR;
}

export namespace DiscreteControlXCBR {
  export type AsObject = {
    logicalnodeforcontrol?: LogicalNodeForControl.AsObject,
    pos?: PhaseDPC.AsObject,
    protectionmode?: ControlINC.AsObject,
    recloseenabled?: ControlSPC.AsObject,
    resetprotectionpickup?: ControlSPC.AsObject,
  }
}

export class EnergyConsumer extends jspb.Message {
  getConductingequipment(): ConductingEquipment | undefined;
  setConductingequipment(value?: ConductingEquipment): EnergyConsumer;
  hasConductingequipment(): boolean;
  clearConductingequipment(): EnergyConsumer;

  getOperatinglimit(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOperatinglimit(value?: google_protobuf_wrappers_pb.StringValue): EnergyConsumer;
  hasOperatinglimit(): boolean;
  clearOperatinglimit(): EnergyConsumer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EnergyConsumer.AsObject;
  static toObject(includeInstance: boolean, msg: EnergyConsumer): EnergyConsumer.AsObject;
  static serializeBinaryToWriter(message: EnergyConsumer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EnergyConsumer;
  static deserializeBinaryFromReader(message: EnergyConsumer, reader: jspb.BinaryReader): EnergyConsumer;
}

export namespace EnergyConsumer {
  export type AsObject = {
    conductingequipment?: ConductingEquipment.AsObject,
    operatinglimit?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class Optional_CalcMethodKind extends jspb.Message {
  getValue(): CalcMethodKind;
  setValue(value: CalcMethodKind): Optional_CalcMethodKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_CalcMethodKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_CalcMethodKind): Optional_CalcMethodKind.AsObject;
  static serializeBinaryToWriter(message: Optional_CalcMethodKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_CalcMethodKind;
  static deserializeBinaryFromReader(message: Optional_CalcMethodKind, reader: jspb.BinaryReader): Optional_CalcMethodKind;
}

export namespace Optional_CalcMethodKind {
  export type AsObject = {
    value: CalcMethodKind,
  }
}

export class ENG_CalcMethodKind extends jspb.Message {
  getSetval(): CalcMethodKind;
  setSetval(value: CalcMethodKind): ENG_CalcMethodKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENG_CalcMethodKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENG_CalcMethodKind): ENG_CalcMethodKind.AsObject;
  static serializeBinaryToWriter(message: ENG_CalcMethodKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENG_CalcMethodKind;
  static deserializeBinaryFromReader(message: ENG_CalcMethodKind, reader: jspb.BinaryReader): ENG_CalcMethodKind;
}

export namespace ENG_CalcMethodKind {
  export type AsObject = {
    setval: CalcMethodKind,
  }
}

export class Optional_GridConnectModeKind extends jspb.Message {
  getValue(): GridConnectModeKind;
  setValue(value: GridConnectModeKind): Optional_GridConnectModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_GridConnectModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_GridConnectModeKind): Optional_GridConnectModeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_GridConnectModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_GridConnectModeKind;
  static deserializeBinaryFromReader(message: Optional_GridConnectModeKind, reader: jspb.BinaryReader): Optional_GridConnectModeKind;
}

export namespace Optional_GridConnectModeKind {
  export type AsObject = {
    value: GridConnectModeKind,
  }
}

export class ENG_GridConnectModeKind extends jspb.Message {
  getSetval(): GridConnectModeKind;
  setSetval(value: GridConnectModeKind): ENG_GridConnectModeKind;

  getSetvalextension(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSetvalextension(value?: google_protobuf_wrappers_pb.StringValue): ENG_GridConnectModeKind;
  hasSetvalextension(): boolean;
  clearSetvalextension(): ENG_GridConnectModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENG_GridConnectModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENG_GridConnectModeKind): ENG_GridConnectModeKind.AsObject;
  static serializeBinaryToWriter(message: ENG_GridConnectModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENG_GridConnectModeKind;
  static deserializeBinaryFromReader(message: ENG_GridConnectModeKind, reader: jspb.BinaryReader): ENG_GridConnectModeKind;
}

export namespace ENG_GridConnectModeKind {
  export type AsObject = {
    setval: GridConnectModeKind,
    setvalextension?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class Optional_PFSignKind extends jspb.Message {
  getValue(): PFSignKind;
  setValue(value: PFSignKind): Optional_PFSignKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_PFSignKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_PFSignKind): Optional_PFSignKind.AsObject;
  static serializeBinaryToWriter(message: Optional_PFSignKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_PFSignKind;
  static deserializeBinaryFromReader(message: Optional_PFSignKind, reader: jspb.BinaryReader): Optional_PFSignKind;
}

export namespace Optional_PFSignKind {
  export type AsObject = {
    value: PFSignKind,
  }
}

export class ENG_PFSignKind extends jspb.Message {
  getSetval(): PFSignKind;
  setSetval(value: PFSignKind): ENG_PFSignKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENG_PFSignKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENG_PFSignKind): ENG_PFSignKind.AsObject;
  static serializeBinaryToWriter(message: ENG_PFSignKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENG_PFSignKind;
  static deserializeBinaryFromReader(message: ENG_PFSignKind, reader: jspb.BinaryReader): ENG_PFSignKind;
}

export namespace ENG_PFSignKind {
  export type AsObject = {
    setval: PFSignKind,
  }
}

export class Optional_BehaviourModeKind extends jspb.Message {
  getValue(): BehaviourModeKind;
  setValue(value: BehaviourModeKind): Optional_BehaviourModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_BehaviourModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_BehaviourModeKind): Optional_BehaviourModeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_BehaviourModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_BehaviourModeKind;
  static deserializeBinaryFromReader(message: Optional_BehaviourModeKind, reader: jspb.BinaryReader): Optional_BehaviourModeKind;
}

export namespace Optional_BehaviourModeKind {
  export type AsObject = {
    value: BehaviourModeKind,
  }
}

export class ENS_BehaviourModeKind extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): ENS_BehaviourModeKind;
  hasQ(): boolean;
  clearQ(): ENS_BehaviourModeKind;

  getStval(): BehaviourModeKind;
  setStval(value: BehaviourModeKind): ENS_BehaviourModeKind;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): ENS_BehaviourModeKind;
  hasT(): boolean;
  clearT(): ENS_BehaviourModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENS_BehaviourModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENS_BehaviourModeKind): ENS_BehaviourModeKind.AsObject;
  static serializeBinaryToWriter(message: ENS_BehaviourModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENS_BehaviourModeKind;
  static deserializeBinaryFromReader(message: ENS_BehaviourModeKind, reader: jspb.BinaryReader): ENS_BehaviourModeKind;
}

export namespace ENS_BehaviourModeKind {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: BehaviourModeKind,
    t?: Timestamp.AsObject,
  }
}

export class Optional_DERGeneratorStateKind extends jspb.Message {
  getValue(): DERGeneratorStateKind;
  setValue(value: DERGeneratorStateKind): Optional_DERGeneratorStateKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_DERGeneratorStateKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_DERGeneratorStateKind): Optional_DERGeneratorStateKind.AsObject;
  static serializeBinaryToWriter(message: Optional_DERGeneratorStateKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_DERGeneratorStateKind;
  static deserializeBinaryFromReader(message: Optional_DERGeneratorStateKind, reader: jspb.BinaryReader): Optional_DERGeneratorStateKind;
}

export namespace Optional_DERGeneratorStateKind {
  export type AsObject = {
    value: DERGeneratorStateKind,
  }
}

export class ENS_DERGeneratorStateKind extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): ENS_DERGeneratorStateKind;
  hasQ(): boolean;
  clearQ(): ENS_DERGeneratorStateKind;

  getStval(): DERGeneratorStateKind;
  setStval(value: DERGeneratorStateKind): ENS_DERGeneratorStateKind;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): ENS_DERGeneratorStateKind;
  hasT(): boolean;
  clearT(): ENS_DERGeneratorStateKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENS_DERGeneratorStateKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENS_DERGeneratorStateKind): ENS_DERGeneratorStateKind.AsObject;
  static serializeBinaryToWriter(message: ENS_DERGeneratorStateKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENS_DERGeneratorStateKind;
  static deserializeBinaryFromReader(message: ENS_DERGeneratorStateKind, reader: jspb.BinaryReader): ENS_DERGeneratorStateKind;
}

export namespace ENS_DERGeneratorStateKind {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: DERGeneratorStateKind,
    t?: Timestamp.AsObject,
  }
}

export class Optional_DynamicTestKind extends jspb.Message {
  getValue(): DynamicTestKind;
  setValue(value: DynamicTestKind): Optional_DynamicTestKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_DynamicTestKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_DynamicTestKind): Optional_DynamicTestKind.AsObject;
  static serializeBinaryToWriter(message: Optional_DynamicTestKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_DynamicTestKind;
  static deserializeBinaryFromReader(message: Optional_DynamicTestKind, reader: jspb.BinaryReader): Optional_DynamicTestKind;
}

export namespace Optional_DynamicTestKind {
  export type AsObject = {
    value: DynamicTestKind,
  }
}

export class ENS_DynamicTestKind extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): ENS_DynamicTestKind;
  hasQ(): boolean;
  clearQ(): ENS_DynamicTestKind;

  getStval(): DynamicTestKind;
  setStval(value: DynamicTestKind): ENS_DynamicTestKind;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): ENS_DynamicTestKind;
  hasT(): boolean;
  clearT(): ENS_DynamicTestKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENS_DynamicTestKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENS_DynamicTestKind): ENS_DynamicTestKind.AsObject;
  static serializeBinaryToWriter(message: ENS_DynamicTestKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENS_DynamicTestKind;
  static deserializeBinaryFromReader(message: ENS_DynamicTestKind, reader: jspb.BinaryReader): ENS_DynamicTestKind;
}

export namespace ENS_DynamicTestKind {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: DynamicTestKind,
    t?: Timestamp.AsObject,
  }
}

export class ENS_GridConnectModeKind extends jspb.Message {
  getStval(): GridConnectModeKind;
  setStval(value: GridConnectModeKind): ENS_GridConnectModeKind;

  getStvalextension(): string;
  setStvalextension(value: string): ENS_GridConnectModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENS_GridConnectModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENS_GridConnectModeKind): ENS_GridConnectModeKind.AsObject;
  static serializeBinaryToWriter(message: ENS_GridConnectModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENS_GridConnectModeKind;
  static deserializeBinaryFromReader(message: ENS_GridConnectModeKind, reader: jspb.BinaryReader): ENS_GridConnectModeKind;
}

export namespace ENS_GridConnectModeKind {
  export type AsObject = {
    stval: GridConnectModeKind,
    stvalextension: string,
  }
}

export class Optional_HealthKind extends jspb.Message {
  getValue(): HealthKind;
  setValue(value: HealthKind): Optional_HealthKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_HealthKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_HealthKind): Optional_HealthKind.AsObject;
  static serializeBinaryToWriter(message: Optional_HealthKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_HealthKind;
  static deserializeBinaryFromReader(message: Optional_HealthKind, reader: jspb.BinaryReader): Optional_HealthKind;
}

export namespace Optional_HealthKind {
  export type AsObject = {
    value: HealthKind,
  }
}

export class ENS_HealthKind extends jspb.Message {
  getD(): google_protobuf_wrappers_pb.StringValue | undefined;
  setD(value?: google_protobuf_wrappers_pb.StringValue): ENS_HealthKind;
  hasD(): boolean;
  clearD(): ENS_HealthKind;

  getStval(): HealthKind;
  setStval(value: HealthKind): ENS_HealthKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENS_HealthKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENS_HealthKind): ENS_HealthKind.AsObject;
  static serializeBinaryToWriter(message: ENS_HealthKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENS_HealthKind;
  static deserializeBinaryFromReader(message: ENS_HealthKind, reader: jspb.BinaryReader): ENS_HealthKind;
}

export namespace ENS_HealthKind {
  export type AsObject = {
    d?: google_protobuf_wrappers_pb.StringValue.AsObject,
    stval: HealthKind,
  }
}

export class Optional_SwitchingCapabilityKind extends jspb.Message {
  getValue(): SwitchingCapabilityKind;
  setValue(value: SwitchingCapabilityKind): Optional_SwitchingCapabilityKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_SwitchingCapabilityKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_SwitchingCapabilityKind): Optional_SwitchingCapabilityKind.AsObject;
  static serializeBinaryToWriter(message: Optional_SwitchingCapabilityKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_SwitchingCapabilityKind;
  static deserializeBinaryFromReader(message: Optional_SwitchingCapabilityKind, reader: jspb.BinaryReader): Optional_SwitchingCapabilityKind;
}

export namespace Optional_SwitchingCapabilityKind {
  export type AsObject = {
    value: SwitchingCapabilityKind,
  }
}

export class ENS_SwitchingCapabilityKind extends jspb.Message {
  getBlkena(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setBlkena(value?: google_protobuf_wrappers_pb.BoolValue): ENS_SwitchingCapabilityKind;
  hasBlkena(): boolean;
  clearBlkena(): ENS_SwitchingCapabilityKind;

  getStval(): SwitchingCapabilityKind;
  setStval(value: SwitchingCapabilityKind): ENS_SwitchingCapabilityKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENS_SwitchingCapabilityKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENS_SwitchingCapabilityKind): ENS_SwitchingCapabilityKind.AsObject;
  static serializeBinaryToWriter(message: ENS_SwitchingCapabilityKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENS_SwitchingCapabilityKind;
  static deserializeBinaryFromReader(message: ENS_SwitchingCapabilityKind, reader: jspb.BinaryReader): ENS_SwitchingCapabilityKind;
}

export namespace ENS_SwitchingCapabilityKind {
  export type AsObject = {
    blkena?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    stval: SwitchingCapabilityKind,
  }
}

export class OperationDCTE extends jspb.Message {
  getRnddltmms(): ControlING | undefined;
  setRnddltmms(value?: ControlING): OperationDCTE;
  hasRnddltmms(): boolean;
  clearRnddltmms(): OperationDCTE;

  getRtndltmms(): ControlING | undefined;
  setRtndltmms(value?: ControlING): OperationDCTE;
  hasRtndltmms(): boolean;
  clearRtndltmms(): OperationDCTE;

  getRtnrmptmms(): ControlING | undefined;
  setRtnrmptmms(value?: ControlING): OperationDCTE;
  hasRtnrmptmms(): boolean;
  clearRtnrmptmms(): OperationDCTE;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDCTE.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDCTE): OperationDCTE.AsObject;
  static serializeBinaryToWriter(message: OperationDCTE, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDCTE;
  static deserializeBinaryFromReader(message: OperationDCTE, reader: jspb.BinaryReader): OperationDCTE;
}

export namespace OperationDCTE {
  export type AsObject = {
    rnddltmms?: ControlING.AsObject,
    rtndltmms?: ControlING.AsObject,
    rtnrmptmms?: ControlING.AsObject,
  }
}

export class EnterServiceAPC extends jspb.Message {
  getEnterserviceparameter(): OperationDCTE | undefined;
  setEnterserviceparameter(value?: OperationDCTE): EnterServiceAPC;
  hasEnterserviceparameter(): boolean;
  clearEnterserviceparameter(): EnterServiceAPC;

  getHzhilim(): number;
  setHzhilim(value: number): EnterServiceAPC;

  getHzlolim(): number;
  setHzlolim(value: number): EnterServiceAPC;

  getRtnsrvauto(): boolean;
  setRtnsrvauto(value: boolean): EnterServiceAPC;

  getVhilim(): number;
  setVhilim(value: number): EnterServiceAPC;

  getVlolim(): number;
  setVlolim(value: number): EnterServiceAPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EnterServiceAPC.AsObject;
  static toObject(includeInstance: boolean, msg: EnterServiceAPC): EnterServiceAPC.AsObject;
  static serializeBinaryToWriter(message: EnterServiceAPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EnterServiceAPC;
  static deserializeBinaryFromReader(message: EnterServiceAPC, reader: jspb.BinaryReader): EnterServiceAPC;
}

export namespace EnterServiceAPC {
  export type AsObject = {
    enterserviceparameter?: OperationDCTE.AsObject,
    hzhilim: number,
    hzlolim: number,
    rtnsrvauto: boolean,
    vhilim: number,
    vlolim: number,
  }
}

export class ESS extends jspb.Message {
  getConductingequipment(): ConductingEquipment | undefined;
  setConductingequipment(value?: ConductingEquipment): ESS;
  hasConductingequipment(): boolean;
  clearConductingequipment(): ESS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESS.AsObject;
  static toObject(includeInstance: boolean, msg: ESS): ESS.AsObject;
  static serializeBinaryToWriter(message: ESS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESS;
  static deserializeBinaryFromReader(message: ESS, reader: jspb.BinaryReader): ESS;
}

export namespace ESS {
  export type AsObject = {
    conductingequipment?: ConductingEquipment.AsObject,
  }
}

export class EventMessageInfo extends jspb.Message {
  getMessageinfo(): MessageInfo | undefined;
  setMessageinfo(value?: MessageInfo): EventMessageInfo;
  hasMessageinfo(): boolean;
  clearMessageinfo(): EventMessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventMessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: EventMessageInfo): EventMessageInfo.AsObject;
  static serializeBinaryToWriter(message: EventMessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventMessageInfo;
  static deserializeBinaryFromReader(message: EventMessageInfo, reader: jspb.BinaryReader): EventMessageInfo;
}

export namespace EventMessageInfo {
  export type AsObject = {
    messageinfo?: MessageInfo.AsObject,
  }
}

export class EventValue extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): EventValue;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): EventValue;

  getModblk(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setModblk(value?: google_protobuf_wrappers_pb.BoolValue): EventValue;
  hasModblk(): boolean;
  clearModblk(): EventValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventValue.AsObject;
  static toObject(includeInstance: boolean, msg: EventValue): EventValue.AsObject;
  static serializeBinaryToWriter(message: EventValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventValue;
  static deserializeBinaryFromReader(message: EventValue, reader: jspb.BinaryReader): EventValue;
}

export namespace EventValue {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
    modblk?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ForecastValueSource extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): ForecastValueSource;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ForecastValueSource;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ForecastValueSource.AsObject;
  static toObject(includeInstance: boolean, msg: ForecastValueSource): ForecastValueSource.AsObject;
  static serializeBinaryToWriter(message: ForecastValueSource, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ForecastValueSource;
  static deserializeBinaryFromReader(message: ForecastValueSource, reader: jspb.BinaryReader): ForecastValueSource;
}

export namespace ForecastValueSource {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
  }
}

export class ForecastIED extends jspb.Message {
  getForecastvaluesource(): ForecastValueSource | undefined;
  setForecastvaluesource(value?: ForecastValueSource): ForecastIED;
  hasForecastvaluesource(): boolean;
  clearForecastvaluesource(): ForecastIED;

  getSourceapplicationid(): string;
  setSourceapplicationid(value: string): ForecastIED;

  getSourcedatetime(): number;
  setSourcedatetime(value: number): ForecastIED;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ForecastIED.AsObject;
  static toObject(includeInstance: boolean, msg: ForecastIED): ForecastIED.AsObject;
  static serializeBinaryToWriter(message: ForecastIED, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ForecastIED;
  static deserializeBinaryFromReader(message: ForecastIED, reader: jspb.BinaryReader): ForecastIED;
}

export namespace ForecastIED {
  export type AsObject = {
    forecastvaluesource?: ForecastValueSource.AsObject,
    sourceapplicationid: string,
    sourcedatetime: number,
  }
}

export class ForecastValue extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): ForecastValue;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): ForecastValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ForecastValue.AsObject;
  static toObject(includeInstance: boolean, msg: ForecastValue): ForecastValue.AsObject;
  static serializeBinaryToWriter(message: ForecastValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ForecastValue;
  static deserializeBinaryFromReader(message: ForecastValue, reader: jspb.BinaryReader): ForecastValue;
}

export namespace ForecastValue {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
  }
}

export class OperationDHFW extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDHFW;

  getOpltmmsmax(): ClearingTime | undefined;
  setOpltmmsmax(value?: ClearingTime): OperationDHFW;
  hasOpltmmsmax(): boolean;
  clearOpltmmsmax(): OperationDHFW;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDHFW.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDHFW): OperationDHFW.AsObject;
  static serializeBinaryToWriter(message: OperationDHFW, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDHFW;
  static deserializeBinaryFromReader(message: OperationDHFW, reader: jspb.BinaryReader): OperationDHFW;
}

export namespace OperationDHFW {
  export type AsObject = {
    modena: boolean,
    opltmmsmax?: ClearingTime.AsObject,
  }
}

export class OperationDLFW extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDLFW;

  getOpltmmsmax(): ClearingTime | undefined;
  setOpltmmsmax(value?: ClearingTime): OperationDLFW;
  hasOpltmmsmax(): boolean;
  clearOpltmmsmax(): OperationDLFW;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDLFW.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDLFW): OperationDLFW.AsObject;
  static serializeBinaryToWriter(message: OperationDLFW, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDLFW;
  static deserializeBinaryFromReader(message: OperationDLFW, reader: jspb.BinaryReader): OperationDLFW;
}

export namespace OperationDLFW {
  export type AsObject = {
    modena: boolean,
    opltmmsmax?: ClearingTime.AsObject,
  }
}

export class HzWPoint extends jspb.Message {
  getDeadbandhzval(): number;
  setDeadbandhzval(value: number): HzWPoint;

  getSlopeval(): number;
  setSlopeval(value: number): HzWPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HzWPoint.AsObject;
  static toObject(includeInstance: boolean, msg: HzWPoint): HzWPoint.AsObject;
  static serializeBinaryToWriter(message: HzWPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HzWPoint;
  static deserializeBinaryFromReader(message: HzWPoint, reader: jspb.BinaryReader): HzWPoint;
}

export namespace HzWPoint {
  export type AsObject = {
    deadbandhzval: number,
    slopeval: number,
  }
}

export class HzWAPC extends jspb.Message {
  getOverhzwpt(): HzWPoint | undefined;
  setOverhzwpt(value?: HzWPoint): HzWAPC;
  hasOverhzwpt(): boolean;
  clearOverhzwpt(): HzWAPC;

  getOverhzwparameter(): OperationDHFW | undefined;
  setOverhzwparameter(value?: OperationDHFW): HzWAPC;
  hasOverhzwparameter(): boolean;
  clearOverhzwparameter(): HzWAPC;

  getUnderhzwpt(): HzWPoint | undefined;
  setUnderhzwpt(value?: HzWPoint): HzWAPC;
  hasUnderhzwpt(): boolean;
  clearUnderhzwpt(): HzWAPC;

  getUnderhzwparameter(): OperationDLFW | undefined;
  setUnderhzwparameter(value?: OperationDLFW): HzWAPC;
  hasUnderhzwparameter(): boolean;
  clearUnderhzwparameter(): HzWAPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HzWAPC.AsObject;
  static toObject(includeInstance: boolean, msg: HzWAPC): HzWAPC.AsObject;
  static serializeBinaryToWriter(message: HzWAPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HzWAPC;
  static deserializeBinaryFromReader(message: HzWAPC, reader: jspb.BinaryReader): HzWAPC;
}

export namespace HzWAPC {
  export type AsObject = {
    overhzwpt?: HzWPoint.AsObject,
    overhzwparameter?: OperationDHFW.AsObject,
    underhzwpt?: HzWPoint.AsObject,
    underhzwparameter?: OperationDLFW.AsObject,
  }
}

export class StatusINS extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): StatusINS;
  hasQ(): boolean;
  clearQ(): StatusINS;

  getStval(): number;
  setStval(value: number): StatusINS;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): StatusINS;
  hasT(): boolean;
  clearT(): StatusINS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusINS.AsObject;
  static toObject(includeInstance: boolean, msg: StatusINS): StatusINS.AsObject;
  static serializeBinaryToWriter(message: StatusINS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusINS;
  static deserializeBinaryFromReader(message: StatusINS, reader: jspb.BinaryReader): StatusINS;
}

export namespace StatusINS {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: number,
    t?: Timestamp.AsObject,
  }
}

export class IntegerEventAndStatusGGIO extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): IntegerEventAndStatusGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): IntegerEventAndStatusGGIO;

  getIntin(): StatusINS | undefined;
  setIntin(value?: StatusINS): IntegerEventAndStatusGGIO;
  hasIntin(): boolean;
  clearIntin(): IntegerEventAndStatusGGIO;

  getPhase(): Optional_PhaseCodeKind | undefined;
  setPhase(value?: Optional_PhaseCodeKind): IntegerEventAndStatusGGIO;
  hasPhase(): boolean;
  clearPhase(): IntegerEventAndStatusGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): IntegerEventAndStatusGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: IntegerEventAndStatusGGIO): IntegerEventAndStatusGGIO.AsObject;
  static serializeBinaryToWriter(message: IntegerEventAndStatusGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): IntegerEventAndStatusGGIO;
  static deserializeBinaryFromReader(message: IntegerEventAndStatusGGIO, reader: jspb.BinaryReader): IntegerEventAndStatusGGIO;
}

export namespace IntegerEventAndStatusGGIO {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    intin?: StatusINS.AsObject,
    phase?: Optional_PhaseCodeKind.AsObject,
  }
}

export class OperationDWMX extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDWMX;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDWMX.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDWMX): OperationDWMX.AsObject;
  static serializeBinaryToWriter(message: OperationDWMX, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDWMX;
  static deserializeBinaryFromReader(message: OperationDWMX, reader: jspb.BinaryReader): OperationDWMX;
}

export namespace OperationDWMX {
  export type AsObject = {
    modena: boolean,
  }
}

export class OperationDWMN extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDWMN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDWMN.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDWMN): OperationDWMN.AsObject;
  static serializeBinaryToWriter(message: OperationDWMN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDWMN;
  static deserializeBinaryFromReader(message: OperationDWMN, reader: jspb.BinaryReader): OperationDWMN;
}

export namespace OperationDWMN {
  export type AsObject = {
    modena: boolean,
  }
}

export class LimitWAPC extends jspb.Message {
  getMaxlimparameter(): OperationDWMX | undefined;
  setMaxlimparameter(value?: OperationDWMX): LimitWAPC;
  hasMaxlimparameter(): boolean;
  clearMaxlimparameter(): LimitWAPC;

  getMinlimparameter(): OperationDWMN | undefined;
  setMinlimparameter(value?: OperationDWMN): LimitWAPC;
  hasMinlimparameter(): boolean;
  clearMinlimparameter(): LimitWAPC;

  getWmaxsptval(): number;
  setWmaxsptval(value: number): LimitWAPC;

  getWminsptval(): number;
  setWminsptval(value: number): LimitWAPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LimitWAPC.AsObject;
  static toObject(includeInstance: boolean, msg: LimitWAPC): LimitWAPC.AsObject;
  static serializeBinaryToWriter(message: LimitWAPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LimitWAPC;
  static deserializeBinaryFromReader(message: LimitWAPC, reader: jspb.BinaryReader): LimitWAPC;
}

export namespace LimitWAPC {
  export type AsObject = {
    maxlimparameter?: OperationDWMX.AsObject,
    minlimparameter?: OperationDWMN.AsObject,
    wmaxsptval: number,
    wminsptval: number,
  }
}

export class LogicalNodeForEventAndStatus extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): LogicalNodeForEventAndStatus;
  hasLogicalnode(): boolean;
  clearLogicalnode(): LogicalNodeForEventAndStatus;

  getBeh(): ENS_BehaviourModeKind | undefined;
  setBeh(value?: ENS_BehaviourModeKind): LogicalNodeForEventAndStatus;
  hasBeh(): boolean;
  clearBeh(): LogicalNodeForEventAndStatus;

  getEehealth(): ENS_HealthKind | undefined;
  setEehealth(value?: ENS_HealthKind): LogicalNodeForEventAndStatus;
  hasEehealth(): boolean;
  clearEehealth(): LogicalNodeForEventAndStatus;

  getHotlinetag(): StatusSPS | undefined;
  setHotlinetag(value?: StatusSPS): LogicalNodeForEventAndStatus;
  hasHotlinetag(): boolean;
  clearHotlinetag(): LogicalNodeForEventAndStatus;

  getRemoteblk(): StatusSPS | undefined;
  setRemoteblk(value?: StatusSPS): LogicalNodeForEventAndStatus;
  hasRemoteblk(): boolean;
  clearRemoteblk(): LogicalNodeForEventAndStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LogicalNodeForEventAndStatus.AsObject;
  static toObject(includeInstance: boolean, msg: LogicalNodeForEventAndStatus): LogicalNodeForEventAndStatus.AsObject;
  static serializeBinaryToWriter(message: LogicalNodeForEventAndStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LogicalNodeForEventAndStatus;
  static deserializeBinaryFromReader(message: LogicalNodeForEventAndStatus, reader: jspb.BinaryReader): LogicalNodeForEventAndStatus;
}

export namespace LogicalNodeForEventAndStatus {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    beh?: ENS_BehaviourModeKind.AsObject,
    eehealth?: ENS_HealthKind.AsObject,
    hotlinetag?: StatusSPS.AsObject,
    remoteblk?: StatusSPS.AsObject,
  }
}

export class MeasurementValue extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): MeasurementValue;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): MeasurementValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MeasurementValue.AsObject;
  static toObject(includeInstance: boolean, msg: MeasurementValue): MeasurementValue.AsObject;
  static serializeBinaryToWriter(message: MeasurementValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MeasurementValue;
  static deserializeBinaryFromReader(message: MeasurementValue, reader: jspb.BinaryReader): MeasurementValue;
}

export namespace MeasurementValue {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
  }
}

export class Meter extends jspb.Message {
  getConductingequipment(): ConductingEquipment | undefined;
  setConductingequipment(value?: ConductingEquipment): Meter;
  hasConductingequipment(): boolean;
  clearConductingequipment(): Meter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Meter.AsObject;
  static toObject(includeInstance: boolean, msg: Meter): Meter.AsObject;
  static serializeBinaryToWriter(message: Meter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Meter;
  static deserializeBinaryFromReader(message: Meter, reader: jspb.BinaryReader): Meter;
}

export namespace Meter {
  export type AsObject = {
    conductingequipment?: ConductingEquipment.AsObject,
  }
}

export class NameplateValue extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): NameplateValue;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): NameplateValue;

  getModel(): google_protobuf_wrappers_pb.StringValue | undefined;
  setModel(value?: google_protobuf_wrappers_pb.StringValue): NameplateValue;
  hasModel(): boolean;
  clearModel(): NameplateValue;

  getSernum(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSernum(value?: google_protobuf_wrappers_pb.StringValue): NameplateValue;
  hasSernum(): boolean;
  clearSernum(): NameplateValue;

  getSwrev(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSwrev(value?: google_protobuf_wrappers_pb.StringValue): NameplateValue;
  hasSwrev(): boolean;
  clearSwrev(): NameplateValue;

  getVendor(): google_protobuf_wrappers_pb.StringValue | undefined;
  setVendor(value?: google_protobuf_wrappers_pb.StringValue): NameplateValue;
  hasVendor(): boolean;
  clearVendor(): NameplateValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NameplateValue.AsObject;
  static toObject(includeInstance: boolean, msg: NameplateValue): NameplateValue.AsObject;
  static serializeBinaryToWriter(message: NameplateValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NameplateValue;
  static deserializeBinaryFromReader(message: NameplateValue, reader: jspb.BinaryReader): NameplateValue;
}

export namespace NameplateValue {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
    model?: google_protobuf_wrappers_pb.StringValue.AsObject,
    sernum?: google_protobuf_wrappers_pb.StringValue.AsObject,
    swrev?: google_protobuf_wrappers_pb.StringValue.AsObject,
    vendor?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class OperationDFPF extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDFPF;

  getPfextset(): boolean;
  setPfextset(value: boolean): OperationDFPF;

  getPfgntgtmxval(): number;
  setPfgntgtmxval(value: number): OperationDFPF;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDFPF.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDFPF): OperationDFPF.AsObject;
  static serializeBinaryToWriter(message: OperationDFPF, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDFPF;
  static deserializeBinaryFromReader(message: OperationDFPF, reader: jspb.BinaryReader): OperationDFPF;
}

export namespace OperationDFPF {
  export type AsObject = {
    modena: boolean,
    pfextset: boolean,
    pfgntgtmxval: number,
  }
}

export class OperationDVAR extends jspb.Message {
  getVartgtspt(): number;
  setVartgtspt(value: number): OperationDVAR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDVAR.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDVAR): OperationDVAR.AsObject;
  static serializeBinaryToWriter(message: OperationDVAR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDVAR;
  static deserializeBinaryFromReader(message: OperationDVAR, reader: jspb.BinaryReader): OperationDVAR;
}

export namespace OperationDVAR {
  export type AsObject = {
    vartgtspt: number,
  }
}

export class OperationDVVR extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDVVR;

  getOpltmmsmax(): ClearingTime | undefined;
  setOpltmmsmax(value?: ClearingTime): OperationDVVR;
  hasOpltmmsmax(): boolean;
  clearOpltmmsmax(): OperationDVVR;

  getVref(): number;
  setVref(value: number): OperationDVVR;

  getVrefadjena(): boolean;
  setVrefadjena(value: boolean): OperationDVVR;

  getVreftmms(): ControlING | undefined;
  setVreftmms(value?: ControlING): OperationDVVR;
  hasVreftmms(): boolean;
  clearVreftmms(): OperationDVVR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDVVR.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDVVR): OperationDVVR.AsObject;
  static serializeBinaryToWriter(message: OperationDVVR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDVVR;
  static deserializeBinaryFromReader(message: OperationDVVR, reader: jspb.BinaryReader): OperationDVVR;
}

export namespace OperationDVVR {
  export type AsObject = {
    modena: boolean,
    opltmmsmax?: ClearingTime.AsObject,
    vref: number,
    vrefadjena: boolean,
    vreftmms?: ControlING.AsObject,
  }
}

export class OperationDVWC extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDVWC;

  getOpltmmsmax(): ClearingTime | undefined;
  setOpltmmsmax(value?: ClearingTime): OperationDVWC;
  hasOpltmmsmax(): boolean;
  clearOpltmmsmax(): OperationDVWC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDVWC.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDVWC): OperationDVWC.AsObject;
  static serializeBinaryToWriter(message: OperationDVWC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDVWC;
  static deserializeBinaryFromReader(message: OperationDVWC, reader: jspb.BinaryReader): OperationDVWC;
}

export namespace OperationDVWC {
  export type AsObject = {
    modena: boolean,
    opltmmsmax?: ClearingTime.AsObject,
  }
}

export class OperationDWGC extends jspb.Message {
  getWspt(): number;
  setWspt(value: number): OperationDWGC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDWGC.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDWGC): OperationDWGC.AsObject;
  static serializeBinaryToWriter(message: OperationDWGC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDWGC;
  static deserializeBinaryFromReader(message: OperationDWGC, reader: jspb.BinaryReader): OperationDWGC;
}

export namespace OperationDWGC {
  export type AsObject = {
    wspt: number,
  }
}

export class OperationDWVR extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): OperationDWVR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationDWVR.AsObject;
  static toObject(includeInstance: boolean, msg: OperationDWVR): OperationDWVR.AsObject;
  static serializeBinaryToWriter(message: OperationDWVR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationDWVR;
  static deserializeBinaryFromReader(message: OperationDWVR, reader: jspb.BinaryReader): OperationDWVR;
}

export namespace OperationDWVR {
  export type AsObject = {
    modena: boolean,
  }
}

export class OptimizationMessageInfo extends jspb.Message {
  getMessageinfo(): MessageInfo | undefined;
  setMessageinfo(value?: MessageInfo): OptimizationMessageInfo;
  hasMessageinfo(): boolean;
  clearMessageinfo(): OptimizationMessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OptimizationMessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: OptimizationMessageInfo): OptimizationMessageInfo.AsObject;
  static serializeBinaryToWriter(message: OptimizationMessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OptimizationMessageInfo;
  static deserializeBinaryFromReader(message: OptimizationMessageInfo, reader: jspb.BinaryReader): OptimizationMessageInfo;
}

export namespace OptimizationMessageInfo {
  export type AsObject = {
    messageinfo?: MessageInfo.AsObject,
  }
}

export class PFSPC extends jspb.Message {
  getCtlval(): boolean;
  setCtlval(value: boolean): PFSPC;

  getPfparameter(): OperationDFPF | undefined;
  setPfparameter(value?: OperationDFPF): PFSPC;
  hasPfparameter(): boolean;
  clearPfparameter(): PFSPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PFSPC.AsObject;
  static toObject(includeInstance: boolean, msg: PFSPC): PFSPC.AsObject;
  static serializeBinaryToWriter(message: PFSPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PFSPC;
  static deserializeBinaryFromReader(message: PFSPC, reader: jspb.BinaryReader): PFSPC;
}

export namespace PFSPC {
  export type AsObject = {
    ctlval: boolean,
    pfparameter?: OperationDFPF.AsObject,
  }
}

export class PhaseAPC extends jspb.Message {
  getPhs3(): ControlAPC | undefined;
  setPhs3(value?: ControlAPC): PhaseAPC;
  hasPhs3(): boolean;
  clearPhs3(): PhaseAPC;

  getPhsa(): ControlAPC | undefined;
  setPhsa(value?: ControlAPC): PhaseAPC;
  hasPhsa(): boolean;
  clearPhsa(): PhaseAPC;

  getPhsb(): ControlAPC | undefined;
  setPhsb(value?: ControlAPC): PhaseAPC;
  hasPhsb(): boolean;
  clearPhsb(): PhaseAPC;

  getPhsc(): ControlAPC | undefined;
  setPhsc(value?: ControlAPC): PhaseAPC;
  hasPhsc(): boolean;
  clearPhsc(): PhaseAPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseAPC.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseAPC): PhaseAPC.AsObject;
  static serializeBinaryToWriter(message: PhaseAPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseAPC;
  static deserializeBinaryFromReader(message: PhaseAPC, reader: jspb.BinaryReader): PhaseAPC;
}

export namespace PhaseAPC {
  export type AsObject = {
    phs3?: ControlAPC.AsObject,
    phsa?: ControlAPC.AsObject,
    phsb?: ControlAPC.AsObject,
    phsc?: ControlAPC.AsObject,
  }
}

export class Optional_DbPosKind extends jspb.Message {
  getValue(): DbPosKind;
  setValue(value: DbPosKind): Optional_DbPosKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_DbPosKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_DbPosKind): Optional_DbPosKind.AsObject;
  static serializeBinaryToWriter(message: Optional_DbPosKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_DbPosKind;
  static deserializeBinaryFromReader(message: Optional_DbPosKind, reader: jspb.BinaryReader): Optional_DbPosKind;
}

export namespace Optional_DbPosKind {
  export type AsObject = {
    value: DbPosKind,
  }
}

export class StatusDPS extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): StatusDPS;
  hasQ(): boolean;
  clearQ(): StatusDPS;

  getStval(): DbPosKind;
  setStval(value: DbPosKind): StatusDPS;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): StatusDPS;
  hasT(): boolean;
  clearT(): StatusDPS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusDPS.AsObject;
  static toObject(includeInstance: boolean, msg: StatusDPS): StatusDPS.AsObject;
  static serializeBinaryToWriter(message: StatusDPS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusDPS;
  static deserializeBinaryFromReader(message: StatusDPS, reader: jspb.BinaryReader): StatusDPS;
}

export namespace StatusDPS {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: DbPosKind,
    t?: Timestamp.AsObject,
  }
}

export class PhaseDPS extends jspb.Message {
  getPhs3(): StatusDPS | undefined;
  setPhs3(value?: StatusDPS): PhaseDPS;
  hasPhs3(): boolean;
  clearPhs3(): PhaseDPS;

  getPhsa(): StatusDPS | undefined;
  setPhsa(value?: StatusDPS): PhaseDPS;
  hasPhsa(): boolean;
  clearPhsa(): PhaseDPS;

  getPhsb(): StatusDPS | undefined;
  setPhsb(value?: StatusDPS): PhaseDPS;
  hasPhsb(): boolean;
  clearPhsb(): PhaseDPS;

  getPhsc(): StatusDPS | undefined;
  setPhsc(value?: StatusDPS): PhaseDPS;
  hasPhsc(): boolean;
  clearPhsc(): PhaseDPS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseDPS.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseDPS): PhaseDPS.AsObject;
  static serializeBinaryToWriter(message: PhaseDPS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseDPS;
  static deserializeBinaryFromReader(message: PhaseDPS, reader: jspb.BinaryReader): PhaseDPS;
}

export namespace PhaseDPS {
  export type AsObject = {
    phs3?: StatusDPS.AsObject,
    phsa?: StatusDPS.AsObject,
    phsb?: StatusDPS.AsObject,
    phsc?: StatusDPS.AsObject,
  }
}

export class PhaseINS extends jspb.Message {
  getPhs3(): StatusINS | undefined;
  setPhs3(value?: StatusINS): PhaseINS;
  hasPhs3(): boolean;
  clearPhs3(): PhaseINS;

  getPhsa(): StatusINS | undefined;
  setPhsa(value?: StatusINS): PhaseINS;
  hasPhsa(): boolean;
  clearPhsa(): PhaseINS;

  getPhsb(): StatusINS | undefined;
  setPhsb(value?: StatusINS): PhaseINS;
  hasPhsb(): boolean;
  clearPhsb(): PhaseINS;

  getPhsc(): StatusINS | undefined;
  setPhsc(value?: StatusINS): PhaseINS;
  hasPhsc(): boolean;
  clearPhsc(): PhaseINS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseINS.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseINS): PhaseINS.AsObject;
  static serializeBinaryToWriter(message: PhaseINS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseINS;
  static deserializeBinaryFromReader(message: PhaseINS, reader: jspb.BinaryReader): PhaseINS;
}

export namespace PhaseINS {
  export type AsObject = {
    phs3?: StatusINS.AsObject,
    phsa?: StatusINS.AsObject,
    phsb?: StatusINS.AsObject,
    phsc?: StatusINS.AsObject,
  }
}

export class PhaseISC extends jspb.Message {
  getPhs3(): ControlISC | undefined;
  setPhs3(value?: ControlISC): PhaseISC;
  hasPhs3(): boolean;
  clearPhs3(): PhaseISC;

  getPhsa(): ControlISC | undefined;
  setPhsa(value?: ControlISC): PhaseISC;
  hasPhsa(): boolean;
  clearPhsa(): PhaseISC;

  getPhsb(): ControlISC | undefined;
  setPhsb(value?: ControlISC): PhaseISC;
  hasPhsb(): boolean;
  clearPhsb(): PhaseISC;

  getPhsc(): ControlISC | undefined;
  setPhsc(value?: ControlISC): PhaseISC;
  hasPhsc(): boolean;
  clearPhsc(): PhaseISC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseISC.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseISC): PhaseISC.AsObject;
  static serializeBinaryToWriter(message: PhaseISC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseISC;
  static deserializeBinaryFromReader(message: PhaseISC, reader: jspb.BinaryReader): PhaseISC;
}

export namespace PhaseISC {
  export type AsObject = {
    phs3?: ControlISC.AsObject,
    phsa?: ControlISC.AsObject,
    phsb?: ControlISC.AsObject,
    phsc?: ControlISC.AsObject,
  }
}

export class ReadingMMTN extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): ReadingMMTN;
  hasLogicalnode(): boolean;
  clearLogicalnode(): ReadingMMTN;

  getDmdvah(): BCR | undefined;
  setDmdvah(value?: BCR): ReadingMMTN;
  hasDmdvah(): boolean;
  clearDmdvah(): ReadingMMTN;

  getDmdvarh(): BCR | undefined;
  setDmdvarh(value?: BCR): ReadingMMTN;
  hasDmdvarh(): boolean;
  clearDmdvarh(): ReadingMMTN;

  getDmdwh(): BCR | undefined;
  setDmdwh(value?: BCR): ReadingMMTN;
  hasDmdwh(): boolean;
  clearDmdwh(): ReadingMMTN;

  getSupvah(): BCR | undefined;
  setSupvah(value?: BCR): ReadingMMTN;
  hasSupvah(): boolean;
  clearSupvah(): ReadingMMTN;

  getSupvarh(): BCR | undefined;
  setSupvarh(value?: BCR): ReadingMMTN;
  hasSupvarh(): boolean;
  clearSupvarh(): ReadingMMTN;

  getSupwh(): BCR | undefined;
  setSupwh(value?: BCR): ReadingMMTN;
  hasSupwh(): boolean;
  clearSupwh(): ReadingMMTN;

  getTotvah(): BCR | undefined;
  setTotvah(value?: BCR): ReadingMMTN;
  hasTotvah(): boolean;
  clearTotvah(): ReadingMMTN;

  getTotvarh(): BCR | undefined;
  setTotvarh(value?: BCR): ReadingMMTN;
  hasTotvarh(): boolean;
  clearTotvarh(): ReadingMMTN;

  getTotwh(): BCR | undefined;
  setTotwh(value?: BCR): ReadingMMTN;
  hasTotwh(): boolean;
  clearTotwh(): ReadingMMTN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadingMMTN.AsObject;
  static toObject(includeInstance: boolean, msg: ReadingMMTN): ReadingMMTN.AsObject;
  static serializeBinaryToWriter(message: ReadingMMTN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadingMMTN;
  static deserializeBinaryFromReader(message: ReadingMMTN, reader: jspb.BinaryReader): ReadingMMTN;
}

export namespace ReadingMMTN {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    dmdvah?: BCR.AsObject,
    dmdvarh?: BCR.AsObject,
    dmdwh?: BCR.AsObject,
    supvah?: BCR.AsObject,
    supvarh?: BCR.AsObject,
    supwh?: BCR.AsObject,
    totvah?: BCR.AsObject,
    totvarh?: BCR.AsObject,
    totwh?: BCR.AsObject,
  }
}

export class PhaseMMTN extends jspb.Message {
  getPhsa(): ReadingMMTN | undefined;
  setPhsa(value?: ReadingMMTN): PhaseMMTN;
  hasPhsa(): boolean;
  clearPhsa(): PhaseMMTN;

  getPhsab(): ReadingMMTN | undefined;
  setPhsab(value?: ReadingMMTN): PhaseMMTN;
  hasPhsab(): boolean;
  clearPhsab(): PhaseMMTN;

  getPhsb(): ReadingMMTN | undefined;
  setPhsb(value?: ReadingMMTN): PhaseMMTN;
  hasPhsb(): boolean;
  clearPhsb(): PhaseMMTN;

  getPhsbc(): ReadingMMTN | undefined;
  setPhsbc(value?: ReadingMMTN): PhaseMMTN;
  hasPhsbc(): boolean;
  clearPhsbc(): PhaseMMTN;

  getPhsc(): ReadingMMTN | undefined;
  setPhsc(value?: ReadingMMTN): PhaseMMTN;
  hasPhsc(): boolean;
  clearPhsc(): PhaseMMTN;

  getPhsca(): ReadingMMTN | undefined;
  setPhsca(value?: ReadingMMTN): PhaseMMTN;
  hasPhsca(): boolean;
  clearPhsca(): PhaseMMTN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseMMTN.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseMMTN): PhaseMMTN.AsObject;
  static serializeBinaryToWriter(message: PhaseMMTN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseMMTN;
  static deserializeBinaryFromReader(message: PhaseMMTN, reader: jspb.BinaryReader): PhaseMMTN;
}

export namespace PhaseMMTN {
  export type AsObject = {
    phsa?: ReadingMMTN.AsObject,
    phsab?: ReadingMMTN.AsObject,
    phsb?: ReadingMMTN.AsObject,
    phsbc?: ReadingMMTN.AsObject,
    phsc?: ReadingMMTN.AsObject,
    phsca?: ReadingMMTN.AsObject,
  }
}

export class Optional_RecloseActionKind extends jspb.Message {
  getValue(): RecloseActionKind;
  setValue(value: RecloseActionKind): Optional_RecloseActionKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_RecloseActionKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_RecloseActionKind): Optional_RecloseActionKind.AsObject;
  static serializeBinaryToWriter(message: Optional_RecloseActionKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_RecloseActionKind;
  static deserializeBinaryFromReader(message: Optional_RecloseActionKind, reader: jspb.BinaryReader): Optional_RecloseActionKind;
}

export namespace Optional_RecloseActionKind {
  export type AsObject = {
    value: RecloseActionKind,
  }
}

export class PhaseRecloseAction extends jspb.Message {
  getPhs3(): Optional_RecloseActionKind | undefined;
  setPhs3(value?: Optional_RecloseActionKind): PhaseRecloseAction;
  hasPhs3(): boolean;
  clearPhs3(): PhaseRecloseAction;

  getPhsa(): Optional_RecloseActionKind | undefined;
  setPhsa(value?: Optional_RecloseActionKind): PhaseRecloseAction;
  hasPhsa(): boolean;
  clearPhsa(): PhaseRecloseAction;

  getPhsb(): Optional_RecloseActionKind | undefined;
  setPhsb(value?: Optional_RecloseActionKind): PhaseRecloseAction;
  hasPhsb(): boolean;
  clearPhsb(): PhaseRecloseAction;

  getPhsc(): Optional_RecloseActionKind | undefined;
  setPhsc(value?: Optional_RecloseActionKind): PhaseRecloseAction;
  hasPhsc(): boolean;
  clearPhsc(): PhaseRecloseAction;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseRecloseAction.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseRecloseAction): PhaseRecloseAction.AsObject;
  static serializeBinaryToWriter(message: PhaseRecloseAction, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseRecloseAction;
  static deserializeBinaryFromReader(message: PhaseRecloseAction, reader: jspb.BinaryReader): PhaseRecloseAction;
}

export namespace PhaseRecloseAction {
  export type AsObject = {
    phs3?: Optional_RecloseActionKind.AsObject,
    phsa?: Optional_RecloseActionKind.AsObject,
    phsb?: Optional_RecloseActionKind.AsObject,
    phsc?: Optional_RecloseActionKind.AsObject,
  }
}

export class PhaseSPC extends jspb.Message {
  getPhs3(): ControlSPC | undefined;
  setPhs3(value?: ControlSPC): PhaseSPC;
  hasPhs3(): boolean;
  clearPhs3(): PhaseSPC;

  getPhsa(): ControlSPC | undefined;
  setPhsa(value?: ControlSPC): PhaseSPC;
  hasPhsa(): boolean;
  clearPhsa(): PhaseSPC;

  getPhsb(): ControlSPC | undefined;
  setPhsb(value?: ControlSPC): PhaseSPC;
  hasPhsb(): boolean;
  clearPhsb(): PhaseSPC;

  getPhsc(): ControlSPC | undefined;
  setPhsc(value?: ControlSPC): PhaseSPC;
  hasPhsc(): boolean;
  clearPhsc(): PhaseSPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseSPC.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseSPC): PhaseSPC.AsObject;
  static serializeBinaryToWriter(message: PhaseSPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseSPC;
  static deserializeBinaryFromReader(message: PhaseSPC, reader: jspb.BinaryReader): PhaseSPC;
}

export namespace PhaseSPC {
  export type AsObject = {
    phs3?: ControlSPC.AsObject,
    phsa?: ControlSPC.AsObject,
    phsb?: ControlSPC.AsObject,
    phsc?: ControlSPC.AsObject,
  }
}

export class PhaseSPS extends jspb.Message {
  getPhs3(): StatusSPS | undefined;
  setPhs3(value?: StatusSPS): PhaseSPS;
  hasPhs3(): boolean;
  clearPhs3(): PhaseSPS;

  getPhsa(): StatusSPS | undefined;
  setPhsa(value?: StatusSPS): PhaseSPS;
  hasPhsa(): boolean;
  clearPhsa(): PhaseSPS;

  getPhsb(): StatusSPS | undefined;
  setPhsb(value?: StatusSPS): PhaseSPS;
  hasPhsb(): boolean;
  clearPhsb(): PhaseSPS;

  getPhsc(): StatusSPS | undefined;
  setPhsc(value?: StatusSPS): PhaseSPS;
  hasPhsc(): boolean;
  clearPhsc(): PhaseSPS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PhaseSPS.AsObject;
  static toObject(includeInstance: boolean, msg: PhaseSPS): PhaseSPS.AsObject;
  static serializeBinaryToWriter(message: PhaseSPS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PhaseSPS;
  static deserializeBinaryFromReader(message: PhaseSPS, reader: jspb.BinaryReader): PhaseSPS;
}

export namespace PhaseSPS {
  export type AsObject = {
    phs3?: StatusSPS.AsObject,
    phsa?: StatusSPS.AsObject,
    phsb?: StatusSPS.AsObject,
    phsc?: StatusSPS.AsObject,
  }
}

export class PMG extends jspb.Message {
  getNet(): MV | undefined;
  setNet(value?: MV): PMG;
  hasNet(): boolean;
  clearNet(): PMG;

  getPhsa(): MV | undefined;
  setPhsa(value?: MV): PMG;
  hasPhsa(): boolean;
  clearPhsa(): PMG;

  getPhsb(): MV | undefined;
  setPhsb(value?: MV): PMG;
  hasPhsb(): boolean;
  clearPhsb(): PMG;

  getPhsc(): MV | undefined;
  setPhsc(value?: MV): PMG;
  hasPhsc(): boolean;
  clearPhsc(): PMG;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PMG.AsObject;
  static toObject(includeInstance: boolean, msg: PMG): PMG.AsObject;
  static serializeBinaryToWriter(message: PMG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PMG;
  static deserializeBinaryFromReader(message: PMG, reader: jspb.BinaryReader): PMG;
}

export namespace PMG {
  export type AsObject = {
    net?: MV.AsObject,
    phsa?: MV.AsObject,
    phsb?: MV.AsObject,
    phsc?: MV.AsObject,
  }
}

export class RampRate extends jspb.Message {
  getNegativereactivepowerkvarpermin(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setNegativereactivepowerkvarpermin(value?: google_protobuf_wrappers_pb.FloatValue): RampRate;
  hasNegativereactivepowerkvarpermin(): boolean;
  clearNegativereactivepowerkvarpermin(): RampRate;

  getNegativerealpowerkwpermin(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setNegativerealpowerkwpermin(value?: google_protobuf_wrappers_pb.FloatValue): RampRate;
  hasNegativerealpowerkwpermin(): boolean;
  clearNegativerealpowerkwpermin(): RampRate;

  getPositivereactivepowerkvarpermin(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPositivereactivepowerkvarpermin(value?: google_protobuf_wrappers_pb.FloatValue): RampRate;
  hasPositivereactivepowerkvarpermin(): boolean;
  clearPositivereactivepowerkvarpermin(): RampRate;

  getPositiverealpowerkwpermin(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPositiverealpowerkwpermin(value?: google_protobuf_wrappers_pb.FloatValue): RampRate;
  hasPositiverealpowerkwpermin(): boolean;
  clearPositiverealpowerkwpermin(): RampRate;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RampRate.AsObject;
  static toObject(includeInstance: boolean, msg: RampRate): RampRate.AsObject;
  static serializeBinaryToWriter(message: RampRate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RampRate;
  static deserializeBinaryFromReader(message: RampRate, reader: jspb.BinaryReader): RampRate;
}

export namespace RampRate {
  export type AsObject = {
    negativereactivepowerkvarpermin?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    negativerealpowerkwpermin?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    positivereactivepowerkvarpermin?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    positiverealpowerkwpermin?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class ReadingMessageInfo extends jspb.Message {
  getMessageinfo(): MessageInfo | undefined;
  setMessageinfo(value?: MessageInfo): ReadingMessageInfo;
  hasMessageinfo(): boolean;
  clearMessageinfo(): ReadingMessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadingMessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: ReadingMessageInfo): ReadingMessageInfo.AsObject;
  static serializeBinaryToWriter(message: ReadingMessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadingMessageInfo;
  static deserializeBinaryFromReader(message: ReadingMessageInfo, reader: jspb.BinaryReader): ReadingMessageInfo;
}

export namespace ReadingMessageInfo {
  export type AsObject = {
    messageinfo?: MessageInfo.AsObject,
  }
}

export class ReadingMMTR extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): ReadingMMTR;
  hasLogicalnode(): boolean;
  clearLogicalnode(): ReadingMMTR;

  getDmdvah(): BCR | undefined;
  setDmdvah(value?: BCR): ReadingMMTR;
  hasDmdvah(): boolean;
  clearDmdvah(): ReadingMMTR;

  getDmdvarh(): BCR | undefined;
  setDmdvarh(value?: BCR): ReadingMMTR;
  hasDmdvarh(): boolean;
  clearDmdvarh(): ReadingMMTR;

  getDmdwh(): BCR | undefined;
  setDmdwh(value?: BCR): ReadingMMTR;
  hasDmdwh(): boolean;
  clearDmdwh(): ReadingMMTR;

  getSupvah(): BCR | undefined;
  setSupvah(value?: BCR): ReadingMMTR;
  hasSupvah(): boolean;
  clearSupvah(): ReadingMMTR;

  getSupvarh(): BCR | undefined;
  setSupvarh(value?: BCR): ReadingMMTR;
  hasSupvarh(): boolean;
  clearSupvarh(): ReadingMMTR;

  getSupwh(): BCR | undefined;
  setSupwh(value?: BCR): ReadingMMTR;
  hasSupwh(): boolean;
  clearSupwh(): ReadingMMTR;

  getTotvah(): BCR | undefined;
  setTotvah(value?: BCR): ReadingMMTR;
  hasTotvah(): boolean;
  clearTotvah(): ReadingMMTR;

  getTotvarh(): BCR | undefined;
  setTotvarh(value?: BCR): ReadingMMTR;
  hasTotvarh(): boolean;
  clearTotvarh(): ReadingMMTR;

  getTotwh(): BCR | undefined;
  setTotwh(value?: BCR): ReadingMMTR;
  hasTotwh(): boolean;
  clearTotwh(): ReadingMMTR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadingMMTR.AsObject;
  static toObject(includeInstance: boolean, msg: ReadingMMTR): ReadingMMTR.AsObject;
  static serializeBinaryToWriter(message: ReadingMMTR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadingMMTR;
  static deserializeBinaryFromReader(message: ReadingMMTR, reader: jspb.BinaryReader): ReadingMMTR;
}

export namespace ReadingMMTR {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    dmdvah?: BCR.AsObject,
    dmdvarh?: BCR.AsObject,
    dmdwh?: BCR.AsObject,
    supvah?: BCR.AsObject,
    supvarh?: BCR.AsObject,
    supwh?: BCR.AsObject,
    totvah?: BCR.AsObject,
    totvarh?: BCR.AsObject,
    totwh?: BCR.AsObject,
  }
}

export class WYE extends jspb.Message {
  getNet(): CMV | undefined;
  setNet(value?: CMV): WYE;
  hasNet(): boolean;
  clearNet(): WYE;

  getNeut(): CMV | undefined;
  setNeut(value?: CMV): WYE;
  hasNeut(): boolean;
  clearNeut(): WYE;

  getPhsa(): CMV | undefined;
  setPhsa(value?: CMV): WYE;
  hasPhsa(): boolean;
  clearPhsa(): WYE;

  getPhsb(): CMV | undefined;
  setPhsb(value?: CMV): WYE;
  hasPhsb(): boolean;
  clearPhsb(): WYE;

  getPhsc(): CMV | undefined;
  setPhsc(value?: CMV): WYE;
  hasPhsc(): boolean;
  clearPhsc(): WYE;

  getRes(): CMV | undefined;
  setRes(value?: CMV): WYE;
  hasRes(): boolean;
  clearRes(): WYE;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WYE.AsObject;
  static toObject(includeInstance: boolean, msg: WYE): WYE.AsObject;
  static serializeBinaryToWriter(message: WYE, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WYE;
  static deserializeBinaryFromReader(message: WYE, reader: jspb.BinaryReader): WYE;
}

export namespace WYE {
  export type AsObject = {
    net?: CMV.AsObject,
    neut?: CMV.AsObject,
    phsa?: CMV.AsObject,
    phsb?: CMV.AsObject,
    phsc?: CMV.AsObject,
    res?: CMV.AsObject,
  }
}

export class ReadingMMXU extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): ReadingMMXU;
  hasLogicalnode(): boolean;
  clearLogicalnode(): ReadingMMXU;

  getA(): WYE | undefined;
  setA(value?: WYE): ReadingMMXU;
  hasA(): boolean;
  clearA(): ReadingMMXU;

  getClcmth(): ENG_CalcMethodKind | undefined;
  setClcmth(value?: ENG_CalcMethodKind): ReadingMMXU;
  hasClcmth(): boolean;
  clearClcmth(): ReadingMMXU;

  getHz(): MV | undefined;
  setHz(value?: MV): ReadingMMXU;
  hasHz(): boolean;
  clearHz(): ReadingMMXU;

  getPf(): WYE | undefined;
  setPf(value?: WYE): ReadingMMXU;
  hasPf(): boolean;
  clearPf(): ReadingMMXU;

  getPfsign(): ENG_PFSignKind | undefined;
  setPfsign(value?: ENG_PFSignKind): ReadingMMXU;
  hasPfsign(): boolean;
  clearPfsign(): ReadingMMXU;

  getPhv(): WYE | undefined;
  setPhv(value?: WYE): ReadingMMXU;
  hasPhv(): boolean;
  clearPhv(): ReadingMMXU;

  getPpv(): DEL | undefined;
  setPpv(value?: DEL): ReadingMMXU;
  hasPpv(): boolean;
  clearPpv(): ReadingMMXU;

  getVa(): WYE | undefined;
  setVa(value?: WYE): ReadingMMXU;
  hasVa(): boolean;
  clearVa(): ReadingMMXU;

  getVar(): WYE | undefined;
  setVar(value?: WYE): ReadingMMXU;
  hasVar(): boolean;
  clearVar(): ReadingMMXU;

  getW(): WYE | undefined;
  setW(value?: WYE): ReadingMMXU;
  hasW(): boolean;
  clearW(): ReadingMMXU;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReadingMMXU.AsObject;
  static toObject(includeInstance: boolean, msg: ReadingMMXU): ReadingMMXU.AsObject;
  static serializeBinaryToWriter(message: ReadingMMXU, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReadingMMXU;
  static deserializeBinaryFromReader(message: ReadingMMXU, reader: jspb.BinaryReader): ReadingMMXU;
}

export namespace ReadingMMXU {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    a?: WYE.AsObject,
    clcmth?: ENG_CalcMethodKind.AsObject,
    hz?: MV.AsObject,
    pf?: WYE.AsObject,
    pfsign?: ENG_PFSignKind.AsObject,
    phv?: WYE.AsObject,
    ppv?: DEL.AsObject,
    va?: WYE.AsObject,
    pb_var?: WYE.AsObject,
    w?: WYE.AsObject,
  }
}

export class SourceCapabilityConfiguration extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): SourceCapabilityConfiguration;
  hasLogicalnode(): boolean;
  clearLogicalnode(): SourceCapabilityConfiguration;

  getAmax(): ASG | undefined;
  setAmax(value?: ASG): SourceCapabilityConfiguration;
  hasAmax(): boolean;
  clearAmax(): SourceCapabilityConfiguration;

  getVamax(): ASG | undefined;
  setVamax(value?: ASG): SourceCapabilityConfiguration;
  hasVamax(): boolean;
  clearVamax(): SourceCapabilityConfiguration;

  getVarmaxabs(): ASG | undefined;
  setVarmaxabs(value?: ASG): SourceCapabilityConfiguration;
  hasVarmaxabs(): boolean;
  clearVarmaxabs(): SourceCapabilityConfiguration;

  getVarmaxinj(): ASG | undefined;
  setVarmaxinj(value?: ASG): SourceCapabilityConfiguration;
  hasVarmaxinj(): boolean;
  clearVarmaxinj(): SourceCapabilityConfiguration;

  getVmax(): ASG | undefined;
  setVmax(value?: ASG): SourceCapabilityConfiguration;
  hasVmax(): boolean;
  clearVmax(): SourceCapabilityConfiguration;

  getVmin(): ASG | undefined;
  setVmin(value?: ASG): SourceCapabilityConfiguration;
  hasVmin(): boolean;
  clearVmin(): SourceCapabilityConfiguration;

  getVnom(): ASG | undefined;
  setVnom(value?: ASG): SourceCapabilityConfiguration;
  hasVnom(): boolean;
  clearVnom(): SourceCapabilityConfiguration;

  getWmax(): ASG | undefined;
  setWmax(value?: ASG): SourceCapabilityConfiguration;
  hasWmax(): boolean;
  clearWmax(): SourceCapabilityConfiguration;

  getWovrext(): ASG | undefined;
  setWovrext(value?: ASG): SourceCapabilityConfiguration;
  hasWovrext(): boolean;
  clearWovrext(): SourceCapabilityConfiguration;

  getWovrextpf(): ASG | undefined;
  setWovrextpf(value?: ASG): SourceCapabilityConfiguration;
  hasWovrextpf(): boolean;
  clearWovrextpf(): SourceCapabilityConfiguration;

  getWundext(): ASG | undefined;
  setWundext(value?: ASG): SourceCapabilityConfiguration;
  hasWundext(): boolean;
  clearWundext(): SourceCapabilityConfiguration;

  getWundextpf(): ASG | undefined;
  setWundextpf(value?: ASG): SourceCapabilityConfiguration;
  hasWundextpf(): boolean;
  clearWundextpf(): SourceCapabilityConfiguration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SourceCapabilityConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: SourceCapabilityConfiguration): SourceCapabilityConfiguration.AsObject;
  static serializeBinaryToWriter(message: SourceCapabilityConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SourceCapabilityConfiguration;
  static deserializeBinaryFromReader(message: SourceCapabilityConfiguration, reader: jspb.BinaryReader): SourceCapabilityConfiguration;
}

export namespace SourceCapabilityConfiguration {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    amax?: ASG.AsObject,
    vamax?: ASG.AsObject,
    varmaxabs?: ASG.AsObject,
    varmaxinj?: ASG.AsObject,
    vmax?: ASG.AsObject,
    vmin?: ASG.AsObject,
    vnom?: ASG.AsObject,
    wmax?: ASG.AsObject,
    wovrext?: ASG.AsObject,
    wovrextpf?: ASG.AsObject,
    wundext?: ASG.AsObject,
    wundextpf?: ASG.AsObject,
  }
}

export class Optional_NorOpCatKind extends jspb.Message {
  getValue(): NorOpCatKind;
  setValue(value: NorOpCatKind): Optional_NorOpCatKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_NorOpCatKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_NorOpCatKind): Optional_NorOpCatKind.AsObject;
  static serializeBinaryToWriter(message: Optional_NorOpCatKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_NorOpCatKind;
  static deserializeBinaryFromReader(message: Optional_NorOpCatKind, reader: jspb.BinaryReader): Optional_NorOpCatKind;
}

export namespace Optional_NorOpCatKind {
  export type AsObject = {
    value: NorOpCatKind,
  }
}

export class Optional_AbnOpCatKind extends jspb.Message {
  getValue(): AbnOpCatKind;
  setValue(value: AbnOpCatKind): Optional_AbnOpCatKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_AbnOpCatKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_AbnOpCatKind): Optional_AbnOpCatKind.AsObject;
  static serializeBinaryToWriter(message: Optional_AbnOpCatKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_AbnOpCatKind;
  static deserializeBinaryFromReader(message: Optional_AbnOpCatKind, reader: jspb.BinaryReader): Optional_AbnOpCatKind;
}

export namespace Optional_AbnOpCatKind {
  export type AsObject = {
    value: AbnOpCatKind,
  }
}

export class SourceCapabilityRatings extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): SourceCapabilityRatings;
  hasLogicalnode(): boolean;
  clearLogicalnode(): SourceCapabilityRatings;

  getAbnopcatrtg(): AbnOpCatKind;
  setAbnopcatrtg(value: AbnOpCatKind): SourceCapabilityRatings;

  getAmaxrtg(): ASG | undefined;
  setAmaxrtg(value?: ASG): SourceCapabilityRatings;
  hasAmaxrtg(): boolean;
  clearAmaxrtg(): SourceCapabilityRatings;

  getFreqnomrtg(): ASG | undefined;
  setFreqnomrtg(value?: ASG): SourceCapabilityRatings;
  hasFreqnomrtg(): boolean;
  clearFreqnomrtg(): SourceCapabilityRatings;

  getNoropcatrtg(): NorOpCatKind;
  setNoropcatrtg(value: NorOpCatKind): SourceCapabilityRatings;

  getReactsusceptrtg(): ASG | undefined;
  setReactsusceptrtg(value?: ASG): SourceCapabilityRatings;
  hasReactsusceptrtg(): boolean;
  clearReactsusceptrtg(): SourceCapabilityRatings;

  getVamaxrtg(): ASG | undefined;
  setVamaxrtg(value?: ASG): SourceCapabilityRatings;
  hasVamaxrtg(): boolean;
  clearVamaxrtg(): SourceCapabilityRatings;

  getVarmaxabsrtg(): ASG | undefined;
  setVarmaxabsrtg(value?: ASG): SourceCapabilityRatings;
  hasVarmaxabsrtg(): boolean;
  clearVarmaxabsrtg(): SourceCapabilityRatings;

  getVarmaxinjrtg(): ASG | undefined;
  setVarmaxinjrtg(value?: ASG): SourceCapabilityRatings;
  hasVarmaxinjrtg(): boolean;
  clearVarmaxinjrtg(): SourceCapabilityRatings;

  getVmaxrtg(): ASG | undefined;
  setVmaxrtg(value?: ASG): SourceCapabilityRatings;
  hasVmaxrtg(): boolean;
  clearVmaxrtg(): SourceCapabilityRatings;

  getVminrtg(): ASG | undefined;
  setVminrtg(value?: ASG): SourceCapabilityRatings;
  hasVminrtg(): boolean;
  clearVminrtg(): SourceCapabilityRatings;

  getVnomrtg(): ASG | undefined;
  setVnomrtg(value?: ASG): SourceCapabilityRatings;
  hasVnomrtg(): boolean;
  clearVnomrtg(): SourceCapabilityRatings;

  getWmaxrtg(): ASG | undefined;
  setWmaxrtg(value?: ASG): SourceCapabilityRatings;
  hasWmaxrtg(): boolean;
  clearWmaxrtg(): SourceCapabilityRatings;

  getWovrextrtg(): ASG | undefined;
  setWovrextrtg(value?: ASG): SourceCapabilityRatings;
  hasWovrextrtg(): boolean;
  clearWovrextrtg(): SourceCapabilityRatings;

  getWovrextrtgpf(): ASG | undefined;
  setWovrextrtgpf(value?: ASG): SourceCapabilityRatings;
  hasWovrextrtgpf(): boolean;
  clearWovrextrtgpf(): SourceCapabilityRatings;

  getWundextrtg(): ASG | undefined;
  setWundextrtg(value?: ASG): SourceCapabilityRatings;
  hasWundextrtg(): boolean;
  clearWundextrtg(): SourceCapabilityRatings;

  getWundextrtgpf(): ASG | undefined;
  setWundextrtgpf(value?: ASG): SourceCapabilityRatings;
  hasWundextrtgpf(): boolean;
  clearWundextrtgpf(): SourceCapabilityRatings;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SourceCapabilityRatings.AsObject;
  static toObject(includeInstance: boolean, msg: SourceCapabilityRatings): SourceCapabilityRatings.AsObject;
  static serializeBinaryToWriter(message: SourceCapabilityRatings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SourceCapabilityRatings;
  static deserializeBinaryFromReader(message: SourceCapabilityRatings, reader: jspb.BinaryReader): SourceCapabilityRatings;
}

export namespace SourceCapabilityRatings {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    abnopcatrtg: AbnOpCatKind,
    amaxrtg?: ASG.AsObject,
    freqnomrtg?: ASG.AsObject,
    noropcatrtg: NorOpCatKind,
    reactsusceptrtg?: ASG.AsObject,
    vamaxrtg?: ASG.AsObject,
    varmaxabsrtg?: ASG.AsObject,
    varmaxinjrtg?: ASG.AsObject,
    vmaxrtg?: ASG.AsObject,
    vminrtg?: ASG.AsObject,
    vnomrtg?: ASG.AsObject,
    wmaxrtg?: ASG.AsObject,
    wovrextrtg?: ASG.AsObject,
    wovrextrtgpf?: ASG.AsObject,
    wundextrtg?: ASG.AsObject,
    wundextrtgpf?: ASG.AsObject,
  }
}

export class StatusAndEventXCBR extends jspb.Message {
  getLogicalnodeforeventandstatus(): LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: LogicalNodeForEventAndStatus): StatusAndEventXCBR;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): StatusAndEventXCBR;

  getDynamictest(): ENS_DynamicTestKind | undefined;
  setDynamictest(value?: ENS_DynamicTestKind): StatusAndEventXCBR;
  hasDynamictest(): boolean;
  clearDynamictest(): StatusAndEventXCBR;

  getPos(): PhaseDPS | undefined;
  setPos(value?: PhaseDPS): StatusAndEventXCBR;
  hasPos(): boolean;
  clearPos(): StatusAndEventXCBR;

  getProtectionpickup(): ACD | undefined;
  setProtectionpickup(value?: ACD): StatusAndEventXCBR;
  hasProtectionpickup(): boolean;
  clearProtectionpickup(): StatusAndEventXCBR;

  getProtectionmode(): StatusINS | undefined;
  setProtectionmode(value?: StatusINS): StatusAndEventXCBR;
  hasProtectionmode(): boolean;
  clearProtectionmode(): StatusAndEventXCBR;

  getRecloseenabled(): PhaseSPS | undefined;
  setRecloseenabled(value?: PhaseSPS): StatusAndEventXCBR;
  hasRecloseenabled(): boolean;
  clearRecloseenabled(): StatusAndEventXCBR;

  getReclosingaction(): PhaseRecloseAction | undefined;
  setReclosingaction(value?: PhaseRecloseAction): StatusAndEventXCBR;
  hasReclosingaction(): boolean;
  clearReclosingaction(): StatusAndEventXCBR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusAndEventXCBR.AsObject;
  static toObject(includeInstance: boolean, msg: StatusAndEventXCBR): StatusAndEventXCBR.AsObject;
  static serializeBinaryToWriter(message: StatusAndEventXCBR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusAndEventXCBR;
  static deserializeBinaryFromReader(message: StatusAndEventXCBR, reader: jspb.BinaryReader): StatusAndEventXCBR;
}

export namespace StatusAndEventXCBR {
  export type AsObject = {
    logicalnodeforeventandstatus?: LogicalNodeForEventAndStatus.AsObject,
    dynamictest?: ENS_DynamicTestKind.AsObject,
    pos?: PhaseDPS.AsObject,
    protectionpickup?: ACD.AsObject,
    protectionmode?: StatusINS.AsObject,
    recloseenabled?: PhaseSPS.AsObject,
    reclosingaction?: PhaseRecloseAction.AsObject,
  }
}

export class StatusINC extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): StatusINC;
  hasQ(): boolean;
  clearQ(): StatusINC;

  getStval(): number;
  setStval(value: number): StatusINC;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): StatusINC;
  hasT(): boolean;
  clearT(): StatusINC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusINC.AsObject;
  static toObject(includeInstance: boolean, msg: StatusINC): StatusINC.AsObject;
  static serializeBinaryToWriter(message: StatusINC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusINC;
  static deserializeBinaryFromReader(message: StatusINC, reader: jspb.BinaryReader): StatusINC;
}

export namespace StatusINC {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: number,
    t?: Timestamp.AsObject,
  }
}

export class StatusISC extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): StatusISC;
  hasQ(): boolean;
  clearQ(): StatusISC;

  getStval(): number;
  setStval(value: number): StatusISC;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): StatusISC;
  hasT(): boolean;
  clearT(): StatusISC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusISC.AsObject;
  static toObject(includeInstance: boolean, msg: StatusISC): StatusISC.AsObject;
  static serializeBinaryToWriter(message: StatusISC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusISC;
  static deserializeBinaryFromReader(message: StatusISC, reader: jspb.BinaryReader): StatusISC;
}

export namespace StatusISC {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: number,
    t?: Timestamp.AsObject,
  }
}

export class StatusMessageInfo extends jspb.Message {
  getMessageinfo(): MessageInfo | undefined;
  setMessageinfo(value?: MessageInfo): StatusMessageInfo;
  hasMessageinfo(): boolean;
  clearMessageinfo(): StatusMessageInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusMessageInfo.AsObject;
  static toObject(includeInstance: boolean, msg: StatusMessageInfo): StatusMessageInfo.AsObject;
  static serializeBinaryToWriter(message: StatusMessageInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusMessageInfo;
  static deserializeBinaryFromReader(message: StatusMessageInfo, reader: jspb.BinaryReader): StatusMessageInfo;
}

export namespace StatusMessageInfo {
  export type AsObject = {
    messageinfo?: MessageInfo.AsObject,
  }
}

export class StatusValue extends jspb.Message {
  getIdentifiedobject(): IdentifiedObject | undefined;
  setIdentifiedobject(value?: IdentifiedObject): StatusValue;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): StatusValue;

  getModblk(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setModblk(value?: google_protobuf_wrappers_pb.BoolValue): StatusValue;
  hasModblk(): boolean;
  clearModblk(): StatusValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusValue.AsObject;
  static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
  static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusValue;
  static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
}

export namespace StatusValue {
  export type AsObject = {
    identifiedobject?: IdentifiedObject.AsObject,
    modblk?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class VSS extends jspb.Message {
  getQ(): Quality | undefined;
  setQ(value?: Quality): VSS;
  hasQ(): boolean;
  clearQ(): VSS;

  getStval(): string;
  setStval(value: string): VSS;

  getT(): Timestamp | undefined;
  setT(value?: Timestamp): VSS;
  hasT(): boolean;
  clearT(): VSS;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VSS.AsObject;
  static toObject(includeInstance: boolean, msg: VSS): VSS.AsObject;
  static serializeBinaryToWriter(message: VSS, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VSS;
  static deserializeBinaryFromReader(message: VSS, reader: jspb.BinaryReader): VSS;
}

export namespace VSS {
  export type AsObject = {
    q?: Quality.AsObject,
    stval: string,
    t?: Timestamp.AsObject,
  }
}

export class StringEventAndStatusGGIO extends jspb.Message {
  getLogicalnode(): LogicalNode | undefined;
  setLogicalnode(value?: LogicalNode): StringEventAndStatusGGIO;
  hasLogicalnode(): boolean;
  clearLogicalnode(): StringEventAndStatusGGIO;

  getPhase(): Optional_PhaseCodeKind | undefined;
  setPhase(value?: Optional_PhaseCodeKind): StringEventAndStatusGGIO;
  hasPhase(): boolean;
  clearPhase(): StringEventAndStatusGGIO;

  getStrin(): VSS | undefined;
  setStrin(value?: VSS): StringEventAndStatusGGIO;
  hasStrin(): boolean;
  clearStrin(): StringEventAndStatusGGIO;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StringEventAndStatusGGIO.AsObject;
  static toObject(includeInstance: boolean, msg: StringEventAndStatusGGIO): StringEventAndStatusGGIO.AsObject;
  static serializeBinaryToWriter(message: StringEventAndStatusGGIO, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StringEventAndStatusGGIO;
  static deserializeBinaryFromReader(message: StringEventAndStatusGGIO, reader: jspb.BinaryReader): StringEventAndStatusGGIO;
}

export namespace StringEventAndStatusGGIO {
  export type AsObject = {
    logicalnode?: LogicalNode.AsObject,
    phase?: Optional_PhaseCodeKind.AsObject,
    strin?: VSS.AsObject,
  }
}

export class SwitchPoint extends jspb.Message {
  getPos(): ControlDPC | undefined;
  setPos(value?: ControlDPC): SwitchPoint;
  hasPos(): boolean;
  clearPos(): SwitchPoint;

  getStarttime(): ControlTimestamp | undefined;
  setStarttime(value?: ControlTimestamp): SwitchPoint;
  hasStarttime(): boolean;
  clearStarttime(): SwitchPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchPoint.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchPoint): SwitchPoint.AsObject;
  static serializeBinaryToWriter(message: SwitchPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchPoint;
  static deserializeBinaryFromReader(message: SwitchPoint, reader: jspb.BinaryReader): SwitchPoint;
}

export namespace SwitchPoint {
  export type AsObject = {
    pos?: ControlDPC.AsObject,
    starttime?: ControlTimestamp.AsObject,
  }
}

export class SwitchCSG extends jspb.Message {
  getCrvptsList(): Array<SwitchPoint>;
  setCrvptsList(value: Array<SwitchPoint>): SwitchCSG;
  clearCrvptsList(): SwitchCSG;
  addCrvpts(value?: SwitchPoint, index?: number): SwitchPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SwitchCSG.AsObject;
  static toObject(includeInstance: boolean, msg: SwitchCSG): SwitchCSG.AsObject;
  static serializeBinaryToWriter(message: SwitchCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SwitchCSG;
  static deserializeBinaryFromReader(message: SwitchCSG, reader: jspb.BinaryReader): SwitchCSG;
}

export namespace SwitchCSG {
  export type AsObject = {
    crvptsList: Array<SwitchPoint.AsObject>,
  }
}

export class TmHzPoint extends jspb.Message {
  getHzval(): number;
  setHzval(value: number): TmHzPoint;

  getTmval(): ClearingTime | undefined;
  setTmval(value?: ClearingTime): TmHzPoint;
  hasTmval(): boolean;
  clearTmval(): TmHzPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TmHzPoint.AsObject;
  static toObject(includeInstance: boolean, msg: TmHzPoint): TmHzPoint.AsObject;
  static serializeBinaryToWriter(message: TmHzPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TmHzPoint;
  static deserializeBinaryFromReader(message: TmHzPoint, reader: jspb.BinaryReader): TmHzPoint;
}

export namespace TmHzPoint {
  export type AsObject = {
    hzval: number,
    tmval?: ClearingTime.AsObject,
  }
}

export class TmHzCSG extends jspb.Message {
  getOvercrvptsList(): Array<TmHzPoint>;
  setOvercrvptsList(value: Array<TmHzPoint>): TmHzCSG;
  clearOvercrvptsList(): TmHzCSG;
  addOvercrvpts(value?: TmHzPoint, index?: number): TmHzPoint;

  getUndercrvptsList(): Array<TmHzPoint>;
  setUndercrvptsList(value: Array<TmHzPoint>): TmHzCSG;
  clearUndercrvptsList(): TmHzCSG;
  addUndercrvpts(value?: TmHzPoint, index?: number): TmHzPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TmHzCSG.AsObject;
  static toObject(includeInstance: boolean, msg: TmHzCSG): TmHzCSG.AsObject;
  static serializeBinaryToWriter(message: TmHzCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TmHzCSG;
  static deserializeBinaryFromReader(message: TmHzCSG, reader: jspb.BinaryReader): TmHzCSG;
}

export namespace TmHzCSG {
  export type AsObject = {
    overcrvptsList: Array<TmHzPoint.AsObject>,
    undercrvptsList: Array<TmHzPoint.AsObject>,
  }
}

export class TmVoltPoint extends jspb.Message {
  getTmval(): ClearingTime | undefined;
  setTmval(value?: ClearingTime): TmVoltPoint;
  hasTmval(): boolean;
  clearTmval(): TmVoltPoint;

  getVoltval(): number;
  setVoltval(value: number): TmVoltPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TmVoltPoint.AsObject;
  static toObject(includeInstance: boolean, msg: TmVoltPoint): TmVoltPoint.AsObject;
  static serializeBinaryToWriter(message: TmVoltPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TmVoltPoint;
  static deserializeBinaryFromReader(message: TmVoltPoint, reader: jspb.BinaryReader): TmVoltPoint;
}

export namespace TmVoltPoint {
  export type AsObject = {
    tmval?: ClearingTime.AsObject,
    voltval: number,
  }
}

export class TmVoltCSG extends jspb.Message {
  getOvercrvptsList(): Array<TmVoltPoint>;
  setOvercrvptsList(value: Array<TmVoltPoint>): TmVoltCSG;
  clearOvercrvptsList(): TmVoltCSG;
  addOvercrvpts(value?: TmVoltPoint, index?: number): TmVoltPoint;

  getUndercrvptsList(): Array<TmVoltPoint>;
  setUndercrvptsList(value: Array<TmVoltPoint>): TmVoltCSG;
  clearUndercrvptsList(): TmVoltCSG;
  addUndercrvpts(value?: TmVoltPoint, index?: number): TmVoltPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TmVoltCSG.AsObject;
  static toObject(includeInstance: boolean, msg: TmVoltCSG): TmVoltCSG.AsObject;
  static serializeBinaryToWriter(message: TmVoltCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TmVoltCSG;
  static deserializeBinaryFromReader(message: TmVoltCSG, reader: jspb.BinaryReader): TmVoltCSG;
}

export namespace TmVoltCSG {
  export type AsObject = {
    overcrvptsList: Array<TmVoltPoint.AsObject>,
    undercrvptsList: Array<TmVoltPoint.AsObject>,
  }
}

export class VarSPC extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): VarSPC;

  getVarparameter(): OperationDVAR | undefined;
  setVarparameter(value?: OperationDVAR): VarSPC;
  hasVarparameter(): boolean;
  clearVarparameter(): VarSPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VarSPC.AsObject;
  static toObject(includeInstance: boolean, msg: VarSPC): VarSPC.AsObject;
  static serializeBinaryToWriter(message: VarSPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VarSPC;
  static deserializeBinaryFromReader(message: VarSPC, reader: jspb.BinaryReader): VarSPC;
}

export namespace VarSPC {
  export type AsObject = {
    modena: boolean,
    varparameter?: OperationDVAR.AsObject,
  }
}

export class VoltVarPoint extends jspb.Message {
  getVarval(): number;
  setVarval(value: number): VoltVarPoint;

  getVoltval(): number;
  setVoltval(value: number): VoltVarPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltVarPoint.AsObject;
  static toObject(includeInstance: boolean, msg: VoltVarPoint): VoltVarPoint.AsObject;
  static serializeBinaryToWriter(message: VoltVarPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltVarPoint;
  static deserializeBinaryFromReader(message: VoltVarPoint, reader: jspb.BinaryReader): VoltVarPoint;
}

export namespace VoltVarPoint {
  export type AsObject = {
    varval: number,
    voltval: number,
  }
}

export class VoltVarCSG extends jspb.Message {
  getCrvptsList(): Array<VoltVarPoint>;
  setCrvptsList(value: Array<VoltVarPoint>): VoltVarCSG;
  clearCrvptsList(): VoltVarCSG;
  addCrvpts(value?: VoltVarPoint, index?: number): VoltVarPoint;

  getVvarparameter(): OperationDVVR | undefined;
  setVvarparameter(value?: OperationDVVR): VoltVarCSG;
  hasVvarparameter(): boolean;
  clearVvarparameter(): VoltVarCSG;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltVarCSG.AsObject;
  static toObject(includeInstance: boolean, msg: VoltVarCSG): VoltVarCSG.AsObject;
  static serializeBinaryToWriter(message: VoltVarCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltVarCSG;
  static deserializeBinaryFromReader(message: VoltVarCSG, reader: jspb.BinaryReader): VoltVarCSG;
}

export namespace VoltVarCSG {
  export type AsObject = {
    crvptsList: Array<VoltVarPoint.AsObject>,
    vvarparameter?: OperationDVVR.AsObject,
  }
}

export class VoltWPoint extends jspb.Message {
  getVoltval(): number;
  setVoltval(value: number): VoltWPoint;

  getWval(): number;
  setWval(value: number): VoltWPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltWPoint.AsObject;
  static toObject(includeInstance: boolean, msg: VoltWPoint): VoltWPoint.AsObject;
  static serializeBinaryToWriter(message: VoltWPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltWPoint;
  static deserializeBinaryFromReader(message: VoltWPoint, reader: jspb.BinaryReader): VoltWPoint;
}

export namespace VoltWPoint {
  export type AsObject = {
    voltval: number,
    wval: number,
  }
}

export class VoltWCSG extends jspb.Message {
  getCrvptsList(): Array<VoltWPoint>;
  setCrvptsList(value: Array<VoltWPoint>): VoltWCSG;
  clearCrvptsList(): VoltWCSG;
  addCrvpts(value?: VoltWPoint, index?: number): VoltWPoint;

  getVoltwparameter(): OperationDVWC | undefined;
  setVoltwparameter(value?: OperationDVWC): VoltWCSG;
  hasVoltwparameter(): boolean;
  clearVoltwparameter(): VoltWCSG;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltWCSG.AsObject;
  static toObject(includeInstance: boolean, msg: VoltWCSG): VoltWCSG.AsObject;
  static serializeBinaryToWriter(message: VoltWCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltWCSG;
  static deserializeBinaryFromReader(message: VoltWCSG, reader: jspb.BinaryReader): VoltWCSG;
}

export namespace VoltWCSG {
  export type AsObject = {
    crvptsList: Array<VoltWPoint.AsObject>,
    voltwparameter?: OperationDVWC.AsObject,
  }
}

export class VSC extends jspb.Message {
  getCtlval(): string;
  setCtlval(value: string): VSC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VSC.AsObject;
  static toObject(includeInstance: boolean, msg: VSC): VSC.AsObject;
  static serializeBinaryToWriter(message: VSC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VSC;
  static deserializeBinaryFromReader(message: VSC, reader: jspb.BinaryReader): VSC;
}

export namespace VSC {
  export type AsObject = {
    ctlval: string,
  }
}

export class WSPC extends jspb.Message {
  getModena(): boolean;
  setModena(value: boolean): WSPC;

  getWparameter(): OperationDWGC | undefined;
  setWparameter(value?: OperationDWGC): WSPC;
  hasWparameter(): boolean;
  clearWparameter(): WSPC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WSPC.AsObject;
  static toObject(includeInstance: boolean, msg: WSPC): WSPC.AsObject;
  static serializeBinaryToWriter(message: WSPC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WSPC;
  static deserializeBinaryFromReader(message: WSPC, reader: jspb.BinaryReader): WSPC;
}

export namespace WSPC {
  export type AsObject = {
    modena: boolean,
    wparameter?: OperationDWGC.AsObject,
  }
}

export class WVarPoint extends jspb.Message {
  getVarval(): number;
  setVarval(value: number): WVarPoint;

  getWval(): number;
  setWval(value: number): WVarPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WVarPoint.AsObject;
  static toObject(includeInstance: boolean, msg: WVarPoint): WVarPoint.AsObject;
  static serializeBinaryToWriter(message: WVarPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WVarPoint;
  static deserializeBinaryFromReader(message: WVarPoint, reader: jspb.BinaryReader): WVarPoint;
}

export namespace WVarPoint {
  export type AsObject = {
    varval: number,
    wval: number,
  }
}

export class WVarCSG extends jspb.Message {
  getCrvptsList(): Array<WVarPoint>;
  setCrvptsList(value: Array<WVarPoint>): WVarCSG;
  clearCrvptsList(): WVarCSG;
  addCrvpts(value?: WVarPoint, index?: number): WVarPoint;

  getWvarparameter(): OperationDWVR | undefined;
  setWvarparameter(value?: OperationDWVR): WVarCSG;
  hasWvarparameter(): boolean;
  clearWvarparameter(): WVarCSG;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WVarCSG.AsObject;
  static toObject(includeInstance: boolean, msg: WVarCSG): WVarCSG.AsObject;
  static serializeBinaryToWriter(message: WVarCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WVarCSG;
  static deserializeBinaryFromReader(message: WVarCSG, reader: jspb.BinaryReader): WVarCSG;
}

export namespace WVarCSG {
  export type AsObject = {
    crvptsList: Array<WVarPoint.AsObject>,
    wvarparameter?: OperationDWVR.AsObject,
  }
}

export class Optional_AlrmKind extends jspb.Message {
  getValue(): AlrmKind;
  setValue(value: AlrmKind): Optional_AlrmKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_AlrmKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_AlrmKind): Optional_AlrmKind.AsObject;
  static serializeBinaryToWriter(message: Optional_AlrmKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_AlrmKind;
  static deserializeBinaryFromReader(message: Optional_AlrmKind, reader: jspb.BinaryReader): Optional_AlrmKind;
}

export namespace Optional_AlrmKind {
  export type AsObject = {
    value: AlrmKind,
  }
}

export class Optional_ControlModeKind extends jspb.Message {
  getValue(): ControlModeKind;
  setValue(value: ControlModeKind): Optional_ControlModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_ControlModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_ControlModeKind): Optional_ControlModeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_ControlModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_ControlModeKind;
  static deserializeBinaryFromReader(message: Optional_ControlModeKind, reader: jspb.BinaryReader): Optional_ControlModeKind;
}

export namespace Optional_ControlModeKind {
  export type AsObject = {
    value: ControlModeKind,
  }
}

export class Optional_DirectionModeKind extends jspb.Message {
  getValue(): DirectionModeKind;
  setValue(value: DirectionModeKind): Optional_DirectionModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_DirectionModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_DirectionModeKind): Optional_DirectionModeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_DirectionModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_DirectionModeKind;
  static deserializeBinaryFromReader(message: Optional_DirectionModeKind, reader: jspb.BinaryReader): Optional_DirectionModeKind;
}

export namespace Optional_DirectionModeKind {
  export type AsObject = {
    value: DirectionModeKind,
  }
}

export class Optional_GridConnectionStateKind extends jspb.Message {
  getValue(): GridConnectionStateKind;
  setValue(value: GridConnectionStateKind): Optional_GridConnectionStateKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_GridConnectionStateKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_GridConnectionStateKind): Optional_GridConnectionStateKind.AsObject;
  static serializeBinaryToWriter(message: Optional_GridConnectionStateKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_GridConnectionStateKind;
  static deserializeBinaryFromReader(message: Optional_GridConnectionStateKind, reader: jspb.BinaryReader): Optional_GridConnectionStateKind;
}

export namespace Optional_GridConnectionStateKind {
  export type AsObject = {
    value: GridConnectionStateKind,
  }
}

export class Optional_OperatingStateKind extends jspb.Message {
  getValue(): OperatingStateKind;
  setValue(value: OperatingStateKind): Optional_OperatingStateKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_OperatingStateKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_OperatingStateKind): Optional_OperatingStateKind.AsObject;
  static serializeBinaryToWriter(message: Optional_OperatingStateKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_OperatingStateKind;
  static deserializeBinaryFromReader(message: Optional_OperatingStateKind, reader: jspb.BinaryReader): Optional_OperatingStateKind;
}

export namespace Optional_OperatingStateKind {
  export type AsObject = {
    value: OperatingStateKind,
  }
}

export class Optional_ReactivePowerControlKind extends jspb.Message {
  getValue(): ReactivePowerControlKind;
  setValue(value: ReactivePowerControlKind): Optional_ReactivePowerControlKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_ReactivePowerControlKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_ReactivePowerControlKind): Optional_ReactivePowerControlKind.AsObject;
  static serializeBinaryToWriter(message: Optional_ReactivePowerControlKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_ReactivePowerControlKind;
  static deserializeBinaryFromReader(message: Optional_ReactivePowerControlKind, reader: jspb.BinaryReader): Optional_ReactivePowerControlKind;
}

export namespace Optional_ReactivePowerControlKind {
  export type AsObject = {
    value: ReactivePowerControlKind,
  }
}

export class Optional_RealPowerControlKind extends jspb.Message {
  getValue(): RealPowerControlKind;
  setValue(value: RealPowerControlKind): Optional_RealPowerControlKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_RealPowerControlKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_RealPowerControlKind): Optional_RealPowerControlKind.AsObject;
  static serializeBinaryToWriter(message: Optional_RealPowerControlKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_RealPowerControlKind;
  static deserializeBinaryFromReader(message: Optional_RealPowerControlKind, reader: jspb.BinaryReader): Optional_RealPowerControlKind;
}

export namespace Optional_RealPowerControlKind {
  export type AsObject = {
    value: RealPowerControlKind,
  }
}

export class Optional_StateKind extends jspb.Message {
  getValue(): StateKind;
  setValue(value: StateKind): Optional_StateKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_StateKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_StateKind): Optional_StateKind.AsObject;
  static serializeBinaryToWriter(message: Optional_StateKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_StateKind;
  static deserializeBinaryFromReader(message: Optional_StateKind, reader: jspb.BinaryReader): Optional_StateKind;
}

export namespace Optional_StateKind {
  export type AsObject = {
    value: StateKind,
  }
}

export class Optional_VoltLimitModeKind extends jspb.Message {
  getValue(): VoltLimitModeKind;
  setValue(value: VoltLimitModeKind): Optional_VoltLimitModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_VoltLimitModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_VoltLimitModeKind): Optional_VoltLimitModeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_VoltLimitModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_VoltLimitModeKind;
  static deserializeBinaryFromReader(message: Optional_VoltLimitModeKind, reader: jspb.BinaryReader): Optional_VoltLimitModeKind;
}

export namespace Optional_VoltLimitModeKind {
  export type AsObject = {
    value: VoltLimitModeKind,
  }
}

export enum FaultDirectionKind {
  FAULTDIRECTIONKIND_UNDEFINED = 0,
  FAULTDIRECTIONKIND_UNKNOWN = 1,
  FAULTDIRECTIONKIND_FORWARD = 2,
  FAULTDIRECTIONKIND_BACKWARD = 3,
  FAULTDIRECTIONKIND_BOTH = 4,
}
export enum PhaseFaultDirectionKind {
  PHASEFAULTDIRECTIONKIND_UNDEFINED = 0,
  PHASEFAULTDIRECTIONKIND_UNKNOWN = 1,
  PHASEFAULTDIRECTIONKIND_FORWARD = 2,
  PHASEFAULTDIRECTIONKIND_BACKWARD = 3,
}
export enum UnitSymbolKind {
  UNITSYMBOLKIND_NONE = 0,
  UNITSYMBOLKIND_METER = 2,
  UNITSYMBOLKIND_GRAM = 3,
  UNITSYMBOLKIND_AMP = 5,
  UNITSYMBOLKIND_DEG = 9,
  UNITSYMBOLKIND_RAD = 10,
  UNITSYMBOLKIND_DEGC = 23,
  UNITSYMBOLKIND_FARAD = 25,
  UNITSYMBOLKIND_SEC = 27,
  UNITSYMBOLKIND_HENRY = 28,
  UNITSYMBOLKIND_V = 29,
  UNITSYMBOLKIND_OHM = 30,
  UNITSYMBOLKIND_JOULE = 31,
  UNITSYMBOLKIND_NEWTON = 32,
  UNITSYMBOLKIND_HZ = 33,
  UNITSYMBOLKIND_W = 38,
  UNITSYMBOLKIND_PA = 39,
  UNITSYMBOLKIND_M2 = 41,
  UNITSYMBOLKIND_SIEMENS = 53,
  UNITSYMBOLKIND_VA = 61,
  UNITSYMBOLKIND_VAR = 63,
  UNITSYMBOLKIND_WPERVA = 65,
  UNITSYMBOLKIND_VAH = 71,
  UNITSYMBOLKIND_WH = 72,
  UNITSYMBOLKIND_VARH = 73,
  UNITSYMBOLKIND_HZPERS = 75,
  UNITSYMBOLKIND_WPERS = 81,
  UNITSYMBOLKIND_OTHER = 100,
  UNITSYMBOLKIND_AH = 106,
  UNITSYMBOLKIND_MIN = 159,
  UNITSYMBOLKIND_HOUR = 160,
  UNITSYMBOLKIND_M3 = 166,
  UNITSYMBOLKIND_WPERM2 = 179,
  UNITSYMBOLKIND_DEGF = 279,
  UNITSYMBOLKIND_MPH = 500,
}
export enum UnitMultiplierKind {
  UNITMULTIPLIERKIND_UNDEFINED = 0,
  UNITMULTIPLIERKIND_NONE = 1,
  UNITMULTIPLIERKIND_OTHER = 2,
  UNITMULTIPLIERKIND_CENTI = 3,
  UNITMULTIPLIERKIND_DECI = 4,
  UNITMULTIPLIERKIND_GIGA = 5,
  UNITMULTIPLIERKIND_KILO = 6,
  UNITMULTIPLIERKIND_MEGA = 7,
  UNITMULTIPLIERKIND_MICRO = 8,
  UNITMULTIPLIERKIND_MILLI = 9,
  UNITMULTIPLIERKIND_NANO = 10,
  UNITMULTIPLIERKIND_PICO = 11,
  UNITMULTIPLIERKIND_TERA = 12,
}
export enum PhaseCodeKind {
  PHASECODEKIND_NONE = 0,
  PHASECODEKIND_OTHER = 1,
  PHASECODEKIND_N = 16,
  PHASECODEKIND_C = 32,
  PHASECODEKIND_CN = 33,
  PHASECODEKIND_AC = 40,
  PHASECODEKIND_ACN = 41,
  PHASECODEKIND_B = 64,
  PHASECODEKIND_BN = 65,
  PHASECODEKIND_BC = 66,
  PHASECODEKIND_BCN = 97,
  PHASECODEKIND_A = 128,
  PHASECODEKIND_AN = 129,
  PHASECODEKIND_AB = 132,
  PHASECODEKIND_ABN = 193,
  PHASECODEKIND_ABC = 224,
  PHASECODEKIND_ABCN = 225,
  PHASECODEKIND_S2 = 256,
  PHASECODEKIND_S2N = 257,
  PHASECODEKIND_S1 = 512,
  PHASECODEKIND_S1N = 513,
  PHASECODEKIND_S12 = 768,
  PHASECODEKIND_S12N = 769,
}
export enum ValidityKind {
  VALIDITYKIND_UNDEFINED = 0,
  VALIDITYKIND_GOOD = 1,
  VALIDITYKIND_INVALID = 2,
  VALIDITYKIND_RESERVED = 3,
  VALIDITYKIND_QUESTIONABLE = 4,
}
export enum SourceKind {
  SOURCEKIND_UNDEFINED = 0,
  SOURCEKIND_PROCESS = 1,
  SOURCEKIND_SUBSTITUTED = 2,
}
export enum TimeAccuracyKind {
  TIMEACCURACYKIND_UNDEFINED = 0,
  TIMEACCURACYKIND_T0 = 7,
  TIMEACCURACYKIND_T1 = 10,
  TIMEACCURACYKIND_T2 = 14,
  TIMEACCURACYKIND_T3 = 16,
  TIMEACCURACYKIND_T4 = 18,
  TIMEACCURACYKIND_T5 = 20,
  TIMEACCURACYKIND_UNSPECIFIED = 31,
}
export enum ScheduleParameterKind {
  SCHEDULEPARAMETERKIND_UNDEFINED = 0,
  SCHEDULEPARAMETERKIND_NONE = 1,
  SCHEDULEPARAMETERKIND_OTHER = 2,
  SCHEDULEPARAMETERKIND_A_NET_MAG = 3,
  SCHEDULEPARAMETERKIND_A_NEUT_MAG = 4,
  SCHEDULEPARAMETERKIND_A_PHSA_MAG = 5,
  SCHEDULEPARAMETERKIND_A_PHSB_MAG = 6,
  SCHEDULEPARAMETERKIND_A_PHSC_MAG = 7,
  SCHEDULEPARAMETERKIND_HZ_MAG = 8,
  SCHEDULEPARAMETERKIND_PF_NET_MAG = 9,
  SCHEDULEPARAMETERKIND_PF_NEUT_MAG = 10,
  SCHEDULEPARAMETERKIND_PF_PHSA_MAG = 11,
  SCHEDULEPARAMETERKIND_PF_PHSB_MAG = 12,
  SCHEDULEPARAMETERKIND_PF_PHSC_MAG = 13,
  SCHEDULEPARAMETERKIND_PHV_NET_ANG = 14,
  SCHEDULEPARAMETERKIND_PHV_NET_MAG = 15,
  SCHEDULEPARAMETERKIND_PHV_NEUT_ANG = 16,
  SCHEDULEPARAMETERKIND_PHV_NEUT_MAG = 17,
  SCHEDULEPARAMETERKIND_PHV_PHSA_ANG = 18,
  SCHEDULEPARAMETERKIND_PHV_PHSA_MAG = 19,
  SCHEDULEPARAMETERKIND_PHV_PHSB_ANG = 20,
  SCHEDULEPARAMETERKIND_PHV_PHSB_MAG = 21,
  SCHEDULEPARAMETERKIND_PHV_PHSC_ANG = 22,
  SCHEDULEPARAMETERKIND_PHV_PHSC_MAG = 23,
  SCHEDULEPARAMETERKIND_PPV_PHSAB_ANG = 24,
  SCHEDULEPARAMETERKIND_PPV_PHSAB_MAG = 25,
  SCHEDULEPARAMETERKIND_PPV_PHSBC_ANG = 26,
  SCHEDULEPARAMETERKIND_PPV_PHSBC_MAG = 27,
  SCHEDULEPARAMETERKIND_PPV_PHSCA_ANG = 28,
  SCHEDULEPARAMETERKIND_PPV_PHSCA_MAG = 29,
  SCHEDULEPARAMETERKIND_VA_NET_MAG = 30,
  SCHEDULEPARAMETERKIND_VA_NEUT_MAG = 31,
  SCHEDULEPARAMETERKIND_VA_PHSA_MAG = 32,
  SCHEDULEPARAMETERKIND_VA_PHSB_MAG = 33,
  SCHEDULEPARAMETERKIND_VA_PHSC_MAG = 34,
  SCHEDULEPARAMETERKIND_VAR_NET_MAG = 35,
  SCHEDULEPARAMETERKIND_VAR_NEUT_MAG = 36,
  SCHEDULEPARAMETERKIND_VAR_PHSA_MAG = 37,
  SCHEDULEPARAMETERKIND_VAR_PHSB_MAG = 38,
  SCHEDULEPARAMETERKIND_VAR_PHSC_MAG = 39,
  SCHEDULEPARAMETERKIND_W_NET_MAG = 40,
  SCHEDULEPARAMETERKIND_W_NEUT_MAG = 41,
  SCHEDULEPARAMETERKIND_W_PHSA_MAG = 42,
  SCHEDULEPARAMETERKIND_W_PHSB_MAG = 43,
  SCHEDULEPARAMETERKIND_W_PHSC_MAG = 44,
}
export enum CalcMethodKind {
  CALCMETHODKIND_UNDEFINED = 0,
  CALCMETHODKIND_P_CLASS = 11,
  CALCMETHODKIND_M_CLASS = 12,
  CALCMETHODKIND_DIFF = 13,
}
export enum GridConnectModeKind {
  GRIDCONNECTMODEKIND_UNDEFINED = 0,
  GRIDCONNECTMODEKIND_CSI = 1,
  GRIDCONNECTMODEKIND_VC_VSI = 2,
  GRIDCONNECTMODEKIND_CC_VSI = 3,
  GRIDCONNECTMODEKIND_NONE = 98,
  GRIDCONNECTMODEKIND_OTHER = 99,
  GRIDCONNECTMODEKIND_VSI_PQ = 2000,
  GRIDCONNECTMODEKIND_VSI_VF = 2001,
  GRIDCONNECTMODEKIND_VSI_ISO = 2002,
}
export enum PFSignKind {
  PFSIGNKIND_UNDEFINED = 0,
  PFSIGNKIND_IEC = 1,
  PFSIGNKIND_EEI = 2,
}
export enum BehaviourModeKind {
  BEHAVIOURMODEKIND_UNDEFINED = 0,
  BEHAVIOURMODEKIND_ON = 1,
  BEHAVIOURMODEKIND_BLOCKED = 2,
  BEHAVIOURMODEKIND_TEST = 3,
  BEHAVIOURMODEKIND_TEST_BLOCKED = 4,
  BEHAVIOURMODEKIND_OFF = 5,
}
export enum DERGeneratorStateKind {
  DERGENERATORSTATEKIND_UNDEFINED = 0,
  DERGENERATORSTATEKIND_NOT_OPERATING = 1,
  DERGENERATORSTATEKIND_OPERATING = 2,
  DERGENERATORSTATEKIND_STARTING_UP = 3,
  DERGENERATORSTATEKIND_SHUTTING_DOWN = 4,
  DERGENERATORSTATEKIND_AT_DISCONNECT_LEVEL = 5,
  DERGENERATORSTATEKIND_RAMPING_IN_POWER = 6,
  DERGENERATORSTATEKIND_RAMPING_IN_REACTIVE_POWER = 7,
  DERGENERATORSTATEKIND_STANDBY = 8,
  DERGENERATORSTATEKIND_NOT_APPLICABLE_UNKNOWN = 98,
  DERGENERATORSTATEKIND_OTHER = 99,
}
export enum DynamicTestKind {
  DYNAMICTESTKIND_UNDEFINED = 0,
  DYNAMICTESTKIND_NONE = 1,
  DYNAMICTESTKIND_TESTING = 2,
  DYNAMICTESTKIND_OPERATING = 3,
  DYNAMICTESTKIND_FAILED = 4,
}
export enum HealthKind {
  HEALTHKIND_UNDEFINED = 0,
  HEALTHKIND_NONE = 1,
  HEALTHKIND_OK = 2,
  HEALTHKIND_WARNING = 3,
  HEALTHKIND_ALARM = 4,
}
export enum SwitchingCapabilityKind {
  SWITCHINGCAPABILITYKIND_UNDEFINED = 0,
  SWITCHINGCAPABILITYKIND_NONE = 1,
  SWITCHINGCAPABILITYKIND_OPEN = 2,
  SWITCHINGCAPABILITYKIND_CLOSE = 3,
  SWITCHINGCAPABILITYKIND_OPEN_AND_CLOSE = 4,
}
export enum DbPosKind {
  DBPOSKIND_UNDEFINED = 0,
  DBPOSKIND_TRANSIENT = 1,
  DBPOSKIND_CLOSED = 2,
  DBPOSKIND_OPEN = 3,
  DBPOSKIND_INVALID = 4,
}
export enum RecloseActionKind {
  RECLOSEACTIONKIND_UNDEFINED = 0,
  RECLOSEACTIONKIND_IDLE = 1,
  RECLOSEACTIONKIND_CYCLING = 2,
  RECLOSEACTIONKIND_LOCKOUT = 3,
}
export enum NorOpCatKind {
  NOROPCATKIND_UNDEFINED = 0,
  NOROPCATKIND_A = 1,
  NOROPCATKIND_B = 2,
}
export enum AbnOpCatKind {
  ABNOPCATKIND_UNDEFINED = 0,
  ABNOPCATKIND_I = 1,
  ABNOPCATKIND_II = 2,
  ABNOPCATKIND_III = 3,
}
export enum AlrmKind {
  ALRMKIND_GROUND_FAULT = 0,
  ALRMKIND_DC_OVER_VOLTAGE = 1,
  ALRMKIND_AC_DISCONNECT_OPEN = 2,
  ALRMKIND_DC_DISCONNECT_OPEN = 3,
  ALRMKIND_GRID_DISCONNECT = 4,
  ALRMKIND_CABINET_OPEN = 5,
  ALRMKIND_MANUAL_SHUTDOWN = 6,
  ALRMKIND_OVER_TEMPERATURE = 7,
  ALRMKIND_FREQUENCY_ABOVE_LIMIT = 8,
  ALRMKIND_FREQUENCY_UNDER_LIMIT = 9,
  ALRMKIND_AC_VOLTAGE_ABOVE_LIMIT = 10,
  ALRMKIND_AC_VOLTAGE_UNDER_LIMIT = 11,
  ALRMKIND_BLOWN_STRING_FUSE_ON_INPUT = 12,
  ALRMKIND_UNDER_TEMPERATURE = 13,
  ALRMKIND_GENERIC_MEMORY_OR_COMMUNICATION_ERROR = 14,
  ALRMKIND_HARDWARE_TEST_FAILURE = 15,
  ALRMKIND_MANUFACTURER_ALARM = 16,
}
export enum ControlModeKind {
  CONTROLMODEKIND_UNDEFINED = 0,
  CONTROLMODEKIND_AUTO = 1,
  CONTROLMODEKIND_MANUAL = 2,
  CONTROLMODEKIND_OVERRIDE = 3,
  CONTROLMODEKIND_REMOTE = 4,
}
export enum DirectionModeKind {
  DIRECTIONMODEKIND_UNDEFINED = 0,
  DIRECTIONMODEKIND_LOCKED_FORWARD = 1,
  DIRECTIONMODEKIND_LOCKED_REVERSE = 2,
  DIRECTIONMODEKIND_REVERSE_IDLE = 3,
  DIRECTIONMODEKIND_BIDIRECTIONAL = 4,
  DIRECTIONMODEKIND_NEUTRAL_IDLE = 5,
  DIRECTIONMODEKIND_COGENERATION = 6,
  DIRECTIONMODEKIND_REACTIVE_BIDIRECTIONAL = 7,
  DIRECTIONMODEKIND_BIAS_BIDIRECTIONAL = 8,
  DIRECTIONMODEKIND_BIAS_COGENERATION = 9,
  DIRECTIONMODEKIND_REVERSE_COGENERATION = 10,
}
export enum GridConnectionStateKind {
  GRIDCONNECTIONSTATEKIND_DISCONNECTED = 0,
  GRIDCONNECTIONSTATEKIND_CONNECTED = 1,
}
export enum OperatingStateKind {
  OPERATINGSTATEKIND_UNDEFINED = 0,
  OPERATINGSTATEKIND_OFF = 1,
  OPERATINGSTATEKIND_DISCONNECTED_AND_STANDBY = 2,
  OPERATINGSTATEKIND_DISCONNECTED_AND_AVAILABLE = 3,
  OPERATINGSTATEKIND_DISCONNECTED_AND_AUTHORIZED = 4,
  OPERATINGSTATEKIND_STARTING_AND_SYNCHRONIZING = 5,
  OPERATINGSTATEKIND_CONNECTED_AND_IDLE = 6,
  OPERATINGSTATEKIND_CONNECTED_AND_GENERATING = 7,
  OPERATINGSTATEKIND_CONNECTED_AND_CONSUMING = 8,
  OPERATINGSTATEKIND_STOPPING = 9,
  OPERATINGSTATEKIND_DISCONNECTED_AND_BLOCKED = 10,
  OPERATINGSTATEKIND_DISCONNECTED_AND_IN_MAINTENANCE = 11,
  OPERATINGSTATEKIND_CEASED_TO_ENERGIZE = 12,
  OPERATINGSTATEKIND_FAILED = 13,
}
export enum ReactivePowerControlKind {
  REACTIVEPOWERCONTROLKIND_UNDEFINED = 0,
  REACTIVEPOWERCONTROLKIND_ADVANCED = 1,
  REACTIVEPOWERCONTROLKIND_DROOP = 2,
  REACTIVEPOWERCONTROLKIND_VOLTAGE = 3,
  REACTIVEPOWERCONTROLKIND_REACTIVEPOWER = 4,
  REACTIVEPOWERCONTROLKIND_POWERFACTOR = 5,
}
export enum RealPowerControlKind {
  REALPOWERCONTROLKIND_UNDEFINED = 0,
  REALPOWERCONTROLKIND_ADVANCED = 1,
  REALPOWERCONTROLKIND_DROOP = 2,
  REALPOWERCONTROLKIND_ISOCHRONOUS = 3,
  REALPOWERCONTROLKIND_REALPOWER = 4,
}
export enum StateKind {
  STATEKIND_UNDEFINED = 0,
  STATEKIND_OFF = 1,
  STATEKIND_ON = 2,
  STATEKIND_STANDBY = 3,
}
export enum VoltLimitModeKind {
  VOLTLIMITMODEKIND_UNDEFINED = 0,
  VOLTLIMITMODEKIND_OFF = 1,
  VOLTLIMITMODEKIND_HIGH_LIMIT_ONLY = 2,
  VOLTLIMITMODEKIND_LOW_LIMIT_ONLY = 3,
  VOLTLIMITMODEKIND_HIGH_LOW_LIMITS = 4,
  VOLTLIMITMODEKIND_IVVC_HIGH_LIMIT_ONLY = 5,
  VOLTLIMITMODEKIND_IVVC_LOW_LIMIT_ONLY = 6,
  VOLTLIMITMODEKIND_IVVC_HIGH_LOW_LIMITS = 7,
}
