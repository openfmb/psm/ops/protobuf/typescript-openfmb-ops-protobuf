import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class Optional_CircuitSegmentServiceModeKind extends jspb.Message {
  getValue(): CircuitSegmentServiceModeKind;
  setValue(value: CircuitSegmentServiceModeKind): Optional_CircuitSegmentServiceModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Optional_CircuitSegmentServiceModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: Optional_CircuitSegmentServiceModeKind): Optional_CircuitSegmentServiceModeKind.AsObject;
  static serializeBinaryToWriter(message: Optional_CircuitSegmentServiceModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Optional_CircuitSegmentServiceModeKind;
  static deserializeBinaryFromReader(message: Optional_CircuitSegmentServiceModeKind, reader: jspb.BinaryReader): Optional_CircuitSegmentServiceModeKind;
}

export namespace Optional_CircuitSegmentServiceModeKind {
  export type AsObject = {
    value: CircuitSegmentServiceModeKind,
  }
}

export class ENG_CircuitSegmentServiceModeKind extends jspb.Message {
  getSetval(): CircuitSegmentServiceModeKind;
  setSetval(value: CircuitSegmentServiceModeKind): ENG_CircuitSegmentServiceModeKind;

  getSetvalextension(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSetvalextension(value?: google_protobuf_wrappers_pb.StringValue): ENG_CircuitSegmentServiceModeKind;
  hasSetvalextension(): boolean;
  clearSetvalextension(): ENG_CircuitSegmentServiceModeKind;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ENG_CircuitSegmentServiceModeKind.AsObject;
  static toObject(includeInstance: boolean, msg: ENG_CircuitSegmentServiceModeKind): ENG_CircuitSegmentServiceModeKind.AsObject;
  static serializeBinaryToWriter(message: ENG_CircuitSegmentServiceModeKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ENG_CircuitSegmentServiceModeKind;
  static deserializeBinaryFromReader(message: ENG_CircuitSegmentServiceModeKind, reader: jspb.BinaryReader): ENG_CircuitSegmentServiceModeKind;
}

export namespace ENG_CircuitSegmentServiceModeKind {
  export type AsObject = {
    setval: CircuitSegmentServiceModeKind,
    setvalextension?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class CircuitSegmentControlDCSC extends jspb.Message {
  getLogicalnodeforcontrol(): commonmodule_commonmodule_pb.LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: commonmodule_commonmodule_pb.LogicalNodeForControl): CircuitSegmentControlDCSC;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): CircuitSegmentControlDCSC;

  getCircuitsegmentservicemode(): ENG_CircuitSegmentServiceModeKind | undefined;
  setCircuitsegmentservicemode(value?: ENG_CircuitSegmentServiceModeKind): CircuitSegmentControlDCSC;
  hasCircuitsegmentservicemode(): boolean;
  clearCircuitsegmentservicemode(): CircuitSegmentControlDCSC;

  getIsland(): commonmodule_commonmodule_pb.ControlDPC | undefined;
  setIsland(value?: commonmodule_commonmodule_pb.ControlDPC): CircuitSegmentControlDCSC;
  hasIsland(): boolean;
  clearIsland(): CircuitSegmentControlDCSC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentControlDCSC.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentControlDCSC): CircuitSegmentControlDCSC.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentControlDCSC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentControlDCSC;
  static deserializeBinaryFromReader(message: CircuitSegmentControlDCSC, reader: jspb.BinaryReader): CircuitSegmentControlDCSC;
}

export namespace CircuitSegmentControlDCSC {
  export type AsObject = {
    logicalnodeforcontrol?: commonmodule_commonmodule_pb.LogicalNodeForControl.AsObject,
    circuitsegmentservicemode?: ENG_CircuitSegmentServiceModeKind.AsObject,
    island?: commonmodule_commonmodule_pb.ControlDPC.AsObject,
  }
}

export class CircuitSegmentControl extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): CircuitSegmentControl;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): CircuitSegmentControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): CircuitSegmentControl;
  hasCheck(): boolean;
  clearCheck(): CircuitSegmentControl;

  getCircuitsegmentcontroldcsc(): CircuitSegmentControlDCSC | undefined;
  setCircuitsegmentcontroldcsc(value?: CircuitSegmentControlDCSC): CircuitSegmentControl;
  hasCircuitsegmentcontroldcsc(): boolean;
  clearCircuitsegmentcontroldcsc(): CircuitSegmentControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentControl.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentControl): CircuitSegmentControl.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentControl;
  static deserializeBinaryFromReader(message: CircuitSegmentControl, reader: jspb.BinaryReader): CircuitSegmentControl;
}

export namespace CircuitSegmentControl {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    circuitsegmentcontroldcsc?: CircuitSegmentControlDCSC.AsObject,
  }
}

export class CircuitSegmentControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): CircuitSegmentControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): CircuitSegmentControlProfile;

  getApplicationsystem(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setApplicationsystem(value?: commonmodule_commonmodule_pb.ApplicationSystem): CircuitSegmentControlProfile;
  hasApplicationsystem(): boolean;
  clearApplicationsystem(): CircuitSegmentControlProfile;

  getCircuitsegmentcontrol(): CircuitSegmentControl | undefined;
  setCircuitsegmentcontrol(value?: CircuitSegmentControl): CircuitSegmentControlProfile;
  hasCircuitsegmentcontrol(): boolean;
  clearCircuitsegmentcontrol(): CircuitSegmentControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentControlProfile): CircuitSegmentControlProfile.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentControlProfile;
  static deserializeBinaryFromReader(message: CircuitSegmentControlProfile, reader: jspb.BinaryReader): CircuitSegmentControlProfile;
}

export namespace CircuitSegmentControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    applicationsystem?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    circuitsegmentcontrol?: CircuitSegmentControl.AsObject,
  }
}

export class CircuitSegmentEventDCSC extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): CircuitSegmentEventDCSC;
  hasLogicalnode(): boolean;
  clearLogicalnode(): CircuitSegmentEventDCSC;

  getCircuitsegmentservicemode(): ENG_CircuitSegmentServiceModeKind | undefined;
  setCircuitsegmentservicemode(value?: ENG_CircuitSegmentServiceModeKind): CircuitSegmentEventDCSC;
  hasCircuitsegmentservicemode(): boolean;
  clearCircuitsegmentservicemode(): CircuitSegmentEventDCSC;

  getIsland(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setIsland(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentEventDCSC;
  hasIsland(): boolean;
  clearIsland(): CircuitSegmentEventDCSC;

  getPermissibleauto(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissibleauto(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentEventDCSC;
  hasPermissibleauto(): boolean;
  clearPermissibleauto(): CircuitSegmentEventDCSC;

  getPermissiblemanual(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblemanual(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentEventDCSC;
  hasPermissiblemanual(): boolean;
  clearPermissiblemanual(): CircuitSegmentEventDCSC;

  getPermissiblenetzero(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblenetzero(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentEventDCSC;
  hasPermissiblenetzero(): boolean;
  clearPermissiblenetzero(): CircuitSegmentEventDCSC;

  getPermissiblestart(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblestart(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentEventDCSC;
  hasPermissiblestart(): boolean;
  clearPermissiblestart(): CircuitSegmentEventDCSC;

  getPermissiblestop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblestop(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentEventDCSC;
  hasPermissiblestop(): boolean;
  clearPermissiblestop(): CircuitSegmentEventDCSC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentEventDCSC.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentEventDCSC): CircuitSegmentEventDCSC.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentEventDCSC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentEventDCSC;
  static deserializeBinaryFromReader(message: CircuitSegmentEventDCSC, reader: jspb.BinaryReader): CircuitSegmentEventDCSC;
}

export namespace CircuitSegmentEventDCSC {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    circuitsegmentservicemode?: ENG_CircuitSegmentServiceModeKind.AsObject,
    island?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissibleauto?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblemanual?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblenetzero?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblestart?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblestop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
  }
}

export class CircuitSegmentEvent extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): CircuitSegmentEvent;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): CircuitSegmentEvent;

  getCircuitsegmenteventdcsc(): CircuitSegmentEventDCSC | undefined;
  setCircuitsegmenteventdcsc(value?: CircuitSegmentEventDCSC): CircuitSegmentEvent;
  hasCircuitsegmenteventdcsc(): boolean;
  clearCircuitsegmenteventdcsc(): CircuitSegmentEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentEvent.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentEvent): CircuitSegmentEvent.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentEvent;
  static deserializeBinaryFromReader(message: CircuitSegmentEvent, reader: jspb.BinaryReader): CircuitSegmentEvent;
}

export namespace CircuitSegmentEvent {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    circuitsegmenteventdcsc?: CircuitSegmentEventDCSC.AsObject,
  }
}

export class CircuitSegmentEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): CircuitSegmentEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): CircuitSegmentEventProfile;

  getApplicationsystem(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setApplicationsystem(value?: commonmodule_commonmodule_pb.ApplicationSystem): CircuitSegmentEventProfile;
  hasApplicationsystem(): boolean;
  clearApplicationsystem(): CircuitSegmentEventProfile;

  getCircuitsegmentevent(): CircuitSegmentEvent | undefined;
  setCircuitsegmentevent(value?: CircuitSegmentEvent): CircuitSegmentEventProfile;
  hasCircuitsegmentevent(): boolean;
  clearCircuitsegmentevent(): CircuitSegmentEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentEventProfile): CircuitSegmentEventProfile.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentEventProfile;
  static deserializeBinaryFromReader(message: CircuitSegmentEventProfile, reader: jspb.BinaryReader): CircuitSegmentEventProfile;
}

export namespace CircuitSegmentEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    applicationsystem?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    circuitsegmentevent?: CircuitSegmentEvent.AsObject,
  }
}

export class CircuitSegmentStatusDCSC extends jspb.Message {
  getLogicalnode(): commonmodule_commonmodule_pb.LogicalNode | undefined;
  setLogicalnode(value?: commonmodule_commonmodule_pb.LogicalNode): CircuitSegmentStatusDCSC;
  hasLogicalnode(): boolean;
  clearLogicalnode(): CircuitSegmentStatusDCSC;

  getCircuitsegmentservicemode(): ENG_CircuitSegmentServiceModeKind | undefined;
  setCircuitsegmentservicemode(value?: ENG_CircuitSegmentServiceModeKind): CircuitSegmentStatusDCSC;
  hasCircuitsegmentservicemode(): boolean;
  clearCircuitsegmentservicemode(): CircuitSegmentStatusDCSC;

  getIsland(): commonmodule_commonmodule_pb.StatusDPS | undefined;
  setIsland(value?: commonmodule_commonmodule_pb.StatusDPS): CircuitSegmentStatusDCSC;
  hasIsland(): boolean;
  clearIsland(): CircuitSegmentStatusDCSC;

  getPermissibleauto(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissibleauto(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentStatusDCSC;
  hasPermissibleauto(): boolean;
  clearPermissibleauto(): CircuitSegmentStatusDCSC;

  getPermissiblemanual(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblemanual(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentStatusDCSC;
  hasPermissiblemanual(): boolean;
  clearPermissiblemanual(): CircuitSegmentStatusDCSC;

  getPermissiblenetzero(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblenetzero(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentStatusDCSC;
  hasPermissiblenetzero(): boolean;
  clearPermissiblenetzero(): CircuitSegmentStatusDCSC;

  getPermissiblestart(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblestart(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentStatusDCSC;
  hasPermissiblestart(): boolean;
  clearPermissiblestart(): CircuitSegmentStatusDCSC;

  getPermissiblestop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setPermissiblestop(value?: commonmodule_commonmodule_pb.StatusSPS): CircuitSegmentStatusDCSC;
  hasPermissiblestop(): boolean;
  clearPermissiblestop(): CircuitSegmentStatusDCSC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentStatusDCSC.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentStatusDCSC): CircuitSegmentStatusDCSC.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentStatusDCSC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentStatusDCSC;
  static deserializeBinaryFromReader(message: CircuitSegmentStatusDCSC, reader: jspb.BinaryReader): CircuitSegmentStatusDCSC;
}

export namespace CircuitSegmentStatusDCSC {
  export type AsObject = {
    logicalnode?: commonmodule_commonmodule_pb.LogicalNode.AsObject,
    circuitsegmentservicemode?: ENG_CircuitSegmentServiceModeKind.AsObject,
    island?: commonmodule_commonmodule_pb.StatusDPS.AsObject,
    permissibleauto?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblemanual?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblenetzero?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblestart?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    permissiblestop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
  }
}

export class CircuitSegmentStatus extends jspb.Message {
  getIdentifiedobject(): commonmodule_commonmodule_pb.IdentifiedObject | undefined;
  setIdentifiedobject(value?: commonmodule_commonmodule_pb.IdentifiedObject): CircuitSegmentStatus;
  hasIdentifiedobject(): boolean;
  clearIdentifiedobject(): CircuitSegmentStatus;

  getCircuitsegmentstatusdcsc(): CircuitSegmentStatusDCSC | undefined;
  setCircuitsegmentstatusdcsc(value?: CircuitSegmentStatusDCSC): CircuitSegmentStatus;
  hasCircuitsegmentstatusdcsc(): boolean;
  clearCircuitsegmentstatusdcsc(): CircuitSegmentStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentStatus.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentStatus): CircuitSegmentStatus.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentStatus;
  static deserializeBinaryFromReader(message: CircuitSegmentStatus, reader: jspb.BinaryReader): CircuitSegmentStatus;
}

export namespace CircuitSegmentStatus {
  export type AsObject = {
    identifiedobject?: commonmodule_commonmodule_pb.IdentifiedObject.AsObject,
    circuitsegmentstatusdcsc?: CircuitSegmentStatusDCSC.AsObject,
  }
}

export class CircuitSegmentStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): CircuitSegmentStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): CircuitSegmentStatusProfile;

  getApplicationsystem(): commonmodule_commonmodule_pb.ApplicationSystem | undefined;
  setApplicationsystem(value?: commonmodule_commonmodule_pb.ApplicationSystem): CircuitSegmentStatusProfile;
  hasApplicationsystem(): boolean;
  clearApplicationsystem(): CircuitSegmentStatusProfile;

  getCircuitsegmentstatus(): CircuitSegmentStatus | undefined;
  setCircuitsegmentstatus(value?: CircuitSegmentStatus): CircuitSegmentStatusProfile;
  hasCircuitsegmentstatus(): boolean;
  clearCircuitsegmentstatus(): CircuitSegmentStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CircuitSegmentStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: CircuitSegmentStatusProfile): CircuitSegmentStatusProfile.AsObject;
  static serializeBinaryToWriter(message: CircuitSegmentStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CircuitSegmentStatusProfile;
  static deserializeBinaryFromReader(message: CircuitSegmentStatusProfile, reader: jspb.BinaryReader): CircuitSegmentStatusProfile;
}

export namespace CircuitSegmentStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    applicationsystem?: commonmodule_commonmodule_pb.ApplicationSystem.AsObject,
    circuitsegmentstatus?: CircuitSegmentStatus.AsObject,
  }
}

export enum CircuitSegmentServiceModeKind {
  CIRCUITSEGMENTSERVICEMODEKIND_UNDEFINED = 0,
  CIRCUITSEGMENTSERVICEMODEKIND_NONE = 1,
  CIRCUITSEGMENTSERVICEMODEKIND_AUTO = 2,
  CIRCUITSEGMENTSERVICEMODEKIND_MANUAL = 3,
  CIRCUITSEGMENTSERVICEMODEKIND_NETZERO = 4,
  CIRCUITSEGMENTSERVICEMODEKIND_START = 5,
  CIRCUITSEGMENTSERVICEMODEKIND_STOP = 6,
}
