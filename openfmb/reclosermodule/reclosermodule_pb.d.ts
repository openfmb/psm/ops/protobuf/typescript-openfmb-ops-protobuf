import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class RecloserDiscreteControlXCBR extends jspb.Message {
  getDiscretecontrolxcbr(): commonmodule_commonmodule_pb.DiscreteControlXCBR | undefined;
  setDiscretecontrolxcbr(value?: commonmodule_commonmodule_pb.DiscreteControlXCBR): RecloserDiscreteControlXCBR;
  hasDiscretecontrolxcbr(): boolean;
  clearDiscretecontrolxcbr(): RecloserDiscreteControlXCBR;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserDiscreteControlXCBR.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserDiscreteControlXCBR): RecloserDiscreteControlXCBR.AsObject;
  static serializeBinaryToWriter(message: RecloserDiscreteControlXCBR, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserDiscreteControlXCBR;
  static deserializeBinaryFromReader(message: RecloserDiscreteControlXCBR, reader: jspb.BinaryReader): RecloserDiscreteControlXCBR;
}

export namespace RecloserDiscreteControlXCBR {
  export type AsObject = {
    discretecontrolxcbr?: commonmodule_commonmodule_pb.DiscreteControlXCBR.AsObject,
  }
}

export class RecloserDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): RecloserDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): RecloserDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): RecloserDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): RecloserDiscreteControl;

  getRecloserdiscretecontrolxcbr(): RecloserDiscreteControlXCBR | undefined;
  setRecloserdiscretecontrolxcbr(value?: RecloserDiscreteControlXCBR): RecloserDiscreteControl;
  hasRecloserdiscretecontrolxcbr(): boolean;
  clearRecloserdiscretecontrolxcbr(): RecloserDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserDiscreteControl): RecloserDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: RecloserDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserDiscreteControl;
  static deserializeBinaryFromReader(message: RecloserDiscreteControl, reader: jspb.BinaryReader): RecloserDiscreteControl;
}

export namespace RecloserDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    recloserdiscretecontrolxcbr?: RecloserDiscreteControlXCBR.AsObject,
  }
}

export class Recloser extends jspb.Message {
  getConductingequipment(): commonmodule_commonmodule_pb.ConductingEquipment | undefined;
  setConductingequipment(value?: commonmodule_commonmodule_pb.ConductingEquipment): Recloser;
  hasConductingequipment(): boolean;
  clearConductingequipment(): Recloser;

  getNormalopen(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setNormalopen(value?: google_protobuf_wrappers_pb.BoolValue): Recloser;
  hasNormalopen(): boolean;
  clearNormalopen(): Recloser;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Recloser.AsObject;
  static toObject(includeInstance: boolean, msg: Recloser): Recloser.AsObject;
  static serializeBinaryToWriter(message: Recloser, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Recloser;
  static deserializeBinaryFromReader(message: Recloser, reader: jspb.BinaryReader): Recloser;
}

export namespace Recloser {
  export type AsObject = {
    conductingequipment?: commonmodule_commonmodule_pb.ConductingEquipment.AsObject,
    normalopen?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class RecloserDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): RecloserDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): RecloserDiscreteControlProfile;

  getRecloser(): Recloser | undefined;
  setRecloser(value?: Recloser): RecloserDiscreteControlProfile;
  hasRecloser(): boolean;
  clearRecloser(): RecloserDiscreteControlProfile;

  getRecloserdiscretecontrol(): RecloserDiscreteControl | undefined;
  setRecloserdiscretecontrol(value?: RecloserDiscreteControl): RecloserDiscreteControlProfile;
  hasRecloserdiscretecontrol(): boolean;
  clearRecloserdiscretecontrol(): RecloserDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserDiscreteControlProfile): RecloserDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: RecloserDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserDiscreteControlProfile;
  static deserializeBinaryFromReader(message: RecloserDiscreteControlProfile, reader: jspb.BinaryReader): RecloserDiscreteControlProfile;
}

export namespace RecloserDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    recloser?: Recloser.AsObject,
    recloserdiscretecontrol?: RecloserDiscreteControl.AsObject,
  }
}

export class RecloserEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): RecloserEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): RecloserEvent;

  getStatusandeventxcbr(): commonmodule_commonmodule_pb.StatusAndEventXCBR | undefined;
  setStatusandeventxcbr(value?: commonmodule_commonmodule_pb.StatusAndEventXCBR): RecloserEvent;
  hasStatusandeventxcbr(): boolean;
  clearStatusandeventxcbr(): RecloserEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserEvent.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserEvent): RecloserEvent.AsObject;
  static serializeBinaryToWriter(message: RecloserEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserEvent;
  static deserializeBinaryFromReader(message: RecloserEvent, reader: jspb.BinaryReader): RecloserEvent;
}

export namespace RecloserEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    statusandeventxcbr?: commonmodule_commonmodule_pb.StatusAndEventXCBR.AsObject,
  }
}

export class RecloserEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): RecloserEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): RecloserEventProfile;

  getRecloser(): Recloser | undefined;
  setRecloser(value?: Recloser): RecloserEventProfile;
  hasRecloser(): boolean;
  clearRecloser(): RecloserEventProfile;

  getRecloserevent(): RecloserEvent | undefined;
  setRecloserevent(value?: RecloserEvent): RecloserEventProfile;
  hasRecloserevent(): boolean;
  clearRecloserevent(): RecloserEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserEventProfile): RecloserEventProfile.AsObject;
  static serializeBinaryToWriter(message: RecloserEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserEventProfile;
  static deserializeBinaryFromReader(message: RecloserEventProfile, reader: jspb.BinaryReader): RecloserEventProfile;
}

export namespace RecloserEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    recloser?: Recloser.AsObject,
    recloserevent?: RecloserEvent.AsObject,
  }
}

export class RecloserReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): RecloserReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): RecloserReading;

  getDiffreadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setDiffreadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): RecloserReading;
  hasDiffreadingmmxu(): boolean;
  clearDiffreadingmmxu(): RecloserReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): RecloserReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): RecloserReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): RecloserReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): RecloserReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): RecloserReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): RecloserReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserReading.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserReading): RecloserReading.AsObject;
  static serializeBinaryToWriter(message: RecloserReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserReading;
  static deserializeBinaryFromReader(message: RecloserReading, reader: jspb.BinaryReader): RecloserReading;
}

export namespace RecloserReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    diffreadingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class RecloserReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): RecloserReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): RecloserReadingProfile;

  getRecloser(): Recloser | undefined;
  setRecloser(value?: Recloser): RecloserReadingProfile;
  hasRecloser(): boolean;
  clearRecloser(): RecloserReadingProfile;

  getRecloserreadingList(): Array<RecloserReading>;
  setRecloserreadingList(value: Array<RecloserReading>): RecloserReadingProfile;
  clearRecloserreadingList(): RecloserReadingProfile;
  addRecloserreading(value?: RecloserReading, index?: number): RecloserReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserReadingProfile): RecloserReadingProfile.AsObject;
  static serializeBinaryToWriter(message: RecloserReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserReadingProfile;
  static deserializeBinaryFromReader(message: RecloserReadingProfile, reader: jspb.BinaryReader): RecloserReadingProfile;
}

export namespace RecloserReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    recloser?: Recloser.AsObject,
    recloserreadingList: Array<RecloserReading.AsObject>,
  }
}

export class RecloserStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): RecloserStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): RecloserStatus;

  getStatusandeventxcbr(): commonmodule_commonmodule_pb.StatusAndEventXCBR | undefined;
  setStatusandeventxcbr(value?: commonmodule_commonmodule_pb.StatusAndEventXCBR): RecloserStatus;
  hasStatusandeventxcbr(): boolean;
  clearStatusandeventxcbr(): RecloserStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserStatus.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserStatus): RecloserStatus.AsObject;
  static serializeBinaryToWriter(message: RecloserStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserStatus;
  static deserializeBinaryFromReader(message: RecloserStatus, reader: jspb.BinaryReader): RecloserStatus;
}

export namespace RecloserStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    statusandeventxcbr?: commonmodule_commonmodule_pb.StatusAndEventXCBR.AsObject,
  }
}

export class RecloserStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): RecloserStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): RecloserStatusProfile;

  getRecloser(): Recloser | undefined;
  setRecloser(value?: Recloser): RecloserStatusProfile;
  hasRecloser(): boolean;
  clearRecloser(): RecloserStatusProfile;

  getRecloserstatus(): RecloserStatus | undefined;
  setRecloserstatus(value?: RecloserStatus): RecloserStatusProfile;
  hasRecloserstatus(): boolean;
  clearRecloserstatus(): RecloserStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecloserStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: RecloserStatusProfile): RecloserStatusProfile.AsObject;
  static serializeBinaryToWriter(message: RecloserStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecloserStatusProfile;
  static deserializeBinaryFromReader(message: RecloserStatusProfile, reader: jspb.BinaryReader): RecloserStatusProfile;
}

export namespace RecloserStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    recloser?: Recloser.AsObject,
    recloserstatus?: RecloserStatus.AsObject,
  }
}

