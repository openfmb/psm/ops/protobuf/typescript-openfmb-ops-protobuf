import * as jspb from 'google-protobuf'

import * as uml_pb from '../uml_pb';
import * as google_protobuf_wrappers_pb from 'google-protobuf/google/protobuf/wrappers_pb';
import * as commonmodule_commonmodule_pb from '../commonmodule/commonmodule_pb';


export class ESSCapabilityConfiguration extends jspb.Message {
  getSourcecapabilityconfiguration(): commonmodule_commonmodule_pb.SourceCapabilityConfiguration | undefined;
  setSourcecapabilityconfiguration(value?: commonmodule_commonmodule_pb.SourceCapabilityConfiguration): ESSCapabilityConfiguration;
  hasSourcecapabilityconfiguration(): boolean;
  clearSourcecapabilityconfiguration(): ESSCapabilityConfiguration;

  getVachartemax(): commonmodule_commonmodule_pb.ASG | undefined;
  setVachartemax(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityConfiguration;
  hasVachartemax(): boolean;
  clearVachartemax(): ESSCapabilityConfiguration;

  getVadischartemax(): commonmodule_commonmodule_pb.ASG | undefined;
  setVadischartemax(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityConfiguration;
  hasVadischartemax(): boolean;
  clearVadischartemax(): ESSCapabilityConfiguration;

  getWchartemax(): commonmodule_commonmodule_pb.ASG | undefined;
  setWchartemax(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityConfiguration;
  hasWchartemax(): boolean;
  clearWchartemax(): ESSCapabilityConfiguration;

  getWdischartemax(): commonmodule_commonmodule_pb.ASG | undefined;
  setWdischartemax(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityConfiguration;
  hasWdischartemax(): boolean;
  clearWdischartemax(): ESSCapabilityConfiguration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCapabilityConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCapabilityConfiguration): ESSCapabilityConfiguration.AsObject;
  static serializeBinaryToWriter(message: ESSCapabilityConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCapabilityConfiguration;
  static deserializeBinaryFromReader(message: ESSCapabilityConfiguration, reader: jspb.BinaryReader): ESSCapabilityConfiguration;
}

export namespace ESSCapabilityConfiguration {
  export type AsObject = {
    sourcecapabilityconfiguration?: commonmodule_commonmodule_pb.SourceCapabilityConfiguration.AsObject,
    vachartemax?: commonmodule_commonmodule_pb.ASG.AsObject,
    vadischartemax?: commonmodule_commonmodule_pb.ASG.AsObject,
    wchartemax?: commonmodule_commonmodule_pb.ASG.AsObject,
    wdischartemax?: commonmodule_commonmodule_pb.ASG.AsObject,
  }
}

export class ESSCapabilityOverride extends jspb.Message {
  getNameplatevalue(): commonmodule_commonmodule_pb.NameplateValue | undefined;
  setNameplatevalue(value?: commonmodule_commonmodule_pb.NameplateValue): ESSCapabilityOverride;
  hasNameplatevalue(): boolean;
  clearNameplatevalue(): ESSCapabilityOverride;

  getEsscapabilityconfiguration(): ESSCapabilityConfiguration | undefined;
  setEsscapabilityconfiguration(value?: ESSCapabilityConfiguration): ESSCapabilityOverride;
  hasEsscapabilityconfiguration(): boolean;
  clearEsscapabilityconfiguration(): ESSCapabilityOverride;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCapabilityOverride.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCapabilityOverride): ESSCapabilityOverride.AsObject;
  static serializeBinaryToWriter(message: ESSCapabilityOverride, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCapabilityOverride;
  static deserializeBinaryFromReader(message: ESSCapabilityOverride, reader: jspb.BinaryReader): ESSCapabilityOverride;
}

export namespace ESSCapabilityOverride {
  export type AsObject = {
    nameplatevalue?: commonmodule_commonmodule_pb.NameplateValue.AsObject,
    esscapabilityconfiguration?: ESSCapabilityConfiguration.AsObject,
  }
}

export class ESSCapabilityOverrideProfile extends jspb.Message {
  getCapabilitymessageinfo(): commonmodule_commonmodule_pb.CapabilityMessageInfo | undefined;
  setCapabilitymessageinfo(value?: commonmodule_commonmodule_pb.CapabilityMessageInfo): ESSCapabilityOverrideProfile;
  hasCapabilitymessageinfo(): boolean;
  clearCapabilitymessageinfo(): ESSCapabilityOverrideProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSCapabilityOverrideProfile;
  hasEss(): boolean;
  clearEss(): ESSCapabilityOverrideProfile;

  getEsscapabilityoverride(): ESSCapabilityOverride | undefined;
  setEsscapabilityoverride(value?: ESSCapabilityOverride): ESSCapabilityOverrideProfile;
  hasEsscapabilityoverride(): boolean;
  clearEsscapabilityoverride(): ESSCapabilityOverrideProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCapabilityOverrideProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCapabilityOverrideProfile): ESSCapabilityOverrideProfile.AsObject;
  static serializeBinaryToWriter(message: ESSCapabilityOverrideProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCapabilityOverrideProfile;
  static deserializeBinaryFromReader(message: ESSCapabilityOverrideProfile, reader: jspb.BinaryReader): ESSCapabilityOverrideProfile;
}

export namespace ESSCapabilityOverrideProfile {
  export type AsObject = {
    capabilitymessageinfo?: commonmodule_commonmodule_pb.CapabilityMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    esscapabilityoverride?: ESSCapabilityOverride.AsObject,
  }
}

export class ESSCapabilityRatings extends jspb.Message {
  getSourcecapabilityratings(): commonmodule_commonmodule_pb.SourceCapabilityRatings | undefined;
  setSourcecapabilityratings(value?: commonmodule_commonmodule_pb.SourceCapabilityRatings): ESSCapabilityRatings;
  hasSourcecapabilityratings(): boolean;
  clearSourcecapabilityratings(): ESSCapabilityRatings;

  getVachartemaxrtg(): commonmodule_commonmodule_pb.ASG | undefined;
  setVachartemaxrtg(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityRatings;
  hasVachartemaxrtg(): boolean;
  clearVachartemaxrtg(): ESSCapabilityRatings;

  getVadischartemaxrtg(): commonmodule_commonmodule_pb.ASG | undefined;
  setVadischartemaxrtg(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityRatings;
  hasVadischartemaxrtg(): boolean;
  clearVadischartemaxrtg(): ESSCapabilityRatings;

  getWchartemaxrtg(): commonmodule_commonmodule_pb.ASG | undefined;
  setWchartemaxrtg(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityRatings;
  hasWchartemaxrtg(): boolean;
  clearWchartemaxrtg(): ESSCapabilityRatings;

  getWdischartemaxrtg(): commonmodule_commonmodule_pb.ASG | undefined;
  setWdischartemaxrtg(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityRatings;
  hasWdischartemaxrtg(): boolean;
  clearWdischartemaxrtg(): ESSCapabilityRatings;

  getWhrtg(): commonmodule_commonmodule_pb.ASG | undefined;
  setWhrtg(value?: commonmodule_commonmodule_pb.ASG): ESSCapabilityRatings;
  hasWhrtg(): boolean;
  clearWhrtg(): ESSCapabilityRatings;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCapabilityRatings.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCapabilityRatings): ESSCapabilityRatings.AsObject;
  static serializeBinaryToWriter(message: ESSCapabilityRatings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCapabilityRatings;
  static deserializeBinaryFromReader(message: ESSCapabilityRatings, reader: jspb.BinaryReader): ESSCapabilityRatings;
}

export namespace ESSCapabilityRatings {
  export type AsObject = {
    sourcecapabilityratings?: commonmodule_commonmodule_pb.SourceCapabilityRatings.AsObject,
    vachartemaxrtg?: commonmodule_commonmodule_pb.ASG.AsObject,
    vadischartemaxrtg?: commonmodule_commonmodule_pb.ASG.AsObject,
    wchartemaxrtg?: commonmodule_commonmodule_pb.ASG.AsObject,
    wdischartemaxrtg?: commonmodule_commonmodule_pb.ASG.AsObject,
    whrtg?: commonmodule_commonmodule_pb.ASG.AsObject,
  }
}

export class ESSCapability extends jspb.Message {
  getNameplatevalue(): commonmodule_commonmodule_pb.NameplateValue | undefined;
  setNameplatevalue(value?: commonmodule_commonmodule_pb.NameplateValue): ESSCapability;
  hasNameplatevalue(): boolean;
  clearNameplatevalue(): ESSCapability;

  getEsscapabilityratings(): ESSCapabilityRatings | undefined;
  setEsscapabilityratings(value?: ESSCapabilityRatings): ESSCapability;
  hasEsscapabilityratings(): boolean;
  clearEsscapabilityratings(): ESSCapability;

  getEsscapabilityconfiguration(): ESSCapabilityConfiguration | undefined;
  setEsscapabilityconfiguration(value?: ESSCapabilityConfiguration): ESSCapability;
  hasEsscapabilityconfiguration(): boolean;
  clearEsscapabilityconfiguration(): ESSCapability;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCapability.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCapability): ESSCapability.AsObject;
  static serializeBinaryToWriter(message: ESSCapability, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCapability;
  static deserializeBinaryFromReader(message: ESSCapability, reader: jspb.BinaryReader): ESSCapability;
}

export namespace ESSCapability {
  export type AsObject = {
    nameplatevalue?: commonmodule_commonmodule_pb.NameplateValue.AsObject,
    esscapabilityratings?: ESSCapabilityRatings.AsObject,
    esscapabilityconfiguration?: ESSCapabilityConfiguration.AsObject,
  }
}

export class ESSCapabilityProfile extends jspb.Message {
  getCapabilitymessageinfo(): commonmodule_commonmodule_pb.CapabilityMessageInfo | undefined;
  setCapabilitymessageinfo(value?: commonmodule_commonmodule_pb.CapabilityMessageInfo): ESSCapabilityProfile;
  hasCapabilitymessageinfo(): boolean;
  clearCapabilitymessageinfo(): ESSCapabilityProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSCapabilityProfile;
  hasEss(): boolean;
  clearEss(): ESSCapabilityProfile;

  getEsscapability(): ESSCapability | undefined;
  setEsscapability(value?: ESSCapability): ESSCapabilityProfile;
  hasEsscapability(): boolean;
  clearEsscapability(): ESSCapabilityProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCapabilityProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCapabilityProfile): ESSCapabilityProfile.AsObject;
  static serializeBinaryToWriter(message: ESSCapabilityProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCapabilityProfile;
  static deserializeBinaryFromReader(message: ESSCapabilityProfile, reader: jspb.BinaryReader): ESSCapabilityProfile;
}

export namespace ESSCapabilityProfile {
  export type AsObject = {
    capabilitymessageinfo?: commonmodule_commonmodule_pb.CapabilityMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    esscapability?: ESSCapability.AsObject,
  }
}

export class FrequencyRegulation extends jspb.Message {
  getFrequencydeadbandminus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setFrequencydeadbandminus(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasFrequencydeadbandminus(): boolean;
  clearFrequencydeadbandminus(): FrequencyRegulation;

  getFrequencydeadbandplus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setFrequencydeadbandplus(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasFrequencydeadbandplus(): boolean;
  clearFrequencydeadbandplus(): FrequencyRegulation;

  getFrequencyregulationctl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setFrequencyregulationctl(value?: google_protobuf_wrappers_pb.BoolValue): FrequencyRegulation;
  hasFrequencyregulationctl(): boolean;
  clearFrequencyregulationctl(): FrequencyRegulation;

  getFrequencysetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setFrequencysetpoint(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasFrequencysetpoint(): boolean;
  clearFrequencysetpoint(): FrequencyRegulation;

  getGridfrequencystablebandminus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setGridfrequencystablebandminus(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasGridfrequencystablebandminus(): boolean;
  clearGridfrequencystablebandminus(): FrequencyRegulation;

  getGridfrequencystablebandplus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setGridfrequencystablebandplus(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasGridfrequencystablebandplus(): boolean;
  clearGridfrequencystablebandplus(): FrequencyRegulation;

  getOverfrequencydroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setOverfrequencydroop(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasOverfrequencydroop(): boolean;
  clearOverfrequencydroop(): FrequencyRegulation;

  getUnderfrequencydroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setUnderfrequencydroop(value?: google_protobuf_wrappers_pb.FloatValue): FrequencyRegulation;
  hasUnderfrequencydroop(): boolean;
  clearUnderfrequencydroop(): FrequencyRegulation;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FrequencyRegulation.AsObject;
  static toObject(includeInstance: boolean, msg: FrequencyRegulation): FrequencyRegulation.AsObject;
  static serializeBinaryToWriter(message: FrequencyRegulation, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FrequencyRegulation;
  static deserializeBinaryFromReader(message: FrequencyRegulation, reader: jspb.BinaryReader): FrequencyRegulation;
}

export namespace FrequencyRegulation {
  export type AsObject = {
    frequencydeadbandminus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    frequencydeadbandplus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    frequencyregulationctl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    frequencysetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    gridfrequencystablebandminus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    gridfrequencystablebandplus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    overfrequencydroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    underfrequencydroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class PeakShaving extends jspb.Message {
  getBaseshavinglimit(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setBaseshavinglimit(value?: google_protobuf_wrappers_pb.FloatValue): PeakShaving;
  hasBaseshavinglimit(): boolean;
  clearBaseshavinglimit(): PeakShaving;

  getPeakshavingctl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setPeakshavingctl(value?: google_protobuf_wrappers_pb.BoolValue): PeakShaving;
  hasPeakshavingctl(): boolean;
  clearPeakshavingctl(): PeakShaving;

  getPeakshavinglimit(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPeakshavinglimit(value?: google_protobuf_wrappers_pb.FloatValue): PeakShaving;
  hasPeakshavinglimit(): boolean;
  clearPeakshavinglimit(): PeakShaving;

  getSocmanagementallowedhighlimit(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSocmanagementallowedhighlimit(value?: google_protobuf_wrappers_pb.FloatValue): PeakShaving;
  hasSocmanagementallowedhighlimit(): boolean;
  clearSocmanagementallowedhighlimit(): PeakShaving;

  getSocmanagementallowedlowlimit(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSocmanagementallowedlowlimit(value?: google_protobuf_wrappers_pb.FloatValue): PeakShaving;
  hasSocmanagementallowedlowlimit(): boolean;
  clearSocmanagementallowedlowlimit(): PeakShaving;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PeakShaving.AsObject;
  static toObject(includeInstance: boolean, msg: PeakShaving): PeakShaving.AsObject;
  static serializeBinaryToWriter(message: PeakShaving, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PeakShaving;
  static deserializeBinaryFromReader(message: PeakShaving, reader: jspb.BinaryReader): PeakShaving;
}

export namespace PeakShaving {
  export type AsObject = {
    baseshavinglimit?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    peakshavingctl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    peakshavinglimit?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    socmanagementallowedhighlimit?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    socmanagementallowedlowlimit?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class SocLimit extends jspb.Message {
  getSochighlimit(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSochighlimit(value?: google_protobuf_wrappers_pb.FloatValue): SocLimit;
  hasSochighlimit(): boolean;
  clearSochighlimit(): SocLimit;

  getSochighlimithysteresis(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSochighlimithysteresis(value?: google_protobuf_wrappers_pb.FloatValue): SocLimit;
  hasSochighlimithysteresis(): boolean;
  clearSochighlimithysteresis(): SocLimit;

  getSoclimitctl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setSoclimitctl(value?: google_protobuf_wrappers_pb.BoolValue): SocLimit;
  hasSoclimitctl(): boolean;
  clearSoclimitctl(): SocLimit;

  getSoclowlimit(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSoclowlimit(value?: google_protobuf_wrappers_pb.FloatValue): SocLimit;
  hasSoclowlimit(): boolean;
  clearSoclowlimit(): SocLimit;

  getSoclowlimithysteresis(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSoclowlimithysteresis(value?: google_protobuf_wrappers_pb.FloatValue): SocLimit;
  hasSoclowlimithysteresis(): boolean;
  clearSoclowlimithysteresis(): SocLimit;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SocLimit.AsObject;
  static toObject(includeInstance: boolean, msg: SocLimit): SocLimit.AsObject;
  static serializeBinaryToWriter(message: SocLimit, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SocLimit;
  static deserializeBinaryFromReader(message: SocLimit, reader: jspb.BinaryReader): SocLimit;
}

export namespace SocLimit {
  export type AsObject = {
    sochighlimit?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    sochighlimithysteresis?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    soclimitctl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    soclowlimit?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    soclowlimithysteresis?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class SOCManagement extends jspb.Message {
  getSocdeadbandminus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSocdeadbandminus(value?: google_protobuf_wrappers_pb.FloatValue): SOCManagement;
  hasSocdeadbandminus(): boolean;
  clearSocdeadbandminus(): SOCManagement;

  getSocdeadbandplus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSocdeadbandplus(value?: google_protobuf_wrappers_pb.FloatValue): SOCManagement;
  hasSocdeadbandplus(): boolean;
  clearSocdeadbandplus(): SOCManagement;

  getSocmanagementctl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setSocmanagementctl(value?: google_protobuf_wrappers_pb.BoolValue): SOCManagement;
  hasSocmanagementctl(): boolean;
  clearSocmanagementctl(): SOCManagement;

  getSocpowersetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSocpowersetpoint(value?: google_protobuf_wrappers_pb.FloatValue): SOCManagement;
  hasSocpowersetpoint(): boolean;
  clearSocpowersetpoint(): SOCManagement;

  getSocsetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setSocsetpoint(value?: google_protobuf_wrappers_pb.FloatValue): SOCManagement;
  hasSocsetpoint(): boolean;
  clearSocsetpoint(): SOCManagement;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SOCManagement.AsObject;
  static toObject(includeInstance: boolean, msg: SOCManagement): SOCManagement.AsObject;
  static serializeBinaryToWriter(message: SOCManagement, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SOCManagement;
  static deserializeBinaryFromReader(message: SOCManagement, reader: jspb.BinaryReader): SOCManagement;
}

export namespace SOCManagement {
  export type AsObject = {
    socdeadbandminus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    socdeadbandplus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    socmanagementctl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    socpowersetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    socsetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class VoltageRegulation extends jspb.Message {
  getOvervoltagedroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setOvervoltagedroop(value?: google_protobuf_wrappers_pb.FloatValue): VoltageRegulation;
  hasOvervoltagedroop(): boolean;
  clearOvervoltagedroop(): VoltageRegulation;

  getUndervoltagedroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setUndervoltagedroop(value?: google_protobuf_wrappers_pb.FloatValue): VoltageRegulation;
  hasUndervoltagedroop(): boolean;
  clearUndervoltagedroop(): VoltageRegulation;

  getVoltagedeadbandminus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setVoltagedeadbandminus(value?: google_protobuf_wrappers_pb.FloatValue): VoltageRegulation;
  hasVoltagedeadbandminus(): boolean;
  clearVoltagedeadbandminus(): VoltageRegulation;

  getVoltagedeadbandplus(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setVoltagedeadbandplus(value?: google_protobuf_wrappers_pb.FloatValue): VoltageRegulation;
  hasVoltagedeadbandplus(): boolean;
  clearVoltagedeadbandplus(): VoltageRegulation;

  getVoltagesetpoint(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setVoltagesetpoint(value?: google_protobuf_wrappers_pb.FloatValue): VoltageRegulation;
  hasVoltagesetpoint(): boolean;
  clearVoltagesetpoint(): VoltageRegulation;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltageRegulation.AsObject;
  static toObject(includeInstance: boolean, msg: VoltageRegulation): VoltageRegulation.AsObject;
  static serializeBinaryToWriter(message: VoltageRegulation, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltageRegulation;
  static deserializeBinaryFromReader(message: VoltageRegulation, reader: jspb.BinaryReader): VoltageRegulation;
}

export namespace VoltageRegulation {
  export type AsObject = {
    overvoltagedroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    undervoltagedroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    voltagedeadbandminus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    voltagedeadbandplus?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    voltagesetpoint?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class VoltageDroop extends jspb.Message {
  getVoltagedroopctl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setVoltagedroopctl(value?: google_protobuf_wrappers_pb.BoolValue): VoltageDroop;
  hasVoltagedroopctl(): boolean;
  clearVoltagedroopctl(): VoltageDroop;

  getVoltageregulation(): VoltageRegulation | undefined;
  setVoltageregulation(value?: VoltageRegulation): VoltageDroop;
  hasVoltageregulation(): boolean;
  clearVoltageregulation(): VoltageDroop;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltageDroop.AsObject;
  static toObject(includeInstance: boolean, msg: VoltageDroop): VoltageDroop.AsObject;
  static serializeBinaryToWriter(message: VoltageDroop, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltageDroop;
  static deserializeBinaryFromReader(message: VoltageDroop, reader: jspb.BinaryReader): VoltageDroop;
}

export namespace VoltageDroop {
  export type AsObject = {
    voltagedroopctl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    voltageregulation?: VoltageRegulation.AsObject,
  }
}

export class VoltagePI extends jspb.Message {
  getVoltagepictl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setVoltagepictl(value?: google_protobuf_wrappers_pb.BoolValue): VoltagePI;
  hasVoltagepictl(): boolean;
  clearVoltagepictl(): VoltagePI;

  getVoltageregulation(): VoltageRegulation | undefined;
  setVoltageregulation(value?: VoltageRegulation): VoltagePI;
  hasVoltageregulation(): boolean;
  clearVoltageregulation(): VoltagePI;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoltagePI.AsObject;
  static toObject(includeInstance: boolean, msg: VoltagePI): VoltagePI.AsObject;
  static serializeBinaryToWriter(message: VoltagePI, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoltagePI;
  static deserializeBinaryFromReader(message: VoltagePI, reader: jspb.BinaryReader): VoltagePI;
}

export namespace VoltagePI {
  export type AsObject = {
    voltagepictl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    voltageregulation?: VoltageRegulation.AsObject,
  }
}

export class CapacityFirming extends jspb.Message {
  getCapacityfirmingctl(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setCapacityfirmingctl(value?: google_protobuf_wrappers_pb.BoolValue): CapacityFirming;
  hasCapacityfirmingctl(): boolean;
  clearCapacityfirmingctl(): CapacityFirming;

  getLimitnegativeDpDt(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setLimitnegativeDpDt(value?: google_protobuf_wrappers_pb.FloatValue): CapacityFirming;
  hasLimitnegativeDpDt(): boolean;
  clearLimitnegativeDpDt(): CapacityFirming;

  getLimitpositiveDpDt(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setLimitpositiveDpDt(value?: google_protobuf_wrappers_pb.FloatValue): CapacityFirming;
  hasLimitpositiveDpDt(): boolean;
  clearLimitpositiveDpDt(): CapacityFirming;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CapacityFirming.AsObject;
  static toObject(includeInstance: boolean, msg: CapacityFirming): CapacityFirming.AsObject;
  static serializeBinaryToWriter(message: CapacityFirming, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CapacityFirming;
  static deserializeBinaryFromReader(message: CapacityFirming, reader: jspb.BinaryReader): CapacityFirming;
}

export namespace CapacityFirming {
  export type AsObject = {
    capacityfirmingctl?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    limitnegativeDpDt?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    limitpositiveDpDt?: google_protobuf_wrappers_pb.FloatValue.AsObject,
  }
}

export class ESSFunction extends jspb.Message {
  getCapacityfirming(): CapacityFirming | undefined;
  setCapacityfirming(value?: CapacityFirming): ESSFunction;
  hasCapacityfirming(): boolean;
  clearCapacityfirming(): ESSFunction;

  getFrequencyregulation(): FrequencyRegulation | undefined;
  setFrequencyregulation(value?: FrequencyRegulation): ESSFunction;
  hasFrequencyregulation(): boolean;
  clearFrequencyregulation(): ESSFunction;

  getPeakshaving(): PeakShaving | undefined;
  setPeakshaving(value?: PeakShaving): ESSFunction;
  hasPeakshaving(): boolean;
  clearPeakshaving(): ESSFunction;

  getSoclimit(): SocLimit | undefined;
  setSoclimit(value?: SocLimit): ESSFunction;
  hasSoclimit(): boolean;
  clearSoclimit(): ESSFunction;

  getSocmanagement(): SOCManagement | undefined;
  setSocmanagement(value?: SOCManagement): ESSFunction;
  hasSocmanagement(): boolean;
  clearSocmanagement(): ESSFunction;

  getVoltagedroop(): VoltageDroop | undefined;
  setVoltagedroop(value?: VoltageDroop): ESSFunction;
  hasVoltagedroop(): boolean;
  clearVoltagedroop(): ESSFunction;

  getVoltagepi(): VoltagePI | undefined;
  setVoltagepi(value?: VoltagePI): ESSFunction;
  hasVoltagepi(): boolean;
  clearVoltagepi(): ESSFunction;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSFunction.AsObject;
  static toObject(includeInstance: boolean, msg: ESSFunction): ESSFunction.AsObject;
  static serializeBinaryToWriter(message: ESSFunction, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSFunction;
  static deserializeBinaryFromReader(message: ESSFunction, reader: jspb.BinaryReader): ESSFunction;
}

export namespace ESSFunction {
  export type AsObject = {
    capacityfirming?: CapacityFirming.AsObject,
    frequencyregulation?: FrequencyRegulation.AsObject,
    peakshaving?: PeakShaving.AsObject,
    soclimit?: SocLimit.AsObject,
    socmanagement?: SOCManagement.AsObject,
    voltagedroop?: VoltageDroop.AsObject,
    voltagepi?: VoltagePI.AsObject,
  }
}

export class ESSPoint extends jspb.Message {
  getBlackstartenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.ControlSPC): ESSPoint;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): ESSPoint;

  getFunction(): ESSFunction | undefined;
  setFunction(value?: ESSFunction): ESSPoint;
  hasFunction(): boolean;
  clearFunction(): ESSPoint;

  getMode(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setMode(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): ESSPoint;
  hasMode(): boolean;
  clearMode(): ESSPoint;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): ESSPoint;
  hasRamprates(): boolean;
  clearRamprates(): ESSPoint;

  getReset(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setReset(value?: commonmodule_commonmodule_pb.ControlSPC): ESSPoint;
  hasReset(): boolean;
  clearReset(): ESSPoint;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): ESSPoint;
  hasState(): boolean;
  clearState(): ESSPoint;

  getTranstoislndongridlossenabled(): commonmodule_commonmodule_pb.ControlSPC | undefined;
  setTranstoislndongridlossenabled(value?: commonmodule_commonmodule_pb.ControlSPC): ESSPoint;
  hasTranstoislndongridlossenabled(): boolean;
  clearTranstoislndongridlossenabled(): ESSPoint;

  getEnterserviceoperation(): commonmodule_commonmodule_pb.EnterServiceAPC | undefined;
  setEnterserviceoperation(value?: commonmodule_commonmodule_pb.EnterServiceAPC): ESSPoint;
  hasEnterserviceoperation(): boolean;
  clearEnterserviceoperation(): ESSPoint;

  getHzwoperation(): commonmodule_commonmodule_pb.HzWAPC | undefined;
  setHzwoperation(value?: commonmodule_commonmodule_pb.HzWAPC): ESSPoint;
  hasHzwoperation(): boolean;
  clearHzwoperation(): ESSPoint;

  getLimitwoperation(): commonmodule_commonmodule_pb.LimitWAPC | undefined;
  setLimitwoperation(value?: commonmodule_commonmodule_pb.LimitWAPC): ESSPoint;
  hasLimitwoperation(): boolean;
  clearLimitwoperation(): ESSPoint;

  getPfoperation(): commonmodule_commonmodule_pb.PFSPC | undefined;
  setPfoperation(value?: commonmodule_commonmodule_pb.PFSPC): ESSPoint;
  hasPfoperation(): boolean;
  clearPfoperation(): ESSPoint;

  getTmhztripoperation(): commonmodule_commonmodule_pb.TmHzCSG | undefined;
  setTmhztripoperation(value?: commonmodule_commonmodule_pb.TmHzCSG): ESSPoint;
  hasTmhztripoperation(): boolean;
  clearTmhztripoperation(): ESSPoint;

  getTmvolttripoperation(): commonmodule_commonmodule_pb.TmVoltCSG | undefined;
  setTmvolttripoperation(value?: commonmodule_commonmodule_pb.TmVoltCSG): ESSPoint;
  hasTmvolttripoperation(): boolean;
  clearTmvolttripoperation(): ESSPoint;

  getVaroperation(): commonmodule_commonmodule_pb.VarSPC | undefined;
  setVaroperation(value?: commonmodule_commonmodule_pb.VarSPC): ESSPoint;
  hasVaroperation(): boolean;
  clearVaroperation(): ESSPoint;

  getVoltvaroperation(): commonmodule_commonmodule_pb.VoltVarCSG | undefined;
  setVoltvaroperation(value?: commonmodule_commonmodule_pb.VoltVarCSG): ESSPoint;
  hasVoltvaroperation(): boolean;
  clearVoltvaroperation(): ESSPoint;

  getVoltwoperation(): commonmodule_commonmodule_pb.VoltWCSG | undefined;
  setVoltwoperation(value?: commonmodule_commonmodule_pb.VoltWCSG): ESSPoint;
  hasVoltwoperation(): boolean;
  clearVoltwoperation(): ESSPoint;

  getWvaroperation(): commonmodule_commonmodule_pb.WVarCSG | undefined;
  setWvaroperation(value?: commonmodule_commonmodule_pb.WVarCSG): ESSPoint;
  hasWvaroperation(): boolean;
  clearWvaroperation(): ESSPoint;

  getWoperation(): commonmodule_commonmodule_pb.WSPC | undefined;
  setWoperation(value?: commonmodule_commonmodule_pb.WSPC): ESSPoint;
  hasWoperation(): boolean;
  clearWoperation(): ESSPoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSPoint.AsObject;
  static toObject(includeInstance: boolean, msg: ESSPoint): ESSPoint.AsObject;
  static serializeBinaryToWriter(message: ESSPoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSPoint;
  static deserializeBinaryFromReader(message: ESSPoint, reader: jspb.BinaryReader): ESSPoint;
}

export namespace ESSPoint {
  export type AsObject = {
    blackstartenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    pb_function?: ESSFunction.AsObject,
    mode?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reset?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    transtoislndongridlossenabled?: commonmodule_commonmodule_pb.ControlSPC.AsObject,
    enterserviceoperation?: commonmodule_commonmodule_pb.EnterServiceAPC.AsObject,
    hzwoperation?: commonmodule_commonmodule_pb.HzWAPC.AsObject,
    limitwoperation?: commonmodule_commonmodule_pb.LimitWAPC.AsObject,
    pfoperation?: commonmodule_commonmodule_pb.PFSPC.AsObject,
    tmhztripoperation?: commonmodule_commonmodule_pb.TmHzCSG.AsObject,
    tmvolttripoperation?: commonmodule_commonmodule_pb.TmVoltCSG.AsObject,
    varoperation?: commonmodule_commonmodule_pb.VarSPC.AsObject,
    voltvaroperation?: commonmodule_commonmodule_pb.VoltVarCSG.AsObject,
    voltwoperation?: commonmodule_commonmodule_pb.VoltWCSG.AsObject,
    wvaroperation?: commonmodule_commonmodule_pb.WVarCSG.AsObject,
    woperation?: commonmodule_commonmodule_pb.WSPC.AsObject,
  }
}

export class ESSCurvePoint extends jspb.Message {
  getControl(): ESSPoint | undefined;
  setControl(value?: ESSPoint): ESSCurvePoint;
  hasControl(): boolean;
  clearControl(): ESSCurvePoint;

  getStarttime(): commonmodule_commonmodule_pb.ControlTimestamp | undefined;
  setStarttime(value?: commonmodule_commonmodule_pb.ControlTimestamp): ESSCurvePoint;
  hasStarttime(): boolean;
  clearStarttime(): ESSCurvePoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCurvePoint.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCurvePoint): ESSCurvePoint.AsObject;
  static serializeBinaryToWriter(message: ESSCurvePoint, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCurvePoint;
  static deserializeBinaryFromReader(message: ESSCurvePoint, reader: jspb.BinaryReader): ESSCurvePoint;
}

export namespace ESSCurvePoint {
  export type AsObject = {
    control?: ESSPoint.AsObject,
    starttime?: commonmodule_commonmodule_pb.ControlTimestamp.AsObject,
  }
}

export class ESSCSG extends jspb.Message {
  getCrvptsList(): Array<ESSCurvePoint>;
  setCrvptsList(value: Array<ESSCurvePoint>): ESSCSG;
  clearCrvptsList(): ESSCSG;
  addCrvpts(value?: ESSCurvePoint, index?: number): ESSCurvePoint;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSCSG.AsObject;
  static toObject(includeInstance: boolean, msg: ESSCSG): ESSCSG.AsObject;
  static serializeBinaryToWriter(message: ESSCSG, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSCSG;
  static deserializeBinaryFromReader(message: ESSCSG, reader: jspb.BinaryReader): ESSCSG;
}

export namespace ESSCSG {
  export type AsObject = {
    crvptsList: Array<ESSCurvePoint.AsObject>,
  }
}

export class ESSControlScheduleFSCH extends jspb.Message {
  getValdcsg(): ESSCSG | undefined;
  setValdcsg(value?: ESSCSG): ESSControlScheduleFSCH;
  hasValdcsg(): boolean;
  clearValdcsg(): ESSControlScheduleFSCH;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSControlScheduleFSCH.AsObject;
  static toObject(includeInstance: boolean, msg: ESSControlScheduleFSCH): ESSControlScheduleFSCH.AsObject;
  static serializeBinaryToWriter(message: ESSControlScheduleFSCH, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSControlScheduleFSCH;
  static deserializeBinaryFromReader(message: ESSControlScheduleFSCH, reader: jspb.BinaryReader): ESSControlScheduleFSCH;
}

export namespace ESSControlScheduleFSCH {
  export type AsObject = {
    valdcsg?: ESSCSG.AsObject,
  }
}

export class EssControlFSCC extends jspb.Message {
  getControlfscc(): commonmodule_commonmodule_pb.ControlFSCC | undefined;
  setControlfscc(value?: commonmodule_commonmodule_pb.ControlFSCC): EssControlFSCC;
  hasControlfscc(): boolean;
  clearControlfscc(): EssControlFSCC;

  getEsscontrolschedulefsch(): ESSControlScheduleFSCH | undefined;
  setEsscontrolschedulefsch(value?: ESSControlScheduleFSCH): EssControlFSCC;
  hasEsscontrolschedulefsch(): boolean;
  clearEsscontrolschedulefsch(): EssControlFSCC;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EssControlFSCC.AsObject;
  static toObject(includeInstance: boolean, msg: EssControlFSCC): EssControlFSCC.AsObject;
  static serializeBinaryToWriter(message: EssControlFSCC, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EssControlFSCC;
  static deserializeBinaryFromReader(message: EssControlFSCC, reader: jspb.BinaryReader): EssControlFSCC;
}

export namespace EssControlFSCC {
  export type AsObject = {
    controlfscc?: commonmodule_commonmodule_pb.ControlFSCC.AsObject,
    esscontrolschedulefsch?: ESSControlScheduleFSCH.AsObject,
  }
}

export class ESSControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): ESSControl;
  hasControlvalue(): boolean;
  clearControlvalue(): ESSControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): ESSControl;
  hasCheck(): boolean;
  clearCheck(): ESSControl;

  getEsscontrolfscc(): EssControlFSCC | undefined;
  setEsscontrolfscc(value?: EssControlFSCC): ESSControl;
  hasEsscontrolfscc(): boolean;
  clearEsscontrolfscc(): ESSControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSControl.AsObject;
  static toObject(includeInstance: boolean, msg: ESSControl): ESSControl.AsObject;
  static serializeBinaryToWriter(message: ESSControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSControl;
  static deserializeBinaryFromReader(message: ESSControl, reader: jspb.BinaryReader): ESSControl;
}

export namespace ESSControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    esscontrolfscc?: EssControlFSCC.AsObject,
  }
}

export class ESSControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): ESSControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): ESSControlProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSControlProfile;
  hasEss(): boolean;
  clearEss(): ESSControlProfile;

  getEsscontrol(): ESSControl | undefined;
  setEsscontrol(value?: ESSControl): ESSControlProfile;
  hasEsscontrol(): boolean;
  clearEsscontrol(): ESSControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSControlProfile): ESSControlProfile.AsObject;
  static serializeBinaryToWriter(message: ESSControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSControlProfile;
  static deserializeBinaryFromReader(message: ESSControlProfile, reader: jspb.BinaryReader): ESSControlProfile;
}

export namespace ESSControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    esscontrol?: ESSControl.AsObject,
  }
}

export class ESSDiscreteControlDBAT extends jspb.Message {
  getLogicalnodeforcontrol(): commonmodule_commonmodule_pb.LogicalNodeForControl | undefined;
  setLogicalnodeforcontrol(value?: commonmodule_commonmodule_pb.LogicalNodeForControl): ESSDiscreteControlDBAT;
  hasLogicalnodeforcontrol(): boolean;
  clearLogicalnodeforcontrol(): ESSDiscreteControlDBAT;

  getControl(): ESSPoint | undefined;
  setControl(value?: ESSPoint): ESSDiscreteControlDBAT;
  hasControl(): boolean;
  clearControl(): ESSDiscreteControlDBAT;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSDiscreteControlDBAT.AsObject;
  static toObject(includeInstance: boolean, msg: ESSDiscreteControlDBAT): ESSDiscreteControlDBAT.AsObject;
  static serializeBinaryToWriter(message: ESSDiscreteControlDBAT, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSDiscreteControlDBAT;
  static deserializeBinaryFromReader(message: ESSDiscreteControlDBAT, reader: jspb.BinaryReader): ESSDiscreteControlDBAT;
}

export namespace ESSDiscreteControlDBAT {
  export type AsObject = {
    logicalnodeforcontrol?: commonmodule_commonmodule_pb.LogicalNodeForControl.AsObject,
    control?: ESSPoint.AsObject,
  }
}

export class ESSDiscreteControl extends jspb.Message {
  getControlvalue(): commonmodule_commonmodule_pb.ControlValue | undefined;
  setControlvalue(value?: commonmodule_commonmodule_pb.ControlValue): ESSDiscreteControl;
  hasControlvalue(): boolean;
  clearControlvalue(): ESSDiscreteControl;

  getCheck(): commonmodule_commonmodule_pb.CheckConditions | undefined;
  setCheck(value?: commonmodule_commonmodule_pb.CheckConditions): ESSDiscreteControl;
  hasCheck(): boolean;
  clearCheck(): ESSDiscreteControl;

  getEssdiscretecontroldbat(): ESSDiscreteControlDBAT | undefined;
  setEssdiscretecontroldbat(value?: ESSDiscreteControlDBAT): ESSDiscreteControl;
  hasEssdiscretecontroldbat(): boolean;
  clearEssdiscretecontroldbat(): ESSDiscreteControl;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSDiscreteControl.AsObject;
  static toObject(includeInstance: boolean, msg: ESSDiscreteControl): ESSDiscreteControl.AsObject;
  static serializeBinaryToWriter(message: ESSDiscreteControl, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSDiscreteControl;
  static deserializeBinaryFromReader(message: ESSDiscreteControl, reader: jspb.BinaryReader): ESSDiscreteControl;
}

export namespace ESSDiscreteControl {
  export type AsObject = {
    controlvalue?: commonmodule_commonmodule_pb.ControlValue.AsObject,
    check?: commonmodule_commonmodule_pb.CheckConditions.AsObject,
    essdiscretecontroldbat?: ESSDiscreteControlDBAT.AsObject,
  }
}

export class ESSDiscreteControlProfile extends jspb.Message {
  getControlmessageinfo(): commonmodule_commonmodule_pb.ControlMessageInfo | undefined;
  setControlmessageinfo(value?: commonmodule_commonmodule_pb.ControlMessageInfo): ESSDiscreteControlProfile;
  hasControlmessageinfo(): boolean;
  clearControlmessageinfo(): ESSDiscreteControlProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSDiscreteControlProfile;
  hasEss(): boolean;
  clearEss(): ESSDiscreteControlProfile;

  getEssdiscretecontrol(): ESSDiscreteControl | undefined;
  setEssdiscretecontrol(value?: ESSDiscreteControl): ESSDiscreteControlProfile;
  hasEssdiscretecontrol(): boolean;
  clearEssdiscretecontrol(): ESSDiscreteControlProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSDiscreteControlProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSDiscreteControlProfile): ESSDiscreteControlProfile.AsObject;
  static serializeBinaryToWriter(message: ESSDiscreteControlProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSDiscreteControlProfile;
  static deserializeBinaryFromReader(message: ESSDiscreteControlProfile, reader: jspb.BinaryReader): ESSDiscreteControlProfile;
}

export namespace ESSDiscreteControlProfile {
  export type AsObject = {
    controlmessageinfo?: commonmodule_commonmodule_pb.ControlMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    essdiscretecontrol?: ESSDiscreteControl.AsObject,
  }
}

export class EssEventZBAT extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): EssEventZBAT;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): EssEventZBAT;

  getBathi(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setBathi(value?: commonmodule_commonmodule_pb.StatusSPS): EssEventZBAT;
  hasBathi(): boolean;
  clearBathi(): EssEventZBAT;

  getBatlo(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setBatlo(value?: commonmodule_commonmodule_pb.StatusSPS): EssEventZBAT;
  hasBatlo(): boolean;
  clearBatlo(): EssEventZBAT;

  getBatst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setBatst(value?: commonmodule_commonmodule_pb.StatusSPS): EssEventZBAT;
  hasBatst(): boolean;
  clearBatst(): EssEventZBAT;

  getSoc(): commonmodule_commonmodule_pb.MV | undefined;
  setSoc(value?: commonmodule_commonmodule_pb.MV): EssEventZBAT;
  hasSoc(): boolean;
  clearSoc(): EssEventZBAT;

  getStdby(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setStdby(value?: commonmodule_commonmodule_pb.StatusSPS): EssEventZBAT;
  hasStdby(): boolean;
  clearStdby(): EssEventZBAT;

  getSoh(): commonmodule_commonmodule_pb.MV | undefined;
  setSoh(value?: commonmodule_commonmodule_pb.MV): EssEventZBAT;
  hasSoh(): boolean;
  clearSoh(): EssEventZBAT;

  getWhavail(): commonmodule_commonmodule_pb.MV | undefined;
  setWhavail(value?: commonmodule_commonmodule_pb.MV): EssEventZBAT;
  hasWhavail(): boolean;
  clearWhavail(): EssEventZBAT;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EssEventZBAT.AsObject;
  static toObject(includeInstance: boolean, msg: EssEventZBAT): EssEventZBAT.AsObject;
  static serializeBinaryToWriter(message: EssEventZBAT, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EssEventZBAT;
  static deserializeBinaryFromReader(message: EssEventZBAT, reader: jspb.BinaryReader): EssEventZBAT;
}

export namespace EssEventZBAT {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    bathi?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    batlo?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    batst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    soc?: commonmodule_commonmodule_pb.MV.AsObject,
    stdby?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    soh?: commonmodule_commonmodule_pb.MV.AsObject,
    whavail?: commonmodule_commonmodule_pb.MV.AsObject,
  }
}

export class ESSPointStatus extends jspb.Message {
  getBlackstartenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setBlackstartenabled(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasBlackstartenabled(): boolean;
  clearBlackstartenabled(): ESSPointStatus;

  getFrequencysetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setFrequencysetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasFrequencysetpointenabled(): boolean;
  clearFrequencysetpointenabled(): ESSPointStatus;

  getFunction(): ESSFunction | undefined;
  setFunction(value?: ESSFunction): ESSPointStatus;
  hasFunction(): boolean;
  clearFunction(): ESSPointStatus;

  getMode(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setMode(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): ESSPointStatus;
  hasMode(): boolean;
  clearMode(): ESSPointStatus;

  getPcthzdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPcthzdroop(value?: google_protobuf_wrappers_pb.FloatValue): ESSPointStatus;
  hasPcthzdroop(): boolean;
  clearPcthzdroop(): ESSPointStatus;

  getPctvdroop(): google_protobuf_wrappers_pb.FloatValue | undefined;
  setPctvdroop(value?: google_protobuf_wrappers_pb.FloatValue): ESSPointStatus;
  hasPctvdroop(): boolean;
  clearPctvdroop(): ESSPointStatus;

  getRamprates(): commonmodule_commonmodule_pb.RampRate | undefined;
  setRamprates(value?: commonmodule_commonmodule_pb.RampRate): ESSPointStatus;
  hasRamprates(): boolean;
  clearRamprates(): ESSPointStatus;

  getReactivepwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setReactivepwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasReactivepwrsetpointenabled(): boolean;
  clearReactivepwrsetpointenabled(): ESSPointStatus;

  getRealpwrsetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setRealpwrsetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasRealpwrsetpointenabled(): boolean;
  clearRealpwrsetpointenabled(): ESSPointStatus;

  getState(): commonmodule_commonmodule_pb.Optional_StateKind | undefined;
  setState(value?: commonmodule_commonmodule_pb.Optional_StateKind): ESSPointStatus;
  hasState(): boolean;
  clearState(): ESSPointStatus;

  getSyncbacktogrid(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setSyncbacktogrid(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasSyncbacktogrid(): boolean;
  clearSyncbacktogrid(): ESSPointStatus;

  getTranstoislndongridlossenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setTranstoislndongridlossenabled(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasTranstoislndongridlossenabled(): boolean;
  clearTranstoislndongridlossenabled(): ESSPointStatus;

  getVoltagesetpointenabled(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setVoltagesetpointenabled(value?: commonmodule_commonmodule_pb.StatusSPS): ESSPointStatus;
  hasVoltagesetpointenabled(): boolean;
  clearVoltagesetpointenabled(): ESSPointStatus;

  getEnterserviceoperation(): commonmodule_commonmodule_pb.EnterServiceAPC | undefined;
  setEnterserviceoperation(value?: commonmodule_commonmodule_pb.EnterServiceAPC): ESSPointStatus;
  hasEnterserviceoperation(): boolean;
  clearEnterserviceoperation(): ESSPointStatus;

  getHzwoperation(): commonmodule_commonmodule_pb.HzWAPC | undefined;
  setHzwoperation(value?: commonmodule_commonmodule_pb.HzWAPC): ESSPointStatus;
  hasHzwoperation(): boolean;
  clearHzwoperation(): ESSPointStatus;

  getLimitwoperation(): commonmodule_commonmodule_pb.LimitWAPC | undefined;
  setLimitwoperation(value?: commonmodule_commonmodule_pb.LimitWAPC): ESSPointStatus;
  hasLimitwoperation(): boolean;
  clearLimitwoperation(): ESSPointStatus;

  getPfoperation(): commonmodule_commonmodule_pb.PFSPC | undefined;
  setPfoperation(value?: commonmodule_commonmodule_pb.PFSPC): ESSPointStatus;
  hasPfoperation(): boolean;
  clearPfoperation(): ESSPointStatus;

  getTmhztripoperation(): commonmodule_commonmodule_pb.TmHzCSG | undefined;
  setTmhztripoperation(value?: commonmodule_commonmodule_pb.TmHzCSG): ESSPointStatus;
  hasTmhztripoperation(): boolean;
  clearTmhztripoperation(): ESSPointStatus;

  getTmvolttripoperation(): commonmodule_commonmodule_pb.TmVoltCSG | undefined;
  setTmvolttripoperation(value?: commonmodule_commonmodule_pb.TmVoltCSG): ESSPointStatus;
  hasTmvolttripoperation(): boolean;
  clearTmvolttripoperation(): ESSPointStatus;

  getVaroperation(): commonmodule_commonmodule_pb.VarSPC | undefined;
  setVaroperation(value?: commonmodule_commonmodule_pb.VarSPC): ESSPointStatus;
  hasVaroperation(): boolean;
  clearVaroperation(): ESSPointStatus;

  getVoltvaroperation(): commonmodule_commonmodule_pb.VoltVarCSG | undefined;
  setVoltvaroperation(value?: commonmodule_commonmodule_pb.VoltVarCSG): ESSPointStatus;
  hasVoltvaroperation(): boolean;
  clearVoltvaroperation(): ESSPointStatus;

  getVoltwoperation(): commonmodule_commonmodule_pb.VoltWCSG | undefined;
  setVoltwoperation(value?: commonmodule_commonmodule_pb.VoltWCSG): ESSPointStatus;
  hasVoltwoperation(): boolean;
  clearVoltwoperation(): ESSPointStatus;

  getWvaroperation(): commonmodule_commonmodule_pb.WVarCSG | undefined;
  setWvaroperation(value?: commonmodule_commonmodule_pb.WVarCSG): ESSPointStatus;
  hasWvaroperation(): boolean;
  clearWvaroperation(): ESSPointStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSPointStatus.AsObject;
  static toObject(includeInstance: boolean, msg: ESSPointStatus): ESSPointStatus.AsObject;
  static serializeBinaryToWriter(message: ESSPointStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSPointStatus;
  static deserializeBinaryFromReader(message: ESSPointStatus, reader: jspb.BinaryReader): ESSPointStatus;
}

export namespace ESSPointStatus {
  export type AsObject = {
    blackstartenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    frequencysetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    pb_function?: ESSFunction.AsObject,
    mode?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
    pcthzdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    pctvdroop?: google_protobuf_wrappers_pb.FloatValue.AsObject,
    ramprates?: commonmodule_commonmodule_pb.RampRate.AsObject,
    reactivepwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    realpwrsetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    state?: commonmodule_commonmodule_pb.Optional_StateKind.AsObject,
    syncbacktogrid?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    transtoislndongridlossenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    voltagesetpointenabled?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    enterserviceoperation?: commonmodule_commonmodule_pb.EnterServiceAPC.AsObject,
    hzwoperation?: commonmodule_commonmodule_pb.HzWAPC.AsObject,
    limitwoperation?: commonmodule_commonmodule_pb.LimitWAPC.AsObject,
    pfoperation?: commonmodule_commonmodule_pb.PFSPC.AsObject,
    tmhztripoperation?: commonmodule_commonmodule_pb.TmHzCSG.AsObject,
    tmvolttripoperation?: commonmodule_commonmodule_pb.TmVoltCSG.AsObject,
    varoperation?: commonmodule_commonmodule_pb.VarSPC.AsObject,
    voltvaroperation?: commonmodule_commonmodule_pb.VoltVarCSG.AsObject,
    voltwoperation?: commonmodule_commonmodule_pb.VoltWCSG.AsObject,
    wvaroperation?: commonmodule_commonmodule_pb.WVarCSG.AsObject,
  }
}

export class ESSEventAndStatusZGEN extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): ESSEventAndStatusZGEN;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): ESSEventAndStatusZGEN;

  getAuxpwrst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setAuxpwrst(value?: commonmodule_commonmodule_pb.StatusSPS): ESSEventAndStatusZGEN;
  hasAuxpwrst(): boolean;
  clearAuxpwrst(): ESSEventAndStatusZGEN;

  getDynamictest(): commonmodule_commonmodule_pb.ENS_DynamicTestKind | undefined;
  setDynamictest(value?: commonmodule_commonmodule_pb.ENS_DynamicTestKind): ESSEventAndStatusZGEN;
  hasDynamictest(): boolean;
  clearDynamictest(): ESSEventAndStatusZGEN;

  getEmgstop(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setEmgstop(value?: commonmodule_commonmodule_pb.StatusSPS): ESSEventAndStatusZGEN;
  hasEmgstop(): boolean;
  clearEmgstop(): ESSEventAndStatusZGEN;

  getGnsynst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setGnsynst(value?: commonmodule_commonmodule_pb.StatusSPS): ESSEventAndStatusZGEN;
  hasGnsynst(): boolean;
  clearGnsynst(): ESSEventAndStatusZGEN;

  getPointstatus(): ESSPointStatus | undefined;
  setPointstatus(value?: ESSPointStatus): ESSEventAndStatusZGEN;
  hasPointstatus(): boolean;
  clearPointstatus(): ESSEventAndStatusZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSEventAndStatusZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: ESSEventAndStatusZGEN): ESSEventAndStatusZGEN.AsObject;
  static serializeBinaryToWriter(message: ESSEventAndStatusZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSEventAndStatusZGEN;
  static deserializeBinaryFromReader(message: ESSEventAndStatusZGEN, reader: jspb.BinaryReader): ESSEventAndStatusZGEN;
}

export namespace ESSEventAndStatusZGEN {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    auxpwrst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    dynamictest?: commonmodule_commonmodule_pb.ENS_DynamicTestKind.AsObject,
    emgstop?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    gnsynst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    pointstatus?: ESSPointStatus.AsObject,
  }
}

export class ESSEventZGEN extends jspb.Message {
  getEsseventandstatuszgen(): ESSEventAndStatusZGEN | undefined;
  setEsseventandstatuszgen(value?: ESSEventAndStatusZGEN): ESSEventZGEN;
  hasEsseventandstatuszgen(): boolean;
  clearEsseventandstatuszgen(): ESSEventZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSEventZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: ESSEventZGEN): ESSEventZGEN.AsObject;
  static serializeBinaryToWriter(message: ESSEventZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSEventZGEN;
  static deserializeBinaryFromReader(message: ESSEventZGEN, reader: jspb.BinaryReader): ESSEventZGEN;
}

export namespace ESSEventZGEN {
  export type AsObject = {
    esseventandstatuszgen?: ESSEventAndStatusZGEN.AsObject,
  }
}

export class ESSEvent extends jspb.Message {
  getEventvalue(): commonmodule_commonmodule_pb.EventValue | undefined;
  setEventvalue(value?: commonmodule_commonmodule_pb.EventValue): ESSEvent;
  hasEventvalue(): boolean;
  clearEventvalue(): ESSEvent;

  getEsseventzbat(): EssEventZBAT | undefined;
  setEsseventzbat(value?: EssEventZBAT): ESSEvent;
  hasEsseventzbat(): boolean;
  clearEsseventzbat(): ESSEvent;

  getEsseventzgen(): ESSEventZGEN | undefined;
  setEsseventzgen(value?: ESSEventZGEN): ESSEvent;
  hasEsseventzgen(): boolean;
  clearEsseventzgen(): ESSEvent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSEvent.AsObject;
  static toObject(includeInstance: boolean, msg: ESSEvent): ESSEvent.AsObject;
  static serializeBinaryToWriter(message: ESSEvent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSEvent;
  static deserializeBinaryFromReader(message: ESSEvent, reader: jspb.BinaryReader): ESSEvent;
}

export namespace ESSEvent {
  export type AsObject = {
    eventvalue?: commonmodule_commonmodule_pb.EventValue.AsObject,
    esseventzbat?: EssEventZBAT.AsObject,
    esseventzgen?: ESSEventZGEN.AsObject,
  }
}

export class ESSEventProfile extends jspb.Message {
  getEventmessageinfo(): commonmodule_commonmodule_pb.EventMessageInfo | undefined;
  setEventmessageinfo(value?: commonmodule_commonmodule_pb.EventMessageInfo): ESSEventProfile;
  hasEventmessageinfo(): boolean;
  clearEventmessageinfo(): ESSEventProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSEventProfile;
  hasEss(): boolean;
  clearEss(): ESSEventProfile;

  getEssevent(): ESSEvent | undefined;
  setEssevent(value?: ESSEvent): ESSEventProfile;
  hasEssevent(): boolean;
  clearEssevent(): ESSEventProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSEventProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSEventProfile): ESSEventProfile.AsObject;
  static serializeBinaryToWriter(message: ESSEventProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSEventProfile;
  static deserializeBinaryFromReader(message: ESSEventProfile, reader: jspb.BinaryReader): ESSEventProfile;
}

export namespace ESSEventProfile {
  export type AsObject = {
    eventmessageinfo?: commonmodule_commonmodule_pb.EventMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    essevent?: ESSEvent.AsObject,
  }
}

export class ESSReading extends jspb.Message {
  getConductingequipmentterminalreading(): commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading | undefined;
  setConductingequipmentterminalreading(value?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading): ESSReading;
  hasConductingequipmentterminalreading(): boolean;
  clearConductingequipmentterminalreading(): ESSReading;

  getPhasemmtn(): commonmodule_commonmodule_pb.PhaseMMTN | undefined;
  setPhasemmtn(value?: commonmodule_commonmodule_pb.PhaseMMTN): ESSReading;
  hasPhasemmtn(): boolean;
  clearPhasemmtn(): ESSReading;

  getReadingmmtr(): commonmodule_commonmodule_pb.ReadingMMTR | undefined;
  setReadingmmtr(value?: commonmodule_commonmodule_pb.ReadingMMTR): ESSReading;
  hasReadingmmtr(): boolean;
  clearReadingmmtr(): ESSReading;

  getReadingmmxu(): commonmodule_commonmodule_pb.ReadingMMXU | undefined;
  setReadingmmxu(value?: commonmodule_commonmodule_pb.ReadingMMXU): ESSReading;
  hasReadingmmxu(): boolean;
  clearReadingmmxu(): ESSReading;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSReading.AsObject;
  static toObject(includeInstance: boolean, msg: ESSReading): ESSReading.AsObject;
  static serializeBinaryToWriter(message: ESSReading, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSReading;
  static deserializeBinaryFromReader(message: ESSReading, reader: jspb.BinaryReader): ESSReading;
}

export namespace ESSReading {
  export type AsObject = {
    conductingequipmentterminalreading?: commonmodule_commonmodule_pb.ConductingEquipmentTerminalReading.AsObject,
    phasemmtn?: commonmodule_commonmodule_pb.PhaseMMTN.AsObject,
    readingmmtr?: commonmodule_commonmodule_pb.ReadingMMTR.AsObject,
    readingmmxu?: commonmodule_commonmodule_pb.ReadingMMXU.AsObject,
  }
}

export class ESSReadingProfile extends jspb.Message {
  getReadingmessageinfo(): commonmodule_commonmodule_pb.ReadingMessageInfo | undefined;
  setReadingmessageinfo(value?: commonmodule_commonmodule_pb.ReadingMessageInfo): ESSReadingProfile;
  hasReadingmessageinfo(): boolean;
  clearReadingmessageinfo(): ESSReadingProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSReadingProfile;
  hasEss(): boolean;
  clearEss(): ESSReadingProfile;

  getEssreading(): ESSReading | undefined;
  setEssreading(value?: ESSReading): ESSReadingProfile;
  hasEssreading(): boolean;
  clearEssreading(): ESSReadingProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSReadingProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSReadingProfile): ESSReadingProfile.AsObject;
  static serializeBinaryToWriter(message: ESSReadingProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSReadingProfile;
  static deserializeBinaryFromReader(message: ESSReadingProfile, reader: jspb.BinaryReader): ESSReadingProfile;
}

export namespace ESSReadingProfile {
  export type AsObject = {
    readingmessageinfo?: commonmodule_commonmodule_pb.ReadingMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    essreading?: ESSReading.AsObject,
  }
}

export class EssStatusZBAT extends jspb.Message {
  getLogicalnodeforeventandstatus(): commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus | undefined;
  setLogicalnodeforeventandstatus(value?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus): EssStatusZBAT;
  hasLogicalnodeforeventandstatus(): boolean;
  clearLogicalnodeforeventandstatus(): EssStatusZBAT;

  getBatst(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setBatst(value?: commonmodule_commonmodule_pb.StatusSPS): EssStatusZBAT;
  hasBatst(): boolean;
  clearBatst(): EssStatusZBAT;

  getGrimod(): commonmodule_commonmodule_pb.ENG_GridConnectModeKind | undefined;
  setGrimod(value?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind): EssStatusZBAT;
  hasGrimod(): boolean;
  clearGrimod(): EssStatusZBAT;

  getSoc(): commonmodule_commonmodule_pb.MV | undefined;
  setSoc(value?: commonmodule_commonmodule_pb.MV): EssStatusZBAT;
  hasSoc(): boolean;
  clearSoc(): EssStatusZBAT;

  getStdby(): commonmodule_commonmodule_pb.StatusSPS | undefined;
  setStdby(value?: commonmodule_commonmodule_pb.StatusSPS): EssStatusZBAT;
  hasStdby(): boolean;
  clearStdby(): EssStatusZBAT;

  getSoh(): commonmodule_commonmodule_pb.MV | undefined;
  setSoh(value?: commonmodule_commonmodule_pb.MV): EssStatusZBAT;
  hasSoh(): boolean;
  clearSoh(): EssStatusZBAT;

  getWhavail(): commonmodule_commonmodule_pb.MV | undefined;
  setWhavail(value?: commonmodule_commonmodule_pb.MV): EssStatusZBAT;
  hasWhavail(): boolean;
  clearWhavail(): EssStatusZBAT;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EssStatusZBAT.AsObject;
  static toObject(includeInstance: boolean, msg: EssStatusZBAT): EssStatusZBAT.AsObject;
  static serializeBinaryToWriter(message: EssStatusZBAT, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EssStatusZBAT;
  static deserializeBinaryFromReader(message: EssStatusZBAT, reader: jspb.BinaryReader): EssStatusZBAT;
}

export namespace EssStatusZBAT {
  export type AsObject = {
    logicalnodeforeventandstatus?: commonmodule_commonmodule_pb.LogicalNodeForEventAndStatus.AsObject,
    batst?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    grimod?: commonmodule_commonmodule_pb.ENG_GridConnectModeKind.AsObject,
    soc?: commonmodule_commonmodule_pb.MV.AsObject,
    stdby?: commonmodule_commonmodule_pb.StatusSPS.AsObject,
    soh?: commonmodule_commonmodule_pb.MV.AsObject,
    whavail?: commonmodule_commonmodule_pb.MV.AsObject,
  }
}

export class ESSStatusZGEN extends jspb.Message {
  getEsseventandstatuszgen(): ESSEventAndStatusZGEN | undefined;
  setEsseventandstatuszgen(value?: ESSEventAndStatusZGEN): ESSStatusZGEN;
  hasEsseventandstatuszgen(): boolean;
  clearEsseventandstatuszgen(): ESSStatusZGEN;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSStatusZGEN.AsObject;
  static toObject(includeInstance: boolean, msg: ESSStatusZGEN): ESSStatusZGEN.AsObject;
  static serializeBinaryToWriter(message: ESSStatusZGEN, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSStatusZGEN;
  static deserializeBinaryFromReader(message: ESSStatusZGEN, reader: jspb.BinaryReader): ESSStatusZGEN;
}

export namespace ESSStatusZGEN {
  export type AsObject = {
    esseventandstatuszgen?: ESSEventAndStatusZGEN.AsObject,
  }
}

export class ESSStatus extends jspb.Message {
  getStatusvalue(): commonmodule_commonmodule_pb.StatusValue | undefined;
  setStatusvalue(value?: commonmodule_commonmodule_pb.StatusValue): ESSStatus;
  hasStatusvalue(): boolean;
  clearStatusvalue(): ESSStatus;

  getEssstatuszbat(): EssStatusZBAT | undefined;
  setEssstatuszbat(value?: EssStatusZBAT): ESSStatus;
  hasEssstatuszbat(): boolean;
  clearEssstatuszbat(): ESSStatus;

  getEssstatuszgen(): ESSStatusZGEN | undefined;
  setEssstatuszgen(value?: ESSStatusZGEN): ESSStatus;
  hasEssstatuszgen(): boolean;
  clearEssstatuszgen(): ESSStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSStatus.AsObject;
  static toObject(includeInstance: boolean, msg: ESSStatus): ESSStatus.AsObject;
  static serializeBinaryToWriter(message: ESSStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSStatus;
  static deserializeBinaryFromReader(message: ESSStatus, reader: jspb.BinaryReader): ESSStatus;
}

export namespace ESSStatus {
  export type AsObject = {
    statusvalue?: commonmodule_commonmodule_pb.StatusValue.AsObject,
    essstatuszbat?: EssStatusZBAT.AsObject,
    essstatuszgen?: ESSStatusZGEN.AsObject,
  }
}

export class ESSStatusProfile extends jspb.Message {
  getStatusmessageinfo(): commonmodule_commonmodule_pb.StatusMessageInfo | undefined;
  setStatusmessageinfo(value?: commonmodule_commonmodule_pb.StatusMessageInfo): ESSStatusProfile;
  hasStatusmessageinfo(): boolean;
  clearStatusmessageinfo(): ESSStatusProfile;

  getEss(): commonmodule_commonmodule_pb.ESS | undefined;
  setEss(value?: commonmodule_commonmodule_pb.ESS): ESSStatusProfile;
  hasEss(): boolean;
  clearEss(): ESSStatusProfile;

  getEssstatus(): ESSStatus | undefined;
  setEssstatus(value?: ESSStatus): ESSStatusProfile;
  hasEssstatus(): boolean;
  clearEssstatus(): ESSStatusProfile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESSStatusProfile.AsObject;
  static toObject(includeInstance: boolean, msg: ESSStatusProfile): ESSStatusProfile.AsObject;
  static serializeBinaryToWriter(message: ESSStatusProfile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESSStatusProfile;
  static deserializeBinaryFromReader(message: ESSStatusProfile, reader: jspb.BinaryReader): ESSStatusProfile;
}

export namespace ESSStatusProfile {
  export type AsObject = {
    statusmessageinfo?: commonmodule_commonmodule_pb.StatusMessageInfo.AsObject,
    ess?: commonmodule_commonmodule_pb.ESS.AsObject,
    essstatus?: ESSStatus.AsObject,
  }
}

