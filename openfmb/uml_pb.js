// source: uml.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

var google_protobuf_descriptor_pb = require('google-protobuf/google/protobuf/descriptor_pb.js');
goog.object.extend(proto, google_protobuf_descriptor_pb);
goog.exportSymbol('proto.uml.optionKey', null, global);
goog.exportSymbol('proto.uml.optionMultiplicityMax', null, global);
goog.exportSymbol('proto.uml.optionMultiplicityMin', null, global);
goog.exportSymbol('proto.uml.optionOpenfmbProfile', null, global);
goog.exportSymbol('proto.uml.optionParentMessage', null, global);
goog.exportSymbol('proto.uml.optionRequiredField', null, global);
goog.exportSymbol('proto.uml.optionUuid', null, global);

/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionParentMessage`.
 * @type {!jspb.ExtensionFieldInfo<boolean>}
 */
proto.uml.optionParentMessage = new jspb.ExtensionFieldInfo(
    50000,
    { optionParentMessage: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.FieldOptions.extensionsBinary[50000] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionParentMessage,
    jspb.BinaryReader.prototype.readBool,
    jspb.BinaryWriter.prototype.writeBool,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.FieldOptions.extensions[50000] = proto.uml.optionParentMessage;


/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionRequiredField`.
 * @type {!jspb.ExtensionFieldInfo<boolean>}
 */
proto.uml.optionRequiredField = new jspb.ExtensionFieldInfo(
    50001,
    { optionRequiredField: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.FieldOptions.extensionsBinary[50001] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionRequiredField,
    jspb.BinaryReader.prototype.readBool,
    jspb.BinaryWriter.prototype.writeBool,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.FieldOptions.extensions[50001] = proto.uml.optionRequiredField;


/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionMultiplicityMin`.
 * @type {!jspb.ExtensionFieldInfo<number>}
 */
proto.uml.optionMultiplicityMin = new jspb.ExtensionFieldInfo(
    50002,
    { optionMultiplicityMin: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.FieldOptions.extensionsBinary[50002] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionMultiplicityMin,
    jspb.BinaryReader.prototype.readInt32,
    jspb.BinaryWriter.prototype.writeInt32,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.FieldOptions.extensions[50002] = proto.uml.optionMultiplicityMin;


/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionMultiplicityMax`.
 * @type {!jspb.ExtensionFieldInfo<number>}
 */
proto.uml.optionMultiplicityMax = new jspb.ExtensionFieldInfo(
    50003,
    { optionMultiplicityMax: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.FieldOptions.extensionsBinary[50003] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionMultiplicityMax,
    jspb.BinaryReader.prototype.readInt32,
    jspb.BinaryWriter.prototype.writeInt32,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.FieldOptions.extensions[50003] = proto.uml.optionMultiplicityMax;


/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionUuid`.
 * @type {!jspb.ExtensionFieldInfo<boolean>}
 */
proto.uml.optionUuid = new jspb.ExtensionFieldInfo(
    50004,
    { optionUuid: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.FieldOptions.extensionsBinary[50004] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionUuid,
    jspb.BinaryReader.prototype.readBool,
    jspb.BinaryWriter.prototype.writeBool,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.FieldOptions.extensions[50004] = proto.uml.optionUuid;


/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionKey`.
 * @type {!jspb.ExtensionFieldInfo<boolean>}
 */
proto.uml.optionKey = new jspb.ExtensionFieldInfo(
    50005,
    { optionKey: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.FieldOptions.extensionsBinary[50005] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionKey,
    jspb.BinaryReader.prototype.readBool,
    jspb.BinaryWriter.prototype.writeBool,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.FieldOptions.extensions[50005] = proto.uml.optionKey;


/**
 * A tuple of {field number, class constructor} for the extension
 * field named `optionOpenfmbProfile`.
 * @type {!jspb.ExtensionFieldInfo<boolean>}
 */
proto.uml.optionOpenfmbProfile = new jspb.ExtensionFieldInfo(
    51000,
    { optionOpenfmbProfile: 0 },
    null,
     /** @type {?function((boolean|undefined),!jspb.Message=): !Object} */(
        null),
    0);

google_protobuf_descriptor_pb.MessageOptions.extensionsBinary[51000] = new jspb.ExtensionFieldBinaryInfo(
    proto.uml.optionOpenfmbProfile,
    jspb.BinaryReader.prototype.readBool,
    jspb.BinaryWriter.prototype.writeBool,
    undefined,
    undefined,
    false);
// This registers the extension field with the extended class, so that
// toObject() will function correctly.
google_protobuf_descriptor_pb.MessageOptions.extensions[51000] = proto.uml.optionOpenfmbProfile;

goog.object.extend(exports, proto.uml);
