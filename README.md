# TypeScript language protobuf for OpenFMB operational use cases

This repository contains the [TypeScript](https://www.typescriptlang.org/) programming language Protocol Buffer (protobuf) definitions based on the OpenFMB operational use case data model located [here](https://gitlab.com/openfmb/data-models/ops).

## Including in your project

```typescript
// Example to come...
```

## Using

```typescript
// Example to come...
```

## Copyright

See the COPYRIGHT file for copyright information of information contained in this repository.

## License

Unless otherwise noted, all files in this repository are distributed under the Apache Version 2.0 license found in the LICENSE file.
